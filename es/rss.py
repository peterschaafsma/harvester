import re
import unidecode

from api.es import Elasticsearch
from dateutil.parser import parse
from urllib.parse import urlparse
from utils import dt


class RSSElasticsearch(Elasticsearch):
    def __init__(self):
        super(RSSElasticsearch, self).__init__()

        self.source_index = 'rss_source'
        self.source_type = 'object'
        self.search_index = 'rss'
        self.search_type = 'entry'

    def _get_id(self, entry):
        return entry['id'] or entry['link']

    def _get_source(self, entry):
        return '{uri.netloc}'.format(uri=urlparse(entry['link']))

    def _get_document(self, entry):
        now = dt.iso8601(dt.utc_now())

        images = []
        if entry.get('media_thumbnail') and entry.get('media_content'):
            image = {
                'thumbnail': {
                    'url': entry['media_thumbnail'][0]['url']
                },
                'original': {
                    'url': entry['media_content'][0]['url']
                },
            }

            images.append(image)

        summary = entry['summary']
        summary = re.sub('<[^>]*>', '', summary)
        summary = unidecode.unidecode(summary)

        document = {
            'title': entry['title'],
            'text': summary,
            'content': summary,
            'source': self._get_source(entry),
            'source_type': 'news',
            'id': entry['id'],
            'created_at': dt.iso8601(parse(entry['published'])),
            'indexed_at': now,
            'updated_it': now,
            'images': images,
            'permalink': entry['link'],
        }

        for key in ['images']:
            if not document[key]:
                del document[key]

        return document

