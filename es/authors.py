import elasticsearch

from api.es import Elasticsearch
from api.twitter import TwitterApi
from utils import dt
from utils.iter import as_list, batchify


class AuthorsElasticsearch(Elasticsearch):
    def __init__(self):
        super(AuthorsElasticsearch, self).__init__()

    def _get_id(self, entry):
        return entry['screen_name']

    def _get_index(self, entry):
        return 'authors'

    def _should_update(self, current, existing):
        return current['account'] not in as_list(existing['account'])

    def _make_update(self, current, existing):
        return {
            'account': list(sorted([current['account']] + as_list(existing['account'])))
        }

    def update_authors(self, screen_names):
        actions = []
        for batch in batchify(screen_names, 100):
            twitter_accounts = TwitterApi(type='app').get('users/lookup.json', screen_name=','.join(batch))

            if 'errors' in twitter_accounts.data:
                print(twitter_accounts.data['errors'], batch)
                continue

            for twitter_account in twitter_accounts.data:
                update_keys = ['profile_image_url', 'followers_count', 'friends_count']
                update = dict((key, twitter_account[key]) for key in update_keys)
                update['updated_at'] = dt.iso8601(dt.utc_now())

                actions.append({
                    '_op_type': 'update',
                    '_id': twitter_account['screen_name'],
                    '_index': 'authors',
                    '_source': {'doc': update}
                })

        elasticsearch.helpers.bulk(self.client, actions)

    def index_authors(self, screen_names):
        actions = []
        for batch in batchify(screen_names, 100):
            twitter_accounts = TwitterApi(type='app').get('users/lookup.json', screen_name=','.join(batch))

            for twitter_account in twitter_accounts.data:
                document = self._get_document(twitter_account)

                actions.append({
                    '_op_type': 'index',
                    '_id': document['screen_name'],
                    '_index': 'authors',
                    '_source': document
                })

        elasticsearch.helpers.bulk(self.client, actions)

    def _get_document(self, entry):
        now = dt.utc_now()

        keys = [
            'id_str',
            'name',
            'screen_name',
            'profile_image_url',
            'followers_count',
            'friends_count',
            'account',
        ]
        key_map = {
            'id_str': 'id'
        }

        document = {key_map.get(key, key): entry[key] for key in keys}
        document['indexed_at'] = dt.iso8601(now)
        document['updated_at'] = document['indexed_at']

        return document