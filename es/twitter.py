import random
import re

from api.es import Elasticsearch
from dateutil.parser import parse
from utils import dt
from utils.iter import as_list


class TwitterElasticsearch(Elasticsearch):
    def __init__(self):
        super(TwitterElasticsearch, self).__init__()

    def _get_id(self, source):
        return source.get('id')

    def _get_index(self, source):
        return parse(source['created_at']).strftime('tweets_%Y_%m')

    def _get_text(self, status):
        # this is what gets indexed
        if 'full_text' in status:
            full_text = status['full_text']
        elif 'extended_tweet' in status:
            full_text = status['extended_tweet']['full_text']
        else:
            full_text = status['text']

        return self._clean_text(full_text)

    def _get_content(self, status):
        # this gets shown
        if 'full_text' in status:
            content = status['full_text']
        elif 'extended_tweet' in status:
            content = status['extended_tweet']['full_text']
        else:
            content = status['text']

        if (len(status['entities']['urls']) > 0):
            for url in status['entities']['urls']:
                content = content.replace(
                    url['url'],
                    '<a href="{}" target="_blank">{}</a>'.format(url['expanded_url'], url['display_url'])
                )

        images = [media for media in status['entities'].get('media', []) if media['type'] == 'photo']
        for source_image in images:
            content = content.replace(source_image['url'], '').strip()

        return content

    def _get_images(self, status):
        images = []
        for source_image in [media for media in status['entities'].get('media', []) if media['type'] == 'photo']:
            image = {}
            for key, size in source_image['sizes'].items():
                if key == 'thumb':
                    image['thumbnail'] = {
                        'width': size['w'],
                        'height': size['h'],
                        'url': '{}:{}'.format(source_image['media_url_https'], key)
                    }
                elif key == 'large':
                    image['original'] = {
                        'width': size['w'],
                        'height': size['h'],
                        'url': '{}:{}'.format(source_image['media_url_https'], key)
                    }

            images.append(image)

        return images

    def _get_urls(self, status, filter_urls=True):
        content_urls = set((media['url'].lower() for media in status['entities'].get('media', []) if media['type'] == 'photo'))

        if 'quoted_status' in status:
            quoted = status['quoted_status']

            url = 'https://twitter.com/{}/status/{}'.format(quoted['user']['screen_name'], quoted['id_str'])

            content_urls.add(url.lower())

        if 'retweeted_status' in status:
            for url in self._get_urls(status['retweeted_status'], False):
                content_urls.add(url.lower().split('?')[0])

        urls = []
        for url in status['entities']['urls']:
            if filter_urls:
                if url['expanded_url'].lower().split('?')[0].replace('mobile.twitter.com', 'twitter.com') in content_urls:
                    continue
                if url['url'].lower() in content_urls:
                    continue
                if url['display_url'].lower() in content_urls:
                    continue

            urls.append(url['expanded_url'])

        return urls

    def _should_update(self, current, existing):
        # if set(current.keys() != set(existing.keys())):
        #     return True
        #
        #
        #
        # if current['source_feed'] not in as_list(existing['source_feed']):
        #     return True
        #
        # return False
        return True

    def _make_update(self, current, existing):
        update = {}

        for key in current.keys():
            if key == 'source_feed':
                # these need to be merged together
                if existing:
                    source_feed = set(as_list(current['source_feed']) + as_list(existing['source_feed']))
                    new_source_feed = None
                    if len(source_feed) == 1:
                        new_source_feed = list(source_feed)[0]
                    else:
                        new_source_feed = sorted(source_feed)

                    if tuple(as_list(new_source_feed)) != tuple(as_list(existing['source_feed'])):
                        update['source_feed'] = new_source_feed
                else:
                    update['source_feed'] = current['source_feed']

            elif key in ['like_count', 'share_count']:
                # new values override old ones
                if not existing or current[key] != existing[key]:
                    update[key] = current[key]
                    update['engagement_updated_at'] = dt.iso8601(dt.utc_now())

            else:
                # copy if key does not exist
                if not existing or key not in existing:
                    update[key] = current[key]

        return update

    def _clean_text(self, text):
        # remove @mentions
        text = re.sub('@\w+', '', text)

        # remove urls
        text = re.sub('https?://[\w/\.]+', '', text)

        # # remove accents
        # text = ''.join(char_map.get(c, c) for c in text)
        #
        # remove extraneous spaces
        text = re.sub('\s+', ' ', text).strip()

        return text

    def _get_document(self, status):
        document = {}

        document['created_at'] = dt.iso8601(parse(status['created_at']))
        if document['created_at'] < '2019-01':
            return None

        document['content'] = self._get_content(status)
        document['text'] = self._get_text(status)
        document['keywords'] = re.findall('\w+', document['text'])  # todo: test on japanese etc.
        document['author'] = status['user']['screen_name']
        document['indexed_at'] = dt.iso8601(dt.utc_now())
        document['conversation_updated_at'] = document['indexed_at']
        document['engagement_updated_at'] = document['indexed_at']
        document['id'] = status['id']
        document['conversation_id'] = [document['id']]
        document['descendant_count'] = 0
        document['permalink'] = 'https://twitter.com/{}/status/{}'.format(status['user']['screen_name'], document['id'])
        document['source_type'] = 'social'
        document['source'] = 'twitter'
        document['language'] = status['lang']
        document['like_count'] = status['favorite_count']
        document['share_count'] = status['retweet_count']
        document['reply_count'] = 0
        document['random'] = random.random()
        document['source_feed'] = status.get('source_feed')
        document['followers_count'] = status['user']['followers_count']

        document['urls'] = self._get_urls(status)
        document['images'] = self._get_images(status)

        document['related'] = []
        if 'quoted_status' in status:
            related = {
                'relation': 'quote',
                'id': int(status['quoted_status_id_str']),
                'author': status['quoted_status']['user']['screen_name'],
                'content': self._get_content(status['quoted_status']),
                'text': self._get_text(status['quoted_status']),
                'keywords': re.findall('\w+', self._get_text(status['quoted_status'])),
            }

            document['related'].append(related)
            document['images'].extend(self._get_images(status['quoted_status']))

        if 'retweeted_status' in status:
            document['text'] = ''
            document['content'] = ''
            document['keywords'] = []

            related = {
                'relation': 'share',
                'id': int(status['retweeted_status']['id_str']),
                'author': status['retweeted_status']['user']['screen_name'],
                'content': self._get_content(status['retweeted_status']),
                'text': self._get_text(status['retweeted_status']),
                'keywords': re.findall('\w+', self._get_text(status['retweeted_status'])),
            }

            if 'quoted_status' in status['retweeted_status']:
                pass

            document['related'].append(related)

        if status['in_reply_to_status_id_str']:
            related = {
                'relation': 'reply',
                'id': int(status['in_reply_to_status_id_str']),
                'author': status['in_reply_to_screen_name'],
            }

            document['related'].append(related)
            document['conversation_id'] = [status['in_reply_to_status_id']] + document['conversation_id']

        mentions_set = set(map(str.lower, re.findall('(?<=@)\w+', status['full_text'])))
        mentions_set = mentions_set - set([document['author'].lower()])

        document['mentions'] = [mention for mention in re.findall('(?<=@)\w+', status['full_text']) if mention.lower() in mentions_set]

        for key in ['images', 'related', 'urls', 'text', 'content', 'keywords', 'mentions', 'source_feed']:
            if not document[key]:
                del document[key]

        return document


