from django.db import models
from django_mysql.models import JSONField

class ModelBase(models.Model):
    row_created_on = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    row_updated_on = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        abstract = True

    @property
    def dict(self):
        dictionary = {}

        field_classes = [
            models.CharField,
            models.IntegerField,
            models.BooleanField,
            models.DateTimeField,
            JSONField,
        ]

        for field in self.__class__._meta.get_fields():
            if field.__class__ not in field_classes:
                continue

            value = self.__dict__[field.name]
            if field.__class__ is models.DateTimeField:
                dictionary[field.name] = value and value.strftime('%Y-%m-%dT%H:%M:%S.%f%z')
            else:
                dictionary[field.name] = value

            dictionary['id'] = self.id

        return dictionary
