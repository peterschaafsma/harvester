import socket


class DefaultServerConfig:
    external_host = 'localhost'


HOST_NAME = socket.gethostname()

if HOST_NAME == 'h2823906.stratoserver.net':
    class ServerConfig(DefaultServerConfig):
        external_host = 'explore.mediagraphs.com'

if HOST_NAME == 'peter-dell':
    class ServerConfig(DefaultServerConfig):
        external_host = 'localhost:8000'

server_config = ServerConfig