source ../bin/activate

./manage.py es --drop-index index=lists
./manage.py es --create-index index=lists config=lists

./manage.py es --drop-index index=authors
./manage.py es --create-index index=authors config=authors

#./manage.py es --drop-index index=authors_source
#./manage.py es --create-index index=authors_source config=source

./manage.py es --drop-index index=tweets
./manage.py es --create-index index=tweets config=tweets

#./manage.py es --drop-index index=tweets_source
#./manage.py es --create-index index=tweets_source config=source

#./manage.py es --drop-index index=rss_source
#./manage.py es --create-index index=rss_source config=source

#./manage.py es --drop-index index=rss
#./manage.py es --create-index index=rss config=rss

mysql -u root -p -e "delete from feeds.feeds_accountfeed where id > 0"
mysql -u root -p -e "delete from feeds.feeds_feed where id > 0"

