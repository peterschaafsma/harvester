# Generated by Django 2.1 on 2019-06-11 20:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('feeds', '0027_feed_enabled'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='enabled',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='account',
            name='expires_on',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
