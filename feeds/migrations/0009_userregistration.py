# Generated by Django 2.1 on 2019-03-05 19:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('feeds', '0008_auto_20190224_0855'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserRegistration',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('row_created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('row_updated_on', models.DateTimeField(auto_now=True, null=True)),
                ('username', models.CharField(max_length=32)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
