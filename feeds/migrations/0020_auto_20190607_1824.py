# Generated by Django 2.1 on 2019-06-07 18:24

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feeds', '0019_auto_20190607_1819'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='feed',
            options={'permissions': []},
        ),
    ]
