# Generated by Django 2.1 on 2019-03-15 18:23

from django.db import migrations, models
import django_mysql.models


class Migration(migrations.Migration):

    dependencies = [
        ('feeds', '0012_userregistration_status'),
    ]

    operations = [
        migrations.CreateModel(
            name='SigTerms',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('row_created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('row_updated_on', models.DateTimeField(auto_now=True, null=True)),
                ('query_name', models.CharField(max_length=32)),
                ('query', django_mysql.models.JSONField(default=dict, max_length=1024)),
                ('term_count', models.IntegerField()),
                ('data', django_mysql.models.JSONField(default=dict, max_length=8192)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
