# Generated by Django 2.1 on 2019-08-13 21:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('feeds', '0038_auto_20190813_2127'),
    ]

    operations = [
        migrations.AlterField(
            model_name='publishedproject',
            name='timeout',
            field=models.CharField(blank=True, max_length=16, null=True),
        ),
    ]
