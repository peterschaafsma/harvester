from django.contrib import admin

from feeds.models import Feed, TwitterRateLimit, TwitterCredentials, AccountAccess, Account

# Register your models here.
admin.site.register(Feed)
admin.site.register(TwitterCredentials)
admin.site.register(TwitterRateLimit)
admin.site.register(AccountAccess)
admin.site.register(Account)
