from django.contrib.auth.models import User
from django.db import models
from django.db.models import Q
from django_mysql.models import JSONField
from django_mysql.locks import Lock
from harvester.models import ModelBase
from random import random
from time import sleep
from tools.queryparser import QueryParser
from utils.base import Object
from utils.dt import utc_now, from_timestamp
from utils.log import print


credentials_lock = Lock('credentials')


# Create your models here.
class Account(ModelBase):
    name = models.CharField(max_length=32)
    enabled = models.BooleanField(default=True)
    expires_on = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.name

class Project(ModelBase):
    name = models.CharField(max_length=32)
    state = JSONField(max_length=8192, default=dict)
    type = models.CharField(max_length=32, default='project')
    query = models.CharField(max_length=4096, default='')

class PublishedProject(ModelBase):
    name = models.CharField(max_length=32)
    uuid = models.CharField(max_length=64, unique=True, db_index=True)
    state = JSONField(max_length=8192, default=dict)
    data = JSONField(max_length=65536, default=dict)
    timeout = models.CharField(max_length=16, null=True, blank=True)
    expires_on = models.DateTimeField(null=True, blank=True)
    last_accessed = models.DateTimeField(null=True, blank=True)
    account = models.ForeignKey(Account, null=True, on_delete=models.CASCADE)
    count = models.IntegerField(default=0)

class AccountProjects(ModelBase):
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)

class AccountAccess(ModelBase):
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    last_project = models.ForeignKey(Project, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return '{} on {}'.format(self.user, self.account)

class Feed(ModelBase):
    class Meta:
        permissions = [
            ('access_all_feeds', 'Access all feeds'),
            ('access_all_time', 'Access all time'),
            ('access_all_data', 'Access all data'),
            ('add_feed_without_account', 'Add feed without account'),
            ('follower_budget_1M', 'Follower budget 1M'),
            ('follower_budget_10M', 'Follower budget 10M'),
            ('follower_budget_100M', 'Follower budget 100M'),
        ]

    state = models.CharField(max_length=32, default='ready', blank=True)

    error_count = models.IntegerField(default=0)
    error_last = models.TextField(blank=True, max_length=256, null=True, default='')

    harvest_last = models.DateTimeField(blank=True, null=True)
    harvest_first = models.DateTimeField(blank=True, null=True)
    harvest_next = models.DateTimeField(blank=True, auto_now_add=True, null=True)
    harvest_count = models.IntegerField(default=0)

    entry_last_id = models.CharField(max_length=128, default='0', blank=True)  #these should be ordered
    entry_last_count = models.IntegerField(blank=True, null=True)
    entry_total_count = models.IntegerField(blank=True, null=True, default=0)

    type = models.CharField(max_length=32)
    parameters = JSONField(max_length=256, default=dict)
    uid = models.CharField(max_length=64)
    enabled = models.BooleanField(default=True)
    refresh = models.IntegerField(default=None, null=True, blank=True)

    def lock(self):
        self.state = 'processing'
        self.save()

    def unlock(self):
        self.state = 'ready'
        self.save()

    def __str__(self):
        return '{}_{}_{}'.format(self.id, self.uid, self.type)

class AccountFeed(ModelBase):
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    feed = models.ForeignKey(Feed, on_delete=models.CASCADE)
    enabled = models.BooleanField(default=True)


class ValidCredentialsManager(models.Manager):
    def get_queryset(self):
        user_query = Q(type='user', token__isnull=False, secret__isnull=False)
        app_query = Q(type='app')

        return super().get_queryset().filter(user_query | app_query)

class AppCredentialsManager(models.Manager):
    def get_queryset(self):
        app_query = Q(type='app')

        return super().get_queryset().filter(app_query)

class UserCredentialsManager(models.Manager):
    def get_queryset(self):
        user_query = Q(type='user', token__isnull=False, secret__isnull=False)

        return super().get_queryset().filter(user_query)


class TwitterCredentials(ModelBase):
    type = models.CharField(max_length=6)
    account = models.ForeignKey(Account, null=True, blank=True, on_delete=models.CASCADE)
    screen_name = models.CharField(null=True, blank=True, max_length=64)

    api_key = models.CharField(max_length=32)
    api_key_secret = models.CharField(max_length=64)
    token = models.CharField(max_length=64, null=True, blank=True)
    secret = models.CharField(max_length=64, null=True, blank=True)

    objects = models.Manager()
    any_objects = ValidCredentialsManager()
    app_objects = AppCredentialsManager()
    user_objects = UserCredentialsManager()

    @classmethod
    def manager(cls, token_type):
        if token_type is None:
            return cls.any_objects
        elif token_type == 'app':
            return cls.app_objects
        elif token_type == 'user':
            return cls.user_objects

    @classmethod
    def wait_for_credentials(cls, endpoint, token_type=None):

        backoff = 1.0
        while True:
            try:
                credentials_lock.acquire()

                # if we have any credentials without associated rate limit, create rate limit, lock it,
                # and return the credentials
                credentials_queryset = TwitterCredentials.manager(token_type).filter().exclude(rate_limits__endpoint=endpoint)
                if credentials_queryset.count():
                    credentials = credentials_queryset.first()
                    print('Serving unknown credentials {}'.format(credentials))

                    TwitterRateLimit.create_or_update(credentials, endpoint, locked=True)

                    return credentials

                # here, all credentials have an associated rate limit

                # if we have any unlocked rate limits that have reset, return the first
                rate_limit = TwitterRateLimit.manager(token_type).filter(endpoint=endpoint, locked=False, reset__lt=utc_now()).first()
                if not rate_limit:
                    # otherwise try unlocked rate limits with any remaining limits
                    rate_limit = TwitterRateLimit.manager(token_type).filter(endpoint=endpoint, locked=False, remaining__gt=0).order_by('-remaining').first()

                # if we have a valid rate limit, lock and return it
                if rate_limit:
                    credentials = rate_limit.credentials

                    TwitterRateLimit.create_or_update(credentials, endpoint, locked=True)

                    return credentials

                # otherwise, if we have any locked credentials, wait a bit
                rate_limit = TwitterRateLimit.manager(token_type).filter(endpoint=endpoint, locked=True).first()
                if rate_limit:
                    sleep_time = backoff

                    if backoff < 60.0:
                        backoff += backoff * random()

                    print('No unlocked credentials. Waiting {} seconds'.format(sleep_time))

                else:
                    # otherwise, if we have any unlocked, empty credentials, wait for reset
                    rate_limit = TwitterRateLimit.manager(token_type).filter(endpoint=endpoint, locked=False, remaining=0).order_by('reset').first()
                    if rate_limit:
                        backoff = 1.0
                        wait_until = rate_limit.reset
                        sleep_time = (wait_until - utc_now()).total_seconds() + random()

                        print('All credentials spent. Sleeping for {} seconds, until about {}'.format(sleep_time, wait_until))

                    else:
                        raise Exception('No credentials available')

            finally:
                credentials_lock.release()

            sleep(sleep_time)

    @classmethod
    def return_credentials(cls, credentials, endpoint, response=None):
        print('Returning credentials {}'.format(credentials))

        usage = None
        if response:
            limit, remaining, reset = response.get('x-rate-limit-limit'), response.get('x-rate-limit-remaining'), response.get('x-rate-limit-reset')

            if not (limit is None or remaining is None or reset is None):
                usage = Object(
                    limit=int(limit),
                    remaining=int(remaining),
                    reset=from_timestamp(int(reset)))

                print('Usage for credentials {}: {}/{} {}'.format(credentials, remaining, limit, reset))

        TwitterRateLimit.create_or_update(credentials, endpoint, usage)

    @classmethod
    def invalidate_credentials(cls, credentials):
        credentials.token = None
        credentials.secret = None
        credentials.save()

    def __str__(self):
        if self.type == 'app':
            return '{}:{}:{}'.format(
                self.type,
                self.api_key[-3:],
                self.api_key_secret[-3:])
        elif self.type == 'user':
            return '{}:{}:{}:{}:{}'.format(
                self.type,
                self.api_key[-3:],
                self.api_key_secret[-3:],
                self.token[-3:] if self.token else 'null',
                self.secret[-3:] if self.secret else 'null')


class ValidRateLimitManager(models.Manager):
    def get_queryset(self):
        user_query = Q(credentials__type='user', credentials__token__isnull=False, credentials__secret__isnull=False)
        app_query = Q(credentials__type='app')

        return super().get_queryset().filter(user_query | app_query)

class AppRateLimitManager(models.Manager):
    def get_queryset(self):
        app_query = Q(credentials__type='app')

        return super().get_queryset().filter(app_query)

class UserRateLimitManager(models.Manager):
    def get_queryset(self):
        user_query = Q(credentials__type='user', credentials__token__isnull=False, credentials__secret__isnull=False)

        return super().get_queryset().filter(user_query)

class TwitterRateLimit(ModelBase):
    credentials = models.ForeignKey(TwitterCredentials, on_delete=models.CASCADE, related_name='rate_limits')
    endpoint = models.CharField(max_length=64)
    usage_count = models.IntegerField(default=0)
    locked = models.BooleanField()
    row_locked_at = models.DateTimeField(null=True, blank=True)

    remaining = models.IntegerField(null=True, blank=True)
    limit = models.IntegerField(null=True, blank=True)
    reset = models.DateTimeField(null=True, blank=True)

    objects = models.Manager()
    valid_rate_limits = ValidRateLimitManager()
    app_rate_limits = AppRateLimitManager()
    user_rate_limits = UserRateLimitManager()

    @classmethod
    def manager(cls, token_type):
        if token_type is None:
            return cls.valid_rate_limits
        elif token_type == 'app':
            return cls.app_rate_limits
        elif token_type == 'user':
            return cls.user_rate_limits

    @classmethod
    def create_or_update(cls, credentials, endpoint, usage=None, locked=False):
        queryset = TwitterRateLimit.objects.filter(credentials=credentials, endpoint=endpoint)
        if queryset.count():
            rate_limit = queryset.first()
        else:
            rate_limit = TwitterRateLimit()
            rate_limit.credentials = credentials
            rate_limit.endpoint = endpoint

        if usage:
            rate_limit.remaining = usage.remaining
            rate_limit.reset = usage.reset
            rate_limit.limit = usage.limit

        rate_limit.usage_count += 1
        rate_limit.locked = locked

        if locked:
            rate_limit.row_locked_at = utc_now()

        rate_limit.save()

    def __str__(self):
        return '{} {}'.format(self.credentials, self.endpoint)


class Topic(ModelBase):
    account = models.ForeignKey(Account, null=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=32, blank=True)
    query = models.CharField(max_length=4096, blank=True)
    parsed = JSONField(max_length=8192, blank=True, default=dict)
    context = JSONField(max_length=8192, blank=True, default=dict)

    def __str__(self):
        return self.name

    def getParsedQuery(self):
        if not self.parsed:
            self.parsed, self.context = QueryParser.instance().parse(self.query)
            self.save()

        return self.parsed

class TopicSet(ModelBase):
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    name = models.CharField(max_length=32)

class TopicSetMembership(ModelBase):
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)
    topic_set = models.ForeignKey(TopicSet, on_delete=models.CASCADE)

class UserRegistration(ModelBase):
    username = models.CharField(max_length=32)
    password = models.CharField(max_length=(256))
    email_address = models.CharField(max_length=(256))
    validation_key = models.CharField(max_length=32)
    expiry_date = models.DateTimeField(null=True, blank=True)
    status = models.CharField(max_length=32)

class Backup(ModelBase):
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    category = models.CharField(max_length=32)
    data = JSONField(max_length=32768000, blank=True, default=dict)

class AlertInstance(ModelBase):
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    status = models.CharField(max_length=32, default='new')  # new -> seen -> dismissed
    parameters = JSONField(max_length=512)
    created_at = models.DateTimeField(null=True, blank=True)
    type = models.CharField(max_length=64)