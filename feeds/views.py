import hashlib
import json
import re
import redis
import time

from api import TwitterApi, AUTHORS_ES, ES
from datetime import timedelta
from dateutil.parser import parse
from django.db.models import Count, Q
from django.http import HttpResponse, HttpResponseBadRequest, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from functools import reduce
from harvester.serverconf import server_config
from rest_framework.decorators import api_view, throttle_classes
from rest_framework.throttling import UserRateThrottle
from tools.queryparser import QueryParser, ParseException, MATCH_ALL, MUST, NOT, RANGE, SHOULD, TERMS, TERM
from feeds.consumers import Consumer
from feeds.exception import SigTermsDateRangeException
from feeds.models import Feed, Project, PublishedProject, AccountProjects, AccountAccess, AccountFeed, TwitterCredentials, AlertInstance, Topic, TopicSet, TopicSetMembership
from urllib.parse import urlparse
from utils.dt import utc_now, date_math, iso8601
from utils.event import event
from utils.iter import flatten
from uuid import uuid1 as get_uuid

REDIS = redis.Redis()

class Query:
    def __init__(self, proto_query, query_context = None):
        if query_context is None:
            query_context = {}

        self.parser = QueryParser(**query_context)

        self._proto_query = proto_query
        self._query_context = query_context

        self._query = None
        self._parsed = None
        self._contextified = None
        self._rewritten = None
        self._variables = None

        self._optimized = None
        self._search_context = None

    def __add__(self, other):
        query = Query('({}) AND ({})'.format(self.query, other.query))

        if self._parsed and other._parsed:
            query._parsed = MUST([self._parsed, other._parsed])

        return query

    def _contextify(self, query, context):
        if not context:
            return query

        queries = [query]

        if 'sort_boundary' in context:
            sort = context['sort']
            boundary = context['sort_boundary']

            if sort in ['newest_first', 'oldest_first']:
                if sort == 'newest_first':
                    query = RANGE(field='created_at', lt=boundary['created_at'])
                elif sort == 'oldest_first':
                    query = RANGE(field='created_at', gt=boundary['created_at'])

            else:
                if sort == 'likes':
                    field = 'like_count'
                elif sort == 'shares':
                    field = 'share_count'
                elif sort == 'replies':
                    field = 'reply_count'

                query = SHOULD([
                    RANGE(field=field, lt=boundary[field]),
                    MUST([
                        TERM(field, boundary[field]),
                        RANGE(field='created_at', lt=boundary['created_at'])
                    ])
                ])

            queries.append(query)

        if 'main_query' in context:
            if context['main_query']:
                sub_context = {'account_id': self.query_context.get('account_id')}
                if 'execution_hint' in context:
                    sub_context['execution_hint'] = context['execution_hint']

                queries.append(Query(context['main_query'], sub_context).parsed)

        if 'date_range' in context:
            queries.append(RANGE('created_at', gte=context['date_range']['start'], lt=context['date_range']['end']))

        return MUST(queries)

    # removed from _search: rewrite the aggregation keys for a time shift query
    # if parameters['chart_type'] not in ['PieChart', 'PostingsChart']:
    #     for index, id in enumerate(ids):
    #         if 'volume' not in response[index]:
    #             continue
    #
    #         query = queries[id]
    #
    #         if 'time_shift' in query.search_context:
    #             time_shift = query.search_context['time_shift']
    #             for item in response[index]['volume']:
    #                 item['key'] = date_math('add', item['key'], '{}'.format(time_shift['count']), time_shift['unit'])
    #         else:
    #             for item in response[index]['volume']:
    #                 item['key'] = date_math('add', item['key'], 0, 'days')
    def _rewrite(self, query, context):
        if type(query) is dict:
            if list(query.keys()) == ['__TOPIC__']:
                # topic = Topic.objects.get(name=list(query.values())[0])

                return self._rewrite(topic.getParsedQuery(), context)
            elif list(query.keys()) == ['__LISTS__']:
                list_names = list(query.values())[0]

                list_data = ES.get_list_authors(context.get('account_id'), list_names)

                authors = list(set(author for author_list in list_data.values() for author in author_list))

                return TERMS('author', authors)
            elif 'time_shift' in context and list(query.keys()) == ['range'] and list(list(query.values())[0].keys()) in [['created_at']]:
                start = query['range']['created_at']['gte']
                end = query['range']['created_at']['lt']

                start = date_math('sub', start, context['time_shift']['count'], context['time_shift']['unit'])
                end = date_math('sub', end, context['time_shift']['count'], context['time_shift']['unit'])

                return RANGE('created_at', gte=start, lt=end)
            else:
                return dict((key, self._rewrite(value, context)) for key, value in query.items())
        elif type(query) is list:
            return list(map(lambda x: self._rewrite(x, context), query))
        else:
            return query

    @property
    def variables(self):
        if self._variables is None:
            self._variables = dict((variable['name'], variable['value']) for variable in self.query_context.get('variables') or [])

        return self._variables

    @property
    def proto_query(self):
        return self._proto_query

    @property
    def query(self):
        if self._query is None:
            self._query = self.proto_query
            for variable in self.variables.keys():
                self._query = self._query.replace('${{{}}}'.format(variable), self.variables[variable])

        return self._query

    @property
    def query_context(self):
        return self._query_context

    @property
    def parsed(self):
        if self._parsed is None:
            self._parsed, self._search_context = self.parser.parse(self.query)

        return self._parsed

    @property
    def search_context(self):
        if self._search_context is None:
            self._parsed, self._search_context = self.parser.parse(self.query)

        return self._search_context

    @property
    def contextified(self):
        if self._contextified is None:
            self._contextified = self._contextify(self.parsed, self.query_context)

        return self._contextified

    @property
    def rewritten(self):
        if self._rewritten is None:
            self._rewritten = self._rewrite(self.contextified, self.search_context)

        return self._rewritten

    @property
    def optimized(self):
        if self._optimized is None:
            self._optimized = self.parser.optimize(self.rewritten)

        return self._optimized


class Search(object):
    def __init__(self, user, project_query, query_data):
        self.user = user
        self.project_query = project_query
        self.query_data = query_data

        self.alerts = []
        self.response = None

        self._account = None
        self._source_query = None
        self._data_query = None
        self._time_query = None
        self._account_feeds = None

    @property
    def account_feeds(self):
        if self._account_feeds is None:
            self._account_feeds = list(flatten(AccountFeed.objects.filter(account=self.account, enabled=True).order_by('feed_id').values_list('feed_id')))

        return self._account_feeds

    @property
    def source_query(self):
        if self._source_query is None:
            if not self._account_feeds:
                self._source_query = NOT([MATCH_ALL()])
            else:
                groups = []
                group = []
                for feed_id in self._account_feeds:
                    if not group:
                        group.append(feed_id)
                    elif len(group) == 1:
                        if feed_id == group[0] + 1:
                            group.append(feed_id)
                        else:
                            groups.append(group)
                            group = [feed_id]
                    elif len(group) == 2:
                        if feed_id == group[1] + 1:
                            group[1] = feed_id
                        else:
                            groups.append(group)
                            group = [feed_id]

                if group:
                    groups.append(group)

                queries = []
                if not self.user.has_perm('feeds.access_all_feeds'):
                    singles = []
                    for group in groups:
                        if len(group) == 1:
                            singles.extend(group)
                        else:
                            if group[1] - group[0] == 1:
                                singles.extend(group)
                            else:
                                queries.append(RANGE('source_feed', gte=group[0], lte=group[1]))

                    if singles:
                        queries.append(TERMS('source_feed', singles))

                if queries:
                    self._source_query = SHOULD(queries)
                else:
                    self._source_query = MATCH_ALL()

        return self._source_query

    @property
    def account(self):
        if self._account is None:
            self._account = AccountAccess.objects.get(user=self.user).account

        return self._account

    @property
    def time_query(self):
        if self._time_query is None:
            if not self.user.has_perm('feeds.access_all_time'):
                access_since = self.account.row_created_on - timedelta(weeks=4)

                self._time_query = RANGE('created_at', gte=iso8601(access_since))
            else:
                self._time_query = MATCH_ALL()

        return self._time_query

    @property
    def data_query(self):
        if self._data_query is None:
            self._data_query = MATCH_ALL()
            if not self.user.has_perm('feeds.access_all_data'):
                self._data_query = RANGE('random', lte=0.05)

        return self._data_query

    @property
    def context(self):
        return self.query_data['context']

    @property
    def topics(self):
        return self.query_data['topics']

    def contextify(self, context=None, **kwargs):
        if context is None:
            context = self.context

        context['account_id'] = self.account.id

        clauses = []

        for key in ['query']:
            if key in kwargs:
                if kwargs.get(key) is not None:
                    clauses.append(Query(kwargs.get(key), context).rewritten)
                else:
                    pass
            else:
                clauses.append(Query(self.__getattribute__(key), context).rewritten)

        for key in ['project_query']:
            if key in kwargs:
                if kwargs.get(key) is not None:
                    clauses.append(Query(kwargs.get(key)).rewritten)
                else:
                    pass
            else:
                clauses.append(Query(self.__getattribute__(key)).rewritten)

        for key in ['source_query', 'time_query', 'data_query']:
            if key in kwargs:
                if kwargs.get(key) is not None:
                    clauses.append(kwargs.get(key))
                else:
                    pass
            else:
                clauses.append(self.__getattribute__(key))

        result = QueryParser().optimize(MUST(clauses))

        return result

class PopularitySearch(Search):
    @property
    def query(self):
        query = [topic['query'] for topic in self.topics.values()]
        query = filter(lambda x: x, query)
        query = ' or '.join(map(lambda x: '({})'.format(x), query)).strip()

        return query

    @property
    def attribute(self):
        return self.query_data['attribute']

    @property
    def foreground(self):
        return self.contextify()

    @property
    def background(self):
        context = dict(self.context)

        time_delta = parse(self.context['date_range']['end']) - parse(self.context['date_range']['start'])

        if self.query_data['attribute'] == 'time':
            # if time_delta.total_seconds() > 7 * 86400:
            #     raise SigTermsDateRangeException()

            days = int(((time_delta.total_seconds() + 1) * 3 + 86399) / 86400)

            query = ' '.join([self.query, self.query_data.get('background', '')]).strip()
            context['date_range'] = {
                'start': date_math('sub', self.context['date_range']['start'], days, 'days'),
                'end': self.context['date_range']['end'],
            }
            context['execution_hint'] = 'expand_index_terms_query'

        elif self.query_data['attribute'] == 'query':
            # if time_delta.total_seconds() > 7 * 86400:
            #     raise SigTermsDateRangeException()

            query = ' '.join([self.query, self.project_query, context.get('main_query', '')])
            match = re.search('lang:\s*\w+', query)
            if match:
                query = match.group()
            else:
                query = ''

            query = ' '.join([query, self.query_data.get('background', '')]).strip()

            context['main_query'] = ''
            context['execution_hint'] = 'expand_index_terms_query'

        return self.contextify(context, query=query, source_query=None, data_query=None, time_query=None, project_query=None)

    def _execute_time(self):
        self.response = ES.sigterms(self.foreground, self.background)

    def _execute_query(self):
        if not self.query and not self.context['main_query']:
            self.alerts.append({'type': 'warning', 'message': 'Empty query in PopularityChart'})

            self.response = {'popularity': []}

        else:
            self.response = ES.sigterms(self.foreground, self.background)

    def _execute_authors(self):
        self.response = ES.terms_aggregation('author', self.query_data.get('metric', 'count'), self.contextify())

    def _execute_related_authors(self):
        self.response = ES.nested_terms_aggregation('related', 'author', self.contextify())

    def execute(self):
        try:
            if self.query_data['attribute'] == 'related_authors':
                self._execute_related_authors()
            elif self.query_data['attribute'] == 'authors':
                self._execute_authors()
            elif self.query_data['attribute'] == 'query':
                self._execute_query()
            elif self.query_data['attribute'] == 'time':
                self._execute_time()

        except SigTermsDateRangeException:
            self.alerts.append({'type': 'warning', 'message': 'Data range is too wide for this chart.'})
            self.response = {'popularity': []}

        return JsonResponse({'alerts': self.alerts, 'response': self.response})

class TopicSearch(Search):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._query_ids = None
        self._aggregations = {}
        self._sort = None
        self._query_bodies = None

    @property
    def query_ids(self):
        if self._query_ids is None:
            self._query_ids = sorted(self.query_data['topics'].keys())

        return self._query_ids

    def sort(self, topic):
        sort_dict = {
            'newest_first': {'created_at': 'desc'},
            'oldest_first': {'created_at': 'asc'},
            'likes': [{'like_count': 'desc'}, {'created_at': 'desc'}],
            'shares': [{'share_count': 'desc'}, {'created_at': 'desc'}],
            'replies': [{'reply_count': 'desc'}, {'created_at': 'desc'}],
            'followers': [{'followers_count': 'desc'}, {'created_at': 'desc'}],
        }

        return sort_dict[topic.get('sort', 'newest_first')]

    @property
    def query_bodies(self):
        if self._query_bodies is None:
            self._query_bodies = []
            for query_id in self.query_ids:
                topic = self.query_data['topics'][query_id]

                query = topic['query']
                context = dict(self.context)

                for key in ['sort', 'sort_boundary']:
                    if key in topic:
                        context[key] = topic[key]

                es_query = self.contextify(context, query=query)
                size = self.query_data['topics'][query_id].get('count', 0)

                query_body = {
                    'query': es_query,
                    'aggregations': self.aggregations,
                    'track_total_hits': True,
                    'size': size
                }

                sort = self.sort(self.query_data['topics'][query_id])
                if sort:
                    query_body['sort'] = sort

                self._query_bodies.append({})
                self._query_bodies.append(query_body)

        return self._query_bodies

    @property
    def date_range(self):
        return self.query_data['context']['date_range']

    @property
    def aggregations(self):
        return self._aggregations

    def enrich(self, result):
        # enrich postings with author lists
        authors = list(set(posting['author'] for topic in result for posting in topic['postings']))

        if authors:
            author_data = ES.get_author_data(self.account.id, authors)
        else:
            author_data = {}

        for topic in result:
            for posting in topic['postings']:
                posting['lists'] = author_data.get(posting['author'], {'lists': []})['lists']

    def execute(self):
        response = {}

        result = ES.msearch(self.query_bodies)
        if sum(topic['total'] for topic in result) == 0:
            self.alerts.append({'type': 'info', 'message': 'No data for this query in the selected time frame.'})

        self.enrich(result)
        for query_id, data in zip(self._query_ids, result):
            response[query_id] = {'id': query_id, 'data': data}

            for item in data.get('volume', []):
                item['key'] = date_math('add', item['key'], 0, 'days')

        print(response)

        for key in response:
            response[key]['data'] = self.post_process(response[key]['data'])

        return JsonResponse({'alerts': self.alerts, 'response': response})

    def post_process(self, data):
        return data

class PieSearch(TopicSearch):
    @property
    def aggregations(self):
        if not self._aggregations:
            metric = self.query_data['context'].get('metric')
            if metric in ['likes', 'replies']:
                field = 'like_count' if metric == 'likes' else 'reply_count'
                self._aggregations = {
                    'metric': {
                        'stats': {
                            'field': field
                        }
                    }
                }

        return self._aggregations


class LineSearch(TopicSearch):
    @property
    def aggregations(self):
        if not self._aggregations:
            min_bound = self.query_data['context']['date_range']['start']
            max_bound = self.query_data['context']['date_range']['end']

            total_seconds = (parse(max_bound) - parse(min_bound)).total_seconds()
            interval_seconds = self.query_data['context']['date_range']['interval_seconds']

            domain = self.context.get('domain', 'epoch')

            # if there are too many data points in the time range, with the current interval, add an alert, and d
            # not aggregate
            if (domain == 'epoch' and total_seconds / interval_seconds > 500) or (domain == 'time_of_day' and interval_seconds < 300) or (domain == 'day_of_week' and interval_seconds < 3600):
                self.alerts.append({'type': 'warning', 'message': 'Line chart resolution is too high for this date range'})

                self._aggregations = {}

            else:
                # the line chart date historgram aggregation
                self._aggregations['by_day'] = {
                    'date_histogram': {
                        'field': 'created_at',
                        'interval': self.date_range['interval'],
                        'time_zone': self.query_data['context']['date_range']['time_zone'],
                        'extended_bounds': {
                            'min': min_bound,
                            'max': max_bound,
                        },
                    }
                }

                metric = self.query_data['context'].get('metric')
                if metric in ['likes', 'replies']:
                    field = 'like_count' if metric == 'likes' else 'reply_count'
                    self._aggregations['by_day']['aggregations'] = {
                        'metric': {
                            'stats': {
                                'field': field
                            }
                        }
                    }

        return self._aggregations

    def post_process(self, data):
        domain = self.context.get('domain', 'epoch')

        if domain == 'time_of_day':
            volume = {}
            volume_keys = []

            if 'volume' in data:
                for item in data['volume']:
                    key = item['key']
                    time_of_day = parse(key).strftime('%H:%M:%S')

                    if time_of_day not in volume:
                        volume_keys.append(time_of_day)

                        volume[time_of_day] = {'count': 0, 'key': key}

                    volume[time_of_day]['count'] += item['count']

            data['volume'] = [volume[key] for key in sorted(volume_keys)]

        elif domain == 'day_of_week':
            volume = {}
            volume_keys = []

            if 'volume' in data:
                for item in data['volume']:
                    key = item['key']
                    parsed_key  = parse(key)
                    day_of_week = parsed_key.strftime('%w'), parsed_key.strftime('%H:%M:%S')

                    if day_of_week not in volume:
                        volume_keys.append(day_of_week)

                        volume[day_of_week] = {'count': 0, 'key': key}

                    volume[day_of_week]['count'] += item['count']

            data['volume'] = [volume[key] for key in sorted(volume_keys)]

        return data

class PostingsSearch(TopicSearch):
    pass


def feeds_summary(request):
    account = AccountAccess.objects.get(user=request.user).account

    if request.method == 'GET':
        summary = {
            'types': []
        }

        account_feeds = AccountFeed.objects.select_related('feed').filter(account=account)
        last_feed_object = AccountFeed.objects.filter(account=account).order_by('row_created_on').last()
        if not last_feed_object:
            return JsonResponse(None, safe=False);

        last_feed = last_feed_object.feed

        summary['last'] = {
            'type': last_feed.type,
            'uid': last_feed.uid,
        }
        summary['total'] = account_feeds.count()
        summary['total_enabled'] = account_feeds.filter(enabled=True).count()

        for count in account_feeds.values('feed__type').annotate(count=Count('feed__type')):
            summary['types'].append({'type': count['feed__type'], 'count': count['count']})

        print(summary)

        return JsonResponse(summary)


# Create your views here.

def feeds(request):
    if request.method == 'GET':
        parameters = {key: request.GET[key] for key in request.GET.keys()}
    else:
        parameters = json.loads(request.body.decode())

    print('FEEDS', request.user)

    account = AccountAccess.objects.get(user=request.user).account
    if request.method == 'GET':
        # get all feeds associated with the user's account
        types = parameters.get('types')
        if types:
            types = types.split(',')
        else:
            types = []

        filter = Q(account=account)

        if types:
            filter &= Q(feed__type__in=types)

        query = parameters.get('query')
        or_filter = None
        if query:
            for query_part in query.split(' '):
                filter_part = Q(feed__uid__icontains=query_part)
                if or_filter is None:
                    or_filter = filter_part
                else:
                    or_filter |= filter_part

            filter &= or_filter

        offset = int(parameters.get('offset', 0))
        count = int(parameters.get('count', 10))
        account_feeds = AccountFeed.objects.filter(filter).order_by('-row_created_on')[offset: offset+count]

        feeds = [{'feed': account_feed.feed.dict, 'enabled': account_feed.enabled} for account_feed in account_feeds]

        alerts = []
        if not feeds:
            alerts.append({'type': 'info', 'message': 'No feeds matching "{}"'.format(query or '')})

        return JsonResponse({'alerts': alerts, 'feeds': list(feeds)}, safe=False)

    elif request.method == 'DELETE':
        account_feeds = AccountFeed.objects.filter(account=account, feed__pk=parameters.get('feedId'))
        account_feeds.delete()

        return HttpResponse()

    elif request.method == 'PUT':
        account_feed = AccountFeed.objects.filter(account=account, feed__pk=parameters.get('feedId')).first()
        account_feed.enabled = not account_feed.enabled
        account_feed.save()

        return HttpResponse()

    elif request.method == 'POST':
        if not account.enabled:
            alert = {'type': 'warning', 'message': 'Your account has expired'}

            return JsonResponse({'alerts': [alert], 'response': {'feeds': []}})

        if not request.user.has_perm('feeds.add_feed'):
            alert = {'type': 'warning', 'message': 'Your account has insufficient permissions to perform this action'}

            return JsonResponse({'alerts': [alert], 'response': {'feeds': []}})

        screen_names = {
            'twitter': set()
        }

        alerts = []
        feeds = []

        author_data = ES.get_author_data(account.id)
        followers_sum = sum(author['followers_count'] for author in author_data.values())
        parameters_followers_sum = sum(author['followers_count'] for author in parameters['author_data'].values())

        follower_alert = None
        if followers_sum + parameters_followers_sum > 1e6 and not request.user.has_perm('feeds.follower_budget_1M'):
            follower_alert = {'type': 'warning', 'message': 'Exceeds follower limit of 1M'}
        elif followers_sum + parameters_followers_sum > 10e6 and not request.user.has_perm('feeds.follower_budget_10M'):
            follower_alert = {'type': 'warning', 'message': 'Exceeds follower limit of 10M'}
        elif followers_sum + parameters_followers_sum > 100e6 and not request.user.has_perm('feeds.follower_budget_100M'):
            follower_alert = {'type': 'warning', 'message': 'Exceeds follower limit of 100M'}

        if follower_alert:
            return JsonResponse({'alerts': [follower_alert], 'response': {'feeds': []}})

        for type in parameters['types']:
            if type in ['twitter_account', 'twitter_firehose']:
                for screen_name in parameters['author_data'].keys():
                    feed = Feed.objects.filter(type=type, uid=screen_name).first()
                    if feed:
                        if not feed.enabled:
                            feed.enabled = True
                            feed.save()

                        account_feed = AccountFeed.objects.filter(feed=feed, account=account).first()
                        if account_feed:
                            print("Feed {} {} already exists".format(type, screen_name))

                            alerts.append({'type': 'warning', 'message': 'Feed {} for {} already exists'.format(type, screen_name)})

                            continue

                        else:
                            account_feed = AccountFeed()
                            account_feed.feed = feed
                            account_feed.account = account
                            account_feed.save()

                            screen_names['twitter'].add(screen_name)
                            feeds.append({'type': type, 'screen_name': screen_name})

                            continue
                    else:
                        if request.user.has_perm('feeds.add_feed_without_account'):
                            feed = Feed()
                            feed.state = 'ready'
                            feed.harvest_next = utc_now()
                            feed.type = type
                            feed.uid = screen_name
                            feed.parameters = {'screen_name': screen_name}
                            feed.save()

                            account_feed = AccountFeed()
                            account_feed.feed = feed
                            account_feed.account = account
                            account_feed.save()

                            screen_names['twitter'].add(screen_name)
                            feeds.append({'type': type, 'screen_name': screen_name})
                        else:
                            alert = {'type': 'warning', 'message': 'Did not add "{}". Connect an account to add new feeds.'.format(screen_name)}
                            alerts.append(alert)

            if type == 'rss':
                for url in parameters['parameters']['urls']:
                    feed = Feed()
                    feed.state = 'ready'
                    feed.harvest_next = utc_now()
                    feed.type = type
                    feed.uid = '{uri.netloc}'.format(uri=urlparse(url))
                    feed.parameters = {'url': url}
                    feed.save()

        authors_updated_total = set()
        lists_updated_total = set()
        for screen_name in screen_names['twitter']:
            twitter_accounts = TwitterApi(type='app').get('users/lookup.json', screen_name=screen_name)

            # set associated account id reference and ingest authors
            author = twitter_accounts.data[0]
            author['account'] = account.id

            AUTHORS_ES.ingest([author])

            print("parameters", parameters)

            authors_updated, lists_updated = ES.update_lists(account.id, [screen_name], parameters['lists'], [])

            authors_updated_total |= set(authors_updated)
            lists_updated_total |= set(lists_updated)

        alerts.extend([
            {'type': 'info', 'message': '{} authors updated'.format(len(authors_updated_total))},
            {'type': 'info', 'message': '{} lists updated'.format(len(lists_updated_total))}
        ])

        if len(feeds) > 0:
            alerts.append({'type': 'info', 'message': '{} feeds added'.format(len(feeds))})

        return JsonResponse({'alerts': alerts, 'response': { 'feeds': feeds}})

def feeds_harvest(request):
    if request.method == 'POST':
        parameters = json.loads(request.body.decode())

        feed_id = parameters['feed_id']
        feed = Feed.objects.get(pk=feed_id)
        feed.harvest_next = utc_now()
        feed.save()

        return HttpResponse()

def user_search(request):
    def _make_user(item):
        return {key: item[key] for key in ['name', 'screen_name', 'followers_count', 'friends_count', 'profile_image_url']}

    if request.method == 'GET':
        # search twitter for authors
        response = TwitterApi(type='user').get('users/search.json', q=request.GET['query'], page=request.GET['page'], count=request.GET['count'], include_entities=False)

        users = list(map(_make_user, response.data))

        alerts = []
        if not users:
            alerts.append({'type': 'info', 'message': 'No users for "{}"'.format(request.GET['query'])})

        return JsonResponse({'users': users, 'alerts': alerts}, safe=False)

@api_view(['POST'])
@throttle_classes([UserRateThrottle])
def search(request):
    try:
        return _search(request)

    except ParseException as exception:
        alert = {
            'type': 'warning',
            'message': 'Parse error in "{}"'.format(exception.query)
        }

        return JsonResponse({'alerts': [alert], 'response': None})

@api_view(['POST'])
def search_estimate(request):
    parameters = json.loads(request.body.decode())

    alerts = []
    estimate = 0

    if parameters['type'] == 'PopularityChart':
        search_object = PopularitySearch(request.user, parameters['query'], parameters['query_data'])

        if search_object.query_data['attribute'] in ['query', 'time']:
            query = search_object.background

            query_bodies = [{}, {'query': query, 'size': 0, 'track_total_hits': True}]

            start_time = time.time()
            response = ES.msearch(query_bodies)
            duration = time.time() - start_time
            total = response[0]['total']

            attribute = search_object.query_data['attribute']
            key_format = 'popularity_{}_*'.format(attribute)
            keys = REDIS.keys(key_format)

            values = [REDIS.get(key) for key in keys]
            values = [json.loads(value.decode()) for value in values]
            values = sorted(values)
            values = list(filter(lambda x: x[1] > 0, values))

            if values:
                value = min(values, key=lambda x: abs(x[0] - total))

                estimate = value[1] * total / value[0] - duration
                if estimate < 1:
                    estimate = 0

            query_hash = hashlib.md5(json.dumps(query, sort_keys=True).encode('utf-8')).hexdigest()
            name = 'popularity_{}_{}'.format(attribute, query_hash)
            value = json.dumps([total, 0])
            REDIS.setex(name, 86400, value)

    estimate = int(10 * estimate + 0.5) / 10

    return JsonResponse({'alerts': alerts, 'estimate': estimate})

def test(request):
    parameters = json.loads(request.body.decode())

    query = parameters['query']

    try:
        Query(query).optimized

        alerts = [{'type': 'info', 'message': 'Query "{}" is valid'.format(query)}]
    except:
        alerts = [{'type': 'warning', 'message': 'Query "{}" is invalid'.format(query)}]

    return JsonResponse({'alerts': alerts})

def _search(request):
    if request.method == 'GET':
        parameters = {key: request.GET[key] for key in request.GET.keys()}
    else:
        parameters = json.loads(request.body.decode())

    # alerts = []
    #
    # # print(parameters)
    #
    # account = AccountAccess.objects.get(user=request.user).account
    # data_query = MATCH_ALL()
    # if not request.user.has_perm('feeds.access_all_data'):
    #     data_query = RANGE('random', lte=0.05)
    #
    # if not request.user.has_perm('feeds.access_all_time'):
    #     access_since = account.row_created_on - timedelta(weeks=4)
    #
    #     account_query = RANGE('created_at', gte=iso8601(access_since))
    # else:
    #     account_query = MATCH_ALL()

    if request.method == 'POST':
        class_map = {
            'PopularityChart': PopularitySearch,
            'PieChart': PieSearch,
            'LineChart': LineSearch,
            'PostingsChart': PostingsSearch,
        }

        SearchClass = class_map[parameters['chart_type']]
        search_object = SearchClass(
            user=request.user,
            project_query=parameters['query'],
            query_data=parameters['queryData'],
        )

        if len(search_object.account_feeds) == 0:
            alert = {'type': 'warning', 'message': 'No feeds'}

            return JsonResponse({'alerts': alert, 'response': {}})

        return search_object.execute()

def lists_summary(request):
    account = AccountAccess.objects.get(user=request.user).account

    if request.method == 'GET':
        lists = sorted(ES.get_lists_summary(account.id), key=lambda x: -x['count'])

        return JsonResponse(lists or None, safe=False)


def authors_update(request):
    if request.method == 'POST':
        parameters = json.loads(request.body.decode())

        AUTHORS_ES.update_authors(parameters['screen_names'])

        return HttpResponse()


def authors(request):
    account = AccountAccess.objects.get(user=request.user).account

    parameters = {}
    if request.method == 'GET':
        parameters = {key: request.GET[key] for key in request.GET.keys()}
    elif request.body:
        parameters = json.loads(request.body.decode())

    if request.method == 'GET':
        if 'authors' in parameters:
            # get a set of authors and their common lists
            author_data = ES.get_author_data(account.id, parameters['authors'].split(','))

            list_sets = list(set(author['lists']) for author in author_data.values())
            common_lists = list(set.intersection(*list_sets))

            authors = {
                'authorList': list(author_data.values()),
                'lists': common_lists
            }
        elif 'lists' in parameters:
            # get the set of authors that are all in the a set of lists
            lists = parameters['lists'].split(',')

            lists = ES.get_lists(account.id, {'terms': lists})

            authors = list(reduce(lambda a, b: a & b if a else b, map(lambda x: set(x['author']), lists), set()))

        else:  # get all authors for this account
            author_data = ES.get_author_data(account.id)

            authors = {
                'authorList': list(author_data.values()),
            }

        return JsonResponse(authors, safe=False)

    elif request.method == 'POST':
        common_lists = list(map(lambda x: x.strip().lower(), parameters['common_lists']))
        other_lists = parameters['other_lists']
        authors = parameters['authors']

        authors_updated, lists_updated = ES.update_lists(account.id, authors, common_lists, other_lists)

        alerts = [
            {'type': 'info', 'message': '{} authors updated'.format(len(authors_updated))},
            {'type': 'info', 'message': '{} lists updated'.format(len(lists_updated))}
        ]

        return JsonResponse({'alerts': alerts})

class finder:
    def lists(request):
        # print('finder lists')
        account = AccountAccess.objects.get(user=request.user).account
        if request.method == 'GET':
            parameters = {key: request.GET[key] for key in request.GET.keys()}

        if request.method == 'GET':
            list_labels = ES.get_lists(account.id, { 'prefix': parameters.get('query', '')} )

            # print('finder', list_labels)

            return JsonResponse(list_labels, safe=False)

    def authors(request):
        # print('finder authors')
        account = AccountAccess.objects.get(user=request.user).account
        if request.method == 'GET':
            parameters = {key: request.GET[key] for key in request.GET.keys()}

        if request.method == 'GET':
            authors = ES.match_authors(account.id, parameters.get('query', ''))

            # print('finder', authors)

            return JsonResponse(authors, safe=False)



def projects(request):
    account = AccountAccess.objects.get(user=request.user).account
    if request.method == 'GET':
        # get the list of projects for the user's account
        account_projects = list(object.project for object in AccountProjects.objects.filter(account=account))

        return JsonResponse(list({'id': project.id, 'name': project.name, 'query': project.query, 'type': project.type} for project in account_projects), safe=False)

    if request.method == 'POST':
        # add a project to the user's account
        parameters = json.loads(request.body.decode())

        account_projects = list(object.project for object in AccountProjects.objects.filter(account=account))

        if parameters['name'] in (project.name for project in account_projects):
            return HttpResponseBadRequest()

        project = Project()
        project.name = parameters['name']
        project.query = parameters['query']
        project.save()

        account_project = AccountProjects()
        account_project.account = account
        account_project.project = project
        account_project.save()

        return HttpResponse()


def project_duplicate(request):
    account = AccountAccess.objects.get(user=request.user).account

    if request.method == 'POST':
        parameters = json.loads(request.body.decode())

        project = Project.objects.get(pk=parameters['id'])

        account_project = AccountProjects.objects.filter(account=account, project=project)

        if not account_project:
            return HttpResponseBadRequest()

        duplicate = Project()
        duplicate.query = project.query
        duplicate.type = parameters.get('type') or project.type
        duplicate.name = parameters.get('name') or project.name + ' (copy)'
        duplicate.state = parameters.get('state') or project.state
        duplicate.save()

        account_project = AccountProjects()
        account_project.account = account
        account_project.project = duplicate
        account_project.save()

        return JsonResponse({'id': duplicate.id});

def project(request, project_id=None):
    # print('project id', project_id)

    account = AccountAccess.objects.get(user=request.user).account
    project = Project.objects.get(pk=project_id)

    if request.method == 'GET':
        # return project state
        if AccountProjects.objects.filter(account=account, project=project).exists():
            account_access = AccountAccess.objects.get(user=request.user)
            account_access.last_project = project
            account_access.save()
        else:
            return HttpResponseBadRequest()


        state = project.state
        state['project'] = { 'name': project.name, 'query': project.query, 'id': project.id, 'type': project.type }

        alerts = []
        if request.user.has_perm('feeds.access_all_data'):
            alerts.append({'type': 'info', 'message': 'Accessing 100% of data'})
        else:
            alerts.append({'type': 'warning', 'message': 'Accessing 5% of data'})

        if request.user.has_perm('feeds.access_all_feeds'):
            alerts.append({'type': 'info', 'message': 'Accessing all feeds'})

        if request.user.has_perm('feeds.access_all_time'):
            alerts.append({'type': 'info', 'message': 'Accessing data since beginning of time'})
        else:
            since = account.row_created_on - timedelta(weeks=4)

            alerts.append({'type': 'warning', 'message': 'Accessing data since {}'.format(since.strftime('%Y-%m-%d'))})

        return JsonResponse({'alerts': alerts, 'state': state})

    if request.method == 'POST':
        # update project state
        if not AccountProjects.objects.filter(account=account, project=project).exists():
            return HttpResponseBadRequest()

        project.state = json.loads(request.body.decode())
        project.save()

        return HttpResponse()

    if request.method == 'PUT':
        # update project
        if not AccountProjects.objects.filter(account=account, project=project).exists():
            return HttpResponseBadRequest()

        project.name = json.loads(request.body.decode())['name']
        project.query = json.loads(request.body.decode())['query']
        project.type = json.loads(request.body.decode())['type']
        project.save()

        return HttpResponse()

    if request.method == 'DELETE':
        # delete a project (by deleting the association; keey the project itself around)
        if not AccountProjects.objects.filter(account=account, project=project).exists():
            return HttpResponseBadRequest()

        AccountProjects.objects.filter(account=account, project=project).delete()

        return HttpResponse()


def twitter_callback(request):
    if request.method == 'GET':
        # when a user authorizes an app, it gets redirected here
        # print('oauth_token', request.GET['oauth_token'])
        # print('oauth_verifier', request.GET['oauth_verifier'])
        api = TwitterApi(type='app')

        if 'denied' not in request.GET:
            account_data = api.connect_account(request.GET['oauth_token'], request.GET['oauth_verifier'])

            account = AccountAccess.objects.get(user=request.user).account
            screen_name = account_data['screen_name']
            if TwitterCredentials.objects.filter(account=account, screen_name=screen_name).exists():
                twitter_account = TwitterCredentials.objects.get(account=account, screen_name=screen_name)
            elif TwitterCredentials.objects.filter(screen_name=screen_name).exists():
                return HttpResponse('<script>alert("account already in use"); window.close();</script>')
            else:
                twitter_account = TwitterCredentials()
                twitter_account.api_key = api.OAUTH_CONSUMER_KEY
                twitter_account.api_key_secret = api.OAUTH_CONSUMER_SECRET
                twitter_account.screen_name = screen_name
                twitter_account.account = account
                twitter_account.type = 'user'

            twitter_account.token = account_data['oauth_token']
            twitter_account.secret = account_data['oauth_token_secret']
            twitter_account.save()

        return HttpResponse('<script>window.close();</script>')


def twitter_connect(request):
    if request.method == 'GET':
        # return initial twitter authorization url
        url = TwitterApi(type='app').get_authorize_url(server_config.external_host)

        return JsonResponse({'url': url})


def twitter_accounts(request):
    if request.method == 'GET':
        # return connected twitter accounts
        account = AccountAccess.objects.get(user=request.user).account

        connected_accounts = []
        for object in TwitterCredentials.objects.filter(account=account):
            connected_accounts.append({
                'screen_name': object.screen_name,
                'token': object.token,
            })

        return JsonResponse(connected_accounts, safe=False)



def twitter_disconnect(request):
    if request.method == 'POST':
        # disconnect an account by deleting the tokens; this does not tell twitter to de-authorize the app
        # keep around for easy reconnecting
        screen_name = json.loads(request.body.decode())['screen_name']

        account = AccountAccess.objects.get(user=request.user).account

        if TwitterCredentials.objects.filter(account=account, screen_name=screen_name).exists():
            twitter_account = TwitterCredentials.objects.get(account=account, screen_name=screen_name)
            twitter_account.token = None
            twitter_account.secret = None
            twitter_account.save()

        return HttpResponse()



def twitter_remove(request):
    if request.method == 'POST':
        # remove a connected account completely
        screen_name = json.loads(request.body.decode())['screen_name']

        account = AccountAccess.objects.get(user=request.user).account

        TwitterCredentials.objects.filter(account=account, screen_name=screen_name).delete()

        return HttpResponse()


def topics(request):
    if request.method == 'GET':
        # return available topics for the user's account
        account = AccountAccess.objects.get(user=request.user).account

        topic_data = {'topics': [], 'topic_sets': []}

        for topic in Topic.objects.filter(account=account):
            topic_data['topics'].append(topic.name)

        for topic_set in TopicSet.objects.filter(account=account):
            topic_set_data = []
            for topic in TopicSetMembership.objects.select_related('topic').filter(topic_set=topic_set):
                topic_set_data.append({
                    'name': topic.name,
                    'query': topic.query
                })

            topic_data['topic_sets'].append(topic_set_data)

        return JsonResponse(topic_data)

def publish(request, uuid=None, name=None):
    if request.method == 'GET':
        published_project = PublishedProject.objects.filter(uuid=uuid).first()

        if not published_project:
            return HttpResponseBadRequest()

        if published_project.name and published_project.name != name:
            return HttpResponseBadRequest()

        published_project.last_accessed = utc_now()
        published_project.count = published_project.count + 1
        published_project.save()

        event('publish.get', uuid=published_project.uuid)

        return JsonResponse({
            'state': published_project.state,
            'data': published_project.data,
            'context': {
                'account_id': published_project.account.id
            }
        })

    if request.method == 'POST':
        parameters = json.loads(request.body.decode())

        if uuid:
            published_project = PublishedProject.objects.filter(uuid=uuid).first()
            if published_project:
                chart_id = parameters['chart_id']
                data = parameters['data']

                timeout = None
                if published_project.timeout == 'one_minute':
                    timeout = utc_now() + timedelta(seconds=60)
                elif published_project.timeout == 'one_hour':
                    timeout = utc_now() + timedelta(seconds=3600)
                elif published_project.timeout == 'one_day':
                    timeout = utc_now() + timedelta(seconds=86400)
                timeout = iso8601(timeout) if timeout else None

                published_project.data[str(chart_id)] = data
                published_project.data[str(chart_id)]['timeout'] = timeout
                published_project.save()

            return HttpResponse()
        else:
            account = AccountAccess.objects.get(user=request.user).account

            published_project = PublishedProject()
            published_project.state = parameters['state']
            published_project.data = parameters['data']
            published_project.uuid = str(get_uuid())
            published_project.account = account
            published_project.timeout = parameters.get('timeout')

            if not parameters.get('timeout'):
                timeout = None
            elif parameters.get('timeout') == 'one_minute':
                timeout = utc_now() + timedelta(seconds=60)
            elif parameters.get('timeout') == 'one_hour':
                timeout = utc_now() + timedelta(seconds=3600)
            elif parameters.get('timeout') == 'one_day':
                timeout = utc_now() + timedelta(seconds=86400)
            timeout = iso8601(timeout) if timeout else None

            for key, value in published_project.data.items():
                published_project.data[key]['timeout'] = timeout

            if 'name' in parameters:
                name = re.sub(r'[^a-z0-9]', r'_', parameters['name'].lower())
                name_path = '{}/'.format(name)

                published_project.name = name
                published_project.state['project']['name'] = parameters['name']
            else:
                name_path = ''

            published_project.save()

            url = 'http://{}/publish/{}{}'.format(server_config.external_host, name_path, published_project.uuid)

            return JsonResponse({'url': url})


def event_view(request):
    if request.method == 'POST':
        parameters = json.loads(request.body.decode())

        kwargs = parameters.pop('parameters')

        event(**parameters, **kwargs)

        return HttpResponse()

@csrf_exempt
def alert(request):
    if request.method == 'POST':
        parameters = json.loads(request.body.decode())

        print('POST', 'alert', parameters)

        alert_instance = AlertInstance.objects.filter(pk=parameters.get('id')).first()
        if alert_instance:
            # print('alert_instance', alert_instance.dict, alert_instance.account.id, Consumer.CONNECTIONS)

            screen_name = alert_instance.parameters['screen_name'].lower()

            Consumer.send_all(screen_name, alert_instance.dict)

        return HttpResponse()

def alerts(request):
    if request.method == 'GET':
        parameters = {key: request.GET[key] for key in request.GET.keys()}

        print('GET', 'alerts', parameters)

        if 'account_id' in parameters and parameters['account_id'] != 'undefined':
            account_id = parameters['account_id']
        elif not request.user.is_anonymous:
            account_id = AccountAccess.objects.get(user=request.user).account.id
        else:
            return HttpResponseBadRequest('No account context')

        # todo: cache this
        lists = parameters.get('lists')
        if lists:
            lists = lists.split(',')
        else:
            lists = []

        list_data = ES.get_list_authors(account_id, ['alert'] + lists)

        items = list(list_data.values())

        authors = set(items[0])
        for item in items[1:]:
            authors &= set(item)
        authors = list(map(str.lower, authors))

        # fetch alerts
        max_id = int(parameters.get('maxid', -1))
        if max_id < 0:
            max_id = AlertInstance.objects.all().order_by('-id')[0].id
        count = int(parameters.get('count', 5))
        offset = int(parameters.get('offset', 0))

        one_day_ago = utc_now() + timedelta(days=-1)

        notifications = list(AlertInstance.objects.filter(account__id=account_id, status__in=['new', 'read'], id__lte=max_id, created_at__gt=one_day_ago).order_by('-id'))
        notifications = [{'id': notification.id, 'created_at': iso8601(notification.created_at), 'parameters': notification.parameters, 'status': notification.status} for notification in notifications]
        notifications = list(filter(lambda x: x['parameters']['screen_name'].lower() in authors, notifications))
        notifications = notifications[offset:offset+count]

        for notification in notifications:
            notification['parameters']['created_at'] = iso8601(parse(notification['parameters']['created_at']))

        print("notifications", notifications)

        return JsonResponse({'notifications': list(notifications)})

    if request.method == 'POST':
        account = AccountAccess.objects.get(user=request.user).account

        parameters = json.loads(request.body.decode())

        print('POST', 'alerts', parameters)

        # dismiss alerts
        if (parameters['action'] == 'dismiss'):
            AlertInstance.objects.filter(account=account, id__lte=parameters['max_id']).update(status='dismissed')

        return HttpResponse()
