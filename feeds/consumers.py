from api import ES
from channels.generic.websocket import WebsocketConsumer
from feeds.models import AccountAccess

import json

class Consumer(WebsocketConsumer):
    CONNECTIONS = {
        'accounts': {},
        'authors': {},
    }

    def connect(self):
        self.accept()

        print('CONNECT', self.scope['user'])

        # if (self.scope['user'].is_anonymous):
        #     print('ANONYMOUS')
        #     account_id = None
        # else:
        #     account = AccountAccess.objects.filter(user_id=self.scope['user']).first().account
        #     account_id = account.id
        #
        # if account_id not in Consumer.CONNECTIONS:
        #     Consumer.CONNECTIONS[account_id] = []
        #
        # Consumer.CONNECTIONS[account_id].append(self)

    def disconnect(self, close_code):
        print('DISCONNECT', close_code)

        if (self.scope['user'].is_anonymous):
            print('ANONYMOUS')

            return

        account = AccountAccess.objects.filter(user_id=self.scope['user']).first().account
        if account.id in Consumer.CONNECTIONS['accounts']:
            if self in Consumer.CONNECTIONS['accounts'][account.id]:
                chart_ids = list(Consumer.CONNECTIONS['accounts'][account.id][self].keys())
                for chart_id in chart_ids:
                    authors =  Consumer.CONNECTIONS['accounts'][account.id][self][chart_id]
                    del Consumer.CONNECTIONS['accounts'][account.id][self][chart_id]
                    if len(Consumer.CONNECTIONS['accounts'][account.id][self]) == 0:
                        del Consumer.CONNECTIONS['accounts'][account.id][self]
                    if len(Consumer.CONNECTIONS['accounts'][account.id]) == 0:
                        del Consumer.CONNECTIONS['accounts'][account.id]

                    for author in authors:
                        Consumer.CONNECTIONS['authors'][author][self].remove((chart_id))

                        if len(Consumer.CONNECTIONS['authors'][author][self]) == 0:
                            del Consumer.CONNECTIONS['authors'][author][self]
                        if len(Consumer.CONNECTIONS['authors'][author]) == 0:
                            del Consumer.CONNECTIONS['authors'][author]

        print(Consumer.CONNECTIONS)

    def receive(self, text_data):
        print('RECEIVE', text_data)

        data = json.loads(text_data)

        if (self.scope['user'].is_anonymous):
            print('ANONYMOUS')

            account_id = data['context']['account_id']
        else:
            account = AccountAccess.objects.filter(user_id=self.scope['user']).first().account
            account_id = account.id

        if 'type' in data:
            if data['type'] == 'lists_subscribe':
                list_data = ES.get_list_authors(account_id, ['alert'] + data['data']['config'].get('lists', []))

                items = list(list_data.values())

                authors = set(items[0])
                for item in items[1:]:
                    authors &= set(item)
                authors = list(map(str.lower, authors))

                chart_id = data['data']['chartId']

                if authors:
                    if account_id not in Consumer.CONNECTIONS['accounts']:
                        Consumer.CONNECTIONS['accounts'][account_id] = {}
                    if self not in Consumer.CONNECTIONS['accounts'][account_id]:
                        Consumer.CONNECTIONS['accounts'][account_id][self] = {}

                    Consumer.CONNECTIONS['accounts'][account_id][self][chart_id] = list(authors)

                    for author in authors:
                        if author not in Consumer.CONNECTIONS['authors']:
                            Consumer.CONNECTIONS['authors'][author] = {}
                        if self not in Consumer.CONNECTIONS['authors'][author]:
                            Consumer.CONNECTIONS['authors'][author][self] = []

                        Consumer.CONNECTIONS['authors'][author][self].append((chart_id))

            if data['type'] == 'lists_unsubscribe':
                chart_id = data['data']['chartId']

                authors = Consumer.CONNECTIONS['accounts'][account_id][self][chart_id]
                del Consumer.CONNECTIONS['accounts'][account_id][self][chart_id]

                if len(Consumer.CONNECTIONS['accounts'][account_id][self]) == 0:
                    del Consumer.CONNECTIONS['accounts'][account_id][self]
                if len(Consumer.CONNECTIONS['accounts'][account_id]) == 0:
                    del Consumer.CONNECTIONS['accounts'][account_id]

                for author in authors:
                    Consumer.CONNECTIONS['authors'][author][self].remove((chart_id))

                    if len(Consumer.CONNECTIONS['authors'][author][self]) == 0:
                        del Consumer.CONNECTIONS['authors'][author][self]
                    if len(Consumer.CONNECTIONS['authors'][author]) == 0:
                        del Consumer.CONNECTIONS['authors'][author]

        # print(Consumer.CONNECTIONS)

    @staticmethod
    def send_all(author, data):
        author = author.lower()
        # print(author, list(Consumer.CONNECTIONS['authors'].keys()))
        for socket in Consumer.CONNECTIONS['authors'].get(author, []):
            socket.send(json.dumps({'chart_ids': Consumer.CONNECTIONS['authors'][author][socket], 'data': data}))