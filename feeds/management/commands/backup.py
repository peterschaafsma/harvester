from api import ES
from datetime import timedelta
from django.core.management.base import BaseCommand
from feeds import models
from utils.event import EventTime
from utils.dt import utc_now, iso8601


class Command(BaseCommand):
    def backup_lists(self):
        for account in models.Account.objects.all():
            all_lists = ES.get_lists_for_account(account.id)

            for list in all_lists.values():
                if 'updated_at' not in list:
                    continue

                if list['updated_at'] < iso8601(utc_now() - timedelta(days=1)):
                    continue

                backup = models.Backup()
                backup.account = account
                backup.category = 'lists'
                backup.data = all_lists
                backup.save()

                break

    def backup_project_states(self):
        for account in models.Account.objects.all():
            for account_project in models.AccountProjects.objects.select_related('project').filter(account=account):
                project = account_project.project

                if project.row_updated_on < utc_now() - timedelta(days=1):
                    continue

                backup = models.Backup()
                backup.account = account
                backup.category = 'project'
                backup.data = {'project_id': project.id, 'state': project.state}
                backup.save()

    def _handle(self, *args, **kwargs):
        self.backup_lists()
        self.backup_project_states()

    def handle(self, *args, **kwargs):
        with EventTime('backup'):
            self._handle(*args, **kwargs)
