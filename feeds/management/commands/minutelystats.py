from api import ES
from django.db.models import Sum
from django.core.management.base import BaseCommand
from feeds import models
from utils.event import event, EventTime
from utils.dt import utc_now


class Command(BaseCommand):
    def _credentials_utilization(self):
        # all valid user credentials
        # per endpoint, 1.0 - sum remaining / sum limits

        for endpoint in set(models.TwitterRateLimit.objects.values_list('endpoint', flat=True)):
            now = utc_now()

            limit = models.TwitterRateLimit.objects.filter(credentials__token__isnull=False, endpoint=endpoint).aggregate(Sum('limit'))['limit__sum']
            limit_used = models.TwitterRateLimit.objects.filter(credentials__token__isnull=False, reset__gt=now, endpoint=endpoint).aggregate(Sum('limit'))['limit__sum']
            remaining_used = models.TwitterRateLimit.objects.filter(credentials__token__isnull=False, reset__gt=now, endpoint=endpoint).aggregate(
            Sum('remaining'))['remaining__sum']

            if limit_used is None:
                utilization = 0.0
            else:
                utilization = (limit_used - remaining_used) / limit

            event('credentials.utilization', value=utilization, endpoint=endpoint)

    def _handle(self, *args, **kwargs):
        self._credentials_utilization()

    def handle(self, *args, **kwargs):
        with EventTime('minutelystats.duration'):
            self._handle(*args, **kwargs)
