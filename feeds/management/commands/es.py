from api import ES, TWITTER_ES, RSS_ES, AUTHORS_ES
from dateutil.parser import parse
from django.core.management.base import BaseCommand
from utils import dt

import elasticsearch.helpers
import json
import re


SETTINGS = {
    'analysis': {
        'normalizer': {
            'lowercase': {
                'type': 'custom',
                'char_filter': [],
                'filter': ['lowercase']
            },
            'keyword': {
                'type': 'custom',
                'char_filter': [],
                'filter': ['lowercase', 'asciifolding']
            }
        },
        'analyzer': {
            'character': {
                'type': 'pattern',
                'pattern': '(?=.)',
                'lowercase': False
            },
            'word': {
                'type': 'custom',
                'tokenizer': 'letter',
                'filter': ['lowercase', 'asciifolding']
            },
        }
    },
    'index': {
        'number_of_shards': 1,
        'number_of_replicas': 0,
    }
}

CONFIG = {
    'rss': {
        'settings': SETTINGS,
        'mappings': {
            'entry': {
                'dynamic': False,
                'properties': {
                    'id': {
                        'type': 'keyword',
                    },
                    'created_at': {
                        'type': 'date'
                    },
                    'indexed_at': {
                        'type': 'date',
                    },
                    'updated_at': {
                        'type': 'date'
                    },
                    'content': {
                        'type': 'text',
                        'index': False
                    },
                    'text': {
                        'type': 'text'
                    },
                    'title': {
                        'type': 'text'
                    },
                    'language': {
                        'type': 'keyword',
                        'normalizer': 'lowercase',
                    },
                    'source': {
                        'type': 'keyword',
                        'normalizer': 'lowercase',
                    },
                    'source_type': {
                        'type': 'keyword',
                        'normalizer': 'lowercase',
                    },
                    'source_feed': {
                        'type': 'integer'
                    },
                    'urls': {
                        'type': 'keyword',
                        'normalizer': 'lowercase',
                    },
                    'permalink': {
                        'type': 'keyword',
                        'index': False,
                    },
                    'images': {
                        'type': 'nested',
                        'properties': {
                            'thumbnail': {
                                'type': 'nested',
                                'properties': {
                                    'width': {
                                        'type': 'integer',
                                        'index': False
                                    },
                                    'height': {
                                        'type': 'integer',
                                        'index': False
                                    },
                                    'url': {
                                        'type': 'keyword',
                                        'index': False
                                    }
                                }
                            },
                            'original': {
                                'type': 'nested',
                                'properties': {
                                    'width': {
                                        'type': 'integer',
                                        'index': False
                                    },
                                    'height': {
                                        'type': 'integer',
                                        'index': False
                                    },
                                    'url': {
                                        'type': 'keyword',
                                        'index': False
                                    }
                                }
                            },
                        }
                    },
                }
            }
        },
    },
    'authors': {
        'mappings': {
            'dynamic': False,
            'properties': {
                'id': {
                    'type': 'keyword',
                },
                'indexed_at': {
                    'type': 'date',
                },
                'updated_at': {
                    'type': 'date',
                },
                'name': {
                    'type': 'text',
                },
                'latest': {
                    'type': 'boolean',
                },
                'account': {
                    'type': 'integer',
                },
                'screen_name': {
                    'type': 'keyword',
                    'normalizer': 'lowercase',
                },
                'profile_image_url': {
                    'type': 'keyword',
                    'index': False,
                },
                'followers_count': {
                    'type': 'integer',
                },
                'friends_count': {
                    'type': 'integer',
                }
            }
        },
        'settings': SETTINGS
    },
    'events': {
        'mappings': {
            'dynamic': True,
            'properties': {
                'timestamp': {
                    'type': 'date',
                },
                'metric': {
                    'type': 'keyword',
                },
                'value': {
                    'type': 'float',
                },
            }
        },
        'settings': SETTINGS
    },
    'lists': {
        'mappings': {
            'list': {
                'dynamic': False,
                'properties': {
                    'name': {
                        'type': 'keyword',
                        'normalizer': 'lowercase',
                    },
                    'account': {
                        'type': 'integer'
                    },
                    'author': {
                        'type': 'keyword',
                        'normalizer': 'lowercase',
                    },
                    'updated_at': {
                        'type': 'date'
                    }
                }
            }
        },
        'settings': SETTINGS
    },
    'tweets': {
        'mappings': {
            'dynamic': False,
            'properties': {
                'id': {
                    'type': 'long',
                },
                'created_at': {
                    'type': 'date'
                },
                'indexed_at': {
                    'type': 'date',
                },
                'conversation_updated_at': {
                    'type': 'date'
                },
                'engagement_updated_at': {
                    'type': 'date'
                },
                'content': {
                    'type': 'text',
                    'index': False
                },
                'text': {
                    'type': 'text',
                    'index': False,
                    'fields': {
                        'character': {
                            'type': 'text',
                            'analyzer': 'character'
                        },
                        'word': {
                            'type': 'text',
                            'analyzer': 'word'
                        },
                    }
                },
                'keywords': {
                    'type': 'keyword',
                    'normalizer': 'keyword',
                    "eager_global_ordinals": True,
                },
                'author': {
                    'type': 'keyword',
                    'normalizer': 'lowercase',
                    "eager_global_ordinals": True,
                },
                'mentions': {
                    'type': 'keyword',
                    'normalizer': 'lowercase',
                    "eager_global_ordinals": True,
                },
                'language': {
                    'type': 'keyword',
                    'normalizer': 'lowercase',
                    "eager_global_ordinals": True,
                },
                'source': {
                    'type': 'keyword',
                    'normalizer': 'lowercase',
                    "eager_global_ordinals": True,
                },
                'source_type': {
                    'type': 'keyword',
                    'normalizer': 'lowercase',
                    "eager_global_ordinals": True,
                },
                'source_feed': {
                    'type': 'integer'
                },
                'followers_count': {
                    'type': 'integer'
                },
                'related': {
                    'type': 'nested',
                    'dynamic': False,
                    'properties': {
                        'relation': {
                            'type': 'keyword'
                        },
                        'id': {
                            'type': 'long',
                        },
                        'author': {
                            'type': 'keyword',
                            'normalizer': 'lowercase',
                            "eager_global_ordinals": True,
                        },
                        'content': {
                            'type': 'text',
                            'index': False
                        },
                        'text': {
                            'type': 'text',
                            'index': False,
                            'fields': {
                                'character': {
                                    'type': 'text',
                                    'analyzer': 'character'
                                },
                                'word': {
                                    'type': 'text',
                                    'analyzer': 'word'
                                },
                            }
                        },
                        'keywords': {
                            'type': 'keyword',
                            'normalizer': 'keyword',
                            "eager_global_ordinals": True,
                        },
                    }
                },
                'conversation_id': {
                    'type': 'long'
                },
                'descendant_count': {
                    'type': 'integer'
                },
                'random': {
                    'type': 'float'
                },
                'like_count': {
                    'type': 'integer'
                },
                'share_count': {
                    'type': 'integer'
                },
                'reply_count': {
                    'type': 'integer'
                },
                'urls': {
                    'type': 'keyword',
                    'normalizer': 'lowercase',
                },
                'permalink': {
                    'type': 'keyword',
                    'index': False,
                },
                'images': {
                    'type': 'nested',
                    'properties': {
                        'thumbnail': {
                            'type': 'nested',
                            'properties': {
                                'width': {
                                    'type': 'integer',
                                    'index': False
                                },
                                'height': {
                                    'type': 'integer',
                                    'index': False
                                },
                                'url': {
                                    'type': 'keyword',
                                    'index': False
                                }
                            }
                        },
                        'original': {
                            'type': 'nested',
                            'properties': {
                                'width': {
                                    'type': 'integer',
                                    'index': False
                                },
                                'height': {
                                    'type': 'integer',
                                    'index': False
                                },
                                'url': {
                                    'type': 'keyword',
                                    'index': False
                                }
                            }
                        },
                    }
                },
            }
        },
        'settings': SETTINGS
    },
}


class Command(BaseCommand):
    def _parse_args(self, *args):
        options = {}
        for arg in args:
            key, value = arg.split('=', 1)
            key = key.replace('-', '_')

            options[key] = value

        return options

    def add_arguments(self, parser):
        parser.add_argument('--create-index', nargs='*')
        parser.add_argument('--update-mapping', nargs='*')
        parser.add_argument('--ensure-aliases-and-indices', nargs='*')
        parser.add_argument('--drop-index', nargs='*')
        parser.add_argument('--dump-index', nargs='*')
        parser.add_argument('--load-index', nargs='*')
        parser.add_argument('--update-reply-counts', nargs='*')
        parser.add_argument('--custom', action='store_true')

    def handle(self, *args, **kwargs):
        if kwargs['create_index'] is not None:
            options = self._parse_args(*kwargs['create_index'])

            ES.create_index(options['index'], CONFIG[options['config']])

        if kwargs['ensure_aliases_and_indices'] is not None:
            ES.ensure_aliases_and_indices(CONFIG['tweets'])

        if kwargs['update_mapping'] is not None:
            options = self._parse_args(*kwargs['update_mapping'])

            mapping = CONFIG[options['config']]['mappings']

            ES.update_mapping(options['index'], mapping)

        if kwargs['drop_index'] is not None:
            options = self._parse_args(*kwargs['drop_index'])

            ES.delete_index(options['index'])

        if kwargs['dump_index'] is not None:
            options = self._parse_args(*kwargs['dump_index'])

            ES.dump_index(options['index'].split(','))

        if kwargs['load_index'] is not None:
            options = self._parse_args(*kwargs['load_index'])

            ES.load_index(options['file_name'].split(','))

        if kwargs['update_reply_counts'] is not None:
            options = self._parse_args(*kwargs['update_reply_counts'])

            ES.update_reply_counts(since=dt.iso8601(parse(options['since'])))

        if kwargs['custom']:
            for batch in ES._scan_batch(index='tweets2', body={'query': {'term': { 'language': 'nl'}}}):
                actions = []
                for item in batch:
                    # print(json.dumps(item))
                    if 'text' not in item['_source']:
                        continue

                    keywords = re.findall('\w+', item['_source']['text'])
                    actions.append({
                        '_op_type': 'update', '_id': item['_id'], '_index': item['_index'], '_type': item['_type'],
                         '_source': {'doc': { 'keywords': keywords }}
                    })

                    # break


                elasticsearch.helpers.bulk(ES.client, actions)

                # break