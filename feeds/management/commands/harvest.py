from api import AUTHORS_ES, TWITTER_ES
from django.core.management.base import BaseCommand
from django.db.models import Q, Min
from filelock import FileLock
from tools.harvester import Harvester
from feeds.models import Feed, AccountFeed, Account
from utils.dt import utc_now
from utils.event import event, EventTime
from utils.log import print

import elasticsearch.helpers


harvest_lock = FileLock('{}.lock'.format(__file__), 0.1)

class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--types')
        parser.add_argument('--ids')
        parser.add_argument('--force', action='store_true')
        parser.add_argument('--full', action='store_true')
        parser.add_argument('--one-page', dest='one_page', action='store_true')
        parser.add_argument('--max-batches', dest='max_batches', type=int)
        parser.add_argument('--debug', action='store_true')
        parser.add_argument('--add-missing-feeds', dest='add_missing_feeds', action='store_true')
        parser.add_argument('--delete-author', dest='delete_author', nargs='*')
        parser.add_argument('--reindex-authors', dest='reindex_authors', action='store_true')
        parser.add_argument('--process-count', dest='process_count', type=int)


    def handle(self, *args, **kwargs):
        if kwargs['reindex_authors']:
            screen_names = [feed.uid for feed in Feed.objects.all()]

            AUTHORS_ES.index_authors(screen_names)

            return

        if kwargs['add_missing_feeds']:
            authors = [author['_source']['screen_name'] for author in AUTHORS_ES.get_all()]

            feeds = Feed.objects.filter(uid__in=authors)

            authors = list(set(authors) - set([feed.uid for feed in feeds]))

            account = Account.objects.get(pk=2)

            for author in authors:
                for type in ['twitter_account', 'twitter_firehose']:
                    feed = Feed()
                    feed.state = 'ready'
                    feed.harvest_next = utc_now()
                    feed.type = type
                    feed.uid = author
                    feed.parameters = {'screen_name': author}
                    feed.save()

                    account_feed = AccountFeed(account=account, feed=feed)
                    account_feed.save()

            return

        if kwargs['delete_author']:
            params = {arg.split('=', 1)[0]: arg.split('=', 1)[1] for arg in kwargs['delete_author']}

            #delete feeds
            Feed.objects.filter(uid=params['author']).delete()

            #delete from authors and authors_source
            body = {
                'query': {
                    'ids': {
                        'values': [params['author']]
                    }
                }
            }
            AUTHORS_ES.delete_by_query('authors', body, 'author')
            AUTHORS_ES.delete_by_query('authors_source', body, 'object')

            #delete from lists
            body = {
                'query': {
                    'term': {
                        'author': params['author']
                    }
                }
            }

            for batch in AUTHORS_ES._scan_batch('lists', body=body):
                actions = []
                for item in batch:
                    authors = item['_source']['author']
                    authors.remove(params['author'])

                    actions.append({
                        '_op_type': 'update',
                        '_id': item['_id'],
                        '_index': 'lists',
                        '_type': 'list',
                        '_source': {
                            'doc': {
                                'author': authors
                            }
                        }
                    })

                elasticsearch.helpers.bulk(AUTHORS_ES.client, actions)

            #delete posts by author
            body = {
                'query': {
                    'term': {
                        'author': params['author']
                    }
                }
            }

            for batch in TWITTER_ES._scan_batch('tweets', body=body):
                actions = []
                for item in batch:
                    actions.append({
                        '_op_type': 'delete',
                        '_id': item['_id'],
                        '_index': 'tweets',
                        '_type': 'tweet'
                    })

                elasticsearch.helpers.bulk(TWITTER_ES.client, actions)

            return

        try:
            harvest_lock.acquire()
            print('Lock aquired')

            with EventTime('harvest.cron'):
                for group in Feed.objects.filter(enabled=True).values('type').annotate(harvest_last=Min('harvest_last')):
                    if group['harvest_last']:
                        event('harvest.delay', (utc_now() - group['harvest_last']).total_seconds(), None, type=group['type'])

                filters = Q(enabled=True)
                if not kwargs['force']:
                    filters &= (Q(harvest_next__lt=utc_now()) | Q(harvest_next=None)) & Q(state='ready')
                if kwargs['types']:
                    filters &= Q(type__in=kwargs['types'].split(','))
                if kwargs['ids']:
                    ids = []
                    for id_element in kwargs['ids'].split(','):
                        if '-' in id_element:
                            min, max = id_element.split('-')
                            ids.extend(range(int(min), int(max)+1))
                        else:
                            ids.append(int(id_element))
                    filters &= Q(pk__in=ids)

                feeds = Feed.objects.filter(filters)

                event('harvest.queuesize', feeds.count())

                print('Harvesting {} feeds'.format(feeds.count()))

                feed_kwargs = {
                    'one_page': kwargs['one_page'],
                    'full': kwargs['full'],
                    'max_batches': kwargs['max_batches'],
                    'process_count': kwargs['process_count'],
                    'debug': kwargs['debug'],
                }

                ordered_feeds = list(feeds.filter(type='twitter_update')) + list(feeds.exclude(type='twitter_update').order_by('id'))

                harvester = Harvester()
                harvester.run(ordered_feeds, **feed_kwargs)

        except:
            print('Locked. Exiting.')

            raise

        finally:
            harvest_lock.release()
            print('Lock released')

        print ('All done')

