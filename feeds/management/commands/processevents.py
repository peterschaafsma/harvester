from api import ES
from dateutil.parser import parse
from django.core.management.base import BaseCommand

import elasticsearch.helpers
import json
import pika


def connection():
    return pika.BlockingConnection(pika.ConnectionParameters('localhost'))

class Consumer:
    def __init__(self):
        self.channel = connection().channel()
        self.channel.queue_declare('events')

    def run(self):
        actions = []

        while True:
            (_, _, body) = self.channel.basic_get(queue='events', auto_ack=True)

            # print('body', body)

            if not body:
                break

            body = json.loads(body.decode())
            body['timestamp'] = parse(body['timestamp'])

            actions.append({'_index': 'events', '_type': 'event', '_source': body})

        self.channel.close()

        elasticsearch.helpers.bulk(ES.client, actions)

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        consumer = Consumer()
        consumer.run()