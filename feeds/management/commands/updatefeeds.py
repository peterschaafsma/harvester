from api import ES
from django.core.management.base import BaseCommand
from feeds import models
from utils.event import event, EventTime
from utils.dt import utc_now


class Command(BaseCommand):
    def _handle(self, *args, **kwargs):
        accounts = models.Account.objects.filter(enabled=True, expires_on__lt=utc_now())
        models.AccountFeed.objects.filter(account__in=accounts).update(enabled=False)

        account_feeds = models.AccountFeed.objects.filter(enabled=True).values('feed_id')

        enabled_feed_ids = set(account_feed['feed_id'] for account_feed in account_feeds)
        all_feed_ids = set(feed['id'] for feed in models.Feed.objects.values('id'))

        models.Feed.objects.filter(type__in=['twitter_account', 'twitter_firehose'], id__in=list(enabled_feed_ids)).update(enabled=True)
        models.Feed.objects.filter(type__in=['twitter_account', 'twitter_firehose'], id__in=list(all_feed_ids - enabled_feed_ids)).update(enabled=False)

    def handle(self, *args, **kwargs):
        with EventTime('updatefeeds'):
            self._handle(*args, **kwargs)
