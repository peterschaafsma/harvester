from api import ES
from django.core.management.base import BaseCommand
from feeds import models
from utils.event import event, EventTime
from utils.dt import utc_now


class Command(BaseCommand):
    def _number_of_feeds(self):
        event('feed.count', models.Feed.objects.all().count())

    def _number_of_postings(self):
        body = {
            'query': {
                'match_all': {}
            }
        }

        response = ES.client.count(index='tweets', doc_type='tweet', body=body)

        event('postings.count', response['count'])

    def _age_of_locked_credentials(self):
        oldest = models.TwitterRateLimit.objects.filter(locked=True).order_by('row_locked_at').first()

        event('credentials.maxlocktime', (utc_now() - oldest.row_locked_at).total_seconds() if oldest else 0)

    def _handle(self, *args, **kwargs):
        self._number_of_feeds()
        self._age_of_locked_credentials()
        self._number_of_postings()

    def handle(self, *args, **kwargs):
        with EventTime('collectstats'):
            self._handle(*args, **kwargs)
