import json
import urllib.request

from bs4 import BeautifulSoup
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def handle(self, **kwargs):
        content = urllib.request.urlopen('https://www.telegraaf.nl/nieuws/2591954/onderzoek-naar-invloed-straling-op-ramp-stint').read()

        soup = BeautifulSoup(content, features='lxml')

        self.traverse(soup)

    def traverse(self, node):
        print(type(node))
        
        for element in node:
            self.traverse(element)