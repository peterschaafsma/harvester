import json
import pprint

from api import TWITTER_API, AUTHORS_ES
from django.core.management.base import BaseCommand
from feeds.models import Feed
from utils import iter
from utils.log import print


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--args', nargs='+', default='')
        parser.add_argument('--keys', nargs='+', default='')
        parser.add_argument('--search')
        parser.add_argument('--harvest-authors', action='store_true')

    def handle(self, *args, **kwargs):
        if kwargs['search']:
            kwargs['keys'] = ['id_str,name,screen_name,followers_count,friends_count']
            args = (
                'path={}'.format('users/search.json'),
                'q={}'.format(kwargs['search'])
            )
            kwargs['search'] = None

            return self.handle(*args, **kwargs)

        elif kwargs['harvest_authors']:
            feed_objects = Feed.objects.filter(type='twitter_account')
            screen_names = (feed.parameters['screen_name'] for feed in feed_objects)

            authors = []
            for batch in iter.batchify(screen_names, 100):
                screen_name_list = ','.join(batch)

                results = TWITTER_API.get('users/lookup.json', screen_name=screen_name_list)

                authors.extend(author for author in results.data)

            AUTHORS_ES.ingest(authors)

            return

        else:
            for arg in args:
                key, value = arg.split('=')
                kwargs[key] = value

            #keys = list(filter(lambda x: x, kwargs.pop('keys')[0].split(','))) if kwargs['keys'] else None
            kwargs.pop('keys')

            results = TWITTER_API.get(kwargs.pop('path'), **kwargs)

        if type(results) is list:
            results = list(map(lambda x: {key: value for key, value in x.items() if not keys or key in keys}, results))

            pprint.pprint(results)
        else:
            print(json.dumps(results))
