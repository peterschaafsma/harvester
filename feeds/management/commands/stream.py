import json
import pika
import time
import tweepy
import urllib.parse
import urllib.request

from api import TWITTER_ES, AUTHORS_ES
from django.core.management.base import BaseCommand
from feeds.models import AlertInstance, TwitterCredentials, Account
from filelock import FileLock
from urllib3.exceptions import ProtocolError
from utils.dt import iso8601, utc_now
from utils.event import event


stream_lock = FileLock('{}.stream.lock'.format(__file__), 1.0)
ingest_lock = FileLock('{}.ingest.lock'.format(__file__), 1.0)

class StreamListener(tweepy.StreamListener):
    def __init__(self, **kwargs):
        super().__init__()

        self.author_alerts = {}

        self._reload_alerts()
        self._last_reload = None

        self.kwargs = kwargs

        self.start = time.time()

    def _reload_alerts(self):
        self.author_alerts = {}

        accounts = list(Account.objects.filter(enabled=True))

        for account in accounts:
            author_data = AUTHORS_ES.get_authors_in_lists(account.id, ['alert'])

            for key, value in author_data.items():
                author_id = str(value['id'])
                if author_id not in self.author_alerts:
                    self.author_alerts[author_id] = []

                alert = {
                    'account': account,
                    'screen_name': value['screen_name']
                }

                self.author_alerts[author_id].append(alert)

        for key, value in self.author_alerts.items():
            print(key, [subscriber['account'].id for subscriber in value])

    def on_status(self, status_object):
        status = status_object._json

        if status['user']['id_str'] not in self.author_alerts.keys():
            return

        if status.get('in_reply_to_status_id_str'):
            return

        if status.get('retweeted_status'):
            return

        if status['user']['id_str'] in self.author_alerts:
            for alert in self.author_alerts[status['user']['id_str']]:
                instance = AlertInstance()
                instance.account = alert['account']
                instance.status = 'new'
                instance.created_at = utc_now()
                instance.type = 'author'
                instance.parameters = {
                    'status_id': status['id_str'],
                    'screen_name': alert['screen_name'],
                    'created_at': status['created_at'],
                    'text': status['text'],
                }
                instance.save()

                url = 'http://{}/api/alert/'.format(self.kwargs['host'])
                params = {'id': instance.id}

                print('url', url)

                sleep = 1
                while True:
                    try:
                        response = urllib.request.urlopen(url, json.dumps(params).encode())
                        if response.status != 200:
                            print('error signalling alert')

                        break

                    except urllib.error.URLError as exception:
                        print('URLError. Sleeping for {} seconds'.format(sleep))

                        time.sleep(sleep)

                        sleep += 2

                        if sleep > 60:
                            print('Something is really wrong. Exiting')

                            raise


        print('queue', status)
        connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
        channel = connection.channel()
        channel.queue_declare('posts')
        channel.basic_publish(exchange='', routing_key='posts', body=json.dumps({'source': 'twitter', 'data': status}))
        channel.close()

        event('streaming.post', source='twitter')

        if time.time() - self.start > 3600:
            return False

class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--harvest', action='store_true')
        parser.add_argument('--host', default='localhost:8000')
        parser.add_argument('--ingest', action='store_true')

    def handle(self, *args, **kwargs):
        if kwargs.get('harvest'):
            self.harvest(host=kwargs['host'])
        elif kwargs.get('ingest'):
            self.ingest()

    def ingest(self):
        try:
            ingest_lock.acquire()

            def callback(channel, method, properties, body):
                print('consume')
                print(body)

                status = json.loads(body.decode())['data']
                status['full_text'] = status['text']
                if 'extended_tweet' in status:
                    status['full_text'] = status['extended_tweet']['full_text']

                TWITTER_ES.ingest([status])

            connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
            channel = connection.channel()
            channel.basic_consume(queue='posts', auto_ack=True, on_message_callback=callback)
            channel.start_consuming()

        except Exception as e:
            print(e)

            raise
        finally:
            ingest_lock.release()

    def harvest(self, host):
        try:
            print('lock')
            stream_lock.acquire()

            while True:
                try:
                    credentials = TwitterCredentials.objects.filter(type='user').first()
                    if credentials:
                        auth = tweepy.OAuthHandler(credentials.api_key, credentials.api_key_secret)
                        auth.set_access_token(credentials.token, credentials.secret)

                        api = tweepy.API(auth)
                        listener = StreamListener(host=host)

                        follows = list(listener.author_alerts.keys())
                        if not follows:
                            print('No alerts')
                        else:
                            stream = tweepy.Stream(auth=api.auth, listener=listener)
                            stream.filter(follow=follows)
                    else:
                        print('No credentials')

                except ProtocolError as exception:
                    print('ProtocolError')

                    time.sleep(1)
        finally:
            print('release')
            stream_lock.release()