from utils.event import event, EventTime

class RequestMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        with EventTime('request.time', method=request.method, path=request.path):
            response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        event('request.status_code', response.status_code)

        return response
