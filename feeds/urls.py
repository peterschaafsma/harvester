from django.urls import path, re_path

import feeds.views

urlpatterns = [
    re_path('feeds/$', feeds.views.feeds),
    re_path('feeds/harvest/$', feeds.views.feeds_harvest),
    re_path('feeds/summary/$', feeds.views.feeds_summary),

    path('user/search/', feeds.views.user_search),

    re_path('search/$', feeds.views.search),
    re_path('search/estimate/$', feeds.views.search_estimate),
    path('test/', feeds.views.test),

    path('finder/lists/', feeds.views.finder.lists),
    path('finder/authors/', feeds.views.finder.authors),

    re_path('authors/$', feeds.views.authors),
    re_path('authors/update/$', feeds.views.authors_update),

    path('lists/summary/', feeds.views.lists_summary),

    re_path(r'^projects/$', feeds.views.projects),
    re_path(r'^project/(?P<project_id>(\d+))/$', feeds.views.project, name='project'),
    re_path(r'^project/duplicate/$', feeds.views.project_duplicate, name='duplicate'),

    path('twitter/callback/', feeds.views.twitter_callback),
    path('twitter/connect/', feeds.views.twitter_connect),
    path('twitter/disconnect/', feeds.views.twitter_disconnect),
    path('twitter/remove/', feeds.views.twitter_remove),
    path('twitter/accounts/', feeds.views.twitter_accounts),

    path('topics/', feeds.views.topics),

    re_path(r'^publish/(?P<uuid>([-a-f0-9]+))/$', feeds.views.publish),
    re_path(r'^publish/(?P<name>([a-z0-9_]+))/(?P<uuid>([-a-f0-9]+))/$', feeds.views.publish),
    re_path(r'^publish/$', feeds.views.publish),

    re_path(r'^event/$', feeds.views.event_view),
    re_path(r'^alert/$', feeds.views.alert),
    re_path(r'^alerts/$', feeds.views.alerts),
]