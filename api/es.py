import datetime
import elasticsearch
import elasticsearch.helpers
import json
import locale
import re
import sys
import urllib3


from tools.queryparser import RANGE, MUST, NESTED, TERM, IDS, TERMS
from utils import dt, iter
from utils.event import EventTime
from utils.log import print


MAX_LISTS = 1024
MAX_AUTHORS = 32768

locale.setlocale(locale.LC_ALL, 'nl_NL.UTF-8')

class Elasticsearch:
    def __init__(self):
        self.client = elasticsearch.Elasticsearch(timeout=30)

    def _scan_batch(self, index=None, body=None, batch_size=10000):
        # iterate over ES results in batches
        parameters = {
            'client': self.client,
            'index': index,
            'scroll': '1m',
        }
        if body:
            parameters['query'] = body

        batch = []
        for doc in elasticsearch.helpers.scan(**parameters):
            batch.append(doc)

            if len(batch) == batch_size:
                yield batch

                batch = []

        if batch:
            yield batch

    def _filter_entries(self, entries):
        # so we have a list of entries we want to be in the index, which are completely new,
        # which exist but need updating, which exists and should be left alone
        all_entries = {(self._get_id(entry), self._get_index(entry)): entry for entry in entries}

        all_ids = [key[0] for key in all_entries.keys()]
        query = {
            'ids': {
                'values': all_ids
            }
        }

        body = {
            'query': query
        }

        new_entries = {}
        update_entries = {}
        ignore_entries = {}

        existing_entries = {}
        for batch in self._scan_batch(index='tweets_all', body=body):
            for item in batch:
                item_id = int(item['_id']) or self._get_id(item['_source'])
                item_index = item['_index'] or self._get_index(item['_source'])
                if item_id and item_index:
                    existing_entries[(item_id, item_index)] = item['_source']

        for (id, index), entry in all_entries.items():
            if (id, index) not in existing_entries:
                new_entries[(id, index)] = entry
            elif self._should_update(all_entries[(id, index)], existing_entries[(id, index)]):
                update_entries[(id, index)] = entry
            else:
                ignore_entries[(id, index)] = entry

        return new_entries, update_entries, ignore_entries, existing_entries

    def ensure_aliases_and_indices(self, body):
        # delete tweets alias
        aliases = list(map(str.split, self.client.cat.aliases().splitlines()))
        aliases = set(alias[0] for alias in aliases)

        if 'tweets_all' in aliases:
            self.client.indices.delete_alias(index='tweets*', name='tweets_all')

        today = datetime.datetime.today()
        day = datetime.datetime.today()
        while today.month == day.month:
            day = day + datetime.timedelta(days=28)

        next_index = day.strftime('tweets_%Y_%m')
        if not self.client.indices.exists(index=next_index):
            self.client.indices.create(index=next_index, body=body)

        tweets_indices = []
        for index in self.client.indices.get('*'):
            if re.match(r'tweets_\d{4,}', index):
                tweets_indices.append(index)

        for index in tweets_indices:
            self.client.indices.put_alias(index=index, name='tweets_all')


    def create_index(self, index, body):
        elasticsearch.client.IndicesClient(self.client).create(index=index, body=body)

    def update_mapping(self, index, mapping):
        elasticsearch.client.IndicesClient(self.client).put_mapping(index=index, body=mapping)

    def delete_index(self, index):
        elasticsearch.client.IndicesClient(self.client).delete(index=index)

    def dump_index(self, index):
        # json dump of index contents
        body = {
            'query': {
                'match_all': {}
            }
        }

        for batch in self._scan_batch(index=index, body=body):
            for item in batch:
                print(json.dumps(item))

    def load_index(self, file_names):
        # import index contents as json
        for file_name in file_names:
            actions = list(map(json.loads, open(file_name).readlines()))

            for action in actions:
                if action['_index'] == 'lists':
                    action['_source']['name'] = action['_id']
                    action['_source']['account'] = 2
                    del action['_source']['id']
                    action['_id'] = action['_id'] + '_2'

                if action['_index'] == 'authors':
                    action['_source']['account'] = 2

            # print('doing actions')
            # for i in range(10):
            #     print(actions[i])

            elasticsearch.helpers.bulk(self.client, actions)

    def update_reply_counts(self, since):
        # slice into hours
        print('Updating reply counts')

        while since < dt.iso8601(dt.utc_now()):
            print(since)

            before = dt.date_math('add', since, 1, 'days')

            # get the number of parent ids for this since-to-before time frame
            query = MUST([RANGE('indexed_at', gte=since, lt=before), NESTED('related', TERM('related.relation', 'reply'))])
            body = {
                'query': query,
                'aggs': {
                    '_': {
                        'nested': {
                            'path': 'related',
                        },
                        'aggs': {
                            '_': {
                                'cardinality': {
                                    'field': 'related.id'
                                }
                            }
                        }
                    }
                },
                'size': 0,
                '_source': False,
            }

            since = before

            response = self.client.search(index='tweets_all', body=body)

            parent_count = response['aggregations']['_']['_']['value']

            # if there are 0 replies, then we have nothing more to do, move to next time frame
            if not parent_count:
                continue

            print('parent count', parent_count)

            processed_ids = []
            while True:
                # get the unique parent ids via a terms aggregation, using the size from before
                body = {
                    'query': MUST(
                        [
                            query,
                            {
                                'nested': {
                                    'path': 'related',
                                    'query': {
                                        'bool': {
                                            'must_not': [
                                                {
                                                    'terms': {
                                                        'related.id': processed_ids
                                                    }
                                                }
                                            ]
                                        }
                                    }
                                }
                            }
                        ]
                    ),
                    'aggs': {
                        '_': {
                            'nested': {
                                'path': 'related',
                            },
                            'aggs': {
                                '_': {
                                    'terms': {
                                        'field': 'related.id',
                                        'size': min(parent_count, 5000)
                                    }
                                }
                            }
                        }
                    },
                    'size': 0,
                    '_source': False,
                }

                response = self.client.search(index='tweets_all', body=body)

                parent_ids = list(bucket['key'] for bucket in response['aggregations']['_']['_']['buckets'])

                if len(parent_ids) == 0:
                    break

                processed_ids.extend(parent_ids)

                print('processing {} ids'.format(len(parent_ids)))

                # get the existing parent ids with a terms query
                body = {
                    'query': {
                        'ids': {
                            'values': list(map(str, parent_ids))
                        }
                    },
                    'aggs': {
                        '_': {
                            'terms': {
                                'field': 'id',
                                'size': len(parent_ids)
                            }
                        }
                    },
                    'size': 0,
                    '_source': False
                }
                response = self.client.search(index='tweets_all', body=body)

                existing_ids = [bucket['key'] for bucket in response['aggregations']['_']['buckets']]

                if len(existing_ids) == 0:
                    break

                # get the reply counts via a nested terms agg
                body = {
                    'query': {
                        'nested': {
                            'path': 'related',
                            'query': {
                                'bool': {
                                    'must': [
                                        {
                                            'terms': {
                                                'related.id': existing_ids
                                            }
                                        },
                                        {
                                            'term': {
                                                'related.relation': 'reply'
                                            }
                                        }
                                    ]
                                }
                            }
                        }
                    },
                    'aggs': {
                        '_': {
                            'nested': {
                                'path': 'related',
                            },
                            'aggs': {
                                '_': {
                                    'terms': {
                                        'field': 'related.id',
                                        'size': len(existing_ids)
                                    }
                                }
                            }
                        }
                    },
                    'size': 0,
                    '_source': False,
                }
                response = self.client.search(index='tweets_all', body=body)

                existing_id_set = set(existing_ids)

                updates = {}
                for bucket in response['aggregations']['_']['_']['buckets']:
                    posting_id = int(bucket['key'])

                    if posting_id not in existing_id_set:
                        continue

                    updates[posting_id] = { 'reply_count': bucket['doc_count'] }

                body = {
                    'query': {
                        'ids': {
                            'values': list(updates.keys())
                        }
                    },
                    'size': len(updates)
                }

                response = self.client.search(index='tweets_all', body=body)

                for hit in response['hits']['hits']:
                    updates[int(hit['_id'])]['_index'] = hit['_index']

                # create update actions only for existing posts
                actions = []
                for _id, update in updates.items():
                    actions.append({'_op_type': 'update', '_id': _id, '_index': update['_index'], '_source': {'doc': {'reply_count': update['reply_count']}}})

                elasticsearch.helpers.bulk(self.client, actions, refresh='wait_for')

    def update_conversations_bottom_up(self, since):
        raise('Needs refactoring to multiple indices')

        while True:
            # get the related-as-reply ids of all posts that were updated since 'since'
            query = MUST([RANGE('conversation_updated_at', gte=since), NESTED('related', TERM('related.relation', 'reply'))])
            body = {
                'query': query,
                '_source': ['id', 'related.id', 'related.relation'],
            }

            parent_ids = []
            for batch in self._scan_batch(index='tweets_all', body=body):
                parent_ids.extend(related['id'] for item in batch for related in item['_source']['related'] if related['relation'] == 'reply')
            parent_ids = list(set(parent_ids))

            print(len(parent_ids))

            # get all the posts with those ids (exclude posts that are referenced, but we don't actually have)
            query = IDS(parent_ids)
            body = {
                'query': query,
                '_source': ['id'],
            }

            parent_ids = []
            for batch in self._scan_batch(index='tweets_all', body=body):
                parent_ids.extend(item['_source']['id'] for item in batch)

            # print('len', len(parent_ids))

            # get all the posts that have those ids as related-as-reply
            query = NESTED('related', MUST([TERMS('related.id', parent_ids), TERM('related.relation', 'reply')]))
            body = {
                '_source': ['descendant_count', 'related.id', 'related.relation'],
                'query': query
            }

            # build a map of parent to list of children by iteration over the children
            parent_map = {}
            for batch in self._scan_batch(index='tweets_all', body=body):
                for item in batch:
                    for related in item['_source']['related']:
                        if related['relation'] != 'reply':
                            continue

                    if related['id'] not in parent_map:
                        parent_map[related['id']] = []

                    parent_map[related['id']].append(item)

            # recalculate descendant counts
            rerun = False
            now = dt.iso8601(dt.utc_now())
            actions = []
            for parent_id in parent_map.keys():
                rerun = True
                update = {
                    'conversation_updated_at': now,
                    'descendant_count': sum(item['_source']['descendant_count'] for item in parent_map[parent_id]) + len(parent_map[parent_id]),
                    'reply_count': len(parent_map[parent_id])
                }
                actions.append({'_op_type': 'update', '_id': str(parent_id), '_index': 'tweets', '_type': 'tweet', '_source': {'doc': update}})

            elasticsearch.helpers.bulk(self.client, actions, refresh='wait_for')

            since = now

            if not rerun:
                break

    def update_conversations_top_down(self, since):
        while True:
            query = RANGE('conversation_updated_at', gte=since)
            body = {
                '_source': ['id', 'conversation_id', 'text', 'content'],
                'query': query
            }

            now = dt.iso8601(dt.utc_now())
            rerun = False
            for parent_batch in self._scan_batch(index='tweets_all', body=body):
                print(len(parent_batch))

                # if (len(parent_batch) == 1):
                #     print(parent_batch)

                # construct dictionary id to document for the parents, so we can fetch the conversation ids
                parent_dict = dict((item['_source']['id'], item['_source']) for item in parent_batch)

                # find children of the parents
                query = NESTED('related', MUST([TERMS('related.id', list(parent_dict.keys())), TERM('related.relation', 'reply')]))
                body = {
                    '_source': ['id', 'conversation_id', 'related.id', 'related.relation'],
                    'query': query
                }

                actions = []
                for child_batch in self._scan_batch(index='tweets_all', body=body):
                    rerun = True

                    for item in child_batch:
                        for related in item['_source']['related']:
                            if related['relation'] != 'reply':
                                continue

                            related['text'] = parent_dict[related['id']].get('text')
                            related['content'] = parent_dict[related['id']].get('content')

                            for key in ['text', 'content']:
                                if not related.get(key):
                                    del related[key]

                            update = {
                                'conversation_updated_at': now,
                                'conversation_id': sorted(set([item['_source']['id'], related['id']] + parent_dict[related['id']]['conversation_id'])),
                                'related': item['_source']['related'],
                            }

                            actions.append({'_op_type': 'update', '_id': item['_id'], '_index': 'tweets', '_source': {'doc': update}})

                elasticsearch.helpers.bulk(self.client, actions, refresh='wait_for')

            since = now

            if not rerun:
                break

    def ingest(self, entries):
        # analyze current contents
        new_entries, update_entries, ignore_entries, existing_entries = self._filter_entries(entries)

        batch_size = 10000

        # index the new entries, plain and simple
        print('Creating {} new entries'.format(len(new_entries)))
        if new_entries:
            for batch in iter.batchify(new_entries.items(), batch_size):
                print('Indexing Batch of {}'.format(len(batch)))
                self.index(batch)

        # determine how to update ES from the current and existing items
        print('Updating {} entries'.format(len(update_entries)))
        if update_entries:
            for batch in iter.batchify(update_entries.items(), batch_size):
                print('Updating batch of {}'.format(len(batch)))

                updates = []
                for (id, index), item in batch:
                    update = self._make_update(self._get_document(item), existing_entries.get((id, index)))

                    if update:
                        updates.append(((id, index), update))

                print('Sending {} updates'.format(len(updates)))

                self.update(updates)

        print('Ignoring {} entries'.format(len(ignore_entries)))
        print('Found {} entries'.format(len(existing_entries)))

    def index(self, entries):
        # get new documents into ES
        actions = []
        for (id, index), entry in entries:
            try:
                source = self._get_document(entry)

                if source:
                    actions.append({
                        '_index': index,
                        '_id': str(id),
                        '_source': source
                    })

            except Exception as exception:
                print('Could not insert entry {} due to {}'.format(entry, exception))

                raise

        elasticsearch.helpers.bulk(self.client, actions, refresh='wait_for')

    def update(self, entries):
        # update documents already existing in ES
        actions = []
        for (id, index), update in entries:
            try:
                actions.append({
                    '_op_type': 'update',
                    '_index': index,
                    '_id': id,
                    'doc': update,
                })
            except Exception as exception:
                print('Could not update entry {} due to {}'.format(update, exception))

                raise

        elasticsearch.helpers.bulk(self.client, actions, refresh='wait_for')

    def delete_by_query(self, index, body):
        # useful to have around,
        for batch in self._scan_batch(index=index, body=body):
            actions = [{'_op_type': 'delete', '_index': index, '_id': item['_id']} for item in batch]

            elasticsearch.helpers.bulk(self.client, actions)

    def msearch(self, query_bodies):
        # search on ES, plain and simple
        body = '\n'.join(map(json.dumps, query_bodies))

        print('msearch body', body)

        with EventTime('es.time', index='tweets', api='msearch'):
            response = self.client.msearch(body=body, index='tweets_all')

        return self._summarize(response)

    def terms_aggregation(self, field, mode, query):
        body = {
            'query': query,
            'aggregations': {
                'popularity': {
                },
                'cardinality': {
                    'cardinality': {
                        'field': field
                    }
                }
            },
            'size': 0
        }

        if mode == 'count':
            body['aggregations']['popularity']['terms'] = {
                'field': field,
                'size': 1000,
            }
        elif mode == 'followers':
            body['aggregations']['popularity']['terms'] = {
                'field': field,
                'size': 1000,
                'order': {
                    'followers_count': 'desc'
                }
            }

            body['aggregations']['popularity']['aggregations'] = {
                'followers_count': {
                    'max': {
                        'field': 'followers_count'
                    }
                }
            }



        # print('sigterm body', json.dumps(body))

        with EventTime('es.time', index='tweets', api='terms_aggregation'):
            response = self.client.search(body=body, index='tweets_all')

        result = self._summarize(response)

        return result

    def nested_terms_aggregation(self, path, field, query):
        body = {
            'query': query,
            'aggregations': {
                'popularity': {
                    'nested': {
                        'path': path
                    },
                    'aggregations': {
                        'popularity': {
                            'terms': {
                                'field': '{}.{}'.format(path, field),
                                'size': 100
                            }
                        },
                        'cardinality': {
                            'cardinality': {
                                'field': '{}.{}'.format(path, field)
                            }
                        }
                    }
                }
            },
            'size': 0
        }

        # print('sigterm body', json.dumps(body))

        with EventTime('es.time', index='tweets', api='nested_terms_aggregation'):
            response = self.client.search(body=body, index='tweets_all')

        result = self._summarize(response)

        return result

    def sigterms(self, foreground, background):
        # do a sigterms aggregation
        body = {
            'query': foreground,
            'aggregations': {
                'popularity': {
                    'significant_terms': {
                        'field': 'keywords',
                        'min_doc_count': 1,
                        'background_filter': background,
                        'size': 100,
                        # 'mutual_information': {
                        #     'include_negatives': True
                        # }
                        # 'jlh': {}
                        'gnd': {
                            "background_is_superset": False
                        }
                        # 'chi_square': {}
                    }
                }
            },
            'size': 0
        }

        print('sigterm FOREGROUND', json.dumps(foreground))
        print('sigterm BACKGROUND', json.dumps(background))

        with EventTime('es.time', index='tweets', api='sigterms'):
            response = self.client.search(body=body, index='tweets_all', request_timeout=30)

        result = self._summarize(response)

        # body = {
        #     'query': foreground,
        #     'highlight': {
        #         'fields': {
        #             'text': {}
        #         }
        #     },
        #     'size': 100
        # }
        #
        # # remove words that match the foreground query, because obviously, they'll be there
        # response = self.client.search(body=body, index='tweets')
        #
        # highlighted_terms = set(iter.flatten(re.findall("(?<=\<em\>)\w+(?=\</em\>)", hit.get('highlight', {'text': ['']})['text'][0].lower()) for hit in response['hits']['hits']))
        #
        # result['popularity'] = [item for item in result['popularity'] if item['key'] not in highlighted_terms]

        return result

    def get_lists_summary(self, account_id):
        body = {
            'query': {
                'term': {
                    'account': account_id
                }
            },
            'size': 0
        }

        response = self.client.search(index='lists', body=body)

        size = response['hits']['total']['value']
        if size == 0:
            return []

        body['aggregations'] = {
            '_': {
                'terms': {
                    'field': 'name',
                    'size': size
                },
                'aggregations': {
                    '_': {
                        'cardinality': {
                            'field': 'author'
                        }
                    }
                }
            }
        }

        print(json.dumps(body))

        response = self.client.search(index='lists', body=body)

        lists = [{'key': bucket['key'], 'count': bucket['_']['value']} for bucket in response['aggregations']['_']['buckets']]

        return lists

    def get_lists_for_account(self, account_id):
        # return a bunch of lists
        body = {
            'query': {
                'bool': {
                    'must': [
                        {
                            'term': {
                                'account': account_id
                            }
                        }
                    ]
                },
            },
            'size': MAX_LISTS,
        }

        response = self.client.search(index='lists', body=body)

        result = dict((hit['_source']['name'], hit['_source']) for hit in response['hits']['hits'])

        return result

    def get_lists(self, account_id, query=None):
        # return a bunch of lists

        fields = ['name']
        body = {
            'query': {
                'bool': {
                    'must': [
                        {
                            'term': {
                                'account': account_id
                            }
                        }
                    ]
                },
            },
            'size': MAX_LISTS,
            '_source': fields
        }

        if query:
            if query.get('prefix'):
                body['query']['bool']['must'].append(
                    {
                        'prefix': {
                            'name': query
                        }
                    }
                )
            elif query.get('terms'):
                body['query']['bool']['must'].append(
                    {
                        'terms': {
                            'name': query.get('terms')
                        }
                    }
                )
                fields.append('author')

        response = self.client.search(index='lists', body=body)

        result = []
        for hit in response['hits']['hits']:
            result.append(dict((field, hit['_source'][field]) for field in fields))

        return result

    def get_all(self):
        # return all documents in this in this index
        body = {
            'query': {
                'match_all': {}
            }
        }

        items = []
        for batch in self._scan_batch(body=body):
            items.extend(batch)

        return items

    def get_list_data_for_authors(self, account_id, authors):
        body = {
            'query': {
                'bool': {
                    'must': [
                        {
                            'terms': {
                                'author': authors
                             },
                        },
                        {
                            'term': {
                                'account': account_id
                            }
                        }
                    ]
                }
            },
            'size': MAX_LISTS,
            '_source': ['author', 'name']
        }

        response = self.client.search(index='lists', body=body)

        list_data = dict((list['_source']['name'], iter.as_list(list['_source']['author'])) for list in response['hits']['hits'])

        return list_data

    def get_list_authors(self, account_id, lists):
        # return the set of authors that are all in the specified lists
        body = {
            'query': {
                'bool': {
                    'must': [
                        {
                            'terms': {
                                'name': lists
                             },
                        },
                        {
                            'term': {
                                'account': account_id
                            }
                        }
                    ]
                }
            },
            'size': len(lists),
            '_source': ['author', 'name']
        }

        response = self.client.search(index='lists', body=body)

        list_data = dict((hit['_source']['name'], iter.as_list(hit['_source']['author'])) for hit in response['hits']['hits'])

        return list_data

    def get_authors_in_lists(self, account_id, lists):
        list_data = self.get_list_authors(account_id, lists)

        authors = list(set(author for authors in list_data.values() for author in authors))

        author_data = self.get_author_data(account_id, authors)

        # print(author_data)

        return author_data

    def match_authors(self, account_id, query=None):
        # find which authors from this account match the query
        body = {
            'query': {
                'bool': {
                    'must': [
                        {
                            'term': {
                                'account': account_id
                            }
                        }
                    ]
                }
            },
            'size': 10,
            '_source': ['screen_name']
        }

        if query:
            body['query']['must'].append({
                'bool': {
                    'should': [
                        {'wildcard': {'name': '*{}*'.format(query)}},
                        {'wildcard': {'screen_name': '*{}*'.format(query)}},
                    ]
                }
            })

        # print(json.dumps(body))

        response = self.client.search(index='authors', body=body)

        result = [hit['_source']['screen_name'] for hit in response['hits']['hits']]

        return result

    def get_valid_authors_map(self, authors):
        if not authors:
            return {}

        body = {
            'query': {
                'bool': {
                    'must': [
                        {
                            'ids': {
                                'values': authors
                            }
                        },
                        {
                            'term': {
                                'latest': True
                            }
                        },
                        {
                            'range': {
                                'indexed_at': {
                                    'gt': dt.iso8601(dt.utc_now())
                                }
                            }
                        }
                    ]
                }
            },
            'size': len(authors),
            '_source': ['followers_count'],
        }

        response = self.client.search(index='authors', body=body)

        return dict((hit['_id'], hit['_source']['followers_count']) for hit in response['hits']['hits'])

    def get_author_data(self, account_id, authors=None):
        # get authors from ES and the lists they're in
        body = {
            'query': {
                'bool': {
                    'must': [
                        {
                            'term': {
                                'account': account_id
                            }
                        }
                    ]
                }
            },
        }

        if authors is not None:
            body['query']['bool']['must'].append(
                {
                    'terms': {
                        'screen_name': authors
                    }
                },
            )

        author_data = {}
        keys = ['name', 'screen_name', 'followers_count', 'profile_image_url', 'id']
        for batch in self._scan_batch('authors', body):
            for author in batch:
                author_data[author['_source']['screen_name']] = {key: author['_source'][key] for key in keys}

        body = {
            'query': {
                'bool': {
                    'must': [
                        {
                            'terms': {
                                'author': list(author_data.keys())
                            }
                        },
                        {
                            'term': {
                                'account': account_id
                            }
                        }
                    ]
                }
            },
            'size': MAX_LISTS
        }

        response = self.client.search(index='lists', body=body)

        for author in author_data:
            author_data[author]['lists'] = []

        for list_ in response['hits']['hits']:
            list_ = list_['_source']
            for author in iter.as_list(list_['author']):
                if author in author_data:
                    author_data[author]['lists'].append(list_['name'])

        return author_data

    def update_lists(self, account_id, authors, common_lists, other_lists):
        author_set = set(authors)
        common_lists_set = set(common_lists)
        other_lists_set = set(other_lists)

        # get existing, new and all list->authors maps
        lists_for_authors = self.get_list_data_for_authors(account_id, authors)
        lists_for_common_lists = self.get_list_authors(account_id, [list for list in common_lists_set if list not in lists_for_authors])

        existing_lists = {**lists_for_authors, **lists_for_common_lists}
        for list_name in existing_lists:
            existing_lists[list_name] = set(existing_lists[list_name])

        new_lists = dict((list_name, set()) for list_name in common_lists_set - existing_lists.keys())

        all_lists = {**existing_lists, **new_lists}

        # invert to get author->lists map
        author_data = {}
        for list_name in all_lists:
            for author in all_lists[list_name]:
                if author not in author_set:
                    continue

                if author not in author_data:
                    author_data[author] = set()

                author_data[author].add(list_name)

        # initialize for authors that are not in any list yet
        for author in author_set:
            if author not in author_data:
                author_data[author] = set()

        # determine which lists each author is added to or removed from
        changed_lists = set()
        changed_authors = set()
        for author in author_set:
            # authors are only added to a list via the common_lists parameter, so an author is added to
            # a list if it is not yet in that list, but the list is mentioned in the common lists parameter
            add_to_lists = common_lists_set - author_data[author]

            # authors are removed from a list if the are currently in it, but the list is not mentioned
            # in both common and other lists
            remove_from_lists = author_data[author] - common_lists_set - other_lists_set

            if add_to_lists or remove_from_lists:
                changed_authors.add(author)
            else:
                continue

            changed_lists |= add_to_lists | remove_from_lists

            for list_name in add_to_lists:
                all_lists[list_name].add(author)

            for list_name in remove_from_lists:
                all_lists[list_name].remove(author)

        now = dt.iso8601(dt.utc_now())

        actions = []
        for list_name in new_lists:
            list_id = '{}_{}'.format(list_name, account_id)
            body = {
                'account': account_id,
                'name': list_name,
                'author': list(new_lists[list_name]),
                'updated_at': now,
            }
            actions.append({'_index': 'lists', '_type': 'list', '_id': list_id, '_source': body})

        for list_name in existing_lists:
            authors = list(existing_lists[list_name])
            list_id = '{}_{}'.format(list_name, account_id)
            if authors:
                actions.append({'_op_type': 'update', '_id': list_id, '_index': 'lists', '_type': 'list',
                                '_source': {'doc': {'author': authors, 'updated_at': now}}})
            else:
                actions.append({'_op_type': 'delete', '_id': list_id, '_index': 'lists', '_type': 'list'})

        elasticsearch.helpers.bulk(self.client, actions, refresh='wait_for')

        return list(changed_authors), list(changed_lists)

    def _summarize(self, response, hint=None):
        # summarize ES response
        if 'responses' in response:
            summary = list(self._summarize(response) for response in response['responses'])
        elif 'buckets' in response:
            summary = list(self._summarize(bucket) for bucket in response['buckets'])
        elif 'key_as_string' in response:
            if 'metric' in response:
                summary = {'key': response['key_as_string'], 'count': int(response['metric']['sum'])}
            else:
                summary = {'key': response['key_as_string'], 'count': response['doc_count']}
        elif 'doc_count' in response and 'bg_count' in response:
            summary = {'key': response['key'], 'score': response['score'], 'doc_count': response['doc_count']}
        elif 'key' in response:
            if 'followers_count' in response:
                if response['followers_count']['value'] is None:
                    response['followers_count'] = None
                else:
                    response['followers_count'] = int(response['followers_count']['value'])
                response['value_key'] = 'followers_count'
            summary = response
        elif 'value' in response:
            return response['value']
        else:
            summary = {}
            if 'hits' in response:
                summary['total'] = response['hits']['total']['value']
                summary['postings'] = list(self._summarize(hit) for hit in response['hits']['hits'])
                summary['postings'] = list(map(self._enrich, summary['postings']))

                for posting in summary['postings']:
                    posting['id'] = str(posting['id'])
                    posting['created_at'] = posting['created_at'][:-2] + ':' + posting['created_at'][-2:]

            if 'aggregations' in response:
                for key, value in response['aggregations'].items():
                    if key == 'by_day':
                        summary['volume'] = self._summarize(value)
                        
                        # what we consider to be 'total' is the sum of what we consider to be the 
                        # individual bucket counts. This is different for simple posting counts, and, say, likes.
                        summary['total'] = sum(bucket['count'] for bucket in summary['volume'])

                    elif key == 'popularity':
                        if 'popularity' in value:
                            summary['popularity'] = self._summarize(value['popularity'])
                        if 'cardinality' in value:
                            summary['cardinality'] = self._summarize(value['cardinality'])
                        if 'buckets' in value:
                            summary['popularity'] = self._summarize(value)

                    elif key == 'cardinality':
                        summary['cardinality'] = self._summarize(value)

                    elif key == 'metric':
                        summary['total'] = int(value['sum'])

            if re.match(r'tweets_\d{4,}', response.get('_index', '')):
                summary = response['_source']

        return summary

    def _enrich(self, posting):
        # add some context information to a posting summary
        if posting['source'] == 'twitter':
            posting['author_name'] = '@{}'.format(posting['author'])
            posting['author_url'] = 'https://www.twitter.com/{}'.format(posting['author'])

            if posting.get('related'):
                for related in posting['related']:
                    if related['relation'] in ['share', 'quote']:
                        if 'author' not in related:
                            continue

                        related['author_name'] = '@{}'.format(related['author'])
                        related['author_url'] = 'https://www.twitter.com/{}'.format(related['author'])

        elif posting['source'] == 'www.telegraaf.nl':
            posting['author_name'] = 'Telegraaf'
            posting['author_url'] = 'https://www.telegraaf.nl'

        return posting
