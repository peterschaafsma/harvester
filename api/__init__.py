from api.es import Elasticsearch
from api.twitter import TwitterApi

from es.twitter import TwitterElasticsearch
from es.authors import AuthorsElasticsearch
from es.rss import RSSElasticsearch

TWITTER_API = TwitterApi()
ES = Elasticsearch()
TWITTER_ES = TwitterElasticsearch()
AUTHORS_ES = AuthorsElasticsearch()
RSS_ES = RSSElasticsearch()

#TWITTER_API = None
#ES = None
#TWITTER_ES = None
#AUTHORS_ES = None
#RSS_ES = None
