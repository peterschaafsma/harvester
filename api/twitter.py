import json
import oauth2
import urllib.parse
import urllib.request

from feeds.models import TwitterCredentials
from utils.base import Object
from utils.log import print

"""
Get bearer token:
curl -u "$CONSUMER_KEY:$CONSUMER_SECRET" --compressed --data 'grant_type=client_credentials' https://api.twitter.com/oauth2/token/

Query firehose:
curl -X GET 'https://api.twitter.com/1.1/search/tweets.json?q=to:geertwilderspvv&lang=nl&count=5&result_type=recent' -H 'Authorization: Bearer AAAAAAAAAAAAAAAAAAAAADOa8QAAAAAAJFmp2%2BRt94B7LwkA3hCEhrJNrik%3Dlg6D0G9k2kDEM2lARDG2sbbYUTfxy3eyKwIZUecHeuSg5RxZrI'
"""

"""
Application authorization

import oauth2 as oauth
import urllib.parse

oauth_callback='http://localhost:8000/api/twitter/callback/'

oauth_consumer_key='cIBOInjUYT6CzGBj78i9zoRp9'
oauth_consumer_secret='p0jHDw9hEEmgfsOQhB0qqkHNj33zNNlnplhwzoWgPP0Hm0w4fG'

# access_token='1028563435100024832-k6QE5zgrnZYkLyxxbMJDyMa7WScXYW'
# access_token_secret='oAAlCW9QvPgkNQQuZMHMWnhzKqeZfTGY8JYqDCUJ0cDW7'

consumer = oauth.Consumer(oauth_consumer_key, oauth_consumer_secret)
# token = oauth.Token(access_token, access_token_secret)
client = oauth.Client(consumer)

request_token_url = 'https://api.twitter.com/oauth/request_token'
access_token_url = 'https://api.twitter.com/oauth/access_token'
authorize_url = 'https://api.twitter.com/oauth/authorize'


# to get redirect URL
# body = {'oauth_callback': oauth_callback}
# response = client.request(request_token_url, 'POST', urllib.parse.urlencode(body))
#
# oauth_token = dict(urllib.parse.parse_qsl(response[1].decode()))['oauth_token']
# oauth_token_secret = dict(urllib.parse.parse_qsl(response[1].decode()))['oauth_token_secret']
#
# print('https://api.twitter.com/oauth/authorize?oauth_token=' + oauth_token)


# to obtain user_oauth_token and secret
oauth_token = 'pb55XAAAAAAA8ZozAAABaJt05Js'
oauth_verifier='QorKJEFLy3epE8UfCVdZEsbFHiSNerFC'

body = {'oauth_verifier': oauth_verifier, 'oauth_consumer_key': oauth_consumer_key, 'oauth_token': oauth_token}

response = client.request(access_token_url, 'POST', urllib.parse.urlencode(body))
oauth_token = dict(urllib.parse.parse_qsl(response[1].decode()))['oauth_token']
oauth_token_secret = dict(urllib.parse.parse_qsl(response[1].decode()))['oauth_token_secret']

print('oauth_token', oauth_token)
print('oauth_token_secret', oauth_token_secret)
"""

class TwitterApi:
    SCHEMA = 'https'
    BASE_URL = 'api.twitter.com'
    VERSION = '1.1'

    OAUTH_CALLBACK_PATH = '/api/twitter/callback/'
    OAUTH_CONSUMER_KEY = 'cIBOInjUYT6CzGBj78i9zoRp9'
    OAUTH_CONSUMER_SECRET = 'p0jHDw9hEEmgfsOQhB0qqkHNj33zNNlnplhwzoWgPP0Hm0w4fG'

    REQUEST_TOKEN_URL = 'https://api.twitter.com/oauth/request_token'
    AUTHORIZE_URL = 'https://api.twitter.com/oauth/authorize'
    ACCESS_TOKEN_URL = 'https://api.twitter.com/oauth/access_token'

    def __init__(self, credentials=None, type='app'):
        if credentials is None:
            credentials = TwitterCredentials.objects.filter(type=type).first()

        self.credentials=credentials

        self._consumer = None
        self._bearer = None
        self._token = None
        self._app_client = None
        self._user_client = None

    @property
    def consumer(self):
        if not self._consumer:
            key = self.credentials.api_key
            secret = self.credentials.api_key_secret

            self._consumer = oauth2.Consumer(key=key, secret=secret)

        return self._consumer

    @property
    def token(self):
        if not self._token:
            token = self.credentials.token
            secret = self.credentials.secret

            self._token = oauth2.Token(key=token, secret=secret)

        return self._token

    @property
    def app_client(self):
        if not self._app_client:
            self._app_client = oauth2.Client(self.consumer)

        return self._app_client

    @property
    def user_client(self):
        if not self._user_client:
            self._user_client = oauth2.Client(self.consumer, self.token)

        return self._user_client

    def _oauth2_request(self, url, method='GET', post_body='', http_headers=None):
        if self.credentials.type == 'app':
            client = self.app_client
        elif self.credentials.type == 'user':
            client = self.user_client

        response, content = client.request(url, method=method, body=post_body.encode(), headers=http_headers)

        return response, json.loads(content.decode())

    def get(self, path, **parameters):
        query_string = urllib.parse.urlencode(parameters)

        url = '{}://{}/{}/{}?{}'.format(self.SCHEMA, self.BASE_URL, self.VERSION, path, query_string)

        response, data = self._oauth2_request(url)

        if path == 'search/tweets.json':
            data = data.get('statuses', [])

        return Object(response=response, data=data)

    def get_authorize_url(self, host_and_port):
        body = {
            'oauth_callback': 'http://{}{}'.format(host_and_port, self.OAUTH_CALLBACK_PATH)
        }

        print('body', body)

        response = self.app_client.request(self.REQUEST_TOKEN_URL, 'POST', urllib.parse.urlencode(body))

        response_data = dict(urllib.parse.parse_qsl(response[1].decode()))

        print("RDATA", response_data)

        url = 'https://api.twitter.com/oauth/authorize?oauth_token=' + response_data['oauth_token']

        return url

    def connect_account(self, oauth_token, oauth_verifier):
        body = {
            'oauth_token': oauth_token,
            'oauth_verifier': oauth_verifier,
            'oauth_consumer_key': self.OAUTH_CONSUMER_KEY,
        }

        response = self.app_client.request(self.ACCESS_TOKEN_URL, 'POST', urllib.parse.urlencode(body))

        response_data = dict(urllib.parse.parse_qsl(response[1].decode()))

        return response_data