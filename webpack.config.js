const path = require('path');

module.exports = {
  resolve: {
    alias: {
      root: path.resolve(__dirname, 'frontend/src/')
    }
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.s?css$/,
        use: ["style-loader", "css-loader", "sass-loader"]
      },
    ],
  }
};
