from cryptography.fernet import Fernet
from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group, User
from django.core.mail import send_mail
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect, HttpResponseBadRequest
from django.shortcuts import redirect, render
from django.urls import reverse
from feeds.models import Account, AccountAccess, UserRegistration, AccountProjects
from utils.dt import utc_now

import datetime
import json
import random



def account_view(request, page):
    if request.method == 'GET':
        if page == 'logout':
            if request.user:
                logout(request)

                return redirect(reverse('account', kwargs={'page': 'login'}))
            else:
                pass

        elif page == 'confirm':
            user_registration = UserRegistration.objects.filter(status='pending', validation_key=request.GET['validation_key']).first()

            if not user_registration or user_registration.expiry_date < utc_now():
                return redirect(reverse('account', kwargs={'page': 'register'}) + '?alert=no_pending_confirmation')

            else:
                user_registration.status = 'confirmed'
                user_registration.save()

                cipher = Fernet(settings.FERNET_KEY)

                user = User.objects.create_user(
                    username=user_registration.username,
                    email=user_registration.email_address,
                    password=cipher.decrypt(user_registration.password.encode('utf-8')).decode('utf-8')
                )

                group = Group.objects.get(name='type_a')
                group.user_set.add(user)

                account = Account()
                account.name = 'Account of {}'.format(user.username)
                account.expires_on = utc_now() + datetime.timedelta(days=30)
                account.enabled = True
                account.save()

                account_access = AccountAccess()
                account_access.account = account
                account_access.user = user
                account_access.save()

                return redirect(reverse('account', kwargs={'page': 'login'}))

        else:
            context = {
                'user': request.user,
                'page': page
            }

            return render(request, 'frontend/account.html', context)

    elif request.method == 'POST':
        if page == 'login':
            body = json.loads(request.body.decode())

            user = authenticate(username=body['username'], password=body['password'])

            if user:
                login(request, user)

                return JsonResponse({'alerts': [], 'logged_in': True, 'redirect': reverse('application', kwargs={'page': 'overview'})})

            else:
                errors = [{'type': 'warning', 'message': 'incorrect credentials'}]

                return JsonResponse({'alerts': errors, 'logged_in': False})

        elif page == 'register':
            body = json.loads(request.body.decode())

            if User.objects.filter(username=body['username']).first() or UserRegistration.objects.filter(username=body['username'], expiry_date__gt=utc_now()).first():
                alert = {'type': 'warning', 'message': 'Username "{}" is already taken'.format(body['username'])}

                return JsonResponse({'alerts': [alert]})


            elif UserRegistration.objects.filter(email_address=body['email_address'], expiry_date__gt=utc_now()).count() >= 5:
                alert = {'type': 'warning', 'message': 'Email address "{}" is already in use at least 5 times'.format(body['email_address'])}

                return JsonResponse({'alerts': [alert]})

            else:
                validation_key = ''.join(random.choice('abcdefghijklmnopqrstuvwxyz') for _ in range(32))
                expiry_date = utc_now() + datetime.timedelta(hours=1)

                cipher = Fernet(settings.FERNET_KEY)

                user_registration = UserRegistration()
                user_registration.username = body['username']
                user_registration.password = cipher.encrypt(body['password'].encode('utf-8')).decode('utf-8')
                user_registration.email_address = body['email_address']
                user_registration.validation_key = validation_key
                user_registration.expiry_date = expiry_date
                user_registration.status = 'pending'
                user_registration.save()

                url = 'http://explore.mediagraphs.com/confirm/?validation_key={}'.format(validation_key)
                message_lines = [
                    'Click here to confirm your registration: {}'.format(url),
                ]

                send_mail('Registration', '\n'.join(message_lines), settings.EMAIL_HOST_USER, [body['email_address']], fail_silently=False)

                alert = {'type': 'info', 'message': 'A confirmation email was sent to {}'.format(body['email_address'])}

                return JsonResponse({'alerts': [alert]})



def contact_view(request):
    context = {
        'application': 'contact'
    }

    return render(request, 'frontend/application.html', context)

def publish_view(request, uuid=None, name=None):
    context = {
        'user': None,
    }

    return render(request, 'frontend/application.html', context)

@login_required
def application_view(request, page):
    context = {
        'user': request.user,
        'application': page
    }

    if page == 'project':
        if request.path == reverse('application', kwargs={'page': page}):
            account_access = AccountAccess.objects.get(user=request.user)
            project = account_access.last_project
            if project is None:
                project = AccountProjects.objects.filter(account=account_access.account).order_by('row_created_on').last()

            if project:
                return HttpResponseRedirect(reverse('project', kwargs={'project_id': project.id}))
            else:
                return HttpResponseRedirect(reverse('application', kwargs={'page': 'overview'}))

    return render(request, 'frontend/application.html', context)
