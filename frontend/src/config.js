const host = window.location.host;

const config = {
    chart: {
        m: {  // margin
            b: 50,  // bottom
            l: 74,
            r: 24,
            t: 30,
        },
        v: {
            w: 1140,
            h: 550,
        },
    },
    expanded: {
        chart: {
            m: {  // margin
                b: 50,  // bottom
                l: 74,
                r: 24,
                t: 30,
            },
            v: {
                w: 2280,
                h: 1100,
            }
        }
    },

    api: `${host}/api`,
    host: host,
}

const stringsAll = {
    en: {
        'save': 'Save',
    },
    nl: {
    },
}

const strings = stringsAll[window.lang];

function S(key) {
    if (strings[key]) {
        return strings[key];
    }
    else {
        return key;
    }
}

export { config, S };
