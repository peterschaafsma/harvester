import * as moment from "moment";
import * as d3timeformat from "d3-time-format";

/*
    x select date range
    x expand boundaries to current aggregation interval boundaries (this automatically aligns it to all lower levels)
    x choose default aggregation interval based on date range length
    x (change aggregation interval)
    x get data
    x get tick interval from date range, aggregationest interval that requires <= Max ticks
    x find all aggregation ticks that are the start of a tick interval
    - format ticks according to the aggregation interval
*/

const minIntervals = 16;
const minTicks = 3;
const keyParse = d3timeformat.timeParse("%Y-%m-%dT%H:%M:%S.000000%Z");

export const intervalObjects = [
    { key: "10_year", aggregation: "year", math: [10, "year"], value: "10 years",seconds: 10*365*86400, format: "%Y" },
    { key: "year", aggregation: "year", math: [1, "year"], value: "year",seconds: 365*86400, format: "%Y" },
    { key: "quarter", value: "quarter", math: [3, "month"], aggregation: "quarter", seconds: 91*86400, format: "%b '%y" },  // should actually try to do Q1 etc.
    { key: "month", aggregation: "month", math: [1, "month"], value: "month",seconds: 30*86400, format: "%b '%y" },
    { key: "2_weeks", value: "2 weeks", math: [14, "day"], aggregation: "14d", seconds: 14*86400, format: "%-d/%-m" },
    { key: "week", value: "week", math: [7, "day"], aggregation: "week", seconds: 7*86400, format: "%-d/%-m" },
    { key: "4_days", value: "4 days", math: [4, "day"], aggregation: "4d", seconds: 4*86400, format: "%-d/%-m" },
    { key: "2_days", value: "2 days", math: [2, "day"], aggregation: "2d", seconds: 2*86400, format: "%-d/%-m" },
    { key: "day", aggregation: "day", math: [1, "day"], value: "day", seconds: 86400, format: "%-d/%-m" },
    { key: "12_hours", value: "12 hours", math: [12, "hour"], aggregation: "12h", seconds: 12*3600, format: "%-d/%-m %-H:%M" },
    { key: "6_hours", value: "6 hours", math: [6, "hour"], aggregation: "6h", seconds: 6*3600, format: "%-d/%-m %-H:%M" },
    { key: "2_hours", value: "2 hours", math: [2, "hour"], aggregation: "2h", seconds: 2*3600, format: "%-d/%-m %-H:%M" },
    { key: "hour", aggregation: "hour", math: [1, "hour"], value: "hour", seconds: 3600, format: "%-H:%M" },
    { key: "30_minutes", aggregation: "30_minutes", value: "30 minutes", math: [30, "minute"], aggregation: "30m", seconds: 1800, format: "%-H:%M" },
    { key: "15_minutes", value: "15 minutes", math: [15, "minute"], aggregation: "15m", seconds: 900, format: "%-H:%M" },
    { key: "5_minutes", value: "5 minutes", math: [5, "minute"], aggregation: "5m", seconds: 300, format: "%-H:%M" },
    { key: "minute", aggregation: "minute", math: [1, "minute"], value: "minute",seconds: 60, format: "%-H:%M" }
];

export const presets = [
    { key: "last_hour", value: "Last hour" },
    { key: "today", value: "Today" },
    { key: "last_24_h", value: "Last 24h" },
    { key: "last_full_day", value: "Yesterday" },
    { key: "past_week", value: "Past week" },
    { key: "last_full_week", value: "Last week" },
    { key: "past_month", value: "Past month" },
    { key: "last_full_month", value: "Last month" },
    { key: "past_quarter", value: "Past quarter" },
    { key: "last_full_quarter", value: "Last quarter" },
    { key: "past_year", value: "Past year" },
    { key: "last_full_year", value: "Last year" },
    { key: "past_5_years", value: "Past 5 years" },
    { key: "all_time", value: "All time" },
];

export let intervalObjectMap = {};
intervalObjects.forEach(intervalObject => {
    intervalObjectMap[intervalObject.key] = intervalObject;
});

export function makeHumanReadable(range) {
    let from = "";
    let to = "";

    const units = ["year", "month", "date"];

    const get = (mom, unit) => {
        const index = mom.get(unit);
        if (unit == "month") {
            return moment(index+1, 'M').format('MMM');
        }
        else if (unit == "year") {
            return "'" + moment(index, 'YYYY').format('YY');
        }
        else {
            return index;
        }
    }

    let crossesUnit = false;
    for (let i=0; i<units.length; i++) {
        const unit = units[i];
        if (range.start.get(unit) == range.end.get(unit)) {
            if (crossesUnit) {
                from = get(range.start, unit) + " " + from;
                to = get(range.end, unit) + " " + to;
                break;
            }
            else {
                to = get(range.end, unit) + " " + to;
            }
        }
        else {
            if (crossesUnit) {
                from = get(range.start, unit) + " " + from;
                to = get(range.end, unit) + " " + to;
                break;
            }
            else {
                if (range.end.get(unit) - range.start.get(unit) == 1) {
                    crossesUnit = true;
                    from = get(range.start, unit) + " " + from;
                    to = get(range.end, unit) + " " + to;
                }
                else {
                    from = get(range.start, unit) + " " + from;
                    to = get(range.end, unit) + " " + to;
                    break;
                }
            }
        }
    }

    if (from.length == 0) {
        return to;
    }
    else {
        return `${from} - ${to}`;
    }
}

export function getIntervalObject(interval) {
    return intervalObjectMap[interval];
}

function isStartOfInterval(key, intervalObject) {
    const keyDate = moment(key);

    let startOfInterval;

    if (intervalObject.math) {
        let unit = intervalObject.math[1];
        let units;
        if (unit == "day") {
            startOfInterval = moment(key).startOf("day");
            units = Math.floor(moment.duration(startOfInterval.diff(moment("1970-01-01T23:00:00" + key.substr(-5)))).asDays());
        }
        else {
            startOfInterval = moment(key).startOf(unit);
            units = startOfInterval.get(unit);
        }

        startOfInterval.subtract(units % intervalObject.math[0], unit);

//        console.log("startOfInterval", startOfInterval, keyDate, startOfInterval.valueOf() - keyDate.valueOf());
//        console.log("isStartOfInterval", key, JSON.stringify(keyDate), JSON.stringify(startOfInterval), keyDate.valueOf() == startOfInterval.valueOf());
    }
    else {
        startOfInterval = moment(key).startOf(intervalObject.aggregation);
    }

    return keyDate.valueOf() == startOfInterval.valueOf();
}

export function getTickValuesAndFormat(range, interval, keys, expanded, domain) {
    let values;
    let format;

//    console.log("getTickValuesAndFormat", keys, expanded, domain);

    if (domain == "time_of_day" || domain == "day_of_week") {
        let n = 1;
        if (expanded) {
            if (keys.length > 12) {
                n = Math.floor(keys.length / 12);
            }
        }
        else {
            if (keys.length > 6) {
                n = Math.floor(keys.length / 6);
            }
        }

        let firstIndex;
        for(let i=1; i<keys.length; i++) {
            if (domain == "time_of_day") {
                if (keys[i].match(/:00:00/)) {
                    firstIndex = i;
                    break;
                }
            }
            else if (domain == "day_of_week") {
                if (keys[i].match(/:00:00/)) {
                    firstIndex = i;
                    break;
                }
            }
        }

//        console.log("firstIndex", firstIndex);

        if (firstIndex == null) {
            firstIndex = 1;
        }

        values = [];
        for(let i=firstIndex; i<keys.length-1; i+=n) {
            values.push(i);
        }

//        console.log("values", values);

        const aggregationIntervalObject = intervalObjectMap[range.interval];

        if (domain == "time_of_day") {
            format = d => {
                const timeFormat = d3timeformat.timeFormat("%-H:%M");

                return timeFormat(keyParse(keys[d]));
            };
        }
        else if (domain == "day_of_week") {
            format = d => {
                const timeFormat = d3timeformat.timeFormat("%a %-H:%M");

                return timeFormat(keyParse(keys[d]));
            };
        }
    }
    else {
        const tickIntervalObject = suggestInterval("ticks", range);

        const incompatible = tickIntervalObject.seconds % intervalObjectMap[range.interval].seconds != 0;

    //    console.log("daterange getTickValuesAndFormat", range, interval || "none", keys, tickIntervalObject, incompatible);

        values = [];
        let n = Math.round(intervalObjectMap[tickIntervalObject.key].seconds / intervalObjectMap[interval || range.interval].seconds);
        if (expanded) {
            n = Math.floor(n/2);
        }
        for (let i=1; i<keys.length - 1; i+=n) {  // don't show first or last tick
            values.push(i);
        }

        const aggregationIntervalObject = intervalObjectMap[range.interval];

    //    console.log("getTickValuesAndFormat", JSON.stringify(aggregationIntervalObject), JSON.stringify(tickIntervalObject))

        format = d => {
            const timeFormat = d3timeformat.timeFormat(aggregationIntervalObject.format);

            return timeFormat(keyParse(keys[d]));
        };
    }

    return {
        values: values,
        format: format
    };
}

export function suggestInterval(type, range) {
    const seconds = moment.duration(moment(range.end).diff(moment(range.start))).asSeconds();
//    console.log("suggestInterval", range, seconds);

    if (type == "aggregation") {
        for(let i=0; i<intervalObjects.length-1; i++) {
            const intervalObject = intervalObjects[i];
//            console.log("try aggregation", range, intervalObject);
            if (seconds / intervalObject.seconds > minIntervals) {
                return intervalObject;
            }
        }
    }
    else if (type == "ticks") {
        for(let i=0; i<intervalObjects.length-1; i++) {
            const intervalObject = intervalObjects[i];
//            console.log("try tick", range, intervalObject);
            if (seconds / intervalObject.seconds > minTicks) {
                return intervalObject;
            }
        }
    }

    return intervalObjects.slice(-1)[0];
}

export function getNamedDateRange(name) {
    let track = false;
    let start;
    let end;
    let intervalObject;

    let now = moment();
    if (name == "last_hour") {
        start = now.clone().subtract(1, "hour").add(1, "minute").startOf("minute");
        end = now.clone().endOf("minute");
        track = true;
    }
    else if(name == "today") {
        start = now.clone().startOf("day");
        end = now.clone().endOf("hour");
        track = true;
    }
    else if(name == "last_24_h") {
        start = now.clone().subtract(1, "day").add(1, "hour").startOf("hour");
        end = now.clone().endOf("hour");
        track = true;
    }
    else if(name == "last_full_day") {
        start = now.clone().subtract(1, "day").startOf("day");
        end = now.clone().subtract(1, "day").endOf("day");
    }
    else if(name == "past_week") {
        start = now.clone().subtract(1, "week").add(1, "day").startOf("day");
        end = now.clone().endOf("day");
    }
    else if(name == "last_full_week") {
        start = now.clone().subtract(1, "week").startOf("week");
        end = now.clone().subtract(1, "week").endOf("week");
    }
    else if(name == "past_month") {
        start = now.clone().subtract(1, "month").add(1, "day").startOf("day");
        end = now.clone().endOf("day");
    }
    else if(name == "last_full_month") {
        start = now.clone().subtract(1, "month").startOf("month");
        end = now.clone().subtract(1, "month").endOf("month");
    }
    else if(name == "past_quarter") {
        start = now.clone().subtract(1, "quarter").add(1, "day").startOf("day");
        end = now.clone().endOf("day");
    }
    else if(name == "last_full_quarter") {
        start = now.clone().subtract(1, "quarter").startOf("quarter");
        end = now.clone().subtract(1, "quarter").endOf("quarter");
    }
    else if(name == "past_year") {
        start = now.clone().subtract(1, "year").add(1, "day").startOf("day");
        end = now.clone().endOf("day");
    }
    else if(name == "last_full_year") {
        start = now.clone().subtract(1, "year").startOf("year");
        end = now.clone().subtract(1, "year").endOf("year");
    }
    else if(name == "past_5_years") {
        start = now.clone().subtract(5, "year").add(1, "day").startOf("day");
        end = now.clone().endOf("day");
    }
    else if(name == "all_time") {
        start = moment("1900-01-01");
        end = now.clone().endOf("day");
    }

    intervalObject = suggestInterval("aggregation", {start: start, end: end});

    return {name: name, track: track, start: start, end: end, interval: intervalObject.key};
}
