import numeral from 'numeral-es6';

export function countFormat(n, d) {
    // n the number
    // d the number of decimals
    if (n < 2000) {
        if (Math.floor(n) == n) {
            return n;
        }
    }
    else {
        let result;
        if (d == 0) {
            result = numeral(n).format("0.[0]a");
        }
        else {
            result = numeral(n).format("0.0a");
        }

        if (n =~ /m/) {
            result = result.toUpperCase();
        }

        return result;
    }
}