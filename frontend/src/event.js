import { config } from "root/config.js";
import { getCSRFToken } from "./utils.js";


export function event(metric, timestamp, value, parameters) {
    const url = `http://${config.api}/event/`;
    const method = "POST";
    const headers = {
        "Content-Type": "Application/Json",
        "X-CSRFToken": getCSRFToken()
    };
    const body = JSON.stringify({metric, value, timestamp, parameters});

//    console.log("event", url, method, headers, body);

    fetch(url, {method, headers, body})
    .catch(error => console.log(error));
}