import * as moment from 'moment';
import * as d3timeformat from "d3-time-format";

export function getCSRFToken() {
    const cookieValue = document.getElementsByName('csrfmiddlewaretoken')[0].value;

    return cookieValue;
}

export function findAll(string, regex, returnTuples) {
    let match;
    let results = [];

    do {
        match = regex.exec(string);
//        console.log("match",  match);
        if (match) {
            if (returnTuples) {
                results.push(match);
            }
            else {
                results.push(match[0]);
            }
        }
    } while (match);

    return results;
}

String.prototype.capitalize = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

export function deepcopy(object) {
//    console.log("deepcopy", object);

    if (object == null) {
        return null;
    }

    let copy = null;
    if (typeof object == "object") {
        if (object.constructor == Object) {
            copy = {};
            Object.keys(object).map(key => {
                copy[key] = deepcopy(object[key]);
            });
        }
        else if (object.constructor == Array) {
            copy = [];
            object.forEach(item => {
                copy.push(deepcopy(item));
            })
        }
    }
    else {
        copy = object;
    }

    return copy;
}

export function deepinit(template, object) {
//    console.log("deepinit", template, object);

    // a null is replaced by anything
    if (template == null) {
        return object;
    }

    // a null is always returned
    if (object == null) {
        return object;
    }

    // if dict or array
    if (typeof object == "object") {
        // if dict, dive into keys
        if (object.constructor == Object) {
            Object.keys(object).map(key => {
                //console.log("deepinit", key);
                // if the key exists in the init as well, recurse
                if (key in template) {
                    template[key] = deepinit(template[key], object[key]);
                }
                // otherwise just assign with what we have
                else {
                    template[key] = object[key];
                }
            });

            // and return the updated template
            return template;
        }
        // if array, just replace
        else {
            return object;
        }
    }
    // if not dict or array, just return the thing
    else {
        return object;
    }
}

export function intersection(lists) {
    if (lists.length == 0) {
        return [];
    }

    let resultSet = new Set(lists[0]);
    for (let i=1; i<lists.length; i++) {
        resultSet = new Set(lists[i].filter(item => resultSet.has(item)));
    }

    return [...resultSet];
}

export function union(lists) {
    if (lists.length == 0) {
        return [];
    }

    let resultSet = new Set(lists[0]);
//    console.log(resultSet);
    for (let i=1; i<lists.length; i++) {
//        console.log(lists[i]);
        for (let j=0; j<lists[i].length; j++) {
            const item = lists[i][j];

            resultSet.add(item);
        }
    }

    return [...resultSet];
}

export function difference(first, second) {
    const secondSet = new Set(second);

    return first.filter(item => !secondSet.has(item));
}
