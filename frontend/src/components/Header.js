import React from "react";
import { config } from "root/config.js";


export default class Header extends React.Component {
    render() {
        return (
            <div style={{display: 'block', ...this.props.style}}>
                {this.props.login && <span className="header"><a href="/login">Login</a></span>}
                {this.props.register && <span className="header"><a href="/register">Register</a></span>}
                {this.props.logout && <span className="header"><a href="/logout">Logout</a></span>}
                {this.props.overview && <span className="header"><a href="/overview">Overview</a></span>}
                {this.props.project && <span className="header"><a href="/project">Project</a></span>}
                {this.props.lists && <span className="header"><a href="/lists">Lists</a></span>}
                {this.props.feeds && <span className="header"><a href="/feeds">Feeds</a></span>}
                {this.props.topics && <span className="header"><a href="/topics">Topics</a></span>}
                {this.props.user && <span className="header">{this.props.user}</span>}
                {this.props.help && <span className="header" style={{float: "right"}}><a href="/wiki">Help</a></span>}
                {this.props.contact && <span className="header" style={{float: "right"}}><a href="/contact">Contact</a></span>}
            </div>
        );
    }
}