import React from "react";

import {Col, Container, Row, Button, Alert, Modal, Dropdown, DropdownButton, Form, Table} from "react-bootstrap";

import LineChart from "./LineChart.js";
import PieChart from "./PieChart.js";
import LiveChart from "./LiveChart.js";
import PostingsChart from "./PostingsChart.js";
import ChartEditor from "./ChartEditor.js";
import PopularityChart from "./PopularityChart.js";
//import GeoChart from "./GeoChart.js";
import Header from "./Header.js";
import { getCSRFToken } from "root/utils.js";
import { Alerts, Confirm, Dialog, Paginator, LabeledDropdown } from "./Controls.js";
import { config, S } from "root/config.js";
import { filterStructure, charMap, deepcopy, deepinit } from "root/utils.js";
import { presets, getNamedDateRange, makeHumanReadable, intervalObjects, intervalObjectMap } from "root/dateRange.js";
import { event } from "root/event.js";

import * as moment from "moment";

class LabeledElement extends React.Component {
    render() {
        return (
            <div style={{display: "flex", marginBottom: "0px"}}>
                <span style={{margin: "auto 4px auto 0"}}>{this.props.label}</span>
                {this.props.children}
            </div>
        );
    }
}

class LabeledControl extends React.Component {
    render() {
        return (
            <LabeledElement {...this.props}>
                <Form.Control
                    value={this.props.value}
                    placeholder={this.props.placeholder ? S(this.props.placeholder).capitalize() + "..." : "" }
                    onChange={this.props.onChange}
                />
            </LabeledElement>
        );
    }
}

class DialogButtons extends React.Component {
    render() {
        let buttons = [];

        this.props.options.forEach(option => {
            let variant = "outline-dark";
            let className = "btn-lightborder";
            if (option.key == this.props.primary) {
                variant = "outline-primary";
                className = "";
            }

            buttons.push(<div key={option.key}><Button variant={variant} className={className} onClick={this.props.buttonMap[option.key]}>{S(option.value)}</Button></div>)
        });

        return (
            <div className="btn-row">
                {buttons}
            </div>
        );
    }
}


class ProjectActionDialog extends React.Component {
    constructor(props) {
        super(props);

        this.onHandlePrimary = this.onHandlePrimary.bind(this);

        this.state = {
            refresh: null,
            name: "",
            url: ""
        }
    }

    onHandlePrimary() {
        if (this.props.mode == "publish") {
            this.props.handleAction(this.props.mode, {name: this.state.name, refresh: this.state.refresh}, url => this.setState({url}));
        }
        else if (this.props.mode.match(/copy|rename/)) {
            const onCopySuccess = json => {
                const alert = { type: 'info', message: 'Copy successful' };

                this.props.addAlerts([alert]);
            };

            this.props.handleAction(this.props.mode, {name: this.state.name}, onCopySuccess);
        }
    }

    render() {
        if (this.props.mode == null) {
            return null;
        }

        let dialogTitle;
        let body;
        let buttons;

        if (this.props.mode == "publish") {
            dialogTitle = "Publish Project";

            const refreshOptions = [
                { key: null, value: "None" },
                { key: "one_minute", value: "One minute" },
                { key: "one_hour", value: "One hour" },
                { key: "one_day", value: "One day" },
            ];

            let urlElement;
            if (this.state.url) {
                urlElement = (
                    <LabeledElement label="Share&nbsp;this&nbsp;URL:">
                        <a href={this.state.url} target="blank">{this.state.url}</a>
                    </LabeledElement>
                );
            }
            else {
                urlElement = <span>{"Press Publish to get a URL"}</span>;
            }

            body = (
                <div>
                    <LabeledDropdown label="Refresh:" selected={this.state.refresh} options={refreshOptions} onChange={refresh => {this.setState({refresh}); console.log("onChange", refresh)}}/>
                    <LabeledControl label="Name:" value={this.state.name} onChange={event => this.setState({name: event.target.value})}/>
                    {urlElement}
                </div>
            );

            const buttonOptions = [
                { key: "publish", value: "Publish" },
                { key: "close", value: "Close" },
            ];

            const buttonMap = {
                "close": this.props.onClose,
                "publish": this.onHandlePrimary,
            };

            buttons = <DialogButtons options={buttonOptions} buttonMap={buttonMap} primary="publish"/>;
        }
        else if (this.props.mode == "copy") {
            dialogTitle = "Copy Project";

            body = (
                <div>
                    <LabeledControl label="New&nbsp;name:" value={this.state.name} onChange={event => this.setState({name: event.target.value})}/>
                </div>
            );

            const buttonOptions = [
                { key: "copy", value: "Copy" },
                { key: "close", value: "Close" },
            ];

            const buttonMap = {
                "close": this.props.onClose,
                "copy": this.onHandlePrimary,
            };

            buttons = <DialogButtons options={buttonOptions} buttonMap={buttonMap} primary="copy"/>;
        }
//        else if (this.props.mode == "rename") {
//            dialogTitle = "Rename Project";
//
//            body = (
//                <div>
//                    <LabeledControl label="New&nbsp;name:" value={this.state.name} onChange={event => this.setState({name: event.target.value})}/>
//                </div>
//            );
//
//            const buttonOptions = [
//                { key: "rename", value: "Rename" },
//                { key: "close", value: "Close" },
//            ];
//
//            const buttonMap = {
//                "close": this.props.onClose,
//                "rename": this.onHandlePrimary,
//            };
//
//            buttons = <DialogButtons options={buttonOptions} buttonMap={buttonMap} primary="rename"/>;
//        }

        return (
            <Modal show={this.props.show || false} onHide={this.props.onClose}>
                <Modal.Header>
                    <Modal.Title>
                        <span>{dialogTitle}</span>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {body}
                </Modal.Body>
                <Modal.Footer>
                    {buttons}
                </Modal.Footer>
            </Modal>
        );
    }
}

class QueryContext extends React.Component {
    constructor(props) {
        super(props);

        this.onChange = this.onChange.bind(this);
        this.onSelect = this.onSelect.bind(this);
        this.onKeyPress = this.onKeyPress.bind(this);
        this.onSearch = this.onSearch.bind(this);
        this.onTestQuery = this.onTestQuery.bind(this);
        this.onAddVariable = this.onAddVariable.bind(this);
        this.onDeleteVariable = this.onDeleteVariable.bind(this);
        this.onSaveVariables = this.onSaveVariables.bind(this);

        this.state = {
            mainQuery: null,
            dateRange: null,
            variables: null,
            mainQueryExpanded: false,
        }
    }

    static getDerivedStateFromProps(props, state) {
//        console.log("project getDerivedStateFromProps", props, state);

        let stateDiff = {};
        if (state.mainQuery == null) {
            stateDiff.mainQuery = props.context.data.mainQuery;
        }
        if (props.context.data.dateRange != null) {
            stateDiff.dateRange = props.context.data.dateRange;
        }
        if (state.variables == null) {
//            console.log("override variables", props.context.data.variables);
            stateDiff.variables = props.context.data.variables;
        }

        if (Object.keys(stateDiff).length > 0) {
            return stateDiff;
        }
        else {
            return null;
        }
    }

    onSelect(mode, key) {
//        console.log("select", JSON.stringify(this.state.dateRange));

        let dateRange;
        if (mode == "date") {
            dateRange = getNamedDateRange(key);
        }

        this.props.context.func.onUpdateDateRange(dateRange);
    }

    onSearch() {
        return this.props.context.func.onUpdateMainQuery(this.state.mainQuery || "");
    }

    onSaveVariables() {
        return this.props.context.func.onSaveVariables(this.state.variables || []);
    }

    onTestQuery() {
        return this.props.context.func.onTestQuery(this.state.mainQuery || "");
    }

    onKeyPress(event) {
        if (event.key == "Enter") {
            if (this.props.mode == "project") {
                return this.onSearch();
            }
        }
    }

    onChange(mode, event) {
        if (mode == "main") {
            this.setState({mainQuery: event.target.value});
        }
        else if (mode == "name" || mode == "value") {
            let variables = [...this.state.variables];

            const id = parseInt(event.target.id.match(/\d+/)[0])
            let index;
            for (let i=0; i<this.state.variables.length; i++) {
                if (this.state.variables[i].id == id) {
                    index = i;

                    break;
                }
            }

            if (mode == "name") {
                variables[index].name = event.target.value;
            }
            else if (mode == "value") {
                variables[index].value = event.target.value;
            }

            this.setState({variables});
        }
    }

    onAddVariable() {
//        console.log("project onAddVariable");

        let variables = this.state.variables == null ? [] : [...this.state.variables];

        const newId = variables.length ? variables[variables.length-1].id + 1 : 0;

        variables.push({id: newId, name: "", value: ""});

//        console.log("variables");

        this.setState({variables});
    }

    onDeleteVariable(id) {
        let variables = [...this.state.variables];

        for (let i=0; i<variables.length; i++) {
            if (variables[i].id == id) {
                variables.splice(i, 1);

                break;
            }
        }

        this.setState({variables});
    }


    onSaveVariables() {
        this.props.context.func.onSaveVariables(this.state.variables);
    }

    render() {
//        console.log("querycontext render", window.bootstrapScale);

        const datePresetItems = presets.map((item, index) => <Dropdown.Item eventKey={item.key} key={index}>{item.value}</Dropdown.Item>);

        let dateTitle = "(custom)";
        presets.forEach(preset => {
            if (preset.key == this.state.dateRange.name) {
                dateTitle = preset.value;
            }
        });

        const humanReadableDateRange = makeHumanReadable(this.state.dateRange);

        let expandCollapse = null;
        if (this.state.mainQueryExpanded) {
            expandCollapse = (
                <Button className="btn-icon" variant="outline-dark" onClick={() => this.setState({mainQueryExpanded: false})}>
                    <span className="fas fa-chevron-up" aria-hidden="true"></span>
                </Button>
            );
        }
        else {
            expandCollapse = (
                <Button className="btn-icon" variant="outline-dark" onClick={() => this.setState({mainQueryExpanded: true})}>
                    <span className="fas fa-chevron-down" aria-hidden="true"></span>
                </Button>
            );
        }

        let variablesControl = null;
        if (this.state.mainQueryExpanded) {
//            console.log("variables", this.state.variables);
            let variables = null;
            if (this.state.variables) {
                variables = this.state.variables.map(variable =>
                    <div key={variable.id} style={{display: "flex", marginBottom: "4px"}}>
                        <Form.Control id={`name-${variable.id}`} style={{maxWidth: "200px"}} type="text" placeholder="Name..." value={variable.name} onChange={event => this.onChange("name", event)}/>
                        <Form.Control id={`value-${variable.id}`} style={{margin: "0 2px 0 4px"}} type="text" as="textarea" placeholder="Value..." value={variable.value} onChange={event => this.onChange("value", event)}/>
                        <Button variant="outline-dark" className="btn-icon" onClick={() => this.onDeleteVariable(variable.id)}>
                            <span className="far fa-trash-alt"/>
                        </Button>
                    </div>
                );
            }

//            console.log("variables", variables);
            variablesControl = (
                <React.Fragment>
                    <Form.Label>
                        <span>Variables</span>
                    </Form.Label>
                    <div>
                        {variables}
                        { this.props.mode != "publish" && <div className="btn-row">
                            <Button variant="outline-dark" className="btn-icon" onClick={this.onAddVariable}>
                                <span className="fas fa-plus" aria-hidden="true"/>
                            </Button>
                        </div> }
                    </div>
                    { this.props.mode != "publish" && <div style={{display: "flex"}}>
                        <span style={{width: "100%"}}/>
                        <Button variant="outline-primary" className="btn-lightborder" onClick={this.onSaveVariables}>Save</Button>
                    </div> }
                </React.Fragment>
            )
        }

        let mainQueryLabel = null;
        if (this.state.mainQueryExpanded) {
            mainQueryLabel = (
                <Form.Label>
                    <span>Main query</span>
                </Form.Label>
            );
        }


        const mainQueryControl = (
            <React.Fragment>
            {mainQueryLabel}
            <Form.Control
                as={this.state.mainQueryExpanded ? "textarea" : "input"}
                style={{height: this.state.mainQueryExpanded ? "144px": "36px"}}
                placeholder="Query..."
                value={this.state.mainQuery || ""}
                onChange={(event) => this.onChange("main", event)}
                onKeyPress={this.onKeyPress}
            />
            <button onClick={() => this.setState({mainQuery: ""})} style={{top: "-30px", right: "8px", position: "relative"}} aria-label="Clear" className="close rbt-close" type="button"><span aria-hidden="true">×</span><span className="sr-only">Clear</span></button>
            </React.Fragment>

        );

        const containerStyle = this.state.mainQueryExpanded ? {} : {display: "flex"};

        let testButton = null;
        let searchButton = null;

        if (this.props.mode == "project") {
            testButton = <Button className="btn-icon" variant="outline-dark" onClick={this.onTestQuery}>
                <span className="fas fa-clipboard-check" aria-hidden="true"></span>
            </Button>
            searchButton = <Button className="btn-icon" variant="outline-dark" onClick={this.onSearch}>
                <span className="fas fa-search" aria-hidden="true"></span>
            </Button>
        }

        const mainQuerySection = (
            <React.Fragment>
                <div style={{display: this.state.mainQueryExpanded ? "block" : "flex"}}>
                    <div style={{width: "100%", marginBottom: "2px"}}>
                        {variablesControl}
                        {mainQueryControl}
                    </div>
                    <div style={{display: "flex", position: "relative", right: this.state.mainQueryExpanded ? "-14px" : "0px"}}>
                        <span style={{width: "100%", marginRight: "2px"}}/>
                        {expandCollapse}
                        {testButton}
                        {searchButton}
                   </div>
                </div>
            </React.Fragment>
        );

        let dateRangeUI = null;
        if (this.props.mode == "project") {
            dateRangeUI = (
                <React.Fragment>
                    <Button className="btn-icon" variant="outline-dark" onClick={this.props.context.func.onPreviousDateRange}>
                        <span className="fas fa-chevron-left" aria-hidden="true"></span>
                    </Button>
                    <div style={{margin: "6px"}}>
                        <Form.Label>Date&nbsp;range:</Form.Label>
                    </div>
                    <DropdownButton className="btn-lightborder" variant="outline-dark" id="datepreset" title={dateTitle} onSelect={(key) => this.onSelect("date", key)}>
                        {datePresetItems}
                    </DropdownButton>
                </React.Fragment>
            )
        }

        return (
            <div>
                {mainQuerySection}
                <div style={{display: "flex", flexWrap: "wrap"}}>
                    {dateRangeUI}
                    <div style={{marginLeft: "auto"}}>
                        <span className="date">{humanReadableDateRange}</span>
                    </div>
                </div>
            </div>
        );
    }
}

class ChartList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
        }

        this.renderChart = this.renderChart.bind(this);
    }

    renderChart(chart) {
        if (!chart) {
            return null;
        }

//        console.log("chartlist", chart);

        const props = {
            chart: chart,
            context: this.props.context,
            mode: this.props.mode,
            position: this.props.position,
            expanded: this.props.expanded,
        }

        let plot;
        if (chart.type === "LineChart") {
            plot = <LineChart {...props}/>;
        }
        if (chart.type === "PieChart") {
            plot = <PieChart {...props}/>;
        }
        if (chart.type === "PostingsChart") {
            plot = <PostingsChart {...props}/>;
        }
        if (chart.type === "PopularityChart") {
            plot = <PopularityChart {...props}/>;
        }
        if (chart.type === "LiveChart") {
            plot = <LiveChart {...props}/>;
        }

        return(
            <div key={chart.id} style={{marginBottom: 20}}>
                {plot}
            </div>
        );
    }

    render() {
//        console.log("chart list render", this.props.charts);

        if (!this.props.charts) {
            return null;
        }

        const charts = this.props.charts.map(this.renderChart);

        return (
            <Row>
                <Col xs={12}>
                    {charts}
                </Col>
            </Row>
        );
    }
}

export default class Project extends React.Component {
    constructor(props) {
        super(props);

        this.onQuery = this.onQuery.bind(this);
        this.onUpdateDateRange = this.onUpdateDateRange.bind(this);
        this.initQueryContext = this.initQueryContext.bind(this);
        this.onDeleteChart = this.onDeleteChart.bind(this);
        this.onDuplicateChart = this.onDuplicateChart.bind(this);
        this.onHideChartEditor = this.onHideChartEditor.bind(this);
        this.onShowChartEditor = this.onShowChartEditor.bind(this);
        this.onUpdateMainQuery = this.onUpdateMainQuery.bind(this);
        this.onSaveVariables = this.onSaveVariables.bind(this);
        this.onTestQuery = this.onTestQuery.bind(this);
        this.updateState = this.updateState.bind(this);
        this.onMoveChart = this.onMoveChart.bind(this);
        this.onPreviousDateRange = this.onPreviousDateRange.bind(this);
        this.handleProjectAction = this.handleProjectAction.bind(this);
        this.onProjectActionDialog = this.onProjectActionDialog.bind(this);
//        this.onNotificationsDialog = this.onNotificationsDialog.bind(this);
//        this.dismissNotifications = this.dismissNotifications.bind(this);
        this.addAlerts = this.addAlerts.bind(this);
        this.confirm = this.confirm.bind(this);
        this.dialog = this.dialog.bind(this);

        this.saveChart = this.saveChart.bind(this);
        this.handleResize = this.handleResize.bind(this);
        this.reconnectWebSocket = this.reconnectWebSocket.bind(this);
        this.updateSocketSubscription = this.updateSocketSubscription.bind(this);

        this.state = {
            queryContext: this.initQueryContext(),

            editor: {
                show: false,
                chart: null,
            },

            charts: [
            ],
            chartOrder: {
                left: [],
                right: [],
                top: [],
                bottom: [],
            },

            alerts: [],
            notifications: {
                show: false,
                list: [],
            },
            projectAction: null,
            confirm: {},
            dialog: {},
            expanded: window.bootstrapScale.match(/lg|xl/) ? true : false,
        };

        this.dateRangeHistory = [];
        this.chartData = {};
        this.projectContext = {};

        this.mode = null;
        this.bootstrapScale = window.bootstrapScale;
        this.socket = null;
        this.socketConnections = {};
    }

    handleResize() {
//        console.log("resize");
        if (window.bootstrapScale == this.bootstrapScale) {
            return;
        }

        this.bootstrapScale = window.bootstrapScale;
//        console.log("change scale");
        const expanded = window.bootstrapScale.match(/lg|xl/) ? true : false;

        if (expanded != this.state.expanded) {
//            console.log("expanded", expanded);
            this.setState({expanded});
        }
    }

    updateSocketSubscription(action, chartId, config, callback) {
//        console.log("project updateSocketSubscription", action, this.projectContext);

        const account_id = this.projectContext.account_id;

        if (action == "unsubscribe") {
            delete this.socketConnections[chartId];

            this.socket.send(JSON.stringify({type: "lists_unsubscribe", context: {account_id}, data: {chartId}}))
        }
        else if(action == "subscribe") {
            this.socketConnections[chartId] = {callback, data: {chartId, config}};

            this.socket.send(JSON.stringify({type: "lists_subscribe", context: {account_id}, data: {chartId, config}}))
        }
    }

    reconnectWebSocket(mode) {
        // TODO: handle socket subscriptions: disconnect was handled already automatically, but need to resend
        // configs for active charts, just iterate from the chart configs by type = live and use selected for lists

//        console.log("CONNECT");

        this.socket = new WebSocket(`ws://${config.host}/`);

//        console.log("SOCKET", this.socket);

        this.socket.onmessage = message => {
            const data = JSON.parse(message.data);

            const notification = data.data;
            const chartIds = data.chart_ids;

//            console.log("MESSAGE", notification, this.state.notifications);

//            console.log("connections", this.socketConnections);
            chartIds.forEach(chartId => {
                this.socketConnections[chartId].callback(notification);
            });
        }
        this.socket.onclose = e => {
//            console.log("CLOSING");

            this.socket = null;

            window.setTimeout(() => this.reconnectWebSocket("reconnect"), 5000);
        }
        this.socket.onopen = e => {
            if ( mode == "reconnect" ) {
//                console.log("REOPEN");

                Object.keys(this.socketConnections).forEach(chartId => {
                    const connection = this.socketConnections[chartId];

                    this.updateSocketSubscription("subscribe", connection.data.chartId, connection.data.config, connection.callback);
                });
            }
            else {
//                console.log("OPEN");
            }
        }
    }

    componentDidMount() {
//        console.log("project componentDidMount", this.props.match);

        window.addEventListener('resize', this.handleResize)

        const projectId = this.props.match.params.id || "";
        const uuId = this.props.match.params.uuid || "";
        const name = this.props.match.params.name || "";

        let url;
        if (uuId != "" && projectId == "") {
            this.mode = "publish";

            if (name == "") {
                url = `http://${config.api}/publish/${uuId}`;
            }
            else {
                url = `http://${config.api}/publish/${name}/${uuId}`;
            }
        }
        else if (uuId == "" && projectId != "") {
            this.mode = "project";

            url = `http://${config.api}/project/${projectId}`;
        }

        this.reconnectWebSocket();

        event("frontend.pageload", null, null, {page: this.mode});

//        console.log("mode", projectId || "none", uuId || "none", this.mode);

        fetch(url)
        .then(response => response.json())
        .then(json => {
            // make moment objects out of the stored strings
            if (json.state.queryContext) {
//                console.log("queryContext", json.state.queryContext);

                if (json.state.queryContext.dateRange.name) {
                    let dateRange = getNamedDateRange(json.state.queryContext.dateRange.name);
                    dateRange.interval = json.state.queryContext.dateRange.interval;
                    dateRange.timeZone = json.state.queryContext.dateRange.timeZone;

                    json.state.queryContext.dateRange = dateRange;
                }
                else {
                    json.state.queryContext.dateRange.start = moment(json.state.queryContext.dateRange.start);
                    json.state.queryContext.dateRange.end = moment(json.state.queryContext.dateRange.end);
                }
            }

//            console.log("project componentDidMount", json);

            if (json.alerts) {
//                console.log("json alerts", json.alerts);

                this.addAlerts(json.alerts);
            }

            if (this.mode == "publish" && json.data) {
                this.chartData = json.data;
                this.projectContext = json.context;

//                console.log("SET CHART DATA", this.chartData);
            }

            // validate incoming state

            const newState = deepinit(this.state, json.state);

//            console.log("deepinit", this.state, json.state, newState);

            this.updateState(newState, false);
        })
        .catch(error => console.log(error));
    }

    initQueryContext() {
        const now = moment();

        const dateRangeStart = now.clone().subtract(1, "month").startOf("day");
        const dateRangeEnd = now.clone().endOf("day");

        const dateRange = {
            start: dateRangeStart,
            end: dateRangeEnd,
            timeZone: "CET",
            interval: "day",
        };

        return {
            mainQuery: null,
            dateRange: dateRange,
            variables: null,
        };
    }

    onQuery(callback, chartId, queryData) {
//        console.log("get data", this.chartData, chartId);
        if (this.mode == "publish") {
            if (chartId in this.chartData) {
//                console.log("chartId in chartData", this.chartData[chartId].timeout, moment().format());
                if ("timeout" in this.chartData[chartId] && this.chartData[chartId].timeout != null) {
                    if (moment(this.chartData[chartId].timeout).isAfter(moment())) {
//                        console.log("data not timed out", chartId);

                        callback(this.chartData[chartId].response)

                        return;
                    }
                    else {
//                        console.log("data timed out", chartId);
                    }
                }
                else {
//                    console.log("no timeout", chartId);

                    callback(this.chartData[chartId].response)

                    return;
                }
            }
            else {
//                console.log("Could not find data", chartId);

                callback({});

                return;
            }
        }

//        console.log("onQuery", this.state.project);

        let chartType = null;
        this.state.charts.forEach(chart => {
            if (chart.id == chartId) {
                chartType = chart.type;
            }
        });

        fetch(`http://${config.api}/search/`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "X-CSRFToken": getCSRFToken(),
            },
            body: JSON.stringify({
                chart_type: chartType,
                query: this.state.project.query,
                queryData: queryData
            })
        })
        .then(response => response.json())
        .then(json => {
//            console.log("onQuery result", json);

            this.addAlerts(json.alerts);

//                console.log('save data', chartId, json);
            this.chartData[chartId] = json;

            if (this.mode == "publish") {
//                console.log("update publish data", this.props.match.params.uuid);

                fetch(`http://${config.api}/publish/${this.props.match.params.uuid}/`, {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                        "X-CSRFToken": getCSRFToken(),
                    },
                    body: JSON.stringify({
                        chart_id: chartId,
                        data: json
                    })
                });
            }

            callback(json.response);
        })
        .catch(error => console.log(error));
    }

    onTestQuery(query) {
        if (this.mode == "publish") {
//            console.log("WARNING. onTestQuery. This shouldn't happen.")

            return;
        }

        fetch(`http://${config.api}/test/`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "X-CSRFToken": getCSRFToken(),
            },
            body: JSON.stringify({
                query: query,
            })
        })
        .then(response => response.json())
        .then(json => {
            this.addAlerts(json.alerts);
        })
        .catch(error => console.log(error));
    }

    onUpdateDateRange(inputDateRange) {
//        console.log("onUpdateDateRange", inputDateRange);

        let dateRange = {...this.state.queryContext.dateRange};
        if ("start" in inputDateRange) {
            dateRange.start = inputDateRange.start;
        }
        if ("end" in inputDateRange) {
            dateRange.end = inputDateRange.end;
        }
        if ("interval" in inputDateRange) {
            dateRange.interval = inputDateRange.interval;
        }
        if ("timeZone" in inputDateRange) {
            dateRange.timeZone = inputDateRange.timeZone;
        }
        if ("track" in inputDateRange) {
            dateRange.track = inputDateRange.track;
        }
        if ("name" in inputDateRange) {
            dateRange.name = inputDateRange.name;
        }

        const queryContext = {...this.state.queryContext, dateRange: dateRange};

        this.dateRangeHistory.push(this.state.queryContext.dateRange);

//        console.log("pushing", JSON.stringify(dateRange));

        this.updateState({queryContext: queryContext});
    }

    onDeleteChart(chartId) {
        this.confirm(S("Delete chart?"), () => {
            let charts = this.state.charts;
            let chartOrder = this.state.chartOrder;

            for(let i=0; i<charts.length; i++) {
                if(charts[i].id == chartId) {
                    charts.splice(i, 1);

                    ["top", "right", "bottom", "left"].forEach(position => {
                        i = chartOrder[position].indexOf(chartId);
                        if (i >= 0) {
                            chartOrder[position].splice(i, 1);
                        }
                    });

                    break;
                }
            }

            this.updateState({charts: charts, chartOrder: chartOrder});
        });
    }

    onProjectActionDialog(mode) {
        if (this.mode == "publish") {
//            console.log("WARNING. onProjectAction. This shouldn't happen.")

            return;
        }

        this.setState({projectAction: {show: true, mode: mode}});
    }

    handleProjectAction(action, parameters, callback) {
//        console.log("onProjectAction", timeout);

        let cleanState = {...this.state};
        delete cleanState.confirm;
        delete cleanState.dialog;
        delete cleanState.alerts;
        delete cleanState.editor;
        delete cleanState.projectAction;

        if (action == "publish") {
            const url = `http://${config.api}/publish/`;

            let data = {};
            this.state.charts.forEach(chart => {
                data[chart.id] = this.chartData[chart.id]
            });

            const body = {
                state: cleanState,
                data: data,
                timeout: parameters.refresh,
                name: parameters.name,
            };

            fetch(url, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "X-CSRFToken": getCSRFToken(),
                },
                body: JSON.stringify(body)
            })
            .then(response => response.json())
            .then(json => {
    //            console.log('publish', json);

                callback(json.url);
            });
        }
        else if (action == "copy") {
            const url = `http://${config.api}/project/duplicate/`;

            const body = {
                name: parameters.name,
                id: this.state.project.id,
                state: cleanState,
                type: "project",
            };

//            console.log("copy project", body);

            fetch(url, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "X-CSRFToken": getCSRFToken(),
                },
                body: JSON.stringify(body)
            })
            .then(response => response.json())
            .then(json => {
//                console.log('copy', json);

                callback(json);
            });
        }
    }

    onUpdateChartData(chartId, data) {
        this.chartData[chartId] = data;
    }

    onDuplicateChart(chartId) {
//        console.log("duplicate", this.state);

        let charts = this.state.charts;
        let chartOrder = this.state.chartOrder;

        for(let i=0; i<charts.length; i++) {
            if(charts[i].id == chartId) {
                let chart = {...charts[i]};
                chart.id = charts.slice(-1)[0].id + 1;

                charts.push(chart);

                ["top", "right", "bottom", "left"].forEach(position => {
                    i = chartOrder[position].indexOf(chartId);
                    if (i >= 0) {
                        chartOrder[position].splice(i + 1, 0, chart.id);
                    }
                });

                break;
            }
        }

        this.updateState({charts: charts, chartOrder: chartOrder});
    }

    onHideChartEditor() {
        this.updateState({editor: {show: false, chart: null}}, false);
    }

    onShowChartEditor(chartId=null) {
//        console.log("project onShowChartEditor", JSON.stringify(this.state));

        let chart = null;
        for (let i=0; i<this.state.charts.length; i++) {
            if (this.state.charts[i].id == chartId) {
                chart = deepcopy(this.state.charts[i]);

                break;
            }
        }

        this.updateState({editor: {show: true, chart: chart}}, false);
    }

    saveChart(chartState) {
//        console.log("save chart state", JSON.stringify(this.state), JSON.stringify(chartState));

        let charts = this.state.charts;
        let chartOrder = this.state.chartOrder;

        if (chartState.id == null) {
//            console.log("new chart");
            chartState.id = charts.length > 0 ? charts.slice(-1)[0].id + 1 : 0;

            charts.push(chartState);
            chartOrder.left.push(chartState.id);

            this.updateState({charts: charts, chartOrder: chartOrder, editor: {show: false, chart: null}});
        }
        else {
//            console.log("saving chart");
            let index = null;
            for (let i=0; i<charts.length; i++) {
                if (charts[i].id == chartState.id) {
                    index = i;

                    break
                }
            }

            if (index == null) {
                chartState.id = charts.length > 0 ? charts.slice(-1)[0].id + 1 : 0;
                charts.push(chartState);
            }
            else {
                charts[index] = chartState;
            }

//            console.log("save chart set state", charts);

            this.updateState({charts: charts, editor: {show: false, chart: null}});
        }
    }

    onSaveVariables(variables) {
//        console.log("project onSaveVariables", variables);

        let queryContext = {...this.state.queryContext};
        queryContext.variables = variables;

        this.updateState({queryContext: queryContext});
    }

    onUpdateMainQuery(query) {
//        console.log("onUpdateMainQuery");

        let queryContext = {...this.state.queryContext};
        queryContext.mainQuery = query;

//        console.log("qc", queryContext);

        this.updateState({queryContext: queryContext});
    }

    updateState(diffState, save=true) {
        this.setState(diffState);

        if (this.state.project.type == "readonly") {
//            console.log("Not saving state; Read only project");

            return;
        }

        if (save) {
            if (this.mode == "publish") {
//                console.log("WARNING. updateState. This shouldn't happen.")

                return;
            }

            let newState = {...this.state, ...diffState};
            delete newState.editor;
            delete newState.alerts;
            delete newState.confirm;
            delete newState.chartData;
            delete newState.expanded;
            delete newState.notifications;

            fetch(`http://${config.api}/project/${this.props.match.params.id}/`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "X-CSRFToken": getCSRFToken(),
                },
                body: JSON.stringify(newState)
            })
            .catch(error => console.log(error));
        }
    }

    onMoveChart(motion, chartId) {
//        console.log("motion", motion);

        let chartOrder = deepcopy(this.state.chartOrder);

//        console.log("chartOrder", chartOrder);

        const orientations = ["top", "left", "bottom", "right"];

        const dual = {
            left: "right",
            right: "left",
            top: "bottom",
            bottom: "top",
        }
        // determine where the chart is now

        // which list, top/left/bottom/right
        let orientation;

        // where in that list
        let position;

        // list sizes
        let lengths = {};

        orientations.forEach(ori => {
            lengths[ori] = this.state.chartOrder[ori].length;

            const pos = this.state.chartOrder[ori].indexOf(chartId);
            if (pos >= 0) {
                orientation = ori;
                position = pos;
            }
        });

        if (position == null) {
//            console.log("CHART NOT FOUND");

            return;
        }

        // determine what action needs to be taken on which lists
        if (motion == "up") {
            if (position > 0) {
                const temp = chartOrder[orientation][position];
                chartOrder[orientation][position] = chartOrder[orientation][position-1];
                chartOrder[orientation][position-1] = temp;
            }
            else {
                chartOrder[dual[orientation]].push(chartOrder[orientation].shift());
            }
        }
        else if (motion == "down") {
            if (position < lengths[orientation] - 1) {
                const temp = chartOrder[orientation][position];
                chartOrder[orientation][position] = chartOrder[orientation][position+1];
                chartOrder[orientation][position+1] = temp;
            }
            else {
                chartOrder[dual[orientation]].unshift(chartOrder[orientation].pop());
            }
        }
        else if (motion == "expand") {
            chartOrder["bottom"].push(chartOrder[orientation].splice(position, 1)[0]);
        }
        else if (motion == "collapse") {
            chartOrder["right"].push(chartOrder[orientation].splice(position, 1)[0]);
        }

        this.updateState({chartOrder}, true);
    }

    onPreviousDateRange() {
//        console.log("onPreviousDateRange", this.dateRangeHistory);
        if (this.dateRangeHistory.length > 0) {
            let queryContext = {...this.state.queryContext};
            queryContext.dateRange = this.dateRangeHistory.pop();

//            console.log("popped dateRange", JSON.stringify(queryContext.dateRange));

            this.updateState({queryContext});
        }
    }

    addAlerts(alerts) {
        let currentAlerts = [...this.state.alerts].concat(alerts);

        if (currentAlerts.length > 5) {
            currentAlerts = currentAlerts.slice(-5);

            currentAlerts.unshift({type: "warning", message: "Too many alerts"});
        }

        this.setState({alerts: currentAlerts});
    }

    confirm(message, callback) {
        this.updateState({confirm: {show: true, message: message, callback: callback}}, false);
    }

    dialog(config, buttonActions) {
        this.updateState({dialog: {show: true, config: config, buttonActions: buttonActions}}, false);
    }

    render() {
//        console.log("render project");

        const chartContext = {
            data: {
                mainQuery: this.state.queryContext.mainQuery,
                dateRange: this.state.queryContext.dateRange,
                variables: this.state.queryContext.variables,
                projectContext: this.projectContext,
            },
            func: {
                onQuery: this.onQuery,
                onUpdateDateRange: this.onUpdateDateRange,
                onDeleteChart: this.onDeleteChart,
                onShowChartEditor: this.onShowChartEditor,
                onDuplicateChart: this.onDuplicateChart,
                onMoveChart: this.onMoveChart,
                updateSocketSubscription: this.updateSocketSubscription,
            }
        };

        const queryContext = {
            data: {
                mainQuery: this.state.queryContext.mainQuery,
                dateRange: this.state.queryContext.dateRange,
                variables: this.state.queryContext.variables,
            },
            func: {
                onUpdateDateRange: this.onUpdateDateRange,
                onUpdateMainQuery: this.onUpdateMainQuery,
                onSaveVariables: this.onSaveVariables,
                onPreviousDateRange: this.onPreviousDateRange,
                onTestQuery: this.onTestQuery,
            }
        };

        const chartMap = {};
        this.state.charts.forEach(chart => {
            chartMap[chart.id] = chart;
        });

        const top_charts = this.state.chartOrder.top.map(id => chartMap[id]);
        const left_charts = this.state.chartOrder.left.map(id => chartMap[id]);
        const right_charts = this.state.chartOrder.right.map(id => chartMap[id]);
        const bottom_charts = this.state.chartOrder.bottom.map(id => chartMap[id]);

        const projectName = this.state.project ? this.state.project.name : "";

        let header = null;
        let topButtons = null;
        let button = null
        let dot = null;

        if (this.state.notifications.list.length > 0) {
            dot = <span className="dot"></span>;
        }

        if (this.mode == "project") {
            header = <Header user={window.user} logout overview lists feeds topics contact help/>;
            topButtons = (
                <div className="btn-row" style={{position: "absolute", right: "14px", top: "11px"}}>
                    <div>
                        <Button variant="primary" size="sm" onClick={() => this.onProjectActionDialog("publish")}><span className="fas fa-globe-americas"/></Button>
                    </div>
                    <div>
                        <Button variant="primary" size="sm" onClick={() => this.onProjectActionDialog("copy")}><span className="far fa-copy"/></Button>
                    </div>
                </div>
            );
            button = <Button className="btn-main" variant="primary" onClick={this.onShowChartEditor}>Add Chart</Button>;
        }
        else if (this.mode == "publish") {
            header = <Header login register contact help/>;
        }

        return (
            <Container>
                <ChartEditor mode={this.mode} show={this.state.editor.show} chart={this.state.editor.chart} onHide={this.onHideChartEditor} saveChart={this.saveChart}/>
                <Row>
                    <Col xs={12}>
                        <Row>
                            <Col xs={12}>
                                {header}
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12}>
                                {topButtons}
                                <h1 style={{textAlign: "center"}}>{projectName}</h1>
                                <QueryContext mode={this.mode} key={queryContext.mainQuery} context={queryContext} addAlerts={this.addAlerts}/>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12}>
                                <ChartList charts={top_charts} position="top" mode={this.mode} expanded={this.state.expanded} context={chartContext}/>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12} lg={6}>
                                <ChartList charts={left_charts} position="left" mode={this.mode} context={chartContext}/>
                            </Col>
                            <Col xs={12} lg={6}>
                                <ChartList charts={right_charts} position="right" mode={this.mode} context={chartContext}/>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12}>
                                <ChartList charts={bottom_charts} position="bottom" mode={this.mode} expanded={this.state.expanded} context={chartContext}/>
                            </Col>
                        </Row>
                        {button}
                    </Col>
                </Row>
                <Alerts alerts={this.state.alerts} setAlerts={alerts => this.setState({alerts: alerts})}/>
                <Confirm confirm={this.state.confirm} update={update => this.updateState(update, false)}/>
                <Dialog dialog={this.state.dialog} update={update => this.updateState(update, false)}/>
                <ProjectActionDialog {...this.state.projectAction} addAlerts={this.addAlerts} handleAction={this.handleProjectAction} onClose={()=>this.setState({projectAction: {show: false}})}/>
            </Container>
        );
    }
}