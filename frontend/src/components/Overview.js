import React from "react";
import { config, S } from "root/config.js";
import { Badge, Row, Col, Container, Label, Button, Modal, Table, Form } from "react-bootstrap";
import { Redirect } from "react-router-dom";
import Header from "./Header.js";
import { deepcopy, getCSRFToken } from "root/utils.js";
import { Confirm } from "./Controls.js";
import { event } from "root/event.js";

class LayoutTest extends React.Component {
    render() {
        const style = {
            height: 50,
            backgroundColor: "#FFAAAA",
            margin: 10,
            borderWidth: 5,
            borderColor: "#AAAAFF",
            borderStyle: "solid",
        };

        return (
            <Container>
                <Row>
                    <Col xs={6}>
                        <div style={{...style, margin: 0}}>A</div>
                    </Col>
                    <Col xs={6}>
                        <div style={{...style, margin: 0}}>B</div>
                    </Col>
                </Row>
                <Row>
                    <Col xs={6}>
                        <div style={{...style, padding: 50, margin: 20}}>C</div>
                    </Col>
                    <Col xs={6}>
                        <div style={{...style, padding: 50}}>D</div>
                    </Col>
                </Row>
                <Row>
                    <Col xs={6} xsOffset={3}>
                        <Button style={{width: "100%"}} >Button</Button>
                    </Col>
                    <Col xs={6}>
                        <Button style={{width: "50%", margin: "auto", display: "block"}}>Button</Button>
                    </Col>
                </Row>
                <Row>
                    <Col xs={6}>
                        <div style={{width: "50%", margin: "auto", display: "block"}}>
                            <p>Button</p>
                        </div>
                    </Col>
                </Row>

            </Container>
        );
    }
}

class Projects extends React.Component {
    render() {
        if (!this.props.projects || !this.props.projects.length) {
            return (
                <div style={{textAlign: "center", margin: "11px"}}>
                    No projects yet
                </div>
            );
        }

        const onRedirect = (event) => this.props.handleProject("redirect", event.target.getAttribute("data-project-id"));
        const onDelete = (event) => {
            const projectName = event.target.getAttribute("data-project-name");
            const projectId = event.target.getAttribute("data-project-id");

            this.props.confirm(S(`Delete project "${projectName}"`), () => {
                this.props.handleProject("delete", projectId);
            });
        }
        const onRename = (event) => this.props.handleProject("rename", event.target.getAttribute("data-project-id"));
        const onDuplicate = (event) => this.props.handleProject("duplicate", event.target.getAttribute("data-project-id"));

        const projects = this.props.projects.map(project => {
            return (
                <tr key={project.id}>
                    <td style={{width: "100%"}}>
                        <h5 data-project-id={project.id} onClick={onRedirect} className="hoverBold">{project.name}</h5>
                    </td>
                    <td>
                        <span style={{marginTop: "10px"}} className={`far ${project.type == "readonly" ? "fa-check-square" : "fa-square"}`} aria-hidden="true"></span>
                    </td>
                    <td>
                        <div style={{display: "flex", flexDirection: "row"}}>
                            <Button className="btn-icon" variant="outline-dark" data-project-id={project.id} onClick={onRename}>
                                <span className="far fa-edit" aria-hidden="true"/>
                            </Button>
                            <Button className="btn-icon" variant="outline-dark" data-project-id={project.id} onClick={onDuplicate}>
                                <span className="far fa-copy" aria-hidden="true"/>
                            </Button>
                            <Button className="btn-icon" variant="outline-dark" data-project-id={project.id} data-project-name={project.name} onClick={onDelete}>
                                <span className="far fa-trash-alt" aria-hidden="true"/>
                            </Button>
                        </div>
                    </td>
                </tr>
            );
        });

        return (
            <Table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Read only</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {projects}
                </tbody>
            </Table>
        );
    }
}

class Accounts extends React.Component {
    render() {
        if (!this.props.accounts.length) {
            return (
                <div style={{textAlign: "center", margin: "11px"}}>
                    No accounts connected yet
                </div>
            );
        }

//        const disconnect = account => {
//            this.props.confirm(S(`Disconnect account "${account.screen_name}"?`), () => {
//                this.props.handleAccount("disconnect", account.screen_name)
//            });
//        };
//
//        const remove = account => {
//            this.props.confirm(S(`Remove account "${account.screen_name}"?`), () => {
//                this.props.handleAccount("remove", account.screen_name)
//            });
//        };
//
//
//                    <td>
//                        <div style={{display: "flex", flexDirection: "row"}}>
//                            <Button className="btn-icon" variant="outline-dark" onClick={() => disconnect(account)}>
//                                <span className="fas fa-ban" aria-hidden="true"></span>
//                            </Button>
//                            <Button className="btn-icon" variant="outline-dark" onClick={() => remove(account)}>
//                                <span className="far fa-trash-alt" aria-hidden="true"></span>
//                            </Button>
//                        </div>
//                    </td>

        const accounts = this.props.accounts.map(account => {
            return (
                <tr key={account.screen_name}>
                    <td><span className={`far ${account.token ? "fa-check-square" : "fa-square"}`} aria-hidden="true"></span></td>
                    <td style={{width: "100%"}}><span>{account.screen_name}</span></td>
                </tr>
            );
        });

        return (
            <Table>
                <thead/>
                <tbody>
                    {accounts}
                </tbody>
            </Table>
        );

    }
}


class FeedsSummary extends React.Component {
    constructor(props) {
        super(props);

        this.typeMap = {
            'twitter_firehose': 'Search',
            'twitter_account': 'Timeline',
        }
    }

    render() {
//        console.log("summary", this.props.summary);

        if (!this.props.summary) {
            return (
                <div style={{textAlign: "center", margin: "11px"}}>
                    No feeds yet
                </div>
            );
        }

        const bodies = this.props.summary.types.map(item => {
            return (
                <tr key={item.type}>
                    <td>{this.typeMap[item.type]}</td>
                    <td>{item.count}</td>
                </tr>
            );
        });

        return (
            <div>
                <Table>
                    <thead>
                        <tr>
                            <td>Feed type</td>
                            <td>Count</td>
                        </tr>
                    </thead>
                    <tbody>
                        {bodies}
                    </tbody>
                </Table>
            </div>
        );
    }
}


class ListsSummary extends React.Component {
    constructor(props) {
        super(props);

        this.summaryLength = 20;

        this.state = {
            summaryCount: this.summaryLength
        }
    }

    render() {
        if (!this.props.summary) {
            return (
                <div style={{textAlign: "center", margin: "11px"}}>
                    No lists yet
                </div>
            );
        }

        let summary = [...this.props.summary];

        let summaryCount = this.state.summaryCount == null ? summary.length : this.state.summaryCount;
        const bodies = summary.slice(0, summaryCount).map(item => {
            return <Button className="btn-lightborder" variant="outline-dark" style={{margin: 2}} key={item.key}>{item.key} ({item.count})</Button>
        });

//        console.log(summary);

        let footer = null;
        if (summary.length > 0) {
//            const authorCount = summary.slice(this.summaryLength, summary.length).reduce((sum, item) => sum += item.count, 0);
//            const listCount = summary.length;
//
//            footer = (
//                <div>
//                    <p>{authorCount} authors in {listCount} other lists</p>
//                </div>
//            );

            if (summary.length > summaryCount) {
                footer = (
                    <Button className="btn-lightborder" variant="outline-dark" style={{margin: 2}} key={-1} onClick={() => {this.setState({summaryCount: null});}}>
                        <span className="fas fa-chevron-down"/>
                    </Button>
                );
            }
            else if (summary.length > this.summaryLength ) {
                footer = (
                    <Button className="btn-lightborder" variant="outline-dark" style={{margin: 2}} key={-1} onClick={() => {this.setState({summaryCount: this.summaryLength});}}>
                        <span className="fas fa-chevron-up"/>
                    </Button>
                );
            }

        }

        return (
            <div>
                {bodies}
                {footer}
            </div>
        );
    }
}

export default class Overview extends React.Component {
    constructor(props) {
        super(props);

        this.onCreateProject = this.onCreateProject.bind(this);
        this.onEditProject = this.onEditProject.bind(this);
        this.onChange = this.onChange.bind(this);
        this.updateProjectList = this.updateProjectList.bind(this);
        this.reloadSummaries = this.reloadSummaries.bind(this);
        this.handleProject = this.handleProject.bind(this);
        this.addTwitterAccount = this.addTwitterAccount.bind(this);
        this.reloadAccounts = this.reloadAccounts.bind(this);
        this.handleAccount = this.handleAccount.bind(this);
        this.confirm = this.confirm.bind(this);

        this.state = {
            modal: {
                show: false,
                mode: null,
                id: null,
                value: {
                    name: "",
                    query: "",
                    type: "project",
                }
            },
            redirect: null,
            projects: [],
            accounts: [],
            feedsSummary: null,
            listsSummary: null,
            confirm: {},
        }
    }

    componentDidMount() {
        this.updateProjectList();
        this.reloadAccounts();
        this.reloadSummaries();

        event("frontend.pageload", null, null, {page: "overview"});
    }

    onCancel(dialog) {
        if (dialog == "modal") {
            this.setState({modal: {...this.state.modal, show: false}});
        }
        else if (dialog == "connect") {
            this.setState({connectUrl: null});
        }
    }

    onCreateProject() {
//        console.log("overview onCreateProject", this.state.modal);
        fetch(`http://${config.api}/projects/`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "X-CSRFToken": getCSRFToken(),
            },
            body: JSON.stringify({
                name: this.state.modal.value.name,
                query: this.state.modal.value.query,
                type: this.state.modal.value.type,
            })
        })
        .then(() => {
            this.updateProjectList();
        });

        this.setState({modal: {...this.state.modal, show: false}});
    }

    onEditProject() {
        const url = `http://${config.api}/project/${this.state.modal.id}/`;

//        console.log("url", url);

        fetch(url, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                "X-CSRFToken": getCSRFToken(),
            },
            body: JSON.stringify({
                name: this.state.modal.value.name,
                query: this.state.modal.value.query,
                type: this.state.modal.value.type,
            })
        })
        .then(() => {
            this.updateProjectList();
        })
        .catch(error => console.log(error));

        this.setState({modal: {...this.state.modal, show: false}});
    }

    onChange(mode, event) {
//        console.log("onChange", mode, event);

        let modal = deepcopy(this.state.modal);

        if (mode == "readonly") {
            modal.value.type = event.target.checked ? "readonly" : "project";
        }
        else {
            modal.value[mode] = event.target.value;
        }

        this.setState({modal});
    }

    updateProjectList() {
        fetch(`http://${config.api}/projects/`)
        .then(response => response.json())
        .then(json => {
            this.setState({projects: json})
        });
    }

    handleProject(action, id) {
//        console.log("overview handleProject", action, id);

        if (action == "redirect") {
            this.setState({redirect: {project: {id: id}}});
        }
        else if (action == "rename") {
//            console.log("rename project", id);

            let project;
            for(let i=0; i<this.state.projects.length; i++) {
                if (this.state.projects[i].id == id) {
                    project = this.state.projects[i];
                    break;
                }
            }

            this.setState({modal: {show: true, mode: "edit", id: id, value: {name: project.name, query: project.query, type: project.type}}});
        }
        else if (action == "addProject") {
            this.setState({modal: {show: true, mode: "add", id: null, value: {name: "", query: "", type: "project"}}});
        }
        else if (action == "duplicate") {
            fetch(`http://${config.api}/project/duplicate/`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "X-CSRFToken": getCSRFToken(),
                },
                body: JSON.stringify({
                    id: id
                })
            })
            .then(() => {
                this.updateProjectList();
            });
        }
        else if (action == "delete") {
            fetch(`http://${config.api}/project/${id}`, {
                method: "DELETE",
                headers: {
                    "Content-Type": "application/json",
                    "X-CSRFToken": getCSRFToken(),
                },
            })
            .then(() => {
                this.updateProjectList();
            });
        }
    }

    addTwitterAccount() {
        fetch(`http://${config.api}/twitter/connect/`)
        .then(response => response.json())
        .then(json => {
//            console.log(json.url);

            let child = window.open(json.url, "Connect Twitter Account", "height=480,width=640");
            let timer = setInterval(() => checkChild(this.reloadAccounts), 500);

            function checkChild(callback) {
                if (child && child.closed) {
                    callback();
                    clearInterval(timer);
                }
            }
        });
    }

    reloadAccounts() {
        fetch(`http://${config.api}/twitter/accounts/`)
        .then(response => response.json())
        .then(json => {
            this.setState({accounts: json})
        });
    }

    reloadSummaries() {
        fetch(`http://${config.api}/feeds/summary/`)
        .then(response => response.json())
        .then(json => {
//            console.log("feedsSummary", json);

            this.setState({feedsSummary: json})
        });

        fetch(`http://${config.api}/lists/summary/`)
        .then(response => response.json())
        .then(json => {
            this.setState({listsSummary: json})
        });
    }

    handleAccount(action, screen_name) {
        if (action == "disconnect") {
            fetch(`http://${config.api}/twitter/disconnect/`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "X-CSRFToken": getCSRFToken(),
                },
                body: JSON.stringify({
                    "screen_name": screen_name,
                })
            })
            .then(this.reloadAccounts);
        }
        else if (action == "remove") {
            fetch(`http://${config.api}/twitter/remove/`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "X-CSRFToken": getCSRFToken(),

                },
                body: JSON.stringify({
                    "screen_name": screen_name,
                })
            })
            .then(this.reloadAccounts);
        }
    }

    redirectTo(where) {
        if (where == "feeds") {
            this.setState({redirect: {url: "/feeds"}});
        }
        else if (where == "lists") {
            this.setState({redirect: {url: "/lists"}});
        }
        else if (where == "topics") {
            this.setState({redirect: {url: "/topics"}});
        }
    }

    confirm(message, callback) {
        this.setState({confirm: {show: true, message: message, callback: callback}});
    }


    render() {
        if (this.state.redirect) {
            let url;

            if (this.state.redirect.url) {
                url = this.state.redirect.url;
            }
            else if (this.state.redirect.project) {
                url = `/project/${this.state.redirect.project.id}`;
            }
            return <Redirect to={url}/>;
        }

        const projects = <Projects projects={this.state.projects} confirm={this.confirm} handleProject={this.handleProject}/>;
        const accounts = <Accounts accounts={this.state.accounts} confirm={this.confirm} handleAccount={this.handleAccount}/>;
        const feedsSummary = <FeedsSummary summary={this.state.feedsSummary}/>;
        const listsSummary = <ListsSummary summary={this.state.listsSummary}/>;

        const modalTitle = this.state.modal.mode == "add" ? "New Project" : "Edit Project Name";
        const modalActionLabel = this.state.modal.mode == "add" ? "Create" : "Save";
        const modalAction = this.state.modal.mode == "add" ? this.onCreateProject : this.onEditProject;

        const modal = (
            <Modal show={this.state.modal.show ? true : false} onHide={() => this.onCancel("modal")}>
                <Modal.Header closeButton>
                    <Modal.Title>{modalTitle}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form.Label style={{marginTop: "auto", marginBottom: "auto"}}>Name</Form.Label>
                    <Form.Control type="text" placeholder="Project name..." value={this.state.modal.value.name} onChange={(event) => this.onChange("name", event)}/>
                    <Form.Label style={{marginTop: "12px", marginBottom: "auto"}}>Query</Form.Label>
                    <Form.Control type="text" placeholder="Project query..." value={this.state.modal.value.query} onChange={(event) => this.onChange("query", event)}/>
                    <Form.Group controlId="readonly">
                        <Form.Check type="checkbox" label="Read only" checked={this.state.modal.value.type == "readonly"} onChange={(event) => this.onChange("readonly", event)}/>
                    </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={modalAction}>{modalActionLabel}</Button>
                    <Button onClick={() => this.onCancel("modal")}>Cancel</Button>
                </Modal.Footer>
            </Modal>
        )

        return (
            <Container>
                {modal}
                <Row>
                    <Col sm={{span:8, offset: 2}}>
                        <Header user={window.user} logout project lists feeds topics contact help/>

                        <h2 style={{marginTop: 16}}>Projects</h2>
                        {projects}
                        <Button block variant="primary" onClick={() => this.handleProject("addProject")}>
                            <span>Add Project</span>
                        </Button>

                        <h2 style={{marginTop: 16}}>Accounts</h2>
                        {accounts}
                        <Button block variant="primary" onClick={this.addTwitterAccount}>
                            <span>Connect Twitter account</span>
                        </Button>

                        <h2 style={{marginTop: 16}}>Feeds</h2>
                        {feedsSummary}
                        <Button block variant="primary" onClick={() => this.redirectTo("feeds")}>
                            <span>Manage feeds</span>
                        </Button>

                        <h2 style={{marginTop: 16}}>Lists</h2>
                        {listsSummary}
                        <Button style={{marginTop: 2}} block variant="primary" onClick={() => this.redirectTo("lists")}>
                            <span>Manage lists</span>
                        </Button>
                    </Col>
                </Row>
                <Confirm confirm={this.state.confirm} update={update => this.setState({confirm: update})}/>
            </Container>
        );
    }
}