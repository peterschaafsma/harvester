import React from "react";
import { timeFormat } from "d3-time-format";
import { Table } from "react-bootstrap";
import { ChartControlButtons, Paginator } from "./Controls.js";
import ChartHeader from "./ChartHeader.js";
import { config, S } from "root/config.js";
import { getCSRFToken } from "root/utils.js";
import { countFormat } from "root/formatting.js";


class PopularityTable extends React.Component {
    constructor(props) {
        super(props);

        this.renderPage = this.renderPage.bind(this);
        this.fetchMoreItems = this.fetchMoreItems.bind(this);
        this.pageSize = this.pageSize.bind(this);
    }

    pageSize() {
//        console.log("pagesize", this.props.pageSize || 10);

        return parseInt(this.props.pageSize || 10);
    }

    renderPage(page) {
        const labelMap = {
            query: "words in query",
            time: "words in time frame",
            authors: "authors",
            related_authors: "reply/quote/share authors",
        }

        let label = S(labelMap[this.props.label]);
        if (this.props.cardinality != null) {
            label = `${label} (${this.props.cardinality})`;
        }

        let metric = "popularity";
        if (this.props.label.match(/authors|related_authors/)) {
            if (this.props.metric == "followers") {
                metric = "followers";
            }
            else {
                metric = "count";
            }
        }

        const head = (
            <thead>
                <tr>
                    <th>{label}</th>
                    <th>{S(metric)}</th>
                </tr>
            </thead>
        );

        const rows = page.map(item => {
            let dataHTML = item.key;
            if (this.props.label.match(/authors|related_authors/)) {
                const url = `https://twitter.com/${item.key}`;
                dataHTML = <a href={url} target="blank">@{item.key}</a>;
            }
            return (
                <tr key={item.key}>
                    <td>{dataHTML}</td>
                    <td>{countFormat(item[item.value_key || "doc_count"], 1)}</td>
                </tr>
            );
        });

        const body = (
            <tbody>
                {rows}
            </tbody>
        )

        let footer = null;
        if (page.length < this.pageSize()) {
            footer = (
                <p style={{textAlign: "center"}}>(no more items)</p>
            );
        }

        return (
            <div>
                <Table size="sm">
                    {head}
                    {body}
                </Table>
                {footer}
            </div>
        );

    }

    fetchMoreItems(offset, callback) {
//        console.log("popularitytable fetchMoreItems", offset, this.props.data);
        if (offset == 0) {
            callback(0, this.props.data);
        }
        else {
            callback(offset, []);
        }
    }

    render() {
//        console.log("popularitytable render", JSON.stringify(this.props))

        const key = `${id(this.props.data)}_${this.pageSize()}`;

        return (
            <Paginator
                key={key}
                pageSize={this.pageSize()}
                fetchMoreItems={this.fetchMoreItems}
                renderPage={this.renderPage}
            />
        );
    }
}

export default class PopularityChart extends React.Component {
    constructor(props) {
//        console.log("popularitychart constructor", JSON.stringify(props));
        super(props);

        this.fetchNewData = this.fetchNewData.bind(this);
        this.onNewData = this.onNewData.bind(this);
        this.shouldFetchNewData = this.shouldFetchNewData.bind(this);

        this.state = {
            popularityData: [],
        };
    }

    componentDidMount() {
        this.fetchNewData();
    }

    shouldFetchNewData(firstProps, secondProps) {
//        console.log("popularitychart shouldFetchNewData", JSON.stringify(firstProps), JSON.stringify(secondProps));

        let fetchNewData = false;
        if (firstProps.context.data.mainQuery != secondProps.context.data.mainQuery) {
            fetchNewData = true;
        }
        if (test(firstProps.context.data.dateRange, secondProps.context.data.dateRange)) {
            fetchNewData = true;
        }
        if (test(firstProps.context.data.variables, secondProps.context.data.variables)) {
            fetchNewData = true;
        }
        if (test(firstProps.chart, secondProps.chart)) {
//            console.log("popularitychart shouldFetchNewData chart");

            if (firstProps.chart.attribute != secondProps.chart.attribute) {
                fetchNewData = true;
            }
            if (firstProps.chart.metrice != secondProps.chart.metric) {
                fetchNewData = true;
            }
            if (firstProps.chart.background != secondProps.chart.background) {
                fetchNewData = true;
            }
            if (firstProps.chart.topics.length != secondProps.chart.topics.length) {
                fetchNewData = true;
            }
            else {
                for (let i=0; i<firstProps.chart.topics.length; i++) {
                    if (firstProps.chart.topics[i].query != secondProps.chart.topics[i].query) {
                        fetchNewData = true;

                        break;
                    }
                }
            }
        }
//        console.log("popularitychart shouldFetchNewData", fetchNewData);
        return fetchNewData;
    }

    shouldComponentUpdate(nextProps, nextState) {
//        console.log("popularitychart shouldComponentUpdate");
        if (this.shouldFetchNewData(this.props, nextProps)) {
            return true;
        }

        if (test(this.state.popularityData, nextState.popularityData)) {
            return true;
        }

        if (this.props.chart.title != nextProps.chart.title) {
//            console.log("piechart shouldComponentUpdate true line");
            return true;
        }
        if (this.props.chart.pageSize != nextProps.chart.pageSize) {
//            console.log("piechart shouldComponentUpdate true line");
            return true;
        }

        return false;
    }

    componentDidUpdate(prevProps, prevState) {
//        console.log("popularitychart shouldComponentUpdate");
        if (this.shouldFetchNewData(prevProps, this.props)) {
            this.fetchNewData();

            return false;
        }
    }


    fetchNewData() {
//        console.log("popularitychart fetchNewData", JSON.stringify(this.props));

        const context = {
            main_query: this.props.context.data.mainQuery,
            date_range: this.props.context.data.dateRange,
            variables: this.props.context.data.variables,
        };

        let queryData = {
            attribute: this.props.chart.attribute,
            metric: this.props.chart.metric,
            topics: {},
            context: context,
        };

        if (this.props.chart.attribute.match(/query|time/)) {
            queryData.background = this.props.chart.background;
        }

        this.setState({popularityData: []});

//        console.log("popularitychart fetchNewData queryData", JSON.stringify(queryData));
        this.props.chart.topics.forEach(topic => {
            queryData.topics[topic.id] = {id: topic.id, query: topic.query};
        });

//            console.log("popularitychart fetchNewData queryData", JSON.stringify(queryData));
        this.props.context.func.onQuery(this.onNewData, this.props.chart.id, queryData);
    }

    onNewData(data) {
//        console.log("popularitychart onNewData", JSON.stringify(data));

        this.setState({popularityData: data.popularity, cardinality: data.cardinality});
    }

    render() {
//        console.log("popularitychart render", this.state.cardinality);

        if (this.state.popularityData == null) {
            return null;
        }

        const context = {
            func: {
                fetchNewData: this.fetchNewData,
                ...this.props.context.func
            },
            data: {
                chart: this.props.chart,
                variables: this.props.context.data.variables,
                dateRange: this.props.context.data.dateRange,
            },
        }

        const plotId = `plot-${this.chartId}`;

        const header = <ChartHeader expanded={this.props.expanded} mode={this.props.mode} context={context}/>;

        const contents = <PopularityTable pageSize={this.props.chart.pageSize} label={this.props.chart.attribute} metric={this.props.chart.metric} data={this.state.popularityData} cardinality={this.state.cardinality}/>

        const result = (
            <div id={this.chartId} className="chart">
                {header}
                {contents}
            </div>
        );

        return result;
    }
}
