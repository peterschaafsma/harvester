import React from "react";
import { config } from "root/config.js";
import { select, mouse, event, touch, clientPoint } from "d3-selection";
import * as d3shape from "d3-shape";
import * as d3scale from "d3-scale";
import * as d3axis from "d3-axis";
import * as d3drag from "d3-drag";
import * as moment from "moment";
import { countFormat } from "root/formatting.js";
import { Button } from "react-bootstrap";
import Legend from "./Legend.js";
import ChartHeader from "./ChartHeader.js";
import { getIntervalObject, suggestInterval, getTickValuesAndFormat, intervalObjectMap } from "root/dateRange.js";
import * as d3transition from "d3-transition";


class LineChartTips extends React.Component {
    constructor(props) {
        super(props);

        this.svg = null;

        this.renderTips = this.renderTips.bind(this);
        this.createTip = this.createTip.bind(this);
        this.getDateRangeString = this.getDateRangeString.bind(this);
        this.mapVariables = this.mapVariables.bind(this);

        this.variablesMap = {};
        (this.props.variables || []).forEach(variable => {
            this.variablesMap[variable.name] = variable.value;
        });
    }

    componentDidMount() {
        this.svg = select(`#${this.props.svgId}`).select("svg");
        this.svg.append("g").attr("class", "tips");

        this.renderTips();
    }

    shouldComponentUpdate(nextProps, nextState) {
//        console.log("legend shouldComponentUpdate");
//        console.log("legend shouldComponentUpdate", JSON.stringify(this.props.data), JSON.stringify(nextProps.data));
//        console.log("legend shouldComponentUpdate", id(this.props.data), id(nextProps.data));
//        console.log("legend shouldComponentUpdate", this.props.data != nextProps.data);

        if (test(this.props.variables, nextProps.variables)) {
//            console.log("linecharttips shouldComponentUpdate true variables", nextProps.variables);

            this.variablesMap = {};
            (nextProps.variables || []).forEach(variable => {
                this.variablesMap[variable.name] = variable.value;
            });
        }

        return true;
    }

    componentDidUpdate() {
//        console.log("linecharttip componentDidUpdate", this.props.data);

        // we just rerender
        this.renderTips();
    }

    mapVariables(string) {
        let copy = `${string}`;
        Object.keys(this.variablesMap).forEach(key => {
            copy = copy.replace('${' + key + '}', this.variablesMap[key]);
        });

        return copy;
    }

    createTip(key, data) {
//        console.log("linecharttip createTip", this.props);

        const font = g => {
            g
                .attr("font-size", "28px")
                .attr("font-family", "'Forum', cursive")
                .style("alignment-baseline", "central");
        }

        const group = this.svg.select("g.tips")
            .append("g")
                .attr("class", "tip");

        const dateRangeString = this.getDateRangeString(data.dateRange);
        const dateRangeElement = group
            .append("text")
                .text(dateRangeString)
                .attr("pointer-events", "none")
                .call(font);
        const dateRangeBBox = dateRangeElement.node().getBBox();

        let valueElement = {};
        let labelElement = {};
        let valueBBox = {};
        let labelBBox = {};
        data.values.forEach((value, index) => {
            valueElement[value.id] = group
                .append("text")
                    .text(countFormat(value.count, 1))
                    .attr("pointer-events", "none")
                    .call(font);
            labelElement[value.id] = group
                .append("text")
                    .text(this.mapVariables(this.props.topicMap[value.id]))
                    .attr("pointer-events", "none")
                    .call(font);

            valueBBox[value.id] = valueElement[value.id].node().getBBox();
            labelBBox[value.id] = labelElement[value.id].node().getBBox();
        });

        //// add text to tip

        const barWidth = 60;

        const valueWidth = Math.max(...Object.values(valueBBox).map(bbox => barWidth + bbox.width));
        const labelWidth = Math.max(...Object.values(labelBBox).map(bbox => barWidth + bbox.width));

        const width = Math.max(120, dateRangeBBox.width, (valueWidth + labelWidth));

        const textHeight = Math.max(...Object.values(valueBBox).map(bbox => bbox.height))

        const height = dateRangeBBox.height + Object.keys(valueBBox).length * textHeight;

        let x = data.x;
        if (width + data.x > this.props.configuration.chart.v.w) {
            x = data.x - width;
        }

        let y = data.y - height;
        if (y < this.props.configuration.chart.m.t) {
            y = data.y;
        }

        group
            .attr("transform", `translate(${x}, ${y})`);

        const rect = group
            .append("rect")
                .attr("width", width)
                .attr("height", height)
                .attr("fill", "#FFFFFF")
                .attr("stroke", "#CCCCCC")
                .attr("stroke-width", 1);

        if (key == "null") {
            rect
                .attr("pointer-events", "none");
        }
        else {
            rect
                .on("mousedown", () => {event.stopPropagation();})
                .on("mouseup", () => {event.stopPropagation(); this.props.onRemoveTip(key);})
                .on("touchstart", () => {event.preventDefault(); event.stopPropagation();})
                .on("touchend", () => {event.preventDefault(); event.stopPropagation(); this.props.onRemoveTip(key);});
        }

        dateRangeElement
            .attr("transform", `translate(0, ${dateRangeBBox.height / 2})`)
            .raise();

        data.values.forEach((value, index) => {
            valueElement[value.id]
                .attr("transform", `translate(${width - valueBBox[value.id].width}, ${textHeight * (2 * index + 3) / 2})`)
                .raise();
            labelElement[value.id]
                .attr("transform", `translate(${barWidth + 4}, ${textHeight * (2 * index + 3) / 2})`)
                .raise();

            group
                .append("rect")
                    .attr("class", `bar field-${value.id}`)
                    .attr("width", barWidth)
                    .attr("height", textHeight)
                    .attr("transform", `translate(0, ${textHeight * (index + 1)})`)
                    .attr("pointer-events", "none");

            group
                .append("circle")
                    .attr("class", `circle field-${value.id}`)
                    .attr("cx", data.x - x)
                    .attr("cy", value.y - y);
        });
//
//        console.log("line", x, data.y + height, configuration.chart.v.h);
        group
            .append("line")
                .style("stroke", "#CCCCCC")
                .attr("x1", data.x - x)
                .attr("x2", data.x - x)
                .attr("y1", height - (data.y - y))
                .attr("y2", Math.max(...data.values.map(value => value.y)) - y);
    }

    getDateRangeString(dateRange) {
        let start = dateRange.start;
        let end = dateRange.end.subtract(1, "second");

        let formatEach = null;
        let formatAll = null;

        if (this.props.domain == "time_of_day") {
            formatEach = "HH:mm";
        }
        else if (this.props.domain == "day_of_week") {
            formatAll = "ddd";
            formatEach = "HH:mm";
        }
        else {
            const intervalObject = intervalObjectMap[dateRange.interval];
            if (intervalObject.seconds < 4* 3600) {
                formatEach = "HH:mm";
            }
            else if (intervalObject.seconds < 86400) {
                formatAll = "D/M";
                formatEach = 'HH:mm';
            }
            else if (intervalObject.seconds < 360 * 86400) {
                formatEach = "D/M";
            }
            else {
                formatEach = "YYYY";
            }
        }

        let dateRangeString = "";
        if (formatAll) {
            dateRangeString = start.format(formatAll);
        }

        const startString = start.format(formatEach);
        const endString = end.format(formatEach);
        if (startString != endString) {
//            console.log("both");
            dateRangeString += " " + startString + " - " + endString;
        }
        else {
//            console.log("single", startString, endString);
            dateRangeString += " " + startString;
        }
        return dateRangeString;
    }

    renderTips() {
        // complete rerender
        this.svg.select("g.tips").remove();

        if (this.props.data == null) {
            return;
        }

        this.svg.append("g").attr("class", "tips");

//        console.log("linecharttip renderTips", this.props.data);

        let tips = {...this.props.data.anchors};
        let keys = Object.keys(tips);
        if (this.props.data.hover) {
            tips["null"] = this.props.data.hover;
        }

        keys.sort((a, b) => {return tips[a].y < tips[b].y ? -1 : 1});
        if (this.props.data.hover) {
//            keys.unshift("null");
            keys.push("null");
        }

        keys.forEach(key => this.createTip(key, tips[key]));
    }

    render() {
//        console.log("linecharttip render", this.props.data);

        return null;
    }
}

class Plot extends React.Component {
    constructor(props) {
//        console.log("linechart plot constructor");

        super(props);

        this.svg = null;
        this.scale = {};

        this.state = {
        };

        this.mouse = {
            startPosition: {
                data: null,
                client: null,
            },
            currentPosition: {
                data: null,
                client: null,
            },
            currentRange: null,
            state: "hover"
        }

        this.updatePlot = this.updatePlot.bind(this);

        this.position = this.position.bind(this);
        this.transform = this.transform.bind(this);

        this.onMouseDown = this.onMouseDown.bind(this);
        this.onMouseMove = this.onMouseMove.bind(this);
        this.onMouseUp = this.onMouseUp.bind(this);
        this.onMouseLeave = this.onMouseLeave.bind(this);

        this.onTouchStart = this.onTouchStart.bind(this);
        this.onTouchMove = this.onTouchMove.bind(this);
        this.onTouchEnd = this.onTouchEnd.bind(this);

        this.getTipData = this.getTipData.bind(this);
    }

    componentDidMount() {
//        console.log("linechart plot componentDidMount");
        this.svg = select(`#${this.props.svgId}`).append("svg")
            .attr("viewBox", `0 0 ${this.props.configuration.chart.v.w} ${this.props.configuration.chart.v.h}`)
            .attr("preserveAspectRatio", "xMidYMid");

        this.svg
            .on("mousemove", () => this.onMouseMove("mouse"))
            .on("mouseleave", () => this.onMouseLeave("mouse"))
            .on("touchmove", this.onTouchMove)

            .on("mousedown", () => this.onMouseDown("mouse"))
            .on("mouseup", () => this.onMouseUp("mouse"))
            .on("touchstart", this.onTouchStart)
            .on("touchend", this.onTouchEnd);

        const plot = this.svg
            .append("g")
            .attr("class", "plot");

        plot.append("g").attr("class", "view");

        plot.append("g").attr("class", "axis xaxis")
            .attr("transform", `translate(0, ${this.props.configuration.chart.v.h - this.props.configuration.chart.m.b})`)
            .attr("pointer-events", "none");

        plot.append("g").attr("class", "axis yaxis")
            .attr("transform", `translate(${this.props.configuration.chart.m.l}, 0)`)
            .attr("pointer-events", "none");

        plot.call(this.updatePlot);
    }

    position(source, outType) {
//        console.log("position", source);
//
        if (outType == "viewport") {
            if (source == "mouse") {
                return mouse(this.svg.node());
            }
            else {
                return touch(this.svg.node(), event.changedTouches[0].identifier);
            }
        }

        if (outType == "data") {
            const viewportPosition = this.position(source, "viewport");

            return [
                this.scale.x.invert(viewportPosition[0]),
                this.scale.y.invert(viewportPosition[1])
            ];
        }

        if (outType == "client") {
            const viewportPosition = this.position(source, "viewport");

            const rect = this.svg.node().getBoundingClientRect();

            return [
                viewportPosition[0] * rect.width / this.props.configuration.chart.v.w,
                viewportPosition[1] * rect.height / this.props.configuration.chart.v.h,
            ]
        }
    }

    transform(inType, inData, outType) {
        // transform inData to viewport coordinates
        if (inType == "data") {
            inData = [
                this.scale.x(inData[0]),
                this.scale.y(inData[1]),
            ]
        }

        if (outType == "viewport") {
            return inData;
        }

        if (outType == "client") {
            const rect = this.svg.node().getBoundingClientRect();

//            console.log("rect", rect, inData, inType, outType);

            return [
                inData[0] * rect.width / this.props.configuration.chart.v.w,
                inData[1] * rect.height / this.props.configuration.chart.v.h,
            ]
        }
    }

    /*
    On mobile:
    hover: N/A
    click: (s=e): set tip
    drag horizontal: select range
    drag vertical: scroll

    On desktop:
    hover: move tip
    click: (s=e): set tip
    drag H: select range
    drag V: select range
    */

    onTouchStart() {
//        alert(["onTouchStart", this.position("touch", "data")]);
        this.onMouseDown("touch");
    }

    onTouchMove() {
        this.onMouseMove("touch");
    }

    onTouchEnd() {
        this.onMouseUp("touch");
    }

    onMouseDown(source) {
//        console.log("onMouseDown", this.props.mode);
        if (!this.props.data[0].data) {
            return;
        }

        this.props.setTipHover(null);

        this.mouse.startPosition.data = this.position(source, "data");
        this.mouse.currentPosition.data = [...this.mouse.startPosition.data];

        this.mouse.startPosition.client = this.position(source, "client");
        this.mouse.currentPosition.client = [...this.mouse.startPosition.client];

        if (this.props.mode == "project" && this.props.domain != "time_of_day" && this.props.domain != "day_of_week") {
            this.mouse.state = "trial";
        }
    }

    onMouseMove(source) {
//        console.log("onMouseMove");
        if (!this.props.data[0].data) {
            return;
        }

        this.mouse.currentPosition.client = this.position(source, "client");

        if (this.mouse.state == "trial") {
            const s = this.mouse.startPosition.client;
            const c = this.mouse.currentPosition.client;

            if (Math.abs(c[0] - s[0]) + Math.abs(c[1] - s[1]) >  20) {
                if (Math.abs(c[0] - s[0]) > Math.abs(c[1] - s[1])) {
//                    console.log("horizontal");
                    this.mouse.state = "range";

                    this.svg.on("mouseleave", null);
                    this.svg.on("mousemove", null);
                    this.svg.on("mouseup", null);

                    select(window).on("mousemove", () => this.onMouseMove("mouse"));
                    select(window).on("mouseup", () => this.onMouseUp("mouse"));

                    this.svg.append("g").attr("class", "select").append("rect")
                        .attr("y", this.props.configuration.chart.m.t)
                        .attr("height", this.props.configuration.chart.v.h - this.props.configuration.chart.m.t - this.props.configuration.chart.m.b)
                        .attr("opacity", 0.2)
                        .attr("fill", "#AAAAAA");
                }
                else {
//                    console.log("vertical");
                    this.mouse.state = "scroll";
                }
            }

            event.preventDefault();
        }

        if (this.mouse.state == "range") {
            event.preventDefault();
        }

//        console.log(this.mouse.state);


        let dataPosition = this.position(source, "data");
        if (dataPosition[0] < 0) {
            dataPosition[0] = 0;
        }
        if (dataPosition[0] > this.props.data[0].data.length - 1) {
            dataPosition[0] = this.props.data[0].data.length - 1;
        }

        // if this is the first mouse move event on drag
        if (this.mouse.state == "range") {
//            console.log(this.mouse.startPosition.data, this.mouse.currentPosition.data, dataPosition);
            this.mouse.currentPosition.data = dataPosition;
            if (dataPosition[0] > this.mouse.startPosition.data[0]) {
                this.mouse.currentRange = [
                    Math.floor(this.mouse.startPosition.data[0]),
                    Math.ceil(dataPosition[0])
                ]
            }
            else {
                this.mouse.currentRange = [
                    Math.floor(dataPosition[0]),
                    Math.ceil(this.mouse.startPosition.data[0])
                ]
            }

//            console.log("linechart currentRange", this.mouse.currentRange);

            this.svg.select("g.select").select("rect")
                .attr("x", this.scale.x(this.mouse.currentRange[0]))
                .attr("width", this.scale.x(this.mouse.currentRange[1]) - this.scale.x(this.mouse.currentRange[0]));
        }
        if (this.mouse.state == "hover") {
            dataPosition = [Math.round(dataPosition[0]), dataPosition[1]];

//            console.log("dataPosition", dataPosition);
            if (dataPosition[0] < 0) {
                dataPosition[0] = 0;
            }
            if (dataPosition[0] > this.props.data[0].data.length - 1) {
                dataPosition[0] = this.props.data[0].data.length - 1;
            }

            this.mouse.currentPosition.dataRounded = dataPosition;

            const values = this.getTipData(dataPosition[0]);

            //values.sort((a,b) => {return a.count > b.count ? -1 : 1});

//            console.log("values", values);

            const clientPosition = this.transform("data", dataPosition, "viewport");
            const interval = this.props.interval || this.props.dateRange.interval;
            const intervalObject = intervalObjectMap[interval];

            const start = moment(this.scale.key[dataPosition[0]]);
            const end = start.clone().add(...intervalObject.math);

//                console.log("dateRange", start, end);

            const dateRange = {start: start, end: end, interval: interval};

            this.props.setTipHover({
                x: clientPosition[0],
                y: clientPosition[1],

                values: values,

                dateRange: dateRange,
            });
        }
//        else {
//        }
//
//        event.preventDefault();
    }

    onMouseUp(source) {
//        console.log("onMouseUp");
        if (!this.props.data[0].data) {
            return;
        }
//
//        console.log("currentRange", this.mouse.currentRange, this.props.interval);
//
        if (this.mouse.state == "trial") {
            const dataPosition = [
                Math.round(this.mouse.currentPosition.data[0]),
                this.mouse.currentPosition.data[1]
            ];

            let values = this.getTipData(dataPosition[0]);
            // values.sort((a,b) => {return a.id < b.id ? -1 : 1});

//            console.log("values", values);

            const clientPosition = this.transform("data", dataPosition, "viewport");
            const interval = this.props.interval || this.props.dateRange.interval;
            const intervalObject = intervalObjectMap[interval];

            const start = moment(this.scale.key[dataPosition[0]]);
            const end = start.clone().add(...intervalObject.math);

//            console.log("dateRange", start, end);

            const dateRange = {start: start, end: end, interval: interval};

            this.props.addTipAnchor({
                x: clientPosition[0],
                y: clientPosition[1],

                values: values,

                dateRange: dateRange,
            });
        }

        if (this.mouse.state == "range") {
            this.svg.select("g.select").remove();

            this.svg.on("mouseleave", () => this.onMouseLeave("mouse"));
            this.svg.on("mousemove", () => this.onMouseMove("mouse"));
            this.svg.on("mouseup", () => this.onMouseUp("mouse"));

            select(window).on("mousemove", null);
            select(window).on("mouseup", null);

            const interval = this.props.interval || this.props.dateRange.interval;
            let math = intervalObjectMap[interval].math;

            const start = moment(this.scale.key[this.mouse.currentRange[0]]);

            let end = moment(this.scale.key[this.mouse.currentRange[1]]);
            end.add(math[0], math[1]);
            end.subtract(1, "second");

//            console.log("end", JSON.stringify(end));

            const range = {
                start: start,
                end: end,
            };

            const intervalObject = suggestInterval("aggregation", range);

            // make sure that the last point of the selected rang is fully included in the new range
            math = intervalObject.math ? intervalObject.math : [1, intervalObject.aggregation];
//            end.add(math[0], math[1]);

    //        console.log("interval", intervalObject);

    //        console.log("set interval", interval);
            this.props.onUpdateDateRange({start: range.start, end: range.end, name: null, interval: intervalObject.key});
        }
//
//        if (this.props.mode == "project" && this.props.domain != "time_of_day" && this.props.domain != "day_of_week") {
//            this.mouse.isActive = false;
//
//            this.svg.select("g.select").remove();
//        }
//
//        event.preventDefault();

        this.mouse.state = "hover";

    }

    onMouseLeave(source) {
        this.props.setTipHover(null);
    }

    getTipData(dataIndex) {
        var values = this.props.data.map(topic => {
            let value = {
                count: topic.data[dataIndex].count,
                id: topic.id
            };

            value["y"] = this.scale.y(value["count"]);

            return value;
        });

        values.sort((a, b) => {return a.count > b.count ? -1 : a.count == b.count ? 0 : 1});

        return values;
    }

    shouldComponentUpdate(nextProps, nextState) {
//        console.log("linechart plot shouldComponentUpdate", this.props.data, nextProps.data);
//        console.log("linechart plot shouldComponentUpdate", id(this.props.data), id(nextProps.data));

        let result = false;
        if( this.props.data != nextProps.data) {
            result = true;
        }
        else if (this.props.expanded != nextProps.expanded) {
            result = true;
        }

//        console.log("linechart plot shouldComponentUpdate", result);

        return result;
    }

    componentDidUpdate(prevProps, prevState) {
//        console.log("linechart plot componentDidUpdate", prevProps.expanded, this.props.expanded);

        if (prevProps.expanded != this.props.expanded) {
//            console.log("linechart new svg", this.props.configuration);
            this.svg.attr("viewBox", `0 0 ${this.props.configuration.chart.v.w} ${this.props.configuration.chart.v.h}`);
            this.svg.select("g.plot").select("g.xaxis").attr("transform", `translate(0, ${this.props.configuration.chart.v.h - this.props.configuration.chart.m.b})`);
            this.svg.select("g.plot").select("g.yaxis").attr("transform", `translate(${this.props.configuration.chart.m.l}, 0)`);
        }

        this.svg.select("g.plot").call(this.updatePlot);
    }

    updatePlot(g) {
//        console.log("linechart plot updatePlot", g, this.props.data);
        if (this.props.data[0].data == null) {
            return;
        }

        const keys = this.props.data[0].data.map(item => item.key);

        this.scale.key = {invert: {}};
        for(let i=0; i<keys.length; i++) {
            this.scale.key[i] = keys[i];
            this.scale.key.invert[keys[i]] = i;
        }

        const maxValue = Math.max(...this.props.data.map(data => data.data).flat().map(data => data.count));
        this.scale.x = d3scale.scaleLinear()
            .domain([0, keys.length - 1])
            .range([this.props.configuration.chart.m.l, this.props.configuration.chart.v.w - this.props.configuration.chart.m.r]);

        this.scale.y = d3scale.scaleLinear()
            .domain([0, maxValue])
            .range([this.props.configuration.chart.v.h - this.props.configuration.chart.m.b, this.props.configuration.chart.m.t]);

        const plot = g.select(".view").selectAll("path")
            .data(this.props.data);

        plot
            .enter()
            .append("path")
            .attr("class", topic => `line field-${topic.id}`)

        plot
            .exit()
            .remove();

        const curve = d3shape.line()
            .x(point => this.scale.x(this.scale.key.invert[point.key]))
            .y(point => this.scale.y(point.count))
            .curve(d3shape.curveMonotoneX);

        g.select(".view").selectAll("path")
            .transition()
            .duration(750)
            .attr("d", topic => curve(topic.data));

//        console.log("linechart updatePlot", this.props.domain);
        const tickValuesAndFormat = getTickValuesAndFormat(this.props.dateRange, this.props.interval, keys, this.props.expanded, this.props.domain);

        const xAxis = d3axis.axisBottom(this.scale.x)
            .tickValues(tickValuesAndFormat.values)
            .tickFormat(tickValuesAndFormat.format);

        const yAxis = d3axis.axisLeft(this.scale.y)
            .tickFormat(n => countFormat(n, 0));

        g.select(".xaxis").call(xAxis);
        g.select(".yaxis").call(yAxis);
    }

    render() {
//        console.log("linechart plot render");
        let style = {
        };
//        style.backgroundColor = "#EEEEEE";

        return <div style={style} id={this.props.svgId} className="plot"/>;
    }
}


export default class LineChart extends React.Component {
    constructor(props) {
        super(props);

        this.chartId = `chart-${this.props.chart.id}`;

        this.onNewData = this.onNewData.bind(this);
        this.setTipHover = this.setTipHover.bind(this);
        this.addTipAnchor = this.addTipAnchor.bind(this);
        this.fetchNewData = this.fetchNewData.bind(this);
        this.onRemoveTip = this.onRemoveTip.bind(this);

        this.shouldFetchNewData = this.shouldFetchNewData.bind(this);

        this.state = {
            legendData: null,
            lineData: null,
            tipData: null,
        };

//        console.log("linechart constructor", JSON.stringify(props));
    }

    componentDidMount() {
//        console.log("linechart componentDidMount");
        this.fetchNewData();
    }

    shouldFetchNewData(firstProps, secondProps) {
//        console.log("linechart shouldFetchNewData", JSON.stringify(firstProps), JSON.stringify(secondProps));

        let fetchNewData = false;
        if (firstProps.context.data.mainQuery != secondProps.context.data.mainQuery) {
//            console.log("linechart shouldFetchNewData mainQuery");
            fetchNewData = true;
        }
        if (test(firstProps.context.data.dateRange, secondProps.context.data.dateRange)) {
//            console.log("linechart shouldFetchNewData dateRange");
            fetchNewData = true;
        }
        if (test(firstProps.context.data.variables, secondProps.context.data.variables)) {
            fetchNewData = true;
        }
        if (test(firstProps.chart, secondProps.chart)) {
            if (firstProps.chart.interval != secondProps.chart.interval) {
//                console.log("linechart shouldFetchNewData chart interval");
                fetchNewData = true;
            }
            else if (firstProps.chart.metric != secondProps.chart.metric) {
//                console.log("linechart shouldFetchNewData chart interval");
                fetchNewData = true;
            }
            else if (firstProps.chart.domain != secondProps.chart.domain) {
//                console.log("linechart shouldFetchNewData chart interval");
                fetchNewData = true;
            }
            else if (firstProps.chart.topics.length != secondProps.chart.topics.length) {
//                console.log("linechart shouldFetchNewData chart topics length");
                fetchNewData = true;
            }
            else {
                for (let i=0; i<firstProps.chart.topics.length; i++) {
                    if (firstProps.chart.topics[i].query != secondProps.chart.topics[i].query) {
//                        console.log("linechart shouldFetchNewData chart topics query ", i);
                        fetchNewData = true;

                        break;
                    }
                }
            }
        }
        return fetchNewData;
    }

    shouldComponentUpdate(nextProps, nextState) {
//        console.log("linechart shouldComponentUpdate");

//        console.log("linechart shouldComponentUpdate chart");
//        console.log("linechart shouldComponentUpdate chart id", id(this.props.chart), id(nextProps.chart));
//        console.log(JSON.stringify(this.props.chart));
//        console.log(JSON.stringify(nextProps.chart));
//        console.log("linechart shouldComponentUpdate state");
//        console.log("linechart shouldComponentUpdate state id", id(this.state), id(nextState));
//        console.log(JSON.stringify(this.state));
//        console.log(JSON.stringify(nextState));

        // TODO: the problem now is that current props do not yet contain the current dateRange/mainQuery
        // that's why the refetch logic needs to be componentDidUpdate
        if (this.shouldFetchNewData(this.props, nextProps)) {
//            console.log("linechart shouldComponentUpdate true fetch");
            return true;
        }

        if (this.props.chart.title != nextProps.chart.title) {
//            console.log("linechart shouldComponentUpdate true line");
            return true;
        }

//        console.log("linechart expanded", this.props.expanded, nextProps.expanded);
        if (this.props.expanded != nextProps.expanded) {
//            console.log("linechart shouldComponentUpdate true expanded");
            return true;
        }

        for (let i=0; i<this.props.chart.topics.length; i++) {
            if (this.props.chart.topics[i].label != nextProps.chart.topics[i].label) {
                let legendData = [...this.state.legendData];
                legendData[i].label = nextProps.chart.topics[i].label

//                console.log("linechart shouldComponentUpdate true legend labels", i, legendData, nextProps.chart.topics);

                this.setState({legendData: legendData})

                return false;
            }
        }
        if (test(this.state.legendData, nextState.legendData)) {
//            console.log("linechart shouldComponentUpdate true legend");
            return true;
        }
        if (test(this.state.tipData, nextState.tipData)) {
//            console.log("linechart shouldComponentUpdate true tip");
            return true;
        }
        if (test(this.state.lineData, nextState.lineData)) {
//            console.log("linechart shouldComponentUpdate true line");
            return true;
        }

//        console.log("linechart shouldComponentUpdate false");
        return false;
    }

    componentDidUpdate(prevProps, prevState) {
//        console.log("linechart componentDidUpdate");
        // we need to distinguish between signalling a rerender and refetching data.
        // If the chart changed or mainQuery or dateRange, we refetch but do not rerender yet.
        // The rerender will eventuall trigger a change in lineData, legendData, which does cause a rerender.
        // In addition, any change to tipData also causes a rerender.
        // It is up to the plot and legend classes to ensure they don't redraw when just the tipData changes.
        if (this.shouldFetchNewData(prevProps, this.props)) {
//            console.log("linechart componentDidUpdate true fetch");
            this.fetchNewData();

            return false;
        }
    }

    fetchNewData() {
//        console.log("linechart fetchNewData");

        let intervalObject;
        if (this.props.chart.interval) {
            intervalObject = getIntervalObject(this.props.chart.interval);
        }
        else {
            intervalObject = getIntervalObject(this.props.context.data.dateRange.interval);
        }

        const context = {
            main_query: this.props.context.data.mainQuery,
            date_range: {
                start: this.props.context.data.dateRange.start,
                end: this.props.context.data.dateRange.end,
                interval: intervalObject.aggregation,
                interval_seconds: intervalObject.seconds,
                time_zone: this.props.context.data.dateRange.timeZone,
            },
            variables: this.props.context.data.variables,
            metric: this.props.chart.metric,
            domain: this.props.chart.domain,
        };

        let queryData = {
            context: context,
            topics: {},
        };
        for(let i=0; i<this.props.chart.topics.length; i++) {
            const topic = this.props.chart.topics[i];

            queryData.topics[topic.id] = {id: topic.id, query: topic.query}
        }
//        this.props.chart.topics.forEach(topic => {
//            queryData[topic.i] = {id: topic.id, query: topic.query, context: context};
//        });

//        console.log("linechart fetchNewData queryData", queryData);
        this.props.context.func.onQuery(this.onNewData, this.props.chart.id, queryData);
    }

    onNewData(data) {
//        console.log("linechart onNewData", data);

        if (data == null) {
            this.setState({
                lineData: null,
                legendData: null,
                tipData: null,
            });

            return;
        }

        let lineData = [];
        let legendData = [];
        let total = 0;

        const topicMap = this.props.chart.topics.reduce((dict, topic) => { dict[topic.id] = topic; return dict; }, {});

//        Object.keys(data).forEach(topicId => {
        this.props.chart.topics.forEach(topic => {
            lineData.push({
                id: topic.id,
                data: data[topic.id].data.volume,
            });

            legendData.push({
                index: legendData.length,
                id: topic.id,
                label: topic.label,
                total: data[topic.id].data.total,
            });

            total += data[topic.id].data.total;
        });

        legendData.forEach(item => {
            item.percentage = 100 * item.total / total;
        });

        this.setState({
            lineData: lineData,
            legendData: legendData,
            tipData: null,
        });
    }

    setTipHover(data) {
//        console.log("linechart setTipHover", data);
        let tipData;
        if (this.state.tipData == null) {
            tipData = {
                anchors: {},
                hover: null,
            }
        }
        else {
            tipData = {...this.state.tipData};
        }

        tipData.hover = data;

        this.setState({tipData: tipData});
    }
    
    addTipAnchor(data) {
//        console.log("linechart addTipAnchor", data);

        let tipData;
        if (this.state.tipData == null) {
            tipData = {
                anchors: {},
                hover: null,
            }
        }
        else {
            tipData = {...this.state.tipData};
        }

        if (Object.keys(tipData.anchors).length == 0) {
            tipData.anchors[0] = data;

//            console.log("new", tipData);

            this.setState({tipData: tipData});
        }
        else {
            const maxId = Math.max(...Object.keys(tipData.anchors));

            tipData.anchors[maxId + 1] = data;

//            console.log("next", tipData);

            this.setState({tipData: tipData});
        }
    }

    onRemoveTip(id) {
//        console.log("linechart onRemoveTip", id, this.state.tipData);
        let anchors = {...this.state.tipData.anchors};

        delete anchors[id];

        let tipData = {
            hover: this.state.tipData.hover,
            anchors: anchors
        };

        this.setState({tipData: tipData});
    }

    render() {
//        console.log("linechart render", this.props.expanded);
        const context = {
            func: {
                fetchNewData: this.fetchNewData,
                ...this.props.context.func,
            },
            data: {
                chart: this.props.chart,
                variables: this.props.context.data.variables,
                dateRange: this.props.context.data.dateRange,
            },
        };

//        console.log("linechart render", this.props.mode);

        const header = <ChartHeader mode={this.props.mode} expanded={this.props.expanded} context={context}/>;

        let plot = null;
        let legend = null;
        let tips = null;

        if (this.state.lineData != null && this.state.legendData != null) {
            const plotId = `plot-${this.chartId}`;
            const legendId = `legend-${this.chartId}`;

            const configuration = this.props.expanded ? config.expanded : config;
    //        console.log(plotId, legendId);

            const topicMap = this.props.chart.topics.reduce((dict, topic) => { dict[topic.id] = topic.label; return dict; }, {});

            plot = <Plot configuration={configuration} expanded={this.props.expanded} mode={this.props.mode} svgId={plotId} data={this.state.lineData} dateRange={this.props.context.data.dateRange} domain={this.props.chart.domain} interval={this.props.chart.interval} addTipAnchor={this.addTipAnchor} setTipHover={this.setTipHover} onUpdateDateRange={this.props.context.func.onUpdateDateRange}/>;
            legend = <Legend configuration={configuration} variables={this.props.context.data.variables} expanded={this.props.expanded} svgId={legendId} data={this.state.legendData}/>;
            tips = <LineChartTips configuration={configuration} variables={this.props.context.data.variables} svgId={plotId} data={this.state.tipData} topicMap={topicMap} domain={this.props.chart.domain} onRemoveTip={this.onRemoveTip}/>
        }
//        console.log("linechart render", this.state.tipData);

        const className = `chart ${this.props.expanded ? "expanded" : ""}`;

        const result = (
            <div id={this.chartId} className={className}>
                {header}
                <div style={{position: "relative"}}>
                    {plot}
                    {legend}
                    {tips}
                </div>
            </div>
        );

        return result;
    }
}