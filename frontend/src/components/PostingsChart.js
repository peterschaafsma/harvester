import React from "react";
import { event } from "d3-selection";
import { timeFormat } from "d3-time-format";
import { Badge, Col, Grid, Row, Button, Image } from "react-bootstrap";
import { ChartControlButtons, NavigationButtons } from "./Controls.js";
import ChartHeader from "./ChartHeader.js";
import { S } from "root/config.js";
import { countFormat } from "root/formatting.js";

class Posting extends React.Component {
    render() {
        const posting = this.props.posting;

        // time created
        const createdAt = new Date(posting.created_at);
        const now = new Date();

        const timeLocal = timeFormat("%H:%Mu")(createdAt);
        const nowDay = parseInt(timeFormat("%j")(now));
        const createdAtDay = parseInt(timeFormat("%j")(createdAt));

        let dayLocal;
        if (nowDay == createdAtDay) {
            dayLocal = "vandaag";
        }
        else if (nowDay == createdAtDay + 1) {
            dayLocal = "gisteren";
        }
        else {
            const options = {weekday: "short", year: "numeric", month: "short", day: "numeric"};
            dayLocal = createdAt.toLocaleDateString("nl-NL", options);
        }

        // element css class
        let className = "posting";
        posting.positions.forEach(position => {
            className = className + ` field-${position}`;
        });
        posting.topicIds.forEach(topicId => {
            className = className + ` topic-${topicId}`;
        });

        // image
        let image;
        if (posting.images) {
            image = (
                <div style={{margin: "2px"}}>
                    <Image className="posting-image" src={posting.images[0].thumbnail.url} rounded/>
                </div>
            );
        }

        // title
        let title;
        if (posting.title) {
            title = <div><h3>{posting.title}</h3></div>
        }

        // content
        let contentText = posting.content;
        let source;
        let relatedDiv = null;

//        console.log("related", posting.related, posting.related[0].relation, posting.related[0].content);

        const relatedStyle = {
            border: "1px solid #DDD",
            borderRadius: "7px",
            marginLeft: "20px",
            padding: "6px 2px 2px 6px",
        };

        let isValidRelatedPosting = false;
        if (posting.related) {
            const related = posting.related[0];
            const relation = related.relation;
            if (relation == "share" || relation == "quote") {
                const relationString = related.relation == "share" ? "shared:" : "quoted:";

                let relatedAuthor = null;
                if (related.author_name) {
                    relatedAuthor = <a href={related.author_url} target="blank">{related.author_name}</a>;
                }

                relatedDiv = (
                    <div style={relatedStyle}>
                        <div>
                            {relatedAuthor}
                        </div>
                        <div dangerouslySetInnerHTML={{__html: related.content}}/>
                    </div>
                );

//                console.log("posting, related", posting, related);

                source = (
                    <div style={{display: "inline"}}>
                        <a href={posting.author_url} target="blank">{posting.author_name}</a>
                        <span> {S(relationString)} </span>
                    </div>
                );

                isValidRelatedPosting = true;
            }
        }

        if (!isValidRelatedPosting) {
            source = <div style={{display: "inline", color: "#7fcdbb"}}><a href={posting.author_url} target="blank">{posting.author_name}</a></div>;
        }

        const content = <div dangerouslySetInnerHTML={{__html: contentText}}/>;

//        const updatedAt = new Date(posting.updated_at);
//        const updatedTimeLocal = timeFormat("%Y-%m-%d %H:%Mu")(updatedAt);

//        console.log("follower count", posting.follower_count);

        let followersCount = null;
        if (posting.followers_count != null && this.props.enriched) {
            followersCount = (
                <React.Fragment>
                    <span style={{marginLeft: 11}} className="fas fa-user" aria-hidden="true"/>
                    <span style={{marginLeft: 6}} >{countFormat(posting.followers_count, 1)}</span>
                </React.Fragment>
            );
        }

        const engagement = (
            <div style={{display: "inline", float: "right"}}>
                {followersCount}
                <span style={{marginLeft: 10}} className="far fa-heart" aria-hidden="true"/>
                <span style={{marginLeft: 5}} >{countFormat(posting.like_count, 1)}</span>
                <span style={{marginLeft: 11}} className="fas fa-retweet" aria-hidden="true"/>
                <span style={{marginLeft: 6}} >{countFormat(posting.share_count, 1)}</span>
                <span style={{marginLeft: 11}} className="fas fa-reply" aria-hidden="true"/>
                <span style={{marginLeft: 6}} >{countFormat(posting.reply_count, 1)}</span>
            </div>
        );

        // permalink
        const permalink = <div style={{display: "inline", float: "right"}}><a href={posting.permalink} target="blank">{dayLocal} {timeLocal}</a></div>

        const style = {margin: "5 0", borderRadius: 7, display: "flex", flexDirection: "row", justifyContent: "flex-start", border: "1px solid #DDDDDD"};
        const innerStyle = {width: "100%", margin: "5px", display: "flex", flexDirection: "column", overflow: "hidden"};
//
//        console.log("posting", posting);

        let lists = null;
        let identifier = null;
        if (this.props.enriched) {
            const badges = posting.lists.sort().map(list => {
                return (
                    <span key={list} style={{fontSize: "20px", marginRight: "4px"}}>
                        <Button size="sm" className="btn-lightborder btn-tiny-badge" variant="outline-dark">{list}</Button>
                    </span>
                );
            });

            lists = (
                <div style={{display: "flex", flexWrap: "wrap", marginBottom: "4px"}}>
                    {badges}
                </div>
            );

            identifier = (
                <div style={{float: "left"}}>
                    <span style={{fontSize: "13px"}}>id: {posting.id} </span>
                </div>
            )
        }

        return (
            <div className={className} style={style}>
                {image}
                <div style={innerStyle} className={"content" + (this.props.enriched || this.props.chartExpanded ? "-expanded" : "")}>
                    <div>
                        {source}
                        {engagement}
                    </div>
                    <div>
                        <div>
                            {lists}
                        </div>
                        {relatedDiv}
                        {title}
                        {content}
                        <div>
                            {identifier}
                            {permalink}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default class PostingsChart extends React.Component {
    constructor(props) {
        super(props);

        this.getPage = this.getPage.bind(this);
        this.makeMorePages = this.makeMorePages.bind(this);
        this.getNewData = this.getNewData.bind(this);
        this.onNewData = this.onNewData.bind(this);
        this.fetchNewData = this.fetchNewData.bind(this);
        this.enrich = this.enrich.bind(this);

        this.onPrevious = this.onPrevious.bind(this);
        this.onNext = this.onNext.bind(this);
        this.onFirst = this.onFirst.bind(this);
        this.onLast = this.onLast.bind(this);

        this.reset = this.reset.bind(this);
        this.pageSize = this.pageSize.bind(this);

        this.pagesPerFetch = 4;
        this.defaultPageSize = 5;

        this.compare = (a, b) => this.compareFields({field: "created_at", order: "desc"})

        this.state = this.reset();
    }

    reset(extraState) {
        this.topicData = {};
        Object.keys(this.props.chart.topics).forEach(topicId => {
            this.topicData[topicId] = {postings: [], nextId: 0, noMoreData: false};
        });

        this.pages = [];
        const state = {
            page: [],
            pageIndex: 0,
            ...(extraState || {}),
        };

        return state;
    }

    pageSize() {
        return this.props.chart.pageSize || this.defaultPageSize;
    }
    
    componentDidMount() {
        this.getNewData();
    }

    componentDidUpdate(prevProps) {
//        console.log("postingschart update");
//        console.log("prevProps", JSON.stringify(prevProps));
//        console.log("this.props", JSON.stringify(this.props));

        const newQuery = prevProps.context.data.mainQuery != this.props.context.data.mainQuery;
        const newVariables = prevProps.context.data.variables != this.props.context.data.variables;
        const newDateRange =
            (prevProps.context.data.dateRange.start !== this.props.context.data.dateRange.start) ||
            (prevProps.context.data.dateRange.end !== this.props.context.data.dateRange.end);

        let chartChanged = prevProps.chart.topics.length != this.props.chart.topics.length;
        chartChanged = chartChanged || (prevProps.chart.pageSize || this.defaultPageSize) != this.pageSize() || prevProps.chart.sorting != this.props.chart.sorting;
        if (!chartChanged) {
            for(let i=0; i<this.props.chart.topics.length; i++) {
                if (prevProps.chart.topics[i].query != this.props.chart.topics[i].query || prevProps.chart.topics[i].label != this.props.chart.topics[i].label) {
                    chartChanged = true;

                    break;
                }
            }
        }

//        console.log(newQuery, newDateRange, chartChanged);
        if (newQuery || newVariables || newDateRange || chartChanged) {
//            console.log("postingschart change");
            this.setState(this.reset());

            this.getNewData();
        }
        else {
//            console.log("postingschart no change");
        }

    }

    onPrevious() {
        if (this.state.pageIndex > 0) {
            this.setState({
                pageIndex: this.state.pageIndex - 1,
                page: this.getPage(this.state.pageIndex - 1),
            })
        }
    }

    onNext() {
        if (this.state.page.length == this.pageSize()) {
            this.setState({
                pageIndex: this.state.pageIndex + 1,
                page: this.getPage(this.state.pageIndex + 1),
            });
        }
    }

    onFirst() {
        this.setState({
            pageIndex: 0,
            page: this.getPage(0),
        })
    }

    onLast() {
        this.setState({
            pageIndex: this.pages.length-1,
            page: this.getPage(this.pages.length-1),
        })
    }

    getPage(pageIndex) {
        if (pageIndex < this.pages.length) {
            return this.pages[pageIndex];
        }
        else {
            this.getNewData();

            return [];
        }
    }

    compareFields(clauses, a, b) {
//        console.log("postingschart compare", a, b);

        for(let i=0; i<clauses.length; i++) {
            const clause = clauses[i];

            if (a[clause.field] == b[clause.field]) {
                continue;
            }

            return (a[clause.field] < b[clause.field]) == (clause.order == "desc") ? -1 : 1;
        }

        return 0;
    }

    makeMorePages() {
//        console.log("make pages");

        // get the topic IDs
        const topicIds = Object.keys(this.topicData);

        // some initialization
        let page = [];
        let nextId = {};
        let lastPosting = null;

        // what's the index of the next posting to be added to the chart, by topic
        // we need to do this by topic in case the next posting is actually from topic
        // A when we have no more of those is this batch, while we do have plenty more
        // for topic B.
        // we also store the current nextIDs in a temporary variable, in case we don't
        // complete a page and we nede to roll back
        topicIds.forEach(topicId => {
            nextId[topicId] = this.topicData[topicId].nextId;
        });

        outer:
        while (true) {
            // do we have a next posting, or do we need to fetch more data
            let nextTopicId = null;
            let nextPosting = null;

            // check all topics, to see from which topic we should add the next posting
            inner:
            for(let i=0; i<topicIds.length; i++) {
                const topicId = topicIds[i];
                const topicData = this.topicData[topicId];

                // if we've reached the end for any topic, we need to fetch new data, so break outer and stop making pages
                if (topicData.nextId == topicData.postings.length) {
                    if (topicData.noMoreData) {
                        continue inner;
                    }
                    else {
                        break outer;
                    }
                }

                // otherwise check if the posting for this topic should be inserted before the others.
                if(nextTopicId === null || this.compare(topicData.postings[topicData.nextId], nextPosting) > 0) {
                    nextTopicId = topicId;
                    nextPosting = topicData.postings[topicData.nextId];
                }
            };

            // do we have a next posting?
            if (nextPosting) {
                // incement the index for this topic
                this.topicData[nextTopicId].nextId++;

                // postings could be returned by multiple topic queries, but we assume
                // the sorting to have them together. We skip postings as long as
                // the posting IDs are the same when it comes to adding them to the chart
                // but we do need to merge topic and position (color) ids, for highlighting
                if (lastPosting !== null && nextPosting.id == lastPosting.id) {
                    lastPosting.positions.push(nextPosting.positions[0]);
                    lastPosting.topicIds.push(nextPosting.topicIds[0]);
                }
                // add the posting to the current page we're building
                else {
                    page.push(nextPosting);

                    lastPosting = nextPosting;
                }

                // if the page we're working on is long enough, or we have no more data,
                // create the page
                if (page.length == this.pageSize()) {
                    // if this is actually the page we're supposed to be showing now, do it.
                    if (this.state.pageIndex == this.pages.length) {
                        this.setState({page: page});
                    }

                    // store the page
                    this.pages.push(page);

                    // start a new page
                    page = [];

                    // Fine, we completed a page, so we can update the rollback data
                    topicIds.forEach(topicId => {
                        nextId[topicId] = this.topicData[topicId].nextId;
                    });
                }
            }
            // if no new posting, we've gone as far we can with creating pages
            else {
                break;
            }
        }

        // if we stopped because no more postings anywhere, we do create and show the page
        let noMoreData = true;
        topicIds.forEach(topicId => {
            if (!this.topicData[topicId].noMoreData) {
                noMoreData = false;
            }
        })

        if (noMoreData) {
            if (this.state.pageIndex == this.pages.length) {
                this.setState({page: page});
            }

            // store the page
            this.pages.push(page);

            // a bit of a formality, but start a new page
            page = [];

            // Also update the rollback data
            topicIds.forEach(topicId => {
                nextId[topicId] = this.topicData[topicId].nextId;
            });
        }
        else {
            // roll back to the last complete page nextIDs
            topicIds.forEach(topicId => {
                this.topicData[topicId].nextId = nextId[topicId];
            });
        }

//        console.log(`Total pages: ${this.pages.length}`);
    }

    fetchNewData() {
//        console.log("postingschart fetchNewData");

        this.setState(this.reset());
        this.getNewData();
    }

    getNewData() {
//        console.log("get new data");

        const context = {
            main_query: this.props.context.data.mainQuery,
            date_range: this.props.context.data.dateRange,
            variables: this.props.context.data.variables,
        };

        let queryData = {
            context: context,
            topics: {}
        };
        Object.keys(this.props.chart.topics).forEach(topicId => {
            const query = this.props.chart.topics[topicId].query;

            queryData.topics[topicId] = {
                id: topicId,
                query: query,
                count: this.pagesPerFetch * this.pageSize(),
            };

            queryData.topics[topicId].sort = this.props.chart.sorting;
            if (this.topicData[topicId].postings.length > 0) {
                const posting = this.topicData[topicId].postings.slice(-1)[0];

                let postingData = {
                    created_at: posting.created_at
                };

                if (this.props.chart.sorting == 'likes') {
                    postingData.like_count = posting.like_count;
                }
                else if (this.props.chart.sorting == 'shares') {
                    postingData.share_count = posting.share_count;
                }
                else if (this.props.chart.sorting == 'replies') {
                    postingData.reply_count = posting.reply_count;
                }
                else if (this.props.chart.sorting == 'followers') {
                    postingData.followers_count = posting.followers_count;
                }

                queryData.topics[topicId].sort_boundary = postingData;
            }
        });

//        console.log("queryData", queryData);

        this.props.context.func.onQuery(this.onNewData, this.props.chart.id, queryData);
    }

    onNewData(data) {
//        console.log("receive new data", data);
        Object.keys(data).forEach(topicId => {
            const postings = data[topicId].data.postings.map(posting => {return {positions: [this.props.chart.topics[topicId].position], topicIds: [topicId], ...posting}});

            this.topicData[topicId].postings = this.topicData[topicId].postings.concat(postings);
            if (postings.length < this.pagesPerFetch * this.pageSize()) {
                this.topicData[topicId].noMoreData = true;
            }
        });


        if (this.props.chart.sorting == "newest_first") {
            this.compare = (a, b) => this.compareFields([{field: "created_at", order: "desc"}], a, b);
        }
        else if (this.props.chart.sorting == "oldest_first") {
            this.compare = (a, b) => this.compareFields([{field: "created_at", order: "asc"}], a, b);
        }
        else if (this.props.chart.sorting == "likes") {
            this.compare = (a, b) => this.compareFields([{field: "like_count", order: "desc"}, {field: "created_at", order: "desc"}], a, b);
        }
        else if (this.props.chart.sorting == "shares") {
            this.compare = (a, b) => this.compareFields([{field: "share_count", order: "desc"}, {field: "created_at", order: "desc"}], a, b);
        }
        else if (this.props.chart.sorting == "replies") {
            this.compare = (a, b) => this.compareFields([{field: "reply_count", order: "desc"}, {field: "created_at", order: "desc"}], a, b);
        }
        else if (this.props.chart.sorting == "followers") {
            this.compare = (a, b) => this.compareFields([{field: "followers_count", order: "desc"}, {field: "created_at", order: "desc"}], a, b);
        }

        this.makeMorePages();
    }

    enrich() {
        this.setState({enriched: !this.state.enriched});
    }

    render() {
        const postings = this.state.page.map(posting => <Posting enriched={this.state.enriched} chartExpanded={this.props.expanded} posting={posting} key={posting.id}/>);

        let footer = null;
        if (this.state.page.length < this.pageSize()) {
            footer = <p style={{textAlign: "center", marginBottom: "20px"}}>(no more items)</p>;
        }

        const context = {
            func: {
                ...this.props.context.func,
                updateChart: () => {this.setState(this.reset({pageIndex: this.state.pageIndex})); this.getNewData()},
                fetchNewData: this.fetchNewData,
                enrich: this.enrich,
            },
            data: {
                chart: this.props.chart,
                variables: this.props.context.data.variables,
                dateRange: this.props.context.data.dateRange,
            },
        };

        const canPrevious = this.state.pageIndex != 0;
        const canNext = this.state.page.length == this.pageSize();

//        console.log("can", canPrevious, canNext, this.state.pageIndex, this.state.page.length, this.pageSize());

        const navigationButtons = (
            <NavigationButtons
                canPrevious={canPrevious}
                canNext={canNext}

                onNext={this.onNext}
                onPrevious={this.onPrevious}
                onFirst={this.onFirst}
                onLast={this.onLast}
                pageIndex={this.state.pageIndex}
            />
        );

        const header = <ChartHeader expanded={this.props.expanded} mode={this.props.mode} hasEnrich context={context}/>;

        return (
            <div>
                {header}
                {postings}
                {footer}
                {navigationButtons}
            </div>
        )
    }
}
