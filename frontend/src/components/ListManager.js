import React from "react";
import {Button, Form, Table, Fragment} from "react-bootstrap";
import {Col, Container, Row, Image} from "react-bootstrap";
import { config, S } from "root/config.js";
import { Alerts, FinderControl, Paginator } from "./Controls.js";
import Header from "./Header.js";
import { getCSRFToken } from "root/utils.js";
import TwitterAuthor from "./TwitterAuthor.js";
import { AsyncTypeahead, Typeahead } from 'react-bootstrap-typeahead';
import { intersection, union, difference } from "root/utils.js";
import { event } from "root/event.js";


export default class ListManager extends React.Component {
    constructor(props) {
        super(props);

        this.onNewData = this.onNewData.bind(this);
        this.onClick = this.onClick.bind(this);
//        this.getOptions = this.getOptions.bind(this);
        this.toggleAuthors = this.toggleAuthors.bind(this);
//        this.selectHighlightedAuthors = this.selectHighlightedAuthors.bind(this);
        this.changeAuthorSelection = this.changeAuthorSelection.bind(this);
        this.onChange = this.onChange.bind(this);
        this.save = this.save.bind(this);
        this.reload = this.reload.bind(this)

        this.state = {
            lists: {},  // all lists
            filteredLists: {},

            listsCollapsed: true,  // is list view collapsed

            authors: {},  // all authors

            filterOptions: [],  // top search typeahead options (lists)

            selectedLists: [],  // selected lists
            selectedListsAuthors: [],   // intersection of authors in selected lists

            highlightedAuthors: [],  // authors selected in intersection list
            selectedAuthors: [],

            commonLists: [],  // lists common to all selected authors
            otherLists: [],  // other lists

            alerts: [],
        }

        this.listsShownCount = 20;
    }

    componentDidMount() {
        event("frontend.pageload", null, null, {page: "listmanager"});

        this.reload();
    }

    reload() {
        fetch(`http://${config.api}/authors/`)
        .then(response => response.json())
        .then(this.onNewData);
    }

    onNewData(data) {
        const lists = {};
        const authors = {};

        for(let i=0; i<data.authorList.length; i++) {
            const author = data.authorList[i];

            authors[author.screen_name] = author;

            for(let j=0; j<author.lists.length; j++) {
                const list = author.lists[j];

                if (!(list in lists)) {
                    lists[list] = [];
                }

                lists[list].push(author.screen_name);
            }
        }

        const filterOptions = Object.keys(lists).sort();

        this.setState({lists: lists, authors: authors, filterOptions: filterOptions});

        this.onChange("filter", this.state.selectedLists);
    }

    onClick(mode, key) {
        if (mode == "list") {
            let selectedLists = [...this.state.selectedLists];
            const index = this.state.selectedLists.indexOf(key);
            if (index == -1) {
                selectedLists.push(key);
            }
            else {
                selectedLists.splice(index, 1);
            }

            this.onChange("filter", selectedLists);
        }
    }

    toggleAuthors(authors) {
        let selectedAuthors = [...this.state.selectedAuthors];
        for (let i=0; i<authors.length; i++) {
            const author = authors[i];

            const index = selectedAuthors.indexOf(author);
            if (index == -1) {
                selectedAuthors.push(author);
            }
            else {
                selectedAuthors.splice(index, 1);
            }
        }

        this.changeAuthorSelection(selectedAuthors);
    }

    changeAuthorSelection(selectedAuthors) {
//        console.log("listmanager changeAuthorSelection", selectedAuthors);

        const lists = selectedAuthors.map(author => this.state.authors[author].lists);

        const commonLists = intersection(lists);
        const otherLists = difference(union(lists), commonLists);

        this.setState({selectedAuthors, commonLists, otherLists})
    }

    onChange(mode, selected) {
//        console.log("selected", selected);
        selected = selected.map(item => item.customOption ? item.label.toLowerCase() : item);
        if (mode == "filter") {
            const selectedLists = selected;

            let selectedListsAuthors;
            if (selected.length == 0) {
                selectedListsAuthors = Object.keys(this.state.authors).filter(author => this.state.authors[author].lists.length == 0);
            }
            else {
                selectedListsAuthors = intersection(selectedLists.map(list => this.state.lists[list]));
            }

            let filteredLists = {};
            if (selectedLists.length == 0) {
                filteredLists = this.state.lists;
            }
            else {
                for (let i=0; i<selectedListsAuthors.length; i++) {
                    const author = this.state.authors[selectedListsAuthors[i]];
                    for (let j=0; j<author.lists.length; j++) {
                        const list = author.lists[j];
                        if (!(list in filteredLists)) {
                            filteredLists[list] = [];
                        }
                        filteredLists[list].push(author);
                    }
                }
            }

            const manualSelectedAuthors = difference(this.state.selectedAuthors, this.state.selectedListsAuthors);

            const selectedAuthors = union([manualSelectedAuthors, intersection([this.state.selectedAuthors, selectedListsAuthors])]);

            this.setState({filteredLists, selectedLists, selectedListsAuthors, selectedAuthors});

        }
        else if (mode == "common") {
            this.setState({commonLists: selected});
        }
        else if (mode == "other") {
            this.setState({otherLists: selected});
        }
    }

    save() {
//        console.log("save");
        fetch(`http://${config.api}/authors/`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                'X-CSRFToken': getCSRFToken(),
            },
            body: JSON.stringify({
                authors: this.state.selectedAuthors,
                common_lists: this.state.commonLists,
                other_lists: this.state.otherLists,
            })
        })
        .then(response => response.json())
        .then(json => {
//            console.log("json", json);

            this.setState({alerts: json.alerts});
            this.reload();
        });
    }

    render() {
        const options = Object.keys(this.state.lists).concat(Object.keys(this.state.authors)).sort();

        const finderControl = (
            <Typeahead
                onChange={selected => this.onChange("filter", selected)}
                options={this.state.filterOptions}
                multiple={true}
                clearButton={true}
                placeholder="Select lists..."
                selected={this.state.selectedLists}
            />
        );

        let lists = Object.keys(this.state.filteredLists).sort((a, b) => {return this.state.filteredLists[a].length < this.state.filteredLists[b].length ? 1 : -1;});
        let expandCollapse = null;
        if (lists.length > this.listsShownCount) {
            if (this.state.listsCollapsed) {
                lists = lists.slice(0, this.listsShownCount);
                expandCollapse = (
                    <Button className="btn-lightborder" variant="outline-dark" style={{margin: "2px"}} key={-1} onClick={() => {this.setState({listsCollapsed: false});}}>
                        <span className="fas fa-chevron-down"/>
                    </Button>
                );
            }
            else {
                expandCollapse = (
                    <Button className="btn-lightborder" variant="outline-dark" style={{margin: "2px"}} key={-1} onClick={() => {this.setState({listsCollapsed: true});}}>
                        <span className="fas fa-chevron-up"/>
                    </Button>
                );
            }
        }

//        console.log("lists", this.state.filteredLists);

        const listsSection = (
            <div style={{display: "flex", flexWrap: "wrap", margin: "0 -2px"}}>
                {lists.map(list => {
                    const buttonLabel = `${list} (${this.state.filteredLists[list].length})`;
                    let buttonVariant = "outline-dark";
                    let buttonClass = "btn-lightborder";

                    if (this.state.selectedLists.indexOf(list) >= 0) {
                        buttonVariant = "outline-primary";
                        buttonClass = "";
                    }

                    return (
                        <Button style={{margin: "2px"}} variant={buttonVariant} className={buttonClass} key={list} onClick={() => this.onClick("list", list)}>
                            {buttonLabel}
                        </Button>)}
                    )
                }
                {expandCollapse}
            </div>
        );

        const renderAuthors = page => {
            const rows = page.map(author => {
                return (
                    <TwitterAuthor
                        key={author}
                        as="tableRow"
                        selected={this.state.selectedAuthors.indexOf(author) > -1}
                        author={this.state.authors[author]}
                        onClick={() => this.toggleAuthors([author])}/>
                )
            });

            return (
                <Table size="sm">
                    <thead>
                        <tr>
                            <th/>
                            <th>Screen name</th>
                            <th>Name</th>
                            <th>Follower count</th>
                            <th/>
                        </tr>
                    </thead>
                    <tbody>
                        {rows}
                    </tbody>
                </Table>
            );
        }

        const listAuthorsSection = (
            <Paginator
                key={this.state.selectedListsAuthors.join('_')}
                pageSize={5}
                selected={this.state.selectedAuthors}
                fetchMoreItems={(offset, callback) => callback(offset, this.state.selectedListsAuthors.slice(offset, this.state.selectedListsAuthors.length))}
                renderPage={renderAuthors}
            />
        )

        const authors = this.state.selectedAuthors.sort();

        const selectedAuthorsSection = (
            <Typeahead
                selected={authors}
                multiple={true}
                clearButton={true}
                placeholder="Select authors..."
                options={Object.keys(this.state.authors)}
                onChange={this.changeAuthorSelection}
            />
        )

        const commonListsSection = (
            <Typeahead
                selected={this.state.commonLists}
                multiple={true}
                allowNew={true}
                placeholder="Common lists..."
                options={Object.keys(this.state.lists)}
                onChange={selected => this.onChange("common", selected)}
            />
        )

        const otherListsSection = (
            <Typeahead
                className="rbt-dont-edit"
                selected={this.state.otherLists}
                multiple={true}
                placeholder="Select lists..."
                options={Object.keys(this.state.lists)}
                onChange={selected => this.onChange("other", selected)}
            />
        )

        return (
            <Container>
                <Row>
                    <Col md={12} lg={{span: 10, offset: 1}}>
                        <Header user={window.user} logout overview project feeds topics help contact/>
                        <h1>List Manager</h1>
                        <h2>1. Explore</h2>
                        {finderControl}
                        {listsSection}
                        <h3>Authors in lists</h3>
                        {listAuthorsSection}
                        <div className="btn-row" style={{marginTop: "16px"}}>
                            <div>
                                <Button variant="outline-primary" className="btn-label" onClick={() => this.toggleAuthors(intersection([this.state.selectedAuthors, this.state.selectedListsAuthors]))}>
                                    <span>{S("select none")}</span>
                                </Button>
                            </div>
                            <div>
                                <Button variant="outline-primary" className="btn-label" onClick={() => this.toggleAuthors(this.state.selectedListsAuthors.filter(author => this.state.selectedAuthors.indexOf(author) == -1))}>
                                    <span>{S("select all")}</span>
                                </Button>
                            </div>
                            <div>
                                <Button variant="outline-primary" className="btn-label" onClick={() => this.toggleAuthors(this.state.selectedListsAuthors)}>
                                    <span>{S("invert selection")}</span>
                                </Button>
                            </div>
                        </div>
                        <h2 style={{marginTop: "24px"}}>2. Update</h2>
                        <h3>Selected authors</h3>
                        {selectedAuthorsSection}
                        <h3>Common lists</h3>
                        {commonListsSection}
                        <h3>Other lists</h3>
                        {otherListsSection}
                        <Button  variant="primary" className="btn-main" onClick={this.save}>
                            <span>{S("save")}</span>
                        </Button>
                        <Alerts alerts={this.state.alerts} setAlerts={alerts => this.setState({alerts: alerts})} timeout={5000}/>
                    </Col>
                </Row>
            </Container>
        );
    }
}