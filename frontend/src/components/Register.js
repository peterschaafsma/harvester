import React from "react";
import {Col, Container, Row, Form, Button} from "react-bootstrap";
import {config} from "root/config.js";
import { getCSRFToken } from "root/utils.js";
import { TimeoutAlert } from "./Controls.js";
import { Redirect } from "react-router-dom";
import Header from "./Header.js";
import queryString from 'query-string';
import { event } from "root/event.js";

export default class Register extends React.Component {
    constructor(props) {
        super(props);

//        console.log("Register props", props);

        this.onClick = this.onClick.bind(this);
        this.onChange = this.onChange.bind(this);
        this.removeAlert = this.removeAlert.bind(this);
        this.onKeyPress = this.onKeyPress.bind(this);
        this.submit = this.submit.bind(this);
        this.validateInput = this.validateInput.bind(this);

        this.state = {
            input: {
                username: "",
                password: "",
                passwordConfirm: "",
                emailAddress: "",
            },
            alerts: {
                index: 0,
                list: [],
            },
            redirect: null,
        }
    }

    componentDidMount() {
        const values = queryString.parse(this.props.location.search);

        if (values.alert == 'no_pending_confirmation') {
            alert = {type: 'warning', message: 'The confirmation link was invalid', index: this.state.alerts.index};

            this.setState({alerts: {list: [alert], index: alert.index+1}});
        }

        event("frontend.pageload", null, null, {page: "register"});
    }

    onClick() {
//        console.log("click");

        this.submit();
    }

    submit() {
        if (!this.validateInput()) {
            return;
        }

        fetch(`http://${config.host}/register/`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "X-CSRFToken": getCSRFToken(),
            },
            body: JSON.stringify({
                username: this.state.input.username,
                password: this.state.input.password,
                email_address: this.state.input.emailAddress,
            })
        })
        .then(response => response.json())
        .then(json => {
            let alerts = [...json.alerts];
            let index = this.state.alerts.index;

            alerts.forEach(alert => {
                alert.index = index++;
            })

            this.setState({alerts: { index: index, list: alerts }});
        });
    }

    onChange(inputType, event) {
        let input = {...this.state.input};

        input[inputType] = event.target.value;

        this.setState({input: input});
    }

    removeAlert(index) {
        let alerts = [...this.state.alerts.list];
        for(let i=0; i<alerts.length; i++) {
            if (alerts[i].index == index) {
                alerts.splice(i, 1);
            }
        }

        this.setState({alerts: {index: this.state.alerts.index, list: alerts}})
    }

    onKeyPress(event) {
        if (event.key == "Enter") {
            this.submit();
        }
    }

    validateInput() {
        let alert = null;
        if (this.state.input.password != this.state.input.passwordConfirm) {
            alert = {type: 'danger', message: 'Passwords do not match', index: this.state.alerts.index};
        }

        else if (!this.state.input.username || !this.state.input.password || !this.state.input.emailAddress) {
            alert = {type: 'danger', message: 'All fields are required', index: this.state.alerts.index};
        }

        if (alert) {
            let alerts = [...this.state.alerts.list];
            alerts.push(alert);

            this.setState({alerts: {list: alerts, index: alert.index+1}});

            return false;
        }

        return true;
    }

    render() {
        const alerts = this.state.alerts.list.map(alert => <TimeoutAlert key={alert.index} onClick={() => this.removeAlert(alert.index)} alert={alert} timeout={3000}/>);

        if (this.state.redirect) {
            return <Redirect to={this.state.redirect}/>
        }

        return (
            <Container>
                <Row>
                    <Col sm={{span: 8, offset: 2}} md={{span: 6, offset: 3}}>
                        <Header login/>
                        <div style={{display: "flex", flexDirection: "column-reverse", height: "200px"}}>
                            {alerts}
                        </div>
                        <div className="registration-table">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            <Form.Label>username:</Form.Label>
                                        </td>
                                        <td>
                                            <Form.Control maxLength={32} type="text" placeholder="Username..." value={this.state.input.username} onKeyPress={this.onKeyPress} onChange={event => this.onChange("username", event)}/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <Form.Label>password:</Form.Label>
                                        </td>
                                        <td>
                                            <Form.Control type="password" placeholder="Password..." value={this.state.input.password} onKeyPress={this.onKeyPress} onChange={event => this.onChange("password", event)}/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p/>
                                        </td>
                                        <td>
                                            <Form.Control type="password" placeholder="Confirm password..." value={this.state.input.passwordConfirm} onKeyPress={this.onKeyPress} onChange={event => this.onChange("passwordConfirm", event)}/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <Form.Label>email:</Form.Label>
                                        </td>
                                        <td>
                                            <Form.Control type="email" placeholder="Email address..." value={this.state.input.emailAddress} onKeyPress={this.onKeyPress} onChange={event => this.onChange("emailAddress", event)}/>
                                        </td>
                                    </tr>
                                 </tbody>
                            </table>
                        </div>
                        <Button block variant="primary" onClick={this.onClick}>Register</Button>
                    </Col>
                </Row>
            </Container>
        );
    }
}
