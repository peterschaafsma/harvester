import React from "react";
import { Alert, Button, Form, Modal, DropdownButton, Dropdown } from "react-bootstrap";
import { AsyncTypeahead, Typeahead, Token } from 'react-bootstrap-typeahead';
import { config, S } from "root/config.js";
import { findAll } from "root/utils.js";
import PropTypes from "prop-types";


export class TimeoutAlert extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            active: true
        }

        this.timer = window.setTimeout(() => {
//            console.log("timeout");
            this.timer = null;
            this.props.removeAlert(id(this.props.alert));}, this.props.timeout);
    }

    componentWillUnmount() {
        if (this.timer) {
            window.clearTimeout(this.timer);

            this.timer = null;
        }
    }

    render() {
        if (this.state.active) {
            return (
                <Alert variant={this.props.alert.type} style={{padding: "0.15rem .55rem", marginBottom: "0.5rem"}} onClick={() => this.props.removeAlert(id(this.props.alert))}>
                    {this.props.alert.message}
                </Alert>
            );
        }
        else {
            return null;
        }
    }
}

export class Alerts extends React.Component {
    constructor(props) {
        super(props);

        this.removeAlert = this.removeAlert.bind(this);
    }

    removeAlert(key) {
        let alerts = [...this.props.alerts];
        for(let i=0; i<alerts.length; i++) {
            if (id(alerts[i]) == key) {
                alerts.splice(i, 1);
            }
        }

        this.props.setAlerts(alerts);
    }

    render() {
//        console.log("alerts render", this.props.alerts);

        const alerts = this.props.alerts.map(alert => <TimeoutAlert key={id(alert)} alert={alert} removeAlert={this.removeAlert} timeout={5000}/>);

        return (
            <div style={{position: "fixed", bottom: "0%", display: "block"}}>
                {alerts}
            </div>
        );
    }
}

export class LabeledDropdown extends React.Component {
    constructor(props) {
        super(props);

        this.optionMap = {};

        this.props.options.forEach(option => {this.optionMap[option.key] = option.value;});
        this.menuItems = this.props.options.map(option => <Dropdown.Item key={option.key} eventKey={option.key}>{option.value}</Dropdown.Item>)
    }

    render() {
        const title = this.props.selected ? this.optionMap[this.props.selected] : "None";

        const dropdown = (
            <DropdownButton className="btn-lightborder" variant="outline-dark" title={title} onSelect={this.props.onChange}>
                {this.menuItems}
            </DropdownButton>
        )

        return (
            <LabeledElement label={this.props.label}>
                {dropdown}
            </LabeledElement>
        );
    }
}


export class ChartControlButtons extends React.Component {
    render() {
        const style={margin: "auto 2px auto 2px", float: "right", marginBottom: "4px"};

        let buttons = null;

        let expandCollapseAction;
        let expandCollapseClass;

        if (this.props.expanded) {
            expandCollapseClass = "fas fa-compress";
            expandCollapseAction = "collapse";
        }
        else {
            expandCollapseClass = "fas fa-expand";
            expandCollapseAction = "expand";
        }

        if (this.props.mode == "publish") {
            const hasCog = this.props.context.data.chart.type != "LiveChart";

            buttons = (
                <React.Fragment>
                    {hasCog && <Button variant="outline-dark"className="btn-icon" style={style} onClick={() => this.props.context.func.onShowChartEditor(this.props.context.data.chart.id)}>
                        <span className="fas fa-cog" aria-hidden="true"></span>
                    </Button>}
                </React.Fragment>
            );
        }
        else {
            buttons = (
                <React.Fragment>
                    <Button variant="outline-dark" className="btn-icon" style={style} onClick={() => this.props.context.func.onDeleteChart(this.props.context.data.chart.id)}>
                        <span className="far fa-trash-alt" aria-hidden="true"></span>
                    </Button>
                    <Button variant="outline-dark"className="btn-icon" style={style} onClick={() => this.props.context.func.onShowChartEditor(this.props.context.data.chart.id)}>
                        <span className="fas fa-cog" aria-hidden="true"></span>
                    </Button>
                    {this.props.hasRefresh != false && <Button variant="outline-dark"className="btn-icon" style={style} onClick={this.props.context.func.fetchNewData}>
                        <span className="fas fa-sync" aria-hidden="true"></span>
                    </Button>}
                    <Button variant="outline-dark"className="btn-icon" style={style} onClick={() => this.props.context.func.onDuplicateChart(this.props.context.data.chart.id)}>
                        <span className="far fa-copy" aria-hidden="true"></span>
                    </Button>
                    <Button variant="outline-dark"className="btn-icon" style={style} onClick={() => this.props.context.func.onMoveChart("up", this.props.context.data.chart.id)}>
                        <span className="fas fa-arrow-up" aria-hidden="true"></span>
                    </Button>
                    <Button variant="outline-dark"className="btn-icon" style={style} onClick={() => this.props.context.func.onMoveChart("down", this.props.context.data.chart.id)}>
                        <span className="fas fa-arrow-down" aria-hidden="true"></span>
                    </Button>
                    {this.props.hasCheck == true && <Button variant="outline-dark"className="btn-icon" style={style} onClick={this.props.context.func.checkAll}>
                        <span className="fas fa-check" aria-hidden="true"></span>
                    </Button>}
                    {this.props.hasEnrich == true && <Button variant="outline-dark"className="btn-icon" style={style} onClick={this.props.context.func.enrich}>
                        <span className="far fa-lightbulb" aria-hidden="true"></span>
                    </Button>}
                    <Button variant="outline-dark"className="btn-icon" style={style} onClick={() => this.props.context.func.onMoveChart(expandCollapseAction, this.props.context.data.chart.id)}>
                        <span className={expandCollapseClass} aria-hidden="true"></span>
                    </Button>
                </React.Fragment>
            );
        }

        return buttons;
    }
}

export class FinderControl extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            options: [],
            isLoading: false,
            editable: true
        }

        this.receiveOptions = this.receiveOptions.bind(this);
    }

    receiveOptions(options) {
        const newState = {
            options: options,
            isLoading: false,
        }

        this.setState(newState);
    }

    render() {
        return (
            <AsyncTypeahead
                isLoading={this.state.isLoading}
                {...this.props.typeaheadProps}

                className={this.props.className}
                style={this.props.style}

                onSearch={query => {
                    this.setState({isLoading: true});
                    fetch(`http://${config.api}/finder/${this.props.target}/?query=${query}`)
                    .then(resp => resp.json())
                    .then(json => {
                        this.receiveOptions(json);
                    });
                }}

                onChange={(selected) => {
                    this.props.onChange(this.props.target, selected);
                }}

                options={this.state.options}
            />
        )
    }
}


export class NavigationButtons extends React.Component {
    render() {
        let firstProps;
        if (this.props.highlightFirstButton) {
            firstProps = { variant: "outline-primary" };
        }
        else {
            firstProps = { variant: "outline-dark", className:"btn-lightborder" };
        }

        let prevProps;
        if (this.props.highlightPrevButton) {
            prevProps = { variant: "outline-primary" };
        }
        else {
            prevProps = { variant: "outline-dark", className:"btn-lightborder" };
        }

        return (
            <div style={{display:"flex", justifyContent: "center"}}>
                <Button disabled={!this.props.canPrevious} {...firstProps} style={{height: "100%", marginRight: 6}} onClick={this.props.onFirst}>First</Button>
                <Button disabled={!this.props.canPrevious} {...prevProps} style={{height: "100%"}} onClick={this.props.onPrevious}>Prev</Button>
                <p style={{margin: "auto 19px"}}>{this.props.pageIndex+1}</p>
                <Button disabled={!this.props.canNext} className="btn-lightborder" style={{height: "100%", marginRight: 6}}  variant="outline-dark" onClick={this.props.onNext}>Next</Button>
                <Button disabled={!this.props.canNext}className="btn-lightborder" style={{height: "100%"}}  variant="outline-dark" onClick={this.props.onLast}>Last</Button>
            </div>
        );
    }
}


export class Paginator extends React.Component {
    constructor(props) {
        super(props);

        this.receiveMoreItems = this.receiveMoreItems.bind(this);
        this.onPrevious = this.onPrevious.bind(this);
        this.onNext = this.onNext.bind(this);
        this.onFirst = this.onFirst.bind(this);
        this.onLast = this.onLast.bind(this);

        this.state = {
            currentPageIndex: -1,
            currentPage: []
        };

        this.items = [];
        this.pages = [];
        this.noMoreItems = false;
    }

    componentDidMount() {
//        console.log("paginator componentDidMount");
        this.setState({currentPageIndex: 0, currentPage: []});
    }

    shouldComponentUpdate(nextProps, nextState) {
//        console.log("paginator shouldComponentUpdate");
//        console.log(JSON.stringify(this.props));
//        console.log(JSON.stringify(nextProps));
//        console.log(JSON.stringify(this.state));
//        console.log(JSON.stringify(nextState));

        if (this.state.currentPageIndex != nextState.currentPageIndex) {
            return true;
        }

        if (this.state.currentPage != nextState.currentPage) {
            return true;
        }

        if ((this.props.selected == null) != (nextProps.selected == null)) {
            return true;
        }

        if (this.props.selected != null && nextProps.selected != null) {
            if (this.props.selected.length != nextProps.selected.length) {
                return true;
            }
        }

        return false;
    }

    componentDidUpdate(prevProps, prevState) {
//        console.log("paginator componentDidUpdate", JSON.stringify(this.state));
        if (!this.noMoreItems && this.state.currentPage.length < this.props.pageSize) {
            this.props.fetchMoreItems(this.items.length, this.receiveMoreItems);
        }

        return true;
    }

    receiveMoreItems(offset, items) {
//        console.log("paginater receiveMoreItems", this.props, items);
        if (items.length < (this.props.count || 1)) {
//            console.log("no more items");
            this.noMoreItems = true;
        }

        if (this.pages.length > 0 && this.pages[this.pages.length-1].length < this.props.pageSize) {
//            console.log("pop");
            this.pages.pop();
        }

//        this.items = this.items.concat(items);
        this.items.splice(offset, items.length, ...items);

        let start = this.pages.length * this.props.pageSize;
        while (this.items.length > start) {
            const page = this.items.slice(start, start + this.props.pageSize);

            this.pages.push(page);
//            console.log("new page", page);

            start += page.length;
        }

        if (this.pages.length == 0 || (this.pages[this.pages.length-1].length == this.props.pageSize)) {
//            console.log("add empty page");
            this.pages.push([]);
        }

//        console.log("get current page", this.state.currentPageIndex, this.pages);
        if (this.state.currentPageIndex >= 0 && this.state.currentPageIndex < this.pages.length) {
//            console.log("page", this.pages[this.state.currentPageIndex]);
            this.setState({currentPage: this.pages[this.state.currentPageIndex]})
        }
        else {
//            console.log("no page");
        }

//        console.log("pages", this.pages);
    }

    onPrevious() {
        if (this.state.currentPageIndex > 0) {
            this.setState({
                currentPageIndex: this.state.currentPageIndex-1,
                currentPage: this.pages[this.state.currentPageIndex-1],
            });
        }
    }

    onNext() {
//        console.log("before next", this.state.currentPageIndex, this.props.pageSize);
        if (this.state.currentPage.length == this.props.pageSize) {
//            console.log("setting new state", this.pages[this.state.currentPageIndex+1]);
            this.setState({
                currentPageIndex: this.state.currentPageIndex+1,
                currentPage: this.pages[this.state.currentPageIndex+1],
            });
        }
    }

    onFirst() {
        if (this.state.currentPageIndex > 0) {
            this.setState({
                currentPageIndex: 0,
                currentPage: this.pages[0],
            });
        }
    }

    onLast() {
        if (this.state.currentPageIndex < this.pages.length - 1) {
            this.setState({
                currentPageIndex: this.pages.length - 1,
                currentPage: this.pages[this.pages.length - 1],
            });
        }
    }

    render() {
        if (this.state.currentPageIndex == -1) {
            return null;
        }

//        console.log("paginator render", JSON.stringify(this.state));
        const content = this.props.renderPage(this.state.currentPage);

        const canPrevious = this.state.currentPageIndex != 0;
        const canNext = this.state.currentPage.length == this.props.pageSize;

        const navigationButtons = (
            <NavigationButtons
                canPrevious={canPrevious}
                canNext={canNext}

                onNext={this.onNext}
                onPrevious={this.onPrevious}
                onFirst={this.onFirst}
                onLast={this.onLast}
                pageIndex={this.state.currentPageIndex}
            />
        );

        return (
            <div>
                {content}
                {navigationButtons}
            </div>
        )
    }
}

export class ButtonSelector extends React.Component {
    render() {
//        console.log("options", this.props.options);
        let options = [...this.props.options];
        let expandCollapse = null;
        if (options.length > this.props.collapsedCount) {
            if (this.props.collapsed) {
                options = options.slice(0, this.props.collapsedCount);
                expandCollapse = (
                    <Button className="btn-lightborder" variant="outline-dark" style={{margin: "2px"}} key={-1} onClick={this.props.expandCollapse}>
                        <span className="fas fa-chevron-down"/>
                    </Button>
                );
            }
            else {
                expandCollapse = (
                    <Button className="btn-lightborder" variant="outline-dark" style={{margin: "2px"}} key={-1} onClick={this.props.expandCollapse}>
                        <span className="fas fa-chevron-up"/>
                    </Button>
                );
            }
        }

//        console.log("options", this.state.filteredLists);

        return (
            <div style={{display: "flex", flexWrap: "wrap", margin: "0 -2px"}}>
                {options.map(option => {
                    let buttonVariant = "outline-dark";
                    let buttonClass = "btn-lightborder";

                    if (this.props.selected.indexOf(option.key) >= 0) {
                        buttonVariant = "outline-primary";
                        buttonClass = "";
                    }

                    return (
                        <Button style={{margin: "2px"}} variant={buttonVariant} className={buttonClass} key={option.key} onClick={() => this.props.onClick(option.key)}>
                            {option.value}
                        </Button>)}
                    )
                }
                {expandCollapse}
            </div>
        );
    }
}


export class Confirm extends React.Component {
    constructor(props) {
        super(props);

        this.handle = this.handle.bind(this);
    }

    handle(action) {
        if (action == "yes") {
            this.props.confirm.callback();
        }

        this.props.update({confirm: {show: false}});
    }

    render() {
        const yes = () => this.handle("yes");
        const no = () => this.handle("no");

        return (
            <Modal show={this.props.confirm.show || false} onHide={no}>
                <Modal.Body>
                    <div>
                        {this.props.confirm.message}
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="outline-primary" onClick={yes}>
                        <span className="btn-label">{S("Yes")}</span>
                    </Button>
                    <Button variant="outline-secondary" onClick={no}>
                        <span className="btn-label">{S("No")}</span>
                    </Button>
                </Modal.Footer>
            </Modal>
        );
    }
}

export class Dialog extends React.Component {
    constructor(props) {
        super(props);

        this.handle = this.handle.bind(this);
    }

    handle(action=null) {
        this.props.update({dialog: {show: false}});

        for(let i=0; i<this.props.dialog.buttonActions.length; i++) {
            const buttonAction = this.props.dialog.buttonActions[i];

            if (buttonAction.action == action) {
                buttonActions.onClick();

                break;
            }
        }
    }

    render() {
        if (!this.props.dialog.buttonActions) {
            return null;
        }

        let buttons = [];

        this.props.dialog.buttonActions.forEach(buttonAction => {
            const onClick = () => {
                this.handle(buttonAction.key)
            }
            const button = (
                <Button key={buttonAction.key} variant="outline-secondary" onClick={onClick}>
                    <span className="btn-label">{buttonAction.value}</span>
                </Button>
            );

            buttons.push(button);
        });

        let body;
        if (this.props.dialog.config.body) {
            body = this.props.dialog.config.body;
        }
        else if (this.props.dialog.config.messageHTML) {
            body = this.props.dialog.config.messageHTML;
        }
        else {
            boy = (
                <div>
                    {this.props.dialog.config.message}
                </div>
            )
        }

        return (
            <Modal show={this.props.dialog.show || false} onHide={this.handle}>
                <Modal.Header>
                    <Modal.Title>
                        <span>{this.props.dialog.config.title}</span>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {body}
                </Modal.Body>
                <Modal.Footer>
                    {buttons}
                </Modal.Footer>
            </Modal>
        );
    }
}

ButtonSelector.propTypes = {
    onClick: PropTypes.func,
    expandCollapse: PropTypes.func,
    options: PropTypes.array,
    selected: PropTypes.array,
    collapsedCount: PropTypes.number,
}