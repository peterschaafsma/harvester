import React from "react";
import { config } from "root/config.js";
import { ChartControlButtons } from "./Controls.js";


export default class ChartHeader extends React.Component {
    constructor(props) {
        super(props);

        this.variablesMap = {};
        (props.context.data.variables || []).forEach(variable => {
            this.variablesMap[variable.name] = variable.value;
        });
    }

    shouldComponentUpdate(nextProps, nextState) {
        const nextData = nextProps.context.data;
        const thisData = this.props.context.data;

        if (nextData.chart.title != thisData.chart.title) {
//            console.log("chartheader shouldComponentUpdate true title");
            return true;
        }
        if (test(nextData.variables, thisData.variables)) {
//            console.log("chartheader shouldComponentUpdate true variables");

            this.variablesMap = {};
            (thisData.variables || []).forEach(variable => {
                this.variablesMap[variable.name] = variable.value;
            });

            return true;
        }

//        console.log("chartheader shouldComponentUpdate false");

        return false;
    }

    render() {
//        console.log("chartheader render", this.props, this.variablesMap);
        let title = this.props.context.data.chart.title || "";

        Object.keys(this.variablesMap).forEach(key => {
            title = title.replace('${' + key + '}', this.variablesMap[key]);
        });

        return (
            <div style={{display: "flex", flexDirection: "row"}}>
                <div style={{minWidth: "30%"}}>
                    <span className="chart-title">{title}</span>
                </div>
                <div style={{minWidth: "30%", marginLeft: "auto", marginTop: "6px"}}>
                    <ChartControlButtons {...this.props}/>
                </div>
            </div>
        );
    }
}