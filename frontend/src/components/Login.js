import React from "react";
import {Button, Form, Col, Container, Row} from "react-bootstrap";
import {config} from "root/config.js";
import { getCSRFToken } from "root/utils.js";
import { TimeoutAlert } from "./Controls.js";
import { Redirect } from "react-router-dom";
import Header from "./Header.js";
import { event } from "root/event.js";

export default class Login extends React.Component {
    constructor(props) {
        super(props);

        this.onClick = this.onClick.bind(this);
        this.onChange = this.onChange.bind(this);
        this.removeAlert = this.removeAlert.bind(this);
        this.onKeyPress = this.onKeyPress.bind(this);
        this.submit = this.submit.bind(this);

        this.state = {
            input: {
                username: "",
                password: ""
            },
            alerts: {
                index: 0,
                list: [],
            },
            redirect: null,
        }
    }

    componentDidMount() {
        event("frontend.pageload", null, null, {page: "login"});
    }

    onClick() {
//        console.log("click");

        this.submit();
    }

    submit() {
        fetch(`http://${config.host}/login/`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "X-CSRFToken": getCSRFToken(),
            },
            body: JSON.stringify({username: this.state.input.username, password: this.state.input.password})
        })
        .then(response => response.json())
        .then(json => {
//            console.log("json", json);

            if (json.logged_in) {
                window.location.replace(`http://${config.host}${json.redirect}`);
            }
            else {
                let alerts = [...json.alerts];
                let index = this.state.alerts.index;

                alerts.forEach(alert => {
                    alert.index = index++;
                })

                this.setState({alerts: { index: index, list: alerts }});
            }
        });
    }

    onChange(inputType, event) {
        let input = {...this.state.input};

        input[inputType] = event.target.value;

        this.setState({input: input});
    }

    removeAlert(index) {
//        console.log("removeAlert", index, this.state.alerts);
        let alerts = [...this.state.alerts.list];
        for(let i=0; i<alerts.length; i++) {
            if (alerts[i].index == index) {
                alerts.splice(i, 1);
            }
        }

        this.setState({alerts: {index: this.state.alerts.index, list: alerts}})
    }

    onKeyPress(event) {
        if (event.key == "Enter") {
            this.submit();
        }
    }

    render() {
        const alerts = this.state.alerts.list.map(alert => <TimeoutAlert key={alert.index} onClick={() => this.removeAlert(alert.index)} alert={alert} timeout={3000}/>);

        if (this.state.redirect) {
            return <Redirect to={this.state.redirect}/>
        }

        return (
            <Container>
                <Row>
                    <Col sm={{span: 8, offset: 2}} md={{span: 6, offset: 3}}>
                        <Header register help contact/>
                        <div style={{display: "flex", flexDirection: "column-reverse", height: "200px"}}>
                            {alerts}
                        </div>
                        <div className="registration-table">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            <Form.Label>username:</Form.Label>
                                        </td>
                                        <td>
                                            <Form.Control type="text" placeholder="Username..." value={this.state.input.username} onKeyPress={this.onKeyPress} onChange={event => this.onChange("username", event)}/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <Form.Label>password:</Form.Label>
                                        </td>
                                        <td>
                                            <Form.Control type="password" placeholder="Password..." value={this.state.input.password} onKeyPress={this.onKeyPress} onChange={event => this.onChange("password", event)}/>
                                        </td>
                                    </tr>
                                 </tbody>
                            </table>
                        </div>
                        <Button block variant="primary" onClick={this.onClick}>Login</Button>
                    </Col>
                </Row>
            </Container>
        );
    }
}
