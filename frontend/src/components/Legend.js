import React from "react";
import { config } from "root/config.js";
import { select, mouse, event } from "d3-selection";
import * as d3shape from "d3-shape";
import { countFormat } from "root/formatting.js";
import numeral from 'numeral-es6';


export default class Legend extends React.Component {
    constructor(props) {
//        console.log("legend constructor", props);

        super(props);

        this.svg = null;

        this.renderLegend = this.renderLegend.bind(this);
        this.mapVariables = this.mapVariables.bind(this);

        this.variablesMap = {};
        (this.props.variables || []).forEach(variable => {
            this.variablesMap[variable.name] = variable.value;
        });
    }

    componentDidMount() {
//        console.log("legend componentDidMount", this.props.svgId);

        this.svg = select(`#${this.props.svgId}`).append("svg")
            .attr("preserveAspectRatio", "xMidYMid");

//        console.log("SVG", this.svg);

        this.renderLegend();
    }

    shouldComponentUpdate(nextProps, nextState) {
//        console.log("legend shouldComponentUpdate");
//        console.log("legend shouldComponentUpdate", JSON.stringify(this.props.data), JSON.stringify(nextProps.data));
//        console.log("legend shouldComponentUpdate", id(this.props.data), id(nextProps.data));
//        console.log("legend shouldComponentUpdate", this.props.data != nextProps.data);

        if (test(this.props.data, nextProps.data)) {
//            console.log("legend shouldComponentUpdate true data", nextProps.data);
            return true;
        }
        if (test(this.props.variables, nextProps.variables)) {
//            console.log("legend shouldComponentUpdate true variables", nextProps.variables);

            this.variablesMap = {};
            (nextProps.variables || []).forEach(variable => {
                this.variablesMap[variable.name] = variable.value;
            });

            return true;
        }
        if (this.props.expanded != nextProps.expanded) {
//            console.log("legend shouldComponentUpdate true");
            return true;
        }

//        console.log("legend shouldComponentUpdate false");
        return false;
    }

    componentDidUpdate() {
//        console.log("legend componentDidUpdate");

        this.renderLegend();
    }

    mapVariables(string) {
        let copy = `${string}`;
        Object.keys(this.variablesMap).forEach(key => {
            copy = copy.replace('${' + key + '}', this.variablesMap[key]);
        });

        return copy;
    }

    renderLegend() {
//        console.log("legend renderLegend", this.props.configuration);

        const h = 36;
        const s = 4;
        const w = 128

        const height = Math.max(0, this.props.data.length * (h + s) - s);

//        console.log("height", height);

        this.svg
            .attr("viewBox", `0 0 ${this.props.configuration.chart.v.w} ${height}`);

        this.svg.select("g.legend").remove();
        this.svg.append("g").attr("class", "legend");

        const renderLegendItem = group => {
//            console.log("legend renderLegendItem");

            group
                .attr("transform", d => `translate(${this.props.configuration.chart.m.l}, ${d.index * (h + s)})`);

            group.append("rect")
                .attr("class", "legend-colorbar")
                .attr("height", h - 12)
                .attr("y", 8)
                .attr("width", w)
                .attr("class", d => `bar field-${d.id}`);

            group.append("text")
                .attr("class", "axis legend-label legend-text")
                .attr("font-size", "32px")
                .style("text-anchor", "start")
                .style("alignment-baseline", "central")
                .text(d => this.mapVariables(d.label))
                .attr("transform", d => `translate(${w+h/2}, ${h/2})`);

            group.append("text")
                .attr("class", "axis legend-total legend-text")
                .attr("font-size", "32px")
                .style("text-anchor", "end")
                .style("alignment-baseline", "central")
                .text(d => `${numeral(d.percentage).format("0.0")}%`)
                .attr("transform", d => `translate(${this.props.configuration.chart.v.w - this.props.configuration.chart.m.l - this.props.configuration.chart.m.r - 150}, ${h/2})`);

            group.append("text")
                .attr("class", "axis legend-total legend-text")
                .attr("font-size", "32px")
                .style("text-anchor", "end")
                .style("alignment-baseline", "central")
                .text(d => `${countFormat(d.total, 1)}`)
                .attr("transform", d => `translate(${this.props.configuration.chart.v.w - this.props.configuration.chart.m.l - this.props.configuration.chart.m.r}, ${h/2})`);
}

        const legend = this.svg.select("g.legend").selectAll("g").data(this.props.data);

        legend
            .enter()
            .append("g")
                .call(renderLegendItem);

    }

    render() {
        return <div id={this.props.svgId} className="legend"/>;
    }
}
