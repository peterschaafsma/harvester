import React from "react";

import {Col, Container, Row, Button, Alert, Modal, Dropdown, DropdownButton, Form} from "react-bootstrap";

import LineChart from "./LineChart.js";
import PieChart from "./PieChart.js";
import PostingsChart from "./PostingsChart.js";
import ChartEditor from "./ChartEditor.js";
import PopularityChart from "./PopularityChart.js";
//import GeoChart from "./GeoChart.js";
import Header from "./Header.js";
import { getCSRFToken } from "root/utils.js";
import { Alerts, QueryControl } from "./Controls.js";
import {config} from "root/config.js";
import { filterStructure, charMap } from "root/utils.js";
import { presets, getNamedDateRange, makeHumanReadable, intervalObjects, intervalObjectMap } from "root/dateRange.js";

import * as moment from "moment";

export default class ChartEditor extends React.Component {
    constructor(props) {
        super(props);

        this.makeTopicEdit = this.makeTopicEdit.bind(this);

        this.onChange = this.onChange.bind(this);
        this.onSelect = this.onSelect.bind(this);

        this.onMoveQueryUp = this.onMoveQueryUp.bind(this);
        this.onDelete = this.onDelete.bind(this);

        this.onSave = this.onSave.bind(this);
        this.onCancel = this.onCancel.bind(this);

        this.state = {
            id: null,
            type: null,
            topics: null,
            title: null,
            interval: null,
            pagesize: null,
            sorting: null,
//            topicSets: [],
//            selectedTopic: null,
        }

        this.sortingItems = {
            'newest_first': {
                key: 'newest_first',
                value: 'Newest first',
                config: [{ created_at: 'desc'}]
            },
            'oldest_first': {
                key: 'oldest_first',
                value: 'Oldest first',
                config: [{ created_at: 'asc'}]
            },
            'likes': {
                key: 'likes',
                value: 'Likes',
                config: [{ like_count: 'desc'}, { created_at: 'desc'}]
            },
            'shares': {
                key: 'shares',
                value: 'Shares',
                config: [{ share_count: 'desc'}, { created_at: 'desc'}]
            },
            'replies': {
                key: 'replies',
                value: 'Replies',
                config: [{ reply_count: 'desc'}, { created_at: 'desc'}]
            },
        };
    }

    static getDerivedStateFromProps(props, state) {
        // if we do not have a chart prop specified, or we already initialized, do not do anything
        if (!props.chart || props.chart.id == state.id) {
            return null;
        }

        // otherwise, if we did get a chart as prop, use it to initialize state
        if (props.chart) {
            const topics = props.chart.topics.map(topic => {return {...topic}});

            return {
                topics: topics,
                id: props.chart.id,
                type: props.chart.type,
                title: props.chart.title,
                interval: props.chart.interval,
                pagesize: props.chart.pagesize,
                sorting: props.chart.sorting,
            };
        }
    }

//    componentDidMount() {
//        fetch(`http://${config.api}/topics`)
//        .then(response => response.json())
//        .then(json => {
//            console.log("topicSets", json);
//            this.setState({topicSets: json});
//        });
//    }

    onMoveQueryUp(topicId) {
        let topics = this.state.topics;

        if (topics[0].id == topicId) {
            return;
        }

        for(let i=0; i<topics.length; i++) {
            if (topics[i].id == topicId) {
                const temp = topics[i-1];
                topics[i-1] = topics[i];
                topics[i] = temp;

                break;
            }
        }

        this.setState({topics: topics});
    }

    onDelete(topicId) {
        let topics = this.state.topics;

        for(let i=0; i<topics.length; i++) {
            if (topics[i].id == topicId) {
                topics.splice(i, 1);

                break;
            }
        }

        this.setState({topics: topics});
    }

    onDuplicateTopic(topicId) {
        let topics = this.state.topics;

        let maxId = 0;
        for(let i=0; i<topics.length; i++) {
            if (topics[i].id > maxId) {
                maxId = topics[i].id;
            }
        }

        for(let i=0; i<topics.length; i++) {
            if (topics[i].id == topicId) {

                let newTopic = {...topics[i]};
                newTopic.id = maxId + 1

                topics.push(newTopic);

                break;
            }
        }

        this.setState({topics: topics});

    }

    makeTopicEdit(topic) {
        if (topic.value == "foreground_query" || topic.value ==  "background_query") {
            const placeHolder = topic.value ==  "foreground_query" ? "Foreground query..." : "Background query...";

            const queryControl = (
                <QueryControl
                    placeholder="Query..."
                    value={topic.query}
                    onChange={(value) => this.onChange("query", topic.id, value)}
                    style={{width: "300%"}}
                />
            );

//            <Form.Control type="text" className="text-lowercase" placeholder={placeHolder} value={topic.query} onChange={(event) => this.onChange("query", topic.id, event)} style={{width: "300%"}} />

            return (
                <div style={{display: "flex", flexDirection: "row"}} key={topic.id}>
                    {queryControl}
                </div>
            );
        }
        if (topic.value == "new_query") {
            const queryControl = (
                <QueryControl
                    placeholder="Query..."
                    value={topic.query}
                    onChange={(value) => this.onChange("query", topic.id, value)}
                    style={{width: "300%"}}
                />
            );

            //<Form.Control type="text" className="text-lowercase" placeholder="Query..." value={topic.query} onChange={(event) => this.onChange("query", topic.id, event)} style={{width: "300%"}} />

            return (
                <div style={{display: "flex", flexDirection: "row"}} key={topic.id}>
                    <Form.Control type="text" placeholder="Label" value={topic.label} onChange={(event) => this.onChange("label", topic.id, event.target.value)}/>
                    {queryControl}
                    <Button className="btn-icon" style={{visibility: "hidden"}}>
                        <span className="fas fa-minus" aria-hidden="true"></span>
                    </Button>
                    <Button className="btn-icon" style={{visibility: "hidden"}}>
                        <span className="fas fa-arrow-up" aria-hidden="true"></span>
                    </Button>
                    <Button className="btn-icon" style={{visibility: "hidden"}}>
                        <span className="far fa-copy" aria-hidden="true"></span>
                    </Button>
                </div>
            );
        }

        if (topic.value == "query") {
            const queryControl = (
                <QueryControl
                    placeholder="Query..."
                    value={topic.query}
                    onChange={(value) => this.onChange("query", topic.id, value)}
                    style={{width: "300%"}}
                />
            );

            // <Form.Control type="text" className="text-lowercase" placeholder="Query..." value={topic.query} onChange={(event) => this.onChange("query", topic.id, event)} style={{width: "300%"}} />

            return (
                <div style={{display: "flex", flexDirection: "row"}} key={topic.id}>
                    <Form.Control type="text" placeholder="Label" value={topic.label} onChange={(event) => this.onChange("label", topic.id, event.target.value)}/>
                    {queryControl}
                    <Button className="btn-icon" variant="outline-dark" onClick={() => this.onDelete(topic.id)}>
                        <span className="far fa-trash-alt" aria-hidden="true"></span>
                    </Button>
                    <Button className="btn-icon" variant="outline-dark" onClick={() => this.onMoveQueryUp(topic.id)}>
                        <span className="fas fa-arrow-up" aria-hidden="true"></span>
                    </Button>
                    <Button className="btn-icon" variant="outline-dark" onClick={() => this.onDuplicateTopic(topic.id)}>
                        <span className="far fa-copy" aria-hidden="true"></span>
                    </Button>
                </div>
            );
        }
    }

    onChange(type, topicId, value) {
        if (type == "title") {
            this.setState({title: value});
        }
        else {
            let topics = this.state.topics || [];

            let found = false;
            topics.forEach(topic => {
                if (topic.id == topicId) {
                    topic[type] =  value;
                    found = true;
                }
            })

            if (!found) {
                let id = 0;
                if (topics.length > 0) {
                    id = topics.slice(-1)[0].id + 1;
                }
                let topic = {value: "query", query: "", label: "", id: id};
                topic[type] = value;

                topics.push(topic);
            }

            this.setState({topics: topics});
        }
    }

    onSave() {
        let state = {...this.state};

        for(let i=0; i<state.topics.length; i++) {
            state.topics[i].id = i;
        }

        this.props.onSaveChart(state);

        this.setState({id: null, type: null, topics: null, title: null, interval: null, pagesize: null, sorting: null});
    }

    onCancel() {
        this.props.onHide();

        this.setState({id: null, type: null, topics: null, title: null, interval: null, pagesize: null, sorting: null});
    }

    onSelect(mode, selected) {
        if (mode == "chartType") {
            let newState = {type: selected};
            if (selected != this.state.type) {
                if (selected == "SigTermChart") {
                    newState.topics = [
                        { query: "", value: "foreground_query", id: 0},
                        { query: "", value: "background_query", id: 1},
                    ];
                }
                else if(/SigTermChart/.exec(this.state.type)) {
                    newState.topics = [];
                }
                else {
                    newState.topics = this.state.topics;
                }
            }

            if (selected != "LineChart") {
                newState.interval = null;
            }

            if (selected != "PostingsChart") {
                newState.pagesize = null;
                newState.sorting = null;
            }

            this.setState(newState);
        }
        else if (mode == "aggregationType") {
            this.setState({interval: selected});
        }
        else if (mode == "pageSize") {
            this.setState({pagesize: parseInt(selected)})
        }
        else if (mode == "sorting") {
            this.setState({sorting: this.sortingItems[selected]})
        }
    }

    render() {
        let topics = [...(this.state.topics || [])];
        if (/LineChart|PieChart|PostingsChart/.exec(this.state.type)) {
            let id = 0;
            if (topics.length > 0) {
                id = Math.max(...topics.map(topic => topic.id)) + 1;
            }
            topics.push({value: "new_query", query: "", label: "", id: id});
        }

        const topicList = (
            <div style={{marginTop: "16px"}}>
                <h3>Topics:</h3>
                { topics.map(this.makeTopicEdit) }
            </div>
        );

        const chartTypeGroups = [
            ["PostingsChart", "LineChart", "PieChart"],
            ["PopularityChart"],
        ];

        let chartTypes;
        if (this.props.chart == null) {
            chartTypes = chartTypeGroups.flat();
        }
        else {
            chartTypeGroups.forEach(chartTypeGroup => {
                if (chartTypeGroup.indexOf(this.props.chart.type) >= 0) {
                    chartTypes = chartTypeGroup;
                }
            });
        }

        const chartTypeMenuItems = chartTypes.map(type => <Dropdown.Item key={type} eventKey={type}>{type}</Dropdown.Item>);
        const chartTypeTitle = this.state.type || "Chart Type";
        const chartTypeDropdown = (
            <div>
                <DropdownButton className="btn-lightborder" variant="outline-dark" id="charttypeselect" title={chartTypeTitle} onSelect={(selected) => this.onSelect("chartType", selected)}>
                    {chartTypeMenuItems}
                </DropdownButton >
            </div>
        );

        let aggregationTypeDropdown = null;
        if (this.state.type == "LineChart") {
            let aggregationTypeMenuItems = intervalObjects.map(item => <Dropdown.Item key={item.key} eventKey={item.key}>{item.value}</Dropdown.Item>);
            aggregationTypeMenuItems.unshift(<Dropdown.Item key={-1} eventKey={null}>(default)</Dropdown.Item>);

            let aggregationTypeTitle = "Resolution";
            if (this.state.interval) {
                aggregationTypeTitle = intervalObjectMap[this.state.interval].value;
            }
            aggregationTypeDropdown = (
                <div style={{marginLeft: 5}}>
                    <DropdownButton className="btn-lightborder" variant="outline-dark" id="aggregationtypeselect" title={aggregationTypeTitle} onSelect={(selected) => this.onSelect("aggregationType", selected)}>
                        {aggregationTypeMenuItems}
                    </DropdownButton >
                </div>
            );
        }

        let pageSizeDropdown = null;
        if (this.state.type == "PostingsChart") {
            let pageSizeMenuItems = [5, 10, 20, 50].map(item => <Dropdown.Item key={item} eventKey={item}>{item}</Dropdown.Item>);

            let pageSizeTitle = "Page size";
            if (this.state.pagesize) {
                pageSizeTitle = `${this.state.pagesize} postings per page`;
            }
            pageSizeDropdown = (
                <div style={{marginLeft: 5}}>
                    <DropdownButton className="btn-lightborder" variant="outline-dark" id="pagesizeselect" title={pageSizeTitle} onSelect={(selected) => this.onSelect("pageSize", selected)}>
                        {pageSizeMenuItems}
                    </DropdownButton >
                </div>
            );
        }

        let sortingDropdown = null;
        if (this.state.type == "PostingsChart") {
            let sortingMenuItems = Object.keys(this.sortingItems).map(key => <Dropdown.Item key={key} eventKey={key}>{this.sortingItems[key].value}</Dropdown.Item>);

            let sortingTitle = "Sorting";
            if (this.state.sorting) {
//                console.log("sorting", this.state.sorting);
                sortingTitle = this.sortingItems[this.state.sorting.key].value;
            }
            sortingDropdown = (
                <div style={{marginLeft: 5}}>
                    <DropdownButton className="btn-lightborder" variant="outline-dark" id="sortingselect" title={sortingTitle} onSelect={(selected) => this.onSelect("sorting", selected)}>
                        {sortingMenuItems}
                    </DropdownButton >
                </div>
            );
        }

        const chartTitle = (
            <div style={{display: "flex", flexDirection: "row", marginTop: "10px"}}>
                <Form.Control style={{width: "100%"}} type="text" placeholder="Chart title..." value={this.state.title || ""} onChange={(event) => this.onChange("title", null, event.target.value)}/>
            </div>
        );

        return (
            <Modal show={this.props.show} onHide={this.onCancel}>
                <Modal.Header closeButton>
                    <Modal.Title>Chart Editor</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div style={{display: "flex", flexDirection: "row"}}>
                        {chartTypeDropdown}
                        {aggregationTypeDropdown}
                        {pageSizeDropdown}
                        {sortingDropdown}
                    </div>
                    {this.state.type ? chartTitle : null}
                    {this.state.type ? topicList : null}
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={this.onSave}>Save</Button>
                    <Button onClick={this.onCancel}>Cancel</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}
