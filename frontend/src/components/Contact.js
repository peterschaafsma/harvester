import React from "react";
import { Row, Col, Container, Table } from "react-bootstrap";
import Header from "./Header.js";
import { event } from "root/event.js";

export default class Contact extends React.Component {
    componentDidMount() {
        event("frontend.pageload", null, null, {page: "contact"});
    }

    render() {
        let header = <Header user={window.user} logout overview project/>;
        if (! window.user) {
            header = <Header login register/>;
        }

        return (
            <Container>
                <Row>
                    <Col sm={{span:8, offset: 2}}>
                        {header}
                        <h2 style={{marginTop: "20px"}}>Contact us:</h2>
                        <Table>
                            <tbody>
                                <tr>
                                    <td><span>Email</span></td>
                                    <td><span>headquarters@snipefix.com</span></td>
                                </tr>
                                <tr>
                                    <td><span>Phone</span></td>
                                    <td><span>+31646753689</span></td>
                                </tr>
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </Container>
        );
    };
}