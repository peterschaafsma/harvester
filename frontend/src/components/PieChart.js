import React from "react";
import { config } from "root/config.js";
import { event, local, mouse, select } from "d3-selection";
import * as d3shape from "d3-shape";
import * as d3scale from "d3-scale";
import * as d3axis from "d3-axis";
import * as d3transition from "d3-transition";
import * as d3interpolate from "d3-interpolate";
import * as moment from "moment";
import { countFormat } from "root/formatting.js";
import Legend from "./Legend.js";
import { ChartControlButtons } from "./Controls.js";
import ChartHeader from "./ChartHeader.js";


/*

Combining d3 and react

Every chart consists of a governing class and various renderables. The governing class is responsible for
triggering the fetching of new data. This happens in the componentDidMount method, and in componentDidUpdate.
We rely on the componentDidUpdate in this class (instead of the tandem mentioned below due to the factor that
we're loading new state, and need rerender anyway)

The former situation applies to after the initial render; the latter occurs when props change (like the main query).

When new data arrives, the renderables are rendered with the new data as props.

In a renderable class, we initialize SVG and the visualization structure in its componentDidMount method, and
subsequently, the updatePlot is calleed. (The render method of the renderable is just to define a div, into which
we'll draw the contents.)

The updatePlot applies the current data (in props) to the visualization structure. Since new data is communicated 
through props, we'll use the shouldComponentUpdate/componentDidUpdate tandem to decide when to apply new data to the
visualization structure.

creation phase

constructor & render
  [constructor & render]*
  [did mount]*
did mount

update phase

should & render
  [should & render]*
  [updated]
updated

*/

class Plot extends React.Component {
    constructor(props) {
//        console.log("piechart plot constructor");

        super(props);

        this.svg = null;
        this.radius = null;

        this.state = {
        };

        this.updatePlot = this.updatePlot.bind(this);

        this.previousData = local();
    }

    componentDidMount() {
//        console.log("piechart plot componentDidMount");

        this.svg = select(`#${this.props.svgId}`).append("svg")
            .attr("viewBox", `0 0 ${config.chart.v.w} ${config.chart.v.h}`)
            .attr("preserveAspectRatio", "xMidYMid");
        this.radius = (config.chart.v.h - config.chart.m.t - config.chart.m.b) / 2;

        const x = config.chart.v.w / 2;
        const y = config.chart.m.t + this.radius;

        const plot = this.svg
            .append("g")
            .attr("class", "plot")
            .attr("transform", `translate(${x}, ${y})`);

        plot.call(this.updatePlot);
    }

    shouldComponentUpdate(nextProps, nextState) {
//        console.log("piechart plot shouldComponentUpdate", id(this.props.data), id(nextProps.data));

        let result = false;
        if( this.props.data != nextProps.data) {
            result = true;
        }
        else if (this.props.expanded != nextProps.expanded) {
            result = true;
        }

//        console.log("piechart plot shouldComponentUpdate", result);

        return this.props.data != nextProps.data;
    }

    componentDidUpdate(prevProps, prevState) {
//        console.log("piechart plot componentDidUpdate");

        if (prevProps.expanded != this.props.expanded) {
//            console.log("linechart new svg", this.props.configuration);
            this.svg.attr("viewBox", `0 0 ${this.props.configuration.chart.v.w} ${this.props.configuration.chart.v.h}`);
            this.svg.select("g.plot").select("g.xaxis").attr("transform", `translate(0, ${this.props.configuration.chart.v.h - this.props.configuration.chart.m.b})`);
            this.svg.select("g.plot").select("g.yaxis").attr("transform", `translate(${this.props.configuration.chart.m.l}, 0)`);
        }

        this.svg.select("g.plot").call(this.updatePlot);
    }

    updatePlot(g) {
//        console.log("piechart plot updatePlot", g);

        const pie = d3shape.pie()
            .value(d => d.total)
            .sort(null);

        const plot = g.selectAll("path")
            .data(pie(this.props.data));

        plot
            .enter()
            .append("path")

        plot
            .exit()
            .remove();

        const arc = d3shape.arc()
            .outerRadius(this.radius)
            .innerRadius(this.radius / 2.5);

        g.selectAll("path")
            .attr("class", topic => {return `arc field-${topic.data.id}`})
            .transition()
                .duration(750)
                .attrTween("d", (d, index, nodeList) => {
                    const prev = this.previousData.get(nodeList[index]);
                    this.previousData.set(nodeList[index], d);

                    let prevEndAngle = 0;
                    let prevStartAngle = 0;
                    if (prev) {
                        prevEndAngle = prev.endAngle;
                        prevStartAngle = prev.startAngle;
                    }

                    const startInterpolator = d3interpolate.interpolate(prevStartAngle, d.startAngle);
                    const endInterpolator = d3interpolate.interpolate(prevEndAngle, d.endAngle);

                    return (t) => arc({
                        ...d,
                        startAngle: startInterpolator(t),
                        endAngle: endInterpolator(t)
                    });
                });
    }

    render() {
//        console.log("piechart plot render");

        return <div id={this.props.svgId} className="plot"/>;
    }
}


export default class PieChart extends React.Component {
    constructor(props) {
        super(props);

        this.chartId = `chart-${this.props.chart.id}`;

        this.onNewData = this.onNewData.bind(this);
        this.fetchNewData = this.fetchNewData.bind(this);
        this.shouldFetchNewData = this.shouldFetchNewData.bind(this);

        this.state = {
            legendData: null,
            pieData: null,
        };
    }

    componentDidMount() {
//        console.log("piechart componentDidMount");
        this.fetchNewData();
    }

    shouldFetchNewData(firstProps, secondProps) {
//        console.log("piechart shouldFetchNewData", JSON.stringify(firstProps), JSON.stringify(secondProps));

        let fetchNewData = false;
        if (firstProps.context.data.mainQuery != secondProps.context.data.mainQuery) {
//            console.log("piechart shouldFetchNewData mainQuery");
            fetchNewData = true;
        }
        if (test(firstProps.context.data.dateRange, secondProps.context.data.dateRange)) {
//            console.log("piechart shouldFetchNewData dateRange");
            fetchNewData = true;
        }
        if (test(firstProps.context.data.variables, secondProps.context.data.variables)) {
            fetchNewData = true;
        }

        if (test(firstProps.chart, secondProps.chart)) {
            if (firstProps.chart.type != secondProps.chart.type) {
//                console.log("piechart shouldFetchNewData chart type");
                fetchNewData = true;
            }
            else if (firstProps.chart.metric != secondProps.chart.metric) {
//                console.log("linechart shouldFetchNewData chart interval");
                fetchNewData = true;
            }
            else if (firstProps.chart.topics.length != secondProps.chart.topics.length) {
//                console.log("piechart shouldFetchNewData chart topics length");
                fetchNewData = true;
            }
            else {
                for (let i=0; i<firstProps.chart.topics.length; i++) {
                    if (firstProps.chart.topics[i].query != secondProps.chart.topics[i].query) {
//                        console.log("piechart shouldFetchNewData chart topics query ", i);
                        fetchNewData = true;

                        break;
                    }
                }
            }
        }
        return fetchNewData;
    }

    shouldComponentUpdate(nextProps, nextState) {
//        console.log("piechart shouldComponentUpdate");

//        console.log("piechart shouldComponentUpdate chart");
//        console.log("piechart shouldComponentUpdate chart id", id(this.props.chart), id(nextProps.chart));
//        console.log(JSON.stringify(this.props.chart));
//        console.log(JSON.stringify(nextProps.chart));
//
//        console.log("piechart shouldComponentUpdate state");
//        console.log("piechart shouldComponentUpdate state id", id(this.state), id(nextState));
//        console.log(JSON.stringify(this.state));
//        console.log(JSON.stringify(nextState));

        // TODO: the problem now is that current props do not yet contain the current dateRange/mainQuery
        // that's why the refetch logic needs to be componentDidUpdate
        if (this.shouldFetchNewData(this.props, nextProps)) {
//            console.log("piechart shouldComponentUpdate true fetch");
            return true;
        }

        if (this.props.chart.title != nextProps.chart.title) {
//            console.log("piechart shouldComponentUpdate true line");
            return true;
        }

//        console.log("piechart expanded", this.props.expanded, nextProps.expanded);
        if (this.props.expanded != nextProps.expanded) {
//            console.log("piechart shouldComponentUpdate true expanded");
            return true;
        }

        for (let i=0; i<this.props.chart.topics.length; i++) {
            if (this.props.chart.topics[i].label != nextProps.chart.topics[i].label) {
                let legendData = [...this.state.legendData];
                legendData[i].label = nextProps.chart.topics[i].label

//                console.log("piechart shouldComponentUpdate true legend labels", i, legendData, nextProps.chart.topics);

                this.setState({legendData: legendData})

                return false;
            }
        }
        if (test(this.state.legendData, nextState.legendData)) {
//            console.log("piechart shouldComponentUpdate true legend");
            return true;
        }
        if (test(this.state.pieData, nextState.pieData)) {
//            console.log("piechart shouldComponentUpdate true line");
            return true;
        }

//        console.log("piechart shouldComponentUpdate false");
        return false;
    }

    componentDidUpdate(prevProps, prevState) {
//        console.log("piechart componentDidUpdate");
        // we need to distinguish between signalling a rerender and refetching data.
        // If the chart changed or mainQuery or dateRange, we refetch but do not rerender yet.
        // The rerender will eventuall trigger a change in pieData, legendData, which does cause a rerender.
        // In addition, any change to tipData also causes a rerender.
        // It is up to the plot and legend classes to ensure they don't redraw when just the tipData changes.
        if (this.shouldFetchNewData(prevProps, this.props)) {
//            console.log("piechart componentDidUpdate true fetch");
            this.fetchNewData();

            return false;
        }
    }

    fetchNewData() {
//        console.log("piechart fetchNewData");
        const context = {
            main_query: this.props.context.data.mainQuery,
            date_range: this.props.context.data.dateRange,
            variables: this.props.context.data.variables,
            metric: this.props.chart.metric,
        };

        let queryData = {
            context: context,
            topics: {}
        };
        this.props.chart.topics.forEach(topic => {
            queryData.topics[topic.id] = {id: topic.id, query: topic.query};
        });

        this.props.context.func.onQuery(this.onNewData, this.props.chart.id, queryData);
    }

    onNewData(data) {
//        console.log("linechart onNewData", data);

        if (data == null) {
            this.setState({
                pieData: null,
                legendData: null
            });

            return;
        }

        let pieData = [];
        let legendData = [];
        let total = 0;

        const topicMap = this.props.chart.topics.reduce((dict, topic) => { dict[topic.id] = topic; return dict; }, {});

//        Object.keys(data).forEach(topicId => {
        this.props.chart.topics.forEach(topic => {
            pieData.push({
                id: topic.id,
                total: data[topic.id].data.total,
            });

            legendData.push({
                index: legendData.length,
                id: topic.id,
                label: topic.label,
                total: data[topic.id].data.total,
            });

            total += data[topic.id].data.total;
        });

        legendData.forEach(item => {
            item.percentage = 100 * item.total / total;
        });

        this.setState({
            pieData: pieData,
            legendData: legendData,
        });
    }

    render() {
//        console.log("piechart render", this.state, this.props);
        const context = {
            func: {
                fetchNewData: this.fetchNewData,
                ...this.props.context.func
            },
            data: {
                chart: this.props.chart,
                variables: this.props.context.data.variables,
                dateRange: this.props.context.data.dateRange,
            },
        }

        const header = <ChartHeader mode={this.props.mode} expanded={this.props.expanded} context={context}/>;

        let plot = null;
        let legend = null;

        if (this.state.pieData != null && this.state.legendData != null) {
            const plotId = `plot-${this.chartId}`;
            const legendId = `legend-${this.chartId}`;

    //        console.log("piechart render", this.state);
            const configuration = this.props.expanded ? config.expanded : config;

            plot = <Plot configuration={configuration} expanded={this.props.expanded} svgId={plotId} data={this.state.pieData}/>;
            legend = <Legend configuration={configuration} expanded={this.props.expanded} variables={this.props.context.data.variables} svgId={legendId} data={this.state.legendData}/>;
        }

        const className = `chart ${this.props.expanded ? "expanded" : ""}`;

        const result = (
            <div id={this.chartId} className={className}>
                {header}
                {plot}
                {legend}
            </div>
        );

        return result;
    }
}