import React from "react";
import {Button, Checkbox, Form, Modal, Radio, Table} from "react-bootstrap";
import {Col, Container, Row, Image} from "react-bootstrap";
import Feed from "./Feed.js";

import { config, S } from "root/config.js";
import { FinderControl, Paginator, Alerts, ButtonSelector } from "./Controls.js";
import Header from "./Header.js";
import TwitterAuthor from "./TwitterAuthor.js";
import { getCSRFToken, intersection, union, difference } from "root/utils.js";
import { Typeahead } from 'react-bootstrap-typeahead';
import { event } from "root/event.js";


export default class FeedManager extends React.Component {
    constructor(props) {
        super(props);

        this.onChange = this.onChange.bind(this);
        this.onKeyPress = this.onKeyPress.bind(this);
        this.onSearch = this.onSearch.bind(this);
        this.onAddFeeds = this.onAddFeeds.bind(this);
        this.onClickAuthor = this.onClickAuthor.bind(this);
        this.onChangeSelected = this.onChangeSelected.bind(this);
        this.onHarvestFeed = this.onHarvestFeed.bind(this);

        this.fetchMoreAuthors = this.fetchMoreAuthors.bind(this);
        this.fetchMoreFeeds = this.fetchMoreFeeds.bind(this);

        this.renderAuthorsPage = this.renderAuthorsPage.bind(this);
        this.renderFeedsPage = this.renderFeedsPage.bind(this);

        this.getTable = this.getTable.bind(this);
        this.getFeedsKey = this.getFeedsKey.bind(this);

        this.reloadLists = this.reloadLists.bind(this);

        this.state = {
            alerts: [],
            searchQuery: "",
            searchQueryInput: "",
            searchedAuthors: [],

            selectedAuthors: [],
            lists: [],
            listsCollapsed: true,
            selectedLists: [],
        }

        this.searchPageSize = 5;
        this.feedPageSize = 5;

        this.pagesPerFetch = 4;
    }

    componentDidMount() {
        this.reloadLists();

        event("frontend.pageload", null, null, {page: "feedmanager"});
    }

    reloadLists() {
        fetch(`http://${config.api}/lists/summary/`)
        .then(response => response.json())
        .then(json => {
            let lists = [];
            for (let i=0; i<json.length; i++) {
                lists.push({
                    key: json[i].key,
                    value: `${json[i].key} (${json[i].count})`,
                });
            }

            const selectedLists = intersection([this.state.selectedLists, lists.map(list => list.key)]);

            this.setState({lists, selectedLists});
        });
    }

    onChange(target, event) {
        if (target == "search") {
            this.setState({searchQueryInput: event.target.value});
        }
    }

    onChangeSelected(target, selected) {
        if (target == "authors") {
            this.setState({selectedAuthors: selected});
        }
        else if (target == "lists") {
            const selectedLists = selected.map(item => typeof(item) == "string" ? item.toLowerCase() : item.label.toLowerCase());

            this.setState({selectedLists: selectedLists});
        }
    }

    onSearch() {
//        console.log("feedmanager onSearch");

        this.setState({searchQuery: this.state.searchQueryInput, searchedAuthors: []});
    }

    onKeyPress(event) {
        if (event.key == "Enter") {
            return this.onSearch();
        }
    }

    onAddFeeds() {
//        console.log("feedmanager onAddFeeds", this.state.searchedAuthors);

        const authors = this.state.selectedAuthors;
        const lists = this.state.selectedLists.map(item => item.customOption ? item.label.toLowerCase() : item);

        let authorMap = {};
        this.state.searchedAuthors.forEach(author => {
            authorMap[author.screen_name] = author;
        });

//        console.log("feedmanager onAddFeeds authorMap", authorMap, authors);

        let authorData = {};
        authors.forEach(author => {
            authorData[author] = {
                followers_count: authorMap[author].followers_count
            }
        });
//        console.log("feedmanager onAddFeeds authorData", authorData);

        fetch(`http://${config.api}/feeds/`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "X-CSRFToken": getCSRFToken(),
            },
            body: JSON.stringify({
                author_data: authorData,
                types: ["twitter_account", "twitter_firehose"],
                lists: lists,
            })
        })
        .then(response => response.json())
        .then(json => {
//            console.log("json", json);
            if (json.alerts.length > 0) {
                this.setState({alerts: this.state.alerts.concat(json.alerts)});
            }

            this.reloadLists();
        });
    }

    onClickAuthor(screen_name) {
        let selectedAuthors = [...this.state.selectedAuthors];

        const index = selectedAuthors.indexOf(screen_name);
        if (index == -1) {
            selectedAuthors.push(screen_name);
        }
        else {
            selectedAuthors.splice(index, 1);
        }

        this.setState({selectedAuthors: selectedAuthors});
    }

    onHarvestFeed(feedId) {
        fetch(`http://${config.api}/feeds/harvest/`, {
            method: "POST",
            headers: {
                "Content-Type": "Application/Json",
                "X-CSRFToken": getCSRFToken(),
            },
            body: JSON.stringify({
                feed_id: feedId
            })
        })
        .then();
    }

    getFeedsKey() {
        return this.state.searchQuery;
    }

    fetchMoreAuthors(offset, callback) {
//        console.log("feedmanager fetchMoreAuthors", offset, this.state.searchQuery || "(none}");

        if (!this.state.searchQuery) {
            return;
        }

        const page = Math.floor(offset / this.searchPageSize);
        const count = 20;

//        console.log("fetch", this.state.searchedAuthors.length, offset, count);

        if (this.state.searchedAuthors.length >= offset + count) {
            callback(0, this.state.searchedAuthors);

            return;
        }

//        console.log("feedmanager fetchMoreAuthors", page, count, page * this.searchPageSize);

        fetch(`http://${config.api}/user/search/?query=${this.state.searchQuery}&page=${page}&count=${count}`)
        .then(response => response.json())
        .then(json => {
            if (json.alerts.length > 0) {
                this.setState({alerts: this.state.alerts.concat(json.alerts)});
            }

            let searchedAuthors = [...this.state.searchedAuthors];
            searchedAuthors.splice(page * this.searchPageSize, json.users.length, ...json.users);
            this.setState({searchedAuthors});

//            console.log("searchedAuthors", searchedAuthors, json.users);

            callback(page * this.searchPageSize, json.users);
        });
    }

    fetchMoreFeeds(offset, callback) {
//        console.log("feedmanager fetchMoreFeeds", offset, this.state.searchQuery || "(none}");

        const count = this.pagesPerFetch * this.feedPageSize;

        fetch(`http://${config.api}/feeds/?types=twitter_account,twitter_firehose,rss&offset=${offset}&count=${count}&query=${this.state.searchQuery}`)
        .then(response => response.json())
        .then(json => {
            if (json.alerts.length > 0) {
                this.setState({alerts: this.state.alerts.concat(json.alerts)});
            }

            callback(offset, json.feeds);
        });
    }

    renderAuthorsPage(page) {
//        console.log("feedmanager renderAuthorsPage", page);

        const authors = page.map(author =>
            <TwitterAuthor
                key={author.screen_name}
                selected={this.state.selectedAuthors.indexOf(author.screen_name) > -1}
                onClick={this.onClickAuthor}
                author={author}/>
        );

        let footer = null;
        if (page.length < this.searchPageSize) {
            footer = (
                <p style={{textAlign: "center"}}>(no more items)</p>
            );
        }

        const users = (
            <div>
                <Table size="sm">
                    <thead>
                        <tr>
                            <th></th>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Followers</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {authors}
                    </tbody>
                </Table>
                {footer}
            </div>
        )

        return users;
    }

    renderFeedsPage(page) {
//        console.log("feedmanager renderFeedsPage", page);

        if (!page) {
            return null;
        }

        const rows = page.map(row => <Feed key={row.feed.id} feed={row} onHarvestFeed={this.onHarvestFeed}/>);

        let footer = null;
        if (page.length < this.feedPageSize) {
            footer = (
                <p style={{textAlign: "center"}}>(no more items)</p>
            );
        }

        const contents = (
            <div>
                <Table size="sm">
                    <thead>
                        <tr>
                            <th>Type</th>
                            <th>Name</th>
                            <th>Last</th>
                            <th>Next</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {rows}
                    </tbody>
                </Table>
                {footer}
            </div>
        );

        return contents;
    }

    getTable(sizeHint) {
        const authorControl = (
            <Typeahead
                className="rbt-dont-edit"
                selected={this.state.selectedAuthors}
                multiple={true}
                clearButton={true}
                placeholder="(no authors selected)"
                options={[]}
                onChange={selected => this.setState({selectedAuthors: selected})}
            />
        );

        const listsControl = (
            <Typeahead
                selected={this.state.selectedLists}
                multiple={true}
                allowNew={true}
                clearButton={true}
                placeholder="Select lists..."
                options={this.state.lists.map(list => list.key)}
                onChange={selected => this.setState({selectedLists: selected})}
            />
        );

        let head;
        let body;

        if (sizeHint == "wide") {
            head = (
                <thead>
                    <tr/>
                    <tr>
                        <th/>
                        <th style={{width: "100%"}}/>
                    </tr>
                </thead>
            );

            body = (
                <tbody>
                    <tr>
                        <td style={{verticalAlign: "middle"}}>
                            <span>{S("authors")}</span>
                        </td>
                        <td>
                            {authorControl}
                        </td>
                    </tr>
                    <tr>
                        <td style={{verticalAlign: "middle"}}>
                            <span>{S("lists")}</span>
                        </td>
                        <td>
                            {listsControl}
                        </td>
                    </tr>
                </tbody>
            );
        }
        else if (sizeHint == "compact") {
            body = (
                <tbody>
                    <tr>
                        <td style={{verticalAlign: "middle"}}>
                            <span>{S("authors")}</span>
                            {authorControl}
                        </td>
                    </tr>
                    <tr>
                        <td style={{verticalAlign: "middle"}}>
                            <span>{S("lists")}</span>
                            {listsControl}
                        </td>
                    </tr>
                </tbody>
            );
        }

        return (
            <Table borderless size="sm">
                {head}
                {body}
            </Table>
        );
    }


    render() {
//        console.log("feedmanager render");

        const searchPaginator = <Paginator
            key={this.state.searchQuery}
            pageSize={this.searchPageSize}
            selected={this.state.selectedAuthors}
            count={this.pagesPerFetch * this.searchPageSize}
            fetchMoreItems={this.fetchMoreAuthors}
            renderPage={this.renderAuthorsPage}
        />;

        const feedsPaginator = <Paginator
            key={this.getFeedsKey()}
            pageSize={this.feedPageSize}
            count={this.pagesPerFetch * this.feedPageSize}
            fetchMoreItems={this.fetchMoreFeeds}
            renderPage={this.renderFeedsPage}
        />;

        const listSelector = <ButtonSelector
            onClick={list => {
                let selected = [...this.state.selectedLists];
                const index = selected.indexOf(list);
                if (index == -1) {
                    selected.push(list);
                }
                else {
                    selected.splice(index, 1);
                }
                this.setState({selectedLists: selected});
            }}
            expandCollapse={() => this.setState({listsCollapsed: !this.state.listsCollapsed})}
            options={this.state.lists}
            selected={this.state.selectedLists}
            collapsedCount={16}
            collapsed={this.state.listsCollapsed}
        />;

        return (
            <Container>
                <Row>
                    <Col md={12} lg={{span: 10, offset: 1}}>
                        <Header user={window.user} logout overview project lists topics help contact/>
                        <h1>Feed Manager</h1>
                        <Row><Col xs={12}>
                            <div className="input-row">
                                <div style={{display: "flex", flexDirection: "row", width: "100%", marginRight: "-12px"}}>
                                    <Form.Control type="text" placeholder="Search feeds..." value={this.state.searchQueryInput} onKeyPress={this.onKeyPress} onChange={event => this.onChange("search", event)}/>
                                    <button onClick={() => this.setState({searchQueryInput: ""})} style={{top: "-3px", right: "22px", position: "relative"}} aria-label="Clear" className="close rbt-close" type="button"><span aria-hidden="true">×</span><span className="sr-only">Clear</span></button>
                                </div>
                                <Button className="btn-icon" variant="outline-dark" onClick={this.onSearch}>
                                    <span className="fas fa-search-plus" aria-hidden="true"></span>
                                </Button>
                                <Button className="btn-icon" variant="outline-dark" onClick={() => {this.setState({selectedLists: [], selectedAuthors: []}); this.onSearch()}}>
                                    <span className="fas fa-search" aria-hidden="true"></span>
                                </Button>
                            </div>
                        </Col></Row>
                    </Col>
                    <Col md={12} lg={{span: 10, offset: 1}}>
                        <h3>Your feeds</h3>
                        <Row><Col xs={12}>
                            {feedsPaginator}
                        </Col></Row>
                    </Col>
                    <Col md={12} lg={{span: 10, offset: 1}}>
                        <h3>Search results</h3>
                        <Row><Col xs={12}>
                            {searchPaginator}
                        </Col></Row>
                    </Col>
                    <Col md={12} lg={{span: 10, offset: 1}}>
                        <h3>Prepare feeds</h3>
                        <Row>
                            <Col xs={12}>
                                <div className="d-block d-sm-none">
                                    {this.getTable("compact")}
                                </div>
                                <div className="d-none d-sm-block">
                                    {this.getTable("wide")}
                                </div>
                                {listSelector}
                            </Col>
                        </Row>
                        <Button variant="primary" className="btn-main" onClick={this.onAddFeeds}>Add feeds</Button>
                    </Col>
                    <Col md={12} lg={{span: 10, offset: 1}}>
                        <Alerts alerts={this.state.alerts} setAlerts={alerts => this.setState({alerts: alerts})} timeout={5000}/>
                    </Col>
                </Row>
            </Container>
        );
    }
}