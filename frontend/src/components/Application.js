import React from "react";
import ReactDOM from "react-dom";
import Project from "./Project.js";
import FeedManager from "./FeedManager.js";
import ListManager from "./ListManager.js";
import Overview from "./Overview.js";
import Contact from "./Contact.js";
import {BrowserRouter, Route} from "react-router-dom";

import './Application.scss';
import 'react-bootstrap-typeahead/css/Typeahead.css';

export default class Application extends React.Component {
    render() {
//        console.log("application render");
        return (
            <BrowserRouter>
                <div>
                    <Route path="/overview/" component={Overview} />
                    <Route path="/lists/" component={ListManager} />
                    <Route path="/feeds/" component={FeedManager} />
                    <Route path="/project/:id?/" component={Project} />
                    <Route exact path="/publish/:uuid" component={Project} />
                    <Route exact path="/publish/:name/:uuid" component={Project} />
                    <Route path="/contact/" component={Contact} />
                </div>
            </BrowserRouter>
        );
    }
}

const wrapper = document.getElementById("application");
wrapper ? ReactDOM.render(<Application/>, wrapper) : null;