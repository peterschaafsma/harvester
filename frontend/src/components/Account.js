import React from "react";
import ReactDOM from "react-dom";
import {BrowserRouter, Route} from "react-router-dom";
import Login from "./Login.js";
import Register from "./Register.js";

import './Application.scss';

export default class Account extends React.Component {
    render() {
//        console.log("render Account");
        return (
            <BrowserRouter>
                <div>
                    <Route path="/login/" component={Login} />
                    <Route path="/register/" component={Register} />
                </div>
            </BrowserRouter>
        );
    }
}


const wrapper = document.getElementById("account");
wrapper ? ReactDOM.render(<Account/>, wrapper) : null;