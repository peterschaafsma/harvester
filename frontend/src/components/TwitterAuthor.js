import React from "react";
import { Button, Image } from "react-bootstrap";
import { config } from "root/config.js";
import { getCSRFToken } from "root/utils.js";
import { countFormat } from "root/formatting.js";


export default class TwitterAuthor extends React.Component {
    constructor(props) {
        super(props);
            this.openProfilePage = this.openProfilePage.bind(this);
            this.onError = this.onError.bind(this);
    }

    openProfilePage(event) {
        event.stopPropagation();

        const url = `https://twitter.com/${this.props.author.screen_name}`;
        window.open(url, '_blank')
    }

    onError() {
//        console.log("image error");

        fetch(`http://${config.api}/authors/update/`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                'X-CSRFToken': getCSRFToken(),
            },
            body: JSON.stringify({
                screen_names: [this.props.author.screen_name],
            })
        })

    }

    render() {
        const image = <Image
            rounded
            src={this.props.author.profile_image_url}
            style={{height: '32px'}}
            onError={this.onError}
        />;

        const as = this.props.as || "tableRow";

        if (as == "tableRow") {
            const className = this.props.selected ? "table-primary" : "";
            return (
                <tr className={className} onClick={() => this.props.onClick(this.props.author.screen_name)}>
                    <td>{image}</td>
                    <td>@{this.props.author.screen_name}</td>
                    <td>{this.props.author.name}</td>
                    <td>{countFormat(parseInt(this.props.author.followers_count))}</td>
                    <td>
                        <div style={{float: "right"}}>
                            <Button className="btn-icon" variant="outline-dark" onClick={this.openProfilePage}>
                                <span className="fas fa-external-link-alt" aria-hidden="true"></span>
                            </Button>
                        </div>
                    </td>
                </tr>
            );
        }
        else if (as == "div") {
            const color = this.props.selected ? "#EEE" : null;
            return (
                <div style={{display: "flex", backgroundColor: color}} onClick={() => this.props.onClick && this.props.onClick(this.props.author.screen_name)}>
                    {image}
                    <span>@{this.props.author.screen_name}</span>
                    <span>{this.props.author.name}</span>
                    <span>{countFormat(parseInt(this.props.author.followers_count))}</span>
                    <div style={{float: "right"}}>
                        <Button className="btn-icon" variant="outline-dark" onClick={this.openProfilePage}>
                            <span className="fas fa-external-link-alt" aria-hidden="true"></span>
                        </Button>
                    </div>
                </div>
            )
        }
    }
}

