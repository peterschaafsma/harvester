import React from "react";

import { Button, Col, Dropdown, DropdownButton, Form, Modal, Row } from "react-bootstrap";
import { Typeahead } from 'react-bootstrap-typeahead';
import { config, S } from "root/config.js";
import { intervalObjectMap, intervalObjects } from "root/dateRange.js";
import { Confirm } from "./Controls.js";
import { getCSRFToken } from "root/utils.js";


class Options extends React.Component {
    shouldComponentUpdate(nextProps) {
        if (nextProps.chartType != this.props.chartType) {
            return true;
        }
        if (nextProps.chart != this.props.chart) {
            return true;
        }
        if (nextProps.chart && this.props.chart && nextProps.chart.type != this.props.chart.type) {
            return true;
        }

        return false;
    }

    render() {
//        console.log("optionlist render", JSON.stringify(this.props));
        const chart = this.props.chart;

        let chartTypeDropdown = null;
        if (this.props.chartType) {
            const options = ["LineChart", "PieChart", "PostingsChart", "PopularityChart", "LiveChart"];
            const menuItems = options.map(type => <Dropdown.Item key={type} eventKey={type}>{type}</Dropdown.Item>);
            const title = chart ? chart.type : "Chart Type";

            chartTypeDropdown = (
                <DropdownButton className="btn-lightborder" variant="outline-dark" title={title} onSelect={selected => this.props.updateState({type: selected})}>
                    {menuItems}
                </DropdownButton >
            );
        }

        let intervalDropdown = null;
        if (this.props.interval) {
            let menuItems = intervalObjects.map(item => <Dropdown.Item key={item.key} eventKey={item.key}>{item.value}</Dropdown.Item>);
            menuItems.unshift(<Dropdown.Item key={-1} eventKey={null}>(default)</Dropdown.Item>);

            const title = chart.interval ? intervalObjectMap[chart.interval].value : "Resolution";

            intervalDropdown = (
                <DropdownButton className="btn-lightborder" variant="outline-dark" title={title} onSelect={selected => this.props.updateState({interval: selected})}>
                    {menuItems}
                </DropdownButton >
            );
        }

        let metricDropdown = null;
        if (this.props.metric) {
            let metricItems = [];
            if (this.props.chart.attribute && this.props.chart.attribute.match(/authors/)) {
                metricItems = {
                    count: "Count",
                    followers: "Followers",
                }
            }
            else {
                metricItems = {
                    count: "Count",
                    likes: "Likes",
                    replies: "Replies",
                }
            }
            const menuItems = Object.keys(metricItems).map(key => <Dropdown.Item key={key} eventKey={key}>{metricItems[key]}</Dropdown.Item>);

            const title = chart.metric ? metricItems[chart.metric] : "Metric";

            metricDropdown = (
                <DropdownButton className="btn-lightborder" variant="outline-dark" title={title} onSelect={selected => this.props.updateState({metric: selected})}>
                    {menuItems}
                </DropdownButton >
            );
        }

        let domainDropdown = null;
        if (this.props.domain) {
            const menuItemOptions = {
                epoch: "Epoch",
                time_of_day: "Time of Day",
                day_of_week: "Day of Week",
            };

            const menuItems = Object.keys(menuItemOptions).map(key => <Dropdown.Item key={key} eventKey={key}>{menuItemOptions[key]}</Dropdown.Item>);

            const title = chart.domain ? menuItemOptions[chart.domain] : "Domain";

            domainDropdown = (
                <DropdownButton className="btn-lightborder" variant="outline-dark" title={title} onSelect={selected => this.props.updateState({domain: selected})}>
                    {menuItems}
                </DropdownButton >
            );
        }

        let pageSizeDropdown = null;
        if (this.props.pageSize) {
            const items = {
                5: "5 per page",
                10: "10 per page",
                20: "20 per page",
            };

            let menuItems = Object.keys(items).map(key => <Dropdown.Item key={key} eventKey={key}>{items[key]}</Dropdown.Item>);

            const title = chart.pageSize ? items[chart.pageSize] : "Results per page";

            pageSizeDropdown = (
                <DropdownButton className="btn-lightborder" variant="outline-dark" title={title} onSelect={selected => this.props.updateState({pageSize: selected})}>
                    {menuItems}
                </DropdownButton >
            );
        }

        let attributeDropdown = null;
        if (this.props.attribute) {
            const items = {
                query: "Words in query",
                time: "Words in time frame",
                authors: "Authors",
                related_authors: "Reply/Quote/Share authors",
            };

            let menuItems = Object.keys(items).map(key => <Dropdown.Item key={key} eventKey={key}>{items[key]}</Dropdown.Item>);

            const title = chart.attribute ? items[chart.attribute] : "Attribute";

            attributeDropdown = (
                <DropdownButton className="btn-lightborder" variant="outline-dark" title={title} onSelect={selected => this.props.updateState({attribute: selected})}>
                    {menuItems}
                </DropdownButton >
            );
        }

        let sortingDropdown = null;
        if (this.props.sorting) {
            const sortingItems = {
                'newest_first': {
                    key: 'newest_first',
                    value: 'Newest first',
//                    config: [{ created_at: 'desc'}]
                },
//                'oldest_first': {
//                    key: 'oldest_first',
//                    value: 'Oldest first',
////                    config: [{ created_at: 'asc'}]
//                },
                'likes': {
                    key: 'likes',
                    value: 'Likes',
//                    config: [{ like_count: 'desc'}, { created_at: 'desc'}]
                },
//                'shares': {
//                    key: 'shares',
//                    value: 'Shares',
////                    config: [{ share_count: 'desc'}, { created_at: 'desc'}]
//                },
                'replies': {
                    key: 'replies',
                    value: 'Replies',
//                    config: [{ reply_count: 'desc'}, { created_at: 'desc'}]
                },
                'followers': {
                    key: 'followers',
                    value: 'Followers',
//                    config: [{ reply_count: 'desc'}, { created_at: 'desc'}]
                },
            };
            let menuItems = Object.keys(sortingItems).map(key => <Dropdown.Item key={key} eventKey={key}>{sortingItems[key].value}</Dropdown.Item>);

            const title = chart.sorting ? sortingItems[chart.sorting].value : "Sorting";

            sortingDropdown = (
                <DropdownButton className="btn-lightborder" variant="outline-dark" title={title} onSelect={selected => this.props.updateState({sorting: selected})}>
                    {menuItems}
                </DropdownButton >
            );
        }

        return (
            <div className="btn-row">
                <span style={{margin: "auto 6px 11px 0px", fontSize: "20px"}}>Options</span>
                {chartTypeDropdown}
                {intervalDropdown}
                {domainDropdown}
                {pageSizeDropdown}
                {attributeDropdown}
                {metricDropdown}
                {sortingDropdown}
            </div>
        );
    }
}

class Title extends React.Component {
    render() {
//        console.log("charttitle render", this.props.chart);
        const contents = (
            <React.Fragment>
                <Form.Label><span>{S("title")}</span></Form.Label>
                <Form.Control type="text" placeholder={S("chart title...").capitalize()} value={this.props.chart.title} onChange={event => this.props.updateState({title: event.target.value})}/>
            </React.Fragment>
        );

        return (
            <React.Fragment>
                <div className="d-block d-sm-none labeled-input">
                    {contents}
                </div>
                <div className="d-none d-sm-flex labeled-input">
                    {contents}
                </div>
            </React.Fragment>
        );
    }
}

class Background extends React.Component {
    constructor(props) {
        super(props);

//        console.log("props", props);
    }

    shouldComponentUpdate(nextProps, nextState) {
//        console.log("background shouldComponentUpdate");

        return true;
    }

    render() {
//        console.log("background render", this.props);

        const label = <span style={{fontSize: "20px"}}>Background query:</span>;
        const input = <Form.Control
            as="textarea"
            value={this.props.chart.background}
            placeholder={S("query...").capitalize()}
            onChange={event => {console.log("value", event.target.value); this.props.updateState({background: event.target.value})}}
        />;

        return (
            <div style={{marginTop: "12px"}}>
                {label}
                {input}
            </div>
        );
    }
}

class Topic extends React.Component {
    render() {
        const labelItem = (
            <Form.Control
                value={this.props.topic.label}
                placeholder={S("label...").capitalize()}
                onChange={event => this.props.updateState(this.props.topic.id, "label", event.target.value)}
            />
        )

        const queryItem = (
            <Form.Control
                as="textarea"
                value={this.props.topic.query}
                placeholder={S("query...").capitalize()}
                onChange={event => this.props.updateState(this.props.topic.id, "query", event.target.value)}
            />
        )

        let buttonItem = (
            <React.Fragment>
                <Button className="btn-icon" variant="outline-dark" onClick={() => this.props.handleTopic("moveUp", this.props.topic.id)}>
                    <span className="fas fa-arrow-up" aria-hidden="true"></span>
                </Button>
                <Button className="btn-icon" variant="outline-dark" onClick={() => this.props.handleTopic("copy", this.props.topic.id)}>
                    <span className="far fa-copy" aria-hidden="true"></span>
                </Button>
                <Button className="btn-icon" variant="outline-dark" onClick={() => this.props.handleTopic("delete", this.props.topic.id)}>
                    <span className="far fa-trash-alt" aria-hidden="true"></span>
                </Button>
            </React.Fragment>
        );

        if (this.props.sizeHint == "compact") {
            return (
                <React.Fragment>
                    <tr>
                        <td>
                            {labelItem}
                        </td>
                        <td>
                            <div className="btn-row-inline">
                                {buttonItem}
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colSpan="2">
                            {queryItem}
                        </td>
                    </tr>
                </React.Fragment>
            );
        }
        else if (this.props.sizeHint == "wide") {
            return (
                <tr>
                    <td>
                        {labelItem}
                    </td>
                    <td>
                        {queryItem}
                    </td>
                    <td>
                        <div className="btn-row-inline">
                            {buttonItem}
                        </div>
                    </td>
                </tr>
            );
        }
    }
}


class ListInput extends React.Component {
    constructor(props) {
        super(props);

        this.selectLists = this.selectLists.bind(this);

        this.state = {
            options: [],
        };
    }

    componentDidMount() {
        fetch(`http://${config.api}/finder/lists/`)
        .then(response => response.json())
        .then(json => {
//            console.log("listinput json", json)
            this.setState({options: json.map(item => item.name)});
        });
    }

    selectLists(selected) {
        this.props.updateState({selected: selected.map(item => item.label ? item.label : item)});
    }

    render() {
        const listSelection = (
            <Typeahead
                selected={this.props.selected}
                multiple={true}
                clearButton={true}
                placeholder="Select lists..."
                options={this.state.options}
                onChange={this.selectLists}
            />
        );

        return listSelection;
    }
}

class Topics extends React.Component {
    constructor(props) {
        super(props);

        this.updateState = this.updateState.bind(this);
        this.asTable = this.asTable.bind(this);
        this.handleTopic = this.handleTopic.bind(this);
        this.confirm = this.confirm.bind(this);
        this.fromLists = this.fromLists.bind(this);

        this.state = {
            confirm: {},
            lists: null,
            named: null,
            manual: true,
        }
    }

    componentDidMount() {
        if (!this.props.chart.topics || this.props.chart.topics.length == 0) {
            this.handleTopic("add");
        }
    }

    updateState(topicId, field, value) {
//        console.log("topics updateState", topicId, field, value);
        let topics = [...this.props.chart.topics];

        topics[topicId][field] = value;

//        console.log("topics updateState", topics);

        this.props.updateState({topics: topics});
    }

    fromLists(lists) {
        console.log("fromLists", lists);

        if (lists.length == 0) {
            return;
        }

        const listsString = this.state.lists.lists.join(',');

        fetch(`http://${config.api}/authors/?lists=${listsString}`)
        .then(response => response.json())
        .then(json => {
            console.log("json", json);

            let topics = [];
            json.forEach(author => {
                topics.push({
                    label: author,
                    query: `author: ${author}`,
                    id: topics.length,
                });
            });

            if (topics.length == 0) {
                topics.push({label: "", query: "", id: 0});
            }

            this.props.updateState({topics});
        });
    }

    asTable(sizeHint, topics) {
//        console.log("topics render", this.props.chart);
        let table = null;
        if (topics.length > 0) {
            const head = sizeHint == "compact" ? (
                <tr>
                    <td style={{width: "100%"}}/>
                </tr>
            ) : (
                <tr>
                    <td style={{width: "20%"}}>{S("label")}</td>
                    <td style={{width: "100%"}}>{S("query")}</td>
                </tr>
            );

            const body = topics.map(topic => <Topic sizeHint={sizeHint} key={topic.id} topic={topic} handleTopic={this.handleTopic} updateState={this.updateState}/>);

            table = (
                <table>
                    <thead>
                        {head}
                    </thead>
                    <tbody>
                        {body}
                    </tbody>
                </table>
            );
        }

        let loadControls = null;
        if (this.state.lists) {
            loadControls = (
                <div style={{marginBottom: "2px", display: "flex"}}>
                    <div style={{width: "100%"}}>
                        <ListInput nolabel selected={this.state.lists.lists} updateState={state => this.setState({lists: {lists: state.selected}})}/>
                    </div>
                    <Button variant="outline-primary" style={{height: "35px", marginLeft: "6px"}} onClick={() => {this.fromLists(this.state.lists.lists)}}>Ok</Button>
                </div>
            );
        }
        if (this.state.named) {
            loadControls = (
                <LabeledDropdown
                    label="Topic set:"
                    selected={this.state.named.name}
                    options={this.props.topics}
                    onChange={name => {this.setState({named: {name: name}})}}
                />
            );
        }

        const types = ["manual", "lists", "named"];
        const labelMap = {
            manual: "Manual",
            lists: "Generate from lists",
            named: "Load from topic sets",
        }
        const onClick = type => {
            let state = {manual: false, lists: null, named: null};
            if (type == "manual") {
                state.manual = true;
            }
            else if (type == "lists") {
                state.lists = {lists: []};
            }
            else if (type == "named") {
                state.named = {name: null};
            }
            this.setState(state);
        }

        let buttonData = {};
        types.forEach(type => {
            buttonData[type] = {variant: "outline-secondary", className: "btn-lightborder", label: labelMap[type]};
        });

        types.forEach(type => {
            if (this.state[type]) {
                buttonData[type].variant = "outline-primary";
                buttonData[type].className = "";
            }
        });

        let buttons = types.map(type => (
            <Button key={type} size="sm" variant={buttonData[type].variant} onClick={() => onClick(type)}>
                <span>{buttonData[type].label}</span>
            </Button>
        ));

        return (
            <div>
                <Form.Label><span style={{fontSize: "20px"}}>{S("Topics")}</span></Form.Label>
                <div>
                    <div className="btn-row" style={{marginBottom: "6px"}}>
                        {buttons}
                    </div>
                    {loadControls}
                </div>
                {table}
            </div>
        );
    }

    handleTopic(action, topicId) {
//        console.log("topics handleTopic", action, topicId, JSON.stringify(this.props.chart.topics));

        let topics = this.props.chart.topics ? [...this.props.chart.topics] : [];

        if (action == "add") {
            let topic = {label: "", query: "", id: topics.length};

            topics.push(topic);
        }
        else if (action == "moveUp") {
            if (topicId > 0) {
                const topic = topics[topicId];
                topics[topicId] = topics[topicId-1];
                topics[topicId-1] = topic;

                topics[topicId].id = topicId;
                topics[topicId-1].id = topicId-1;
            }
        }
        else if (action == "copy") {
            const topic = {...topics[topicId], id: topics.length};

            topics.push(topic);
        }
        else if (action == "delete") {
            this.confirm(S("Delete topic?"), () => {
                if (topics.length > 1) {
//                    console.log("delete normal");
                    topics.splice(topicId, 1);

                    for (let i=topicId; i<topics.length; i++) {
                        topics[i].id = i;
                    }
                }
                else {
//                    console.log("delete single");
                    topics[0].label = "";
                    topics[0].query = "";
                }
            });
        }

        this.props.updateState({topics: topics});
    }

    confirm(message, callback) {
        this.setState({confirm: {show: true, message: message, callback: callback}});
    }

    render() {
        let topics = this.props.chart.topics || [];

        return (
            <React.Fragment>
                <div className="d-block d-sm-none">
                    {this.asTable("compact", topics)}
                </div>
                <div className="d-none d-sm-block">
                    {this.asTable("wide", topics)}
                </div>
                <div className="btn-row">
                    <Button className="btn-icon" variant="outline-dark" onClick={() => this.handleTopic("add")}>
                        <span className="fas fa-plus" aria-hidden="true"></span>
                    </Button>
                </div>
                <Confirm confirm={this.state.confirm} update={update => this.setState({confirm: update})}/>
            </React.Fragment>
        );
    }
}

class ChartConfig extends React.Component {
    render() {
        return (
            <div>
                {this.props.options}
                {this.props.title}
                {this.props.topics}
                {this.props.listInput}
                {this.props.background}
            </div>
        );
    }
}

class LineChartConfig extends React.Component {
    shouldComponentUpdate(nextProps) {
        if (this.props.chart.title != nextProps.chart.title) {
//            console.log("linechartconfig shouldComponentUpdate true");
            return true;
        }
        if (this.props.chart.interval != nextProps.chart.interval) {
//            console.log("linechartconfig shouldComponentUpdate true");
            return true;
        }
        if (this.props.chart.metric != nextProps.chart.metric) {
//            console.log("linechartconfig shouldComponentUpdate metric true");
            return true;
        }
        if (this.props.chart.domain != nextProps.chart.domain) {
//            console.log("linechartconfig shouldComponentUpdate domain true");
            return true;
        }
        if (test(this.props.chart.topics, nextProps.chart.topics)) {
//            console.log("linechartconfig shouldComponentUpdate true");
            return true;
        }

//        console.log("linechartconfig shouldComponentUpdate false");
        return false;
    }

    render() {
//        console.log("linechartconfig render");
        const options = <Options {...this.props} chartType interval domain metric/>;
        const title = <Title {...this.props}/>;
        const topics = <Topics {...this.props}/>;

        return (
            <ChartConfig {...{options, title, topics}}/>
        );
    }
}

class PieChartConfig extends React.Component {
    shouldComponentUpdate(nextProps) {
        if (this.props.chart.title != nextProps.chart.title) {
//            console.log("linechartconfig shouldComponentUpdate true");
            return true;
        }
        if (test(this.props.chart.topics, nextProps.chart.topics)) {
//            console.log("piechartconfig shouldComponentUpdate true");
            return true;
        }
        if (this.props.chart.metric != nextProps.chart.metric) {
//            console.log("linechartconfig shouldComponentUpdate metric true");
            return true;
        }

//        console.log("piechartconfig shouldComponentUpdate false");
        return false;
    }

    render() {
//        console.log("piechartconfig render");
        const options = <Options {...this.props} chartType metric/>;
        const title = <Title {...this.props}/>;
        const topics = <Topics {...this.props}/>;

        return (
            <ChartConfig {...{options, title, topics}}/>
        );
    }
}

class PopularityChartConfig extends React.Component {
    shouldComponentUpdate(nextProps) {
        if (this.props.chart.title != nextProps.chart.title) {
//            console.log("linechartconfig shouldComponentUpdate true");
            return true;
        }
        if (this.props.chart.attribute != nextProps.chart.attribute) {
//            console.log("postingschartconfig shouldComponentUpdate true");
            return true;
        }
        if (this.props.chart.metric != nextProps.chart.metric) {
//            console.log("postingschartconfig shouldComponentUpdate true");
            return true;
        }
        if (this.props.chart.background != nextProps.chart.background) {
//            console.log("postingschartconfig shouldComponentUpdate true");
            return true;
        }
        if (test(this.props.chart.topics, nextProps.chart.topics)) {
//            console.log("piechartconfig shouldComponentUpdate true");
            return true;
        }
        if (this.props.chart.pageSize != nextProps.chart.pageSize) {
//            console.log("postingschartconfig shouldComponentUpdate true");
            return true;
        }

//        console.log("piechartconfig shouldComponentUpdate false");
        return false;
    }

    render() {
//        console.log("piechartconfig render", this.props.chart);
        let metric = {};
        if (this.props.chart.attribute && this.props.chart.attribute.match(/authors/)) {
            metric = {metric: true};
        }

        const options = <Options {...this.props} chartType pageSize attribute {...metric}/>;
        const title = <Title {...this.props}/>;
        const topics = <Topics {...this.props}/>;

        let background = null;
        if (this.props.chart.attribute && this.props.chart.attribute.match(/query|time/)) {
            background = <Background {...this.props}/>;
        }

        return (
            <ChartConfig {...{options, title, topics, background}}/>
        );
    }
}

class PostingsChartConfig extends React.Component {
    shouldComponentUpdate(nextProps) {
        if (this.props.chart.title != nextProps.chart.title) {
//            console.log("linechartconfig shouldComponentUpdate true");
            return true;
        }
        if (this.props.chart.pageSize != nextProps.chart.pageSize) {
//            console.log("postingschartconfig shouldComponentUpdate true");
            return true;
        }
        if (this.props.chart.sorting != nextProps.chart.sorting) {
//            console.log("postingschartconfig shouldComponentUpdate true");
            return true;
        }
        if (test(this.props.chart.topics, nextProps.chart.topics)) {
//            console.log("postingschartconfig shouldComponentUpdate true");
            return true;
        }

//        console.log("postingschartconfig shouldComponentUpdate false");
        return false;
    }

    render() {
//        console.log("postingschartconfig render");
        const options = <Options {...this.props} chartType pageSize sorting/>;
        const title = <Title {...this.props}/>;
        const topics = <Topics {...this.props}/>;

        return (
            <ChartConfig {...{options, title, topics}}/>
        );
    }
}

class LiveChartConfig extends React.Component {
    shouldComponentUpdate(nextProps) {
        if (this.props.chart.title != nextProps.chart.title) {
//            console.log("linechartconfig shouldComponentUpdate true");
            return true;
        }
        if (this.props.chart.pageSize != nextProps.chart.pageSize) {
//            console.log("postingschartconfig shouldComponentUpdate true");
            return true;
        }
        if (test(this.props.chart.selected, nextProps.chart.selected)) {
//            console.log("postingschartconfig shouldComponentUpdate true");
            return true;
        }

//        console.log("postingschartconfig shouldComponentUpdate false");
        return false;
    }

    render() {
//        console.log("postingschartconfig render");
        const options = <Options {...this.props} chartType pageSize/>;
        const title = <Title {...this.props}/>;
        const listInput = (
            <div style={{display: "flex", marginTop: "6px"}}>
                <span style={{fontSize: "20px", margin: "auto 6px auto 0px"}}>Lists</span>
                <div style={{width: "100%"}}>
                    <ListInput selected={this.props.selected} updateState={this.props.updateState}/>
                </div>
            </div>
        );

        return (
            <ChartConfig {...{options, title, listInput}}/>
        );
    }
}

class NewChartConfig extends React.Component {
    shouldComponentUpdate() {
        return false;
    }

    render() {
//        console.log("newchartconfig render");
        const updateState = this.props.updateState;

        const options = <Options {...{updateState}} chartType/>;

        return (
            <ChartConfig {...{options}}/>
        );
    }
}

export default class ChartEditor extends React.Component {
    constructor(props) {
        super(props);

        this.hide = this.hide.bind(this);
        this.saveChart = this.saveChart.bind(this);
        this.updateState = this.updateState.bind(this);

        let state = {};
        if (this.props.chart) {
            state = {...this.props.chart}
        }
        this.state = state;
    }

    componentDidMount() {
        fetch(`http://${config.api}/topics`)
        .then(response => response.json())
        .then(json => {
            console.log("topics", json)
        });

    }

    static getDerivedStateFromProps(props, state) {
//        console.log("charteditor getDerivedStateFromProps", JSON.stringify(props), JSON.stringify(state));
        // if we do not have a chart prop specified, or we already initialized, do not do anything
        if (!props.chart || props.chart.id == state.id) {
//            console.log("charteditor getDerivedStateFromProps null");
            let diffState = {};
            if (state.type) {
                if (state.type == "PostingsChart") {
                    if (!state.sorting) {
                        diffState.sorting = "newest_first";
                    }

                    if (!state.pageSize) {
                        diffState.pageSize = 5;
                    }
                }
                else if (state.type == "PopularityChart") {
                    if (!state.attribute) {
                        diffState.attribute = "query";
                    }
                    if (!state.pageSize) {
                        diffState.pageSize = 10;
                    }
                    if (!state.background) {
                        diffState.background = "";
                    }
                }
                else if (state.type == "LiveChart") {
                    if (!state.selected) {
                        diffState.selected = [];
                    }
                    if (!state.pageSize) {
                        diffState.pageSize = 5;
                    }
                }

            }

//            console.log("charteditor getDerivedStateFromProps diffState", diffState);

            return diffState;
        }

        // otherwise, if we did get a chart as prop, use it to initialize state
        if (props.chart) {
//            console.log("charteditor getDerivedStateFromProps chart", props.chart);
            return props.chart;
        }

        return null;
    }

    saveChart() {
//        console.log("chartediter saveChart", this.state);
        if (this.props.mode == "publish") {
//            console.log("WARNING. This should not happen. ChartEditor.saveChart");
        }

        this.props.saveChart(this.state);

        let state = {};
        Object.keys(this.state).map(key => {state[key] = undefined;});

        this.setState(state);
    }

    hide() {
        this.props.onHide();

        let state = {};
        Object.keys(this.state).map(key => {state[key] = undefined;});

        this.setState(state);
    }

    updateState(diffState) {
//        console.log("charteditor updateState", diffState);

        this.setState(diffState);
    }

    render() {
//        console.log("charteditor render", JSON.stringify(this.state));
        let chartConfig = null;
        if (this.props.show) {
            const chart = this.state;
            const updateState = this.updateState;

            if (chart && chart.type == "LineChart") {
//                console.log("charteditor render linechart");
                chartConfig = <LineChartConfig {...{chart, updateState}}/>;
            }
            else if (chart && chart.type == "PieChart") {
//                console.log("charteditor render piechart");
                chartConfig = <PieChartConfig {...{chart, updateState}}/>;
            }
            else if (chart && chart.type == "PostingsChart") {
//                console.log("charteditor render postingschart");
                chartConfig = <PostingsChartConfig {...{chart, updateState}}/>;
            }
            else if (chart && chart.type == "PopularityChart") {
                chartConfig = <PopularityChartConfig {...{chart, updateState}}/>;
            }
            else if (chart && chart.type == "LiveChart") {
                chartConfig = <LiveChartConfig {...{chart, updateState}}/>;
            }
            else {
//                console.log("charteditor render new");
                chartConfig = <NewChartConfig {...{updateState}}/>
            }
        }

        let saveButton = null;
        if (this.props.mode == "project") {
            saveButton = (
                <Button variant="outline-primary" onClick={this.saveChart}>
                    <span className="btn-label">
                        {S("save")}
                    </span>
                </Button>
            );
        }

        return (
            <Modal show={this.props.show} dialogClassName="chart-editor" onHide={this.hide}>
                <Modal.Header>
                    <Modal.Title>
                        <span className="dialog-title">
                            {S("chart editor")}
                        </span>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {chartConfig}
                </Modal.Body>
                <Modal.Footer>
                    {saveButton}
                    <Button variant="outline-secondary" onClick={this.hide}>
                        <span className="btn-label">
                            {S("cancel")}
                        </span>
                    </Button>
                </Modal.Footer>
            </Modal>
        );
    }
}
