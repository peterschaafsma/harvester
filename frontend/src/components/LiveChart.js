import React from "react";
//import { event } from "d3-selection";
//import { timeFormat } from "d3-time-format";
import { Table } from "react-bootstrap";
import { NavigationButtons } from "./Controls.js";
import ChartHeader from "./ChartHeader.js";
import { config, S } from "root/config.js";
//import { countFormat } from "root/formatting.js";

import * as moment from "moment";


export default class LiveChart extends React.Component {
    constructor(props) {
        super(props);

//        console.log("livechart constructor", props);

        this.onMessage = this.onMessage.bind(this);
        this.onPrevious = this.onPrevious.bind(this);
        this.onNext = this.onNext.bind(this);
        this.onFirst = this.onFirst.bind(this);
        this.onLast = this.onLast.bind(this);
        this.renderPage = this.renderPage.bind(this);
        this.makeNotification = this.makeNotification.bind(this);
        this.updatePages = this.updatePages.bind(this);
        this.pageSize = this.pageSize.bind(this);
        this.checkAll = this.checkAll.bind(this);
        this.onClick = this.onClick.bind(this);
        this.updateSocketSubscription = this.updateSocketSubscription.bind(this);
        this.reload = this.reload.bind(this);

        this.state = {
            currentPageIndex: -1,
            currentPage: [],
            highlighted: {},
        };

        this.items = [];
        this.pages = [];
    }

    componentDidMount() {
//        console.log("livechart componentDidMount", this.props);

//        console.log("props", this.props);

        this.updateSocketSubscription("subscribe");

        this.reload();

    }

    reload() {
//        console.log("context", this.props.context.data);

        const count = 4 * this.pageSize();
        const lists = (this.props.chart.selected || []).join(',');
        const account_id = this.props.context.data.projectContext.account_id

        fetch(`http://${config.api}/alerts/?offset=0&&count=${count}&&lists=${lists}&&account_id=${account_id}`)
        .then(response => response.json())
        .then(json => {
            if (json.notifications.length > 0) {
                this.items = this.items.concat(json.notifications);
                this.items.sort((a, b) => a.id < b.id ? 1 : -1);
                this.updatePages();

                this.setState({currentPageIndex: 0, currentPage: this.pages[0]});
            }
        });
    }

    componentWillUnmount() {
        this.updateSocketSubscription("subscribe");
    }

    shouldComponentUpdate(nextProps, nextState) {
//        console.log("paginator shouldComponentUpdate");
//        console.log(JSON.stringify(this.props));
//        console.log(JSON.stringify(nextProps));
//        console.log(JSON.stringify(this.state));
//        console.log(JSON.stringify(nextState));

        if (this.state.currentPageIndex != nextState.currentPageIndex) {
            return true;
        }

        if (this.state.currentPage != nextState.currentPage) {
            return true;
        }

        if (test(this.state.highlighted != nextState.highlighted)) {
            return true;
        }

        if (this.props.pageSize != nextProps.pageSize) {
            return true;
        }

        if (this.props.chart.title != nextProps.chart.title) {
//            console.log("linechart shouldComponentUpdate true line");
            return true;
        }

        return false;
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.chart.pageSize != this.props.chart.pageSize) {
            this.updatePages();

            this.setState({currentPageIndex: 0, currentPage: this.pages[0]});
        }

        if (test(prevProps.chart.selected, this.props.chart.selected)) {
            this.updateSocketSubscription("unsubscribe");

            this.items = [];
            this.reload();

            this.updateSocketSubscription("subscribe");
        }
    }

    updateSocketSubscription(action) {
//        console.log("livechart updateSocketSubscription", action);

        if (action == "subscribe") {
            const lists = this.props.chart.selected || [];

            this.props.context.func.updateSocketSubscription("subscribe", this.props.chart.id, { lists: lists }, this.onMessage);
        }
        else if (action == "unsubscribe") {
            this.props.context.func.updateSocketSubscription("unsubscribe", this.props.chart.id);
        }
    }

    pageSize() {
//        console.log("pageSize", this.props.pageSize);
        return this.props.chart.pageSize || 5;
    }

    onMessage(message) {
//        console.log("livechart onMessage", message);

        if (message.type != "author") {
            return;
        }

        this.items.unshift(message);

        if (this.state.currentPageIndex == 0) {
            if (this.pages.length == 0) {
                this.pages.push([message]);
            }
            else {
                this.updatePages();
            }

            let highlighted = {...this.state.highlighted};
            highlighted[message.id] = 1;

            this.setState({currentPage: this.pages[0], highlighted: highlighted});
        }
        else {
            let highlighted = {...this.state.highlighted};
            highlighted[message.id] = 1;

            this.setState({highlighted});
        }
    }

    checkAll() {
        this.setState({highlighted: {}});
    }

    onClick(notificationId) {
        if (notificationId in this.state.highlighted) {
            let highlighted = {...this.state.highlighted};
            delete highlighted[notificationId];
            this.setState({highlighted});
        }
    }

    updatePages() {
        let items = [...this.items];

        this.pages = [];
        while (items.length > 0) {
            this.pages.push(items.splice(0, this.pageSize()));
        }

        if (this.pages.slice(-1)[0].length == this.pageSize()) {
            this.pages.push([]);
        }

//        console.log("pages", this.pages);
    }

    onPrevious() {
//        console.log("prev", this.items, this.pages);
        if (this.state.currentPageIndex > 0) {
            if (this.state.currentPageIndex == 1) {
                if (this.items[0].id != this.pages[0][0].id) {
                    this.updatePages();
                }
            }

            this.setState({
                currentPageIndex: this.state.currentPageIndex-1,
                currentPage: this.pages[this.state.currentPageIndex-1],
            });
        }
    }

    onNext() {
//        console.log("before next", this.state.currentPageIndex, this.props.pageSize);
        if (this.state.currentPage.length == this.pageSize()) {
//            console.log("setting new state", this.pages[this.state.currentPageIndex+1]);
            this.setState({
                currentPageIndex: this.state.currentPageIndex+1,
                currentPage: this.pages[this.state.currentPageIndex+1],
            });
        }
    }

    onFirst() {
        if (this.state.currentPageIndex > 0) {
            if (this.items[0].id != this.pages[0][0].id) {
                this.updatePages();
            }

            this.setState({
                currentPageIndex: 0,
                currentPage: this.pages[0],
            });
        }
    }

    onLast() {
        if (this.state.currentPageIndex < this.pages.length - 1) {
            this.setState({
                currentPageIndex: this.pages.length - 1,
                currentPage: this.pages[this.pages.length - 1],
            });
        }
    }


    decodeHTMLEntities(text) {
        const textArea = document.createElement('textarea');
        textArea.innerHTML = text;
        return textArea.value;
    }
    makeNotification(notification) {
//        console.log("notif makeNotification", notification);

        const tweetUrl = `http://twitter.com/${notification.parameters.screen_name}/statuses/${notification.parameters.status_id}`;


        let text = notification.parameters.text.replace(/https?:\/\/[-#?&\w\.\/]+/, "");

        if (text.length == 0) {
            text = "(empty)";
        }

        const message = (
            <span>
                @{notification.parameters.screen_name}{": "}
                <a href={tweetUrl} target="blank">
                    {this.decodeHTMLEntities(text)}
                </a>
            </span>
        );

//        console.log("created_at", notification.parameters.created_at);

        const created_at = moment(notification.parameters.created_at).calendar();

        let style = {};
        if (notification.id in this.state.highlighted) {
            style = { fontWeight: "bold" };
        }

        return (
            <tr style={style} key={notification.id} onClick={() => this.onClick(notification.id)}>
                <td>{created_at}</td>
                <td>{message}</td>
            </tr>
        );
    }

    renderPage(page) {
        let footer = null;
        if (page.length < this.pageSize()) {
            footer = <p style={{textAlign: "center", marginBottom: "20px"}}>(no more items)</p>;
        }

        const rows = page.map(notification => this.makeNotification(notification));

        return (
            <div>
                <Table>
                    <thead>
                        <tr>
                            <th>Received</th>
                            <th>Message</th>
                        </tr>
                    </thead>
                    <tbody>
                        {rows}
                    </tbody>
                </Table>
                {footer}
            </div>
        );
    }

    render() {
//        console.log("livechart render");

        const context = {
            func: {
                ...this.props.context.func,
                checkAll: this.checkAll,
            },
            data: {
                chart: this.props.chart,
                variables: this.props.context.data.variables,
            },
        };

        const header = <ChartHeader mode={this.props.mode} expanded={this.props.expanded} hasRefresh={false} hasCheck context={context}/>;

        const content = this.renderPage(this.state.currentPage);

        const canPrevious = this.state.currentPageIndex != 0;
        const canNext = this.state.currentPage.length == this.pageSize();

        let parameters = {}
        if (this.state.currentPageIndex > 0 && this.items[0].id != this.pages[0][0].id) {
            parameters.highlightFirstButton = true;
        }
        if (this.state.currentPageIndex == 1 && this.items[0].id != this.pages[0][0].id) {
            parameters.highlightPrevButton = true;
        }

        const navigationButtons = (
            <NavigationButtons
                canPrevious={canPrevious}
                canNext={canNext}

                onNext={this.onNext}
                onPrevious={this.onPrevious}
                onFirst={this.onFirst}
                onLast={this.onLast}
                pageIndex={this.state.currentPageIndex}

                {...parameters}
            />
        );

        return (
            <div>
                {header}
                {content}
                {navigationButtons}
            </div>
        )
    }
}
