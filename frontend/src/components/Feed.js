import React from "react";
import {Button} from "react-bootstrap";
import {timeFormat, timeParse} from "d3-time-format";


export default class Feed extends React.Component {
    constructor(props) {
        super(props);

        this.typeMap = {
            'twitter_firehose': 'Search',
            'twitter_account': 'Timeline',
        }
    }

    render() {
        const timeLocal = timeFormat("%d %b %H:%Mh");

        const harvest_last = timeLocal(new Date(this.props.feed.feed.harvest_last));
        const harvest_next = timeLocal(new Date(this.props.feed.feed.harvest_next));

        let type = this.typeMap[this.props.feed.feed.type];

        let style = {};
        return (
            <tr style={{...style}} key={this.props.feed.feed.id}>
                <td><span style={{verticalAlign: "middle"}}>{type}</span></td>
                <td><span style={{verticalAlign: "middle"}}>{this.props.feed.feed.uid}</span></td>
                <td><span>{harvest_last}</span></td>
                <td><span>{harvest_next}</span></td>
                <td>
                    <Button style={{float: 'right'}} className="btn-icon" variant="outline-dark" onClick={() => this.props.onHarvestFeed(this.props.feed.feed.id)}>
                        <span className="fas fa-sync" aria-hidden="true"></span>
                    </Button>
                </td>
            </tr>
        );
    }
}