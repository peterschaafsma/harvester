from django.urls import path, re_path, reverse_lazy
from django.views.generic import RedirectView

from frontend import views


urlpatterns = [
    # re_path(r'login/$', views.login_view, name='login'),
    # re_path(r'logout/$', views.logout_view, name='logout'),
    # re_path(r'register/$', views.register, name='register'),
    re_path(r'^(?P<page>(overview|feeds|project|lists|topics))/', views.application_view, name='application'),
    re_path(r'^contact/', views.contact_view, name='contact'),
    re_path(r'^publish/(?P<uuid>([-a-z0-9]+))$', views.publish_view, name='publish'),
    re_path(r'^publish/(?P<name>([a-z0-9_]+))/(?P<uuid>([-a-z0-9]+))$', views.publish_view, name='publish'),
    re_path(r'^project/(?P<project_id>(\d+))/', views.application_view, name='project'),
    re_path(r'^(?P<page>(login|register|confirm|logout))/$', views.account_view, name='account'),
    re_path(r'^$', RedirectView.as_view(url=reverse_lazy('account', kwargs={'page': 'login'}))),
]