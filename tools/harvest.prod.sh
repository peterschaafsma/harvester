#!/bin/bash

if [ $(systemctl is-active es) != "active" ] ; then
  echo "Elasticsearch not running"
  exit 1
fi

source /home/jetze/site/bin/activate
/home/jetze/site/harvester/manage.py harvest

exit 0
