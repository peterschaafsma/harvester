#!/bin/bash
date
if [ $(systemctl --user is-active es) != "active" ] ; then
  echo "Elasticsearch not running"
  exit 1
fi

source /home/jetze/programming/bin/activate
/home/jetze/programming/harvester/manage.py harvest

exit 0
