cd /home/jetze/programming/harvester/ && npm run app-prod
cd /home/jetze/programming/harvester/ && npm run login-prod
rsync -ur --exclude=".git" --exclude="node_modules" --exclude="frontend/static/frontend/application.js"  --exclude="frontend/static/frontend/account.js" /home/jetze/programming/harvester/* h2823906.stratoserver.net:site/harvester/
ssh h2823906.stratoserver.net "cd site/harvester && npm i"
ssh h2823906.stratoserver.net /home/jetze/site/bin/python /home/jetze/site/harvester/manage.py migrate
#ssh h2823906.stratoserver.net /home/jetze/site/bin/python /home/jetze/site/harvester/manage.py es --update-mapping index=authors type=author config=authors
#ssh h2823906.stratoserver.net /home/jetze/site/bin/python /home/jetze/site/harvester/manage.py es --update-mapping index=tweets type=tweet config=tweets
ssh h2823906.stratoserver.net mv /home/jetze/site/harvester/frontend/static/frontend/application.min.js /home/jetze/site/harvester/frontend/static/frontend/application.js
ssh h2823906.stratoserver.net mv /home/jetze/site/harvester/frontend/static/frontend/account.min.js /home/jetze/site/harvester/frontend/static/frontend/account.js
