import pyparsing as pp

from threading import Lock

pp.ParserElement.enablePackrat()

lock = Lock()

memo = {}
def memoize(function):
    global memo
    def f(query):
        if query not in memo:
            # this calls the _parse method, which will return the context update, that we store with the bm query
            memo[query] = function(query)

        return memo[query]

    return f


class QueryParser:
    """
    wildcards
    near
    """

    CONTEXT = {}
    INSTANCE = None

    @staticmethod
    def instance():
        if not QueryParser.INSTANCE:
            QueryParser.INSTANCE = QueryParser()

        return QueryParser.INSTANCE

    def __init__(self, **_query_kwargs):
        self._parser = None
        self._query_kwargs = _query_kwargs

    def optimize(self, query):
        # NOTE: this function is not idempotent due to the fact that it generated things like
        # {"match": {"text": "aap noot"}}, which is not seen by this function as a query with 2 words in disjunction.
        # Instead, it sees "aap noot" as a single string. This runs into trouble in a conjunction of such things.
        # For example, instead of the expected
        # {"bool": {"must": [{"match": {"text": "aap noot"}}, {"match": {"text": "wim zus"}}]}}
        # we get
        # {"match": { "text": { "query": "aap noot wim zus", "operator": "and" }}}
        # which would be correct if "aap noot" and "wiw zus" actually were single strings


        # recursively group together all SHOULD and MUST clauses that are at a logically equivalent position in the
        # boolean tree. Effectively, rewrite (a | (b | (c | d))) to (a | b | c | d), and similarly for &.
        query = self.hoist(query)

        # recursively group together all MUST clauses directly under a MUST_NOT clause. So rewrite (~a & ~b) &
        # (~c & ~d) to (~a & ~b & ~c & ~d)
        query = self.unify(query)

        # term->terms, match->multi_match etc.
        query = self.minimize(query)

        # single field multi_matches back to match queries
        query = self.reduce(query)

        return query

    def hoist(self, query_body, boolean_query=None):
        """
        Hoist queries under logically equivalent boolean clauses to the highest level possible. This will allow more
        effective optimization.
        """
        if type(query_body) is dict:
            hoisted = {}
            for key, value in query_body.items():
                if key == 'bool':
                    hoisted['bool'] = self.hoist(value)
                elif key in ['should', 'must']:
                    hoisted[key] = self.hoist(value, boolean_query=key)
                else:
                    hoisted[key] = value
        else:
            hoisted = []
            for query in query_body:
                key, value = list(query.items())[0]
                if key == 'bool':
                    sub_key, sub_value = list(value.items())[0]
                    if boolean_query == sub_key:
                        for sub_query in self.hoist(sub_value, boolean_query=sub_key):
                            query_body.append(sub_query)
                        continue
                    else:
                        hoisted.append(self.hoist(query))
                else:
                    hoisted.append(query)

        return hoisted

    def unify(self, query_body, boolean_query=None):
        """
        Merge must_not queries under must. Very similar to hoisting.
        """
        if type(query_body) is dict:
            unified = {}
            for key, value in query_body.items():
                if key == 'bool':
                    unified['bool'] = self.unify(value)
                elif key in ['must', 'must_not']:
                    unified[key] = self.unify(value, boolean_query=key)
                else:
                    unified[key] = value
        else:
            unified = []
            if boolean_query == 'must':
                must_not_queries = []
                for query in query_body:
                    if list(query.keys())[0] == 'bool' and list(list(query.values())[0].keys())[0] == 'must_not':
                        for sub_query in list(list(query.values())[0].values())[0]:
                            must_not_queries.append(sub_query)
                    else:
                        unified.append(query)
                if must_not_queries:
                    unified.append(NOT(must_not_queries))
            else:
                unified = query_body

        return unified

    def compare_ranges(self, a, b):
        if a[0] is None:
            return -1

        if b[0] is None:
            return 1

        if a[0] < b[0]:
            return -1
        elif a[0] == b[0]:
            if a[1] or not b[1]:
                return -1
            elif a[1] == b[1]:
                return 0
            elif not a[1] and b[1]:
                return 1
        elif a[0] > b[0]:
            return 1

    def merge_ranges(self, ranges, is_conjunction):
        def f(range):
            low = [None, None]
            high = [None, None]

            if 'gte' in range:
                low = [range['gte'], True]
            if 'gt' in range:
                low = [range['gt'], False]
            if 'lte' in range:
                high = [range['lte'], True]
            if 'lt' in range:
                high = [range['lt'], False]

            return [low, high]

        if is_conjunction:
            merged_range = [[None, None], [None, None]]

            for low, high in map(f, ranges):
                if merged_range[0][0] is None:
                    merged_range[0] = low
                elif low[0] is not None:
                    if low[0] > merged_range[0][0]:
                        merged_range[0] = low
                    elif low[0] == merged_range[0][0] and merged_range[0][1]:
                        merged_range[0][1] = low[1]

                if merged_range[1][0] is None:
                    merged_range[1] = high
                elif high[0] is not None:
                    if high[0] < merged_range[1][0]:
                        merged_range[0] = high
                    elif high[0] == merged_range[1][0] and merged_range[1][1]:
                        merged_range[1][1] = high[1]

            merged_ranges = [merged_range]

            if len(merged_ranges) == 1:
                low, high = merged_ranges[0]

                if low[0] > high[0]:
                    merged_ranges = [[[None, None], [None, None]]]

                if low[0] == high[0] and (not low[1] or not high[1]):
                    merged_ranges = [[[None, None], [None, None]]]

        else:
            ranges = map(f, ranges)
            sorted_ranges = sorted(ranges, key=lambda x: (-float('inf') if x[0][0] is None else x[0][0], False if x[0][1] else True))

            merged_ranges = [[[sorted_ranges[0][0][0], sorted_ranges[0][0][1]],[sorted_ranges[0][1][0], sorted_ranges[0][1][1]]]]
            for low, high in sorted_ranges:
                merged = False
                done = False
                for range in merged_ranges:
                    if low[0] is None:
                        if high[0] > range[1][0]:
                            range[1][0] = high
                        elif high[0] == range[1][0] and high[1]:
                            range[1][1] = True

                        merged = True
                    elif high[0] is None:
                        range[1] = [None, None]

                        merged = True
                        done = True
                    else:
                        if low[0] > range[1][0]:
                            break
                        elif low[0] == range[1][0] and (low[1] or range[1][1]):
                            range[1] = high
                            merged = True
                        elif low[0] <= range[1][0]:
                            if high[0] > range[1][1]:
                                range[1] = high
                            elif high[0] == range[1][0]:
                                range[1][1] = high[1] or range[1][1]
                            merged = True

                if not merged:
                    merged_ranges.append([low, high])

                if done:
                    break

        ranges = []
        for low, high in merged_ranges:
            range = {}

            if low[0] is not None:
                range['gte' if low[1] else 'gt'] = low[0]

            if high[0] is not None:
                range['lte' if high[1] else 'lt'] = high[0]

            ranges.append(range)

        return ranges

    def minimize(self, query_body, **kwargs):
        """
        We minimize ES queries into more readable, and possibly faster structures, by reducing lists of queries to
        logically equivalent short-hand ES syntax.

        Below is an overview of how various types of queries can be merged under different boolean containers. Queries
        that do not match any of the patterns are not altered. Note that for optimization of boolean query lists,
        'should' and 'must_not' are equivalent, since ~a & ~b <==> ~(a | b)

        However, before we get around to merging queries, we reorganize the boolean structure so that queries that
        logically are in the same 'should' clause actually are in the same 'should' container. This process is
        conducted in the hoist() method, and rewrites (a | (b | c)) to (a | b | c). Similar optimizations are handled for
        'must' containers, and in a slightly different way, also for some 'must_not' containers in unify()

        The only situation where we can't determine precisely how a query might be merged with others is, unfortunately,
        the common {'match': {field: value}} query under a 'should' container. If there are other match queries on the
        same field, we'd like to concatenate query values, And if there are other match queries with the same value, we
        will want to create a 'multi_match' query. We handle this by prioritizing merging on query value into a
        multi_match, and then merging the multi_match queries on field. This is also why we have the post-processing
        reduce() method to defer rewriting multi_match queries on a single field back to simpler match queries until
        after all other optimizations have been done.

        For example, "any: buzz capture" will, after hoisting, result in a list of 8 match queries in the same
        'should' container, for 2 query terms times the 4 default fields. The first optimization iteration then merges
        {'header'; 'buzz'} and {'header': 'capture'} into {'multi_match': {'fields': ['header'], 'query': 'buzz capture'
        'type': 'cross_fields', 'operator': 'or'}}, and in the second iteration ,the 4 multi_match queries are merged
        into a single multi_match query on all the default fields. Finally, the 'should' container that originally
        held 8 match queries now holds only a single multi_match query, so we can collapse the 'should' container
        to only its single clause.

            QUERY                           QUERY_KEY        SHOULD/MUST_NOT                          MUST
         1  term                            term             (=field) terms                           -
         2  terms                           terms            (=field) terms                           -
         3  wildcard                        wildcard         (=field) regexp                          -
         4  regexp                          regexp           (=field) regexp                          -
         5  match                           match            (=value) multi_match, cross_fields, or   (=field) match_and
         6  match, and                      match_and        (=value) multi_match, most_fields, and   (=field) match_and
         7  match_phrase                    match_phrase     (=value) multi_match, phrase             -
         8  multi_match, cross_fields, and  multi_cross_and  (=value) multi_match, cross_fields, and  (=field) multi_match, cross_fields, and
         9  multi_match, cross_fields, or   multi_cross_or   (=field) multi_match, cross_fields, or  *(=field) multi_match, cross_fields, and
        10  multi_match, most_fields, and   multi_most       (=value) multi_match, most_fields, and   (=field) multi_match, most_fields, and
        11  multi_match, phrase             multi_phrase     (=value) multi_match, phrase             -
        12  range                           range            (=field) range                           (=field) range

        * only when the value has only one word. These are the multi_matches that for instance result directly from a
        query term on the default fields. We have to resort to using str.split() to determine if a query value is
        actually only a single string.
        """
        if type(query_body) is dict:
            # The query is a dict, so it must be a bool query container, a should/must/must_not bool query, or a
            # regular query like term, match, prefix, wildcard, etc.
            minimized = {}
            for key, value in query_body.items():
                if key == 'bool':
                    # Not much to do for the bool container except minimize its contents.
                    value = self.minimize(value, **kwargs)

                    if list(value.keys())[0] in ['should', 'must'] and len(list(value.values())[0]) == 1:
                        # But! If it's a should/must container, and after optimization it holds just a single
                        # should/must clause, collapse the entire thing. By construction, every bool clause contains
                        # just a single subclause, so it's ok to do it this way.
                        minimized = list(value.values())[0][0]
                    else:
                        # Otherwise just move on with the minimized contents
                        minimized['bool'] = value

                elif key in ['should', 'must', 'must_not']:
                    # Optimization of clauses depends on the type of boolean query under which we
                    # are optimizing, so pass that information. For instance, we can group term queries into
                    # a terms query, only when they are combined under a 'should' clause.
                    value = self.minimize(value, boolean_query=key)

                    # some plugins may yield match_all clauses, which can usually be simplified away or make
                    # other clauses irrelevant entirely.
                    if key == 'should':
                        if MATCH_ALL() in value:
                            # a single match_all in a should clause makes any other clause irrelevant
                            minimized[key] = [MATCH_ALL()]
                        else:
                            # negated match_all clauses in a should are irrelevant, but always keep at least one clause
                            minimized[key] = list(filter(lambda x: x != NOT([MATCH_ALL()]), value)) or value[:1]
                    elif key == 'must':
                        if NOT([MATCH_ALL()]) in value:
                            # a single negated match_all in a must clause makes any other clause irrelevant
                            minimized[key] = [NOT([MATCH_ALL()])]
                        else:
                            # match_all clauses in a must are irrelevant, but always keep at least one clause
                            minimized[key] = list(filter(lambda x: x != MATCH_ALL(), value)) or value[:1]
                    elif key == 'must_not':
                        if len(value) == 1 and list(value[0].keys())[0] == 'bool' and list(list(value[0].values())[0].keys())[0] == 'must_not':
                            # undo double negation
                            minimized['must'] = list(list(value[0].values())[0].values())[0]
                        else:
                            # negated match_all clauses in a must_not clause are irrelevant, but keep at least one clause
                            minimized[key] = list(filter(lambda x: x != NOT([MATCH_ALL()]), value)) or value[:1]
                elif key == 'nested':
                    minimized[key] = {
                        'path': value['path'],
                        'query': self.optimize(value['query'])
                    }
                else:
                    # just a normal query, which we can't minimize
                    minimized[key] = value

        elif type(query_body) is list:
            # Not a dict, so must be the clause lists under should/must/must_not queries
            minimized = []

            if 'boolean_query' not in kwargs:
                pass

            # get the boolean query context
            boolean_query = kwargs.pop('boolean_query')

            query_groups = {}
            for query_element in query_body:
                # first, recursively minimize this element
                query_element = self.minimize(query_element, **kwargs)

                # we first group together queries that can be merged together into a shorter query
                #
                # sub_query_key is the thing that needs to match when grouping queries together.
                # sub_query_value is the thing that will be merged together in the resulting query.
                query_key, query_value = list(query_element.items())[0]
                if boolean_query in ['should', 'must_not']:
                    if query_key == 'terms' and type(list(query_value.values())[0]) != dict:  # exclude term lookup queries, they don't compress
                        # 2 terms: { field: value } -> terms: { field: value+ }
                        sub_query_key, sub_query_value = list(query_value.items())[0]
                    elif query_key in ['term', 'wildcard']:
                        # 1 term: { field: value } -> terms: { field: value+ }
                        # 3 wildcard: { field: value } -> regexp: { field: value+ }
                        sub_query_key, sub_query_value = list(query_value.items())[0]
                    elif query_key == 'regexp':
                        # 4 regexp: { field: value } -> regexp: {field: value+}
                        sub_query_key, sub_query_value = list(query_value.keys())[0], list(query_value.values())[0].split('|')
                    elif query_key == 'match':
                        if type(list(query_value.values())[0]) is dict:
                            # 5 match: { field: { query, and }} -> multi: { field+: { query, most }}
                            query_key = 'match_and'
                            sub_query_value, sub_query_key = list(query_value.keys())[0], list(query_value.values())[0]['query']
                        else:
                            # 6 match: { field: query } -> multi: { field+: { query, cross }}
                            sub_query_value, sub_query_key = list(query_value.items())[0]
                    elif query_key == 'match_phrase':
                        # 7 phrase { field: query } -> multi: { field+, phrase }
                        sub_query_value, sub_query_key = list(query_value.items())[0]
                    elif query_key == 'multi_match':
                        if query_value['type'] == 'cross_fields':
                            if query_value.get('operator') == 'and':
                                # 8 multi: { field: { query, cross, and }} -> multi: { field: { query+, cross, and }}
                                query_key = 'multi_cross_and'
                                sub_query_value, sub_query_key = query_value['fields'], query_value['query']
                            elif query_value.get('operator') == 'or':
                                # 9 multi: { field: { query, cross }} -> multi: { field: { query+, cross }}
                                query_key = 'multi_cross_or'
                                sub_query_key, sub_query_value = tuple(query_value['fields']), query_value['query'].split()
                        elif query_value['type'] == 'most_fields':
                            # 10 multi: { field: { query, most }} -> multi: { field+: { query, most }}
                            query_key = 'multi_most'
                            sub_query_value, sub_query_key = query_value['fields'], query_value['query']
                        elif query_value['type'] == 'phrase':
                            # 11 multi: { field: { query, phrase }} -> multi: { field+: { query, phrase }}
                            query_key = 'multi_phrase'
                            sub_query_value , sub_query_key = query_value['fields'], query_value['query']
                        else:
                            minimized.append(query_element)
                            continue
                    elif query_key == 'range':
                        sub_query_key, sub_query_value = list(query_value.items())[0]
                    else:
                        minimized.append(query_element)
                        continue

                elif boolean_query == 'must':
                    if query_key == 'match':
                        if type(list(query_value.values())[0]) is not dict:
                            # 5 match: { field, query } -> match: { field: { query+, and }}
                            sub_query_key, sub_query_value = list(query_value.items())[0]
                        else:
                            # 6 match: { field: { query, and }} -> match: { field: { query+, and }}
                            query_key = 'match_and'
                            sub_query_key, sub_query_value = list(query_value.keys())[0], list(query_value.values())[0]['query'].split()
                    elif query_key == 'multi_match' and query_value['type'] == 'cross_fields' and query_value.get('operator') == 'and':
                        # 8 multi: { field, query, and } -> multi: { field, query+, and }
                        query_key = 'multi_cross_and'
                        sub_query_key, sub_query_value = tuple(query_value['fields']), query_value['query'].split()
                    elif query_key == 'multi_match' and query_value['type'] == 'cross_fields' and query_value.get('operator') == 'or' and len(query_value['query'].split()) == 1:
                        # 9 multi: { field, query* } -> multi: { field, query+ and}  * iff query contains only one word
                        query_key = 'multi_cross_or'
                        sub_query_key, sub_query_value = tuple(query_value['fields']), query_value['query']
                    elif query_key == 'multi_match' and query_value['type'] == 'most_fields':
                        # 10 multi: { field, most } -> multi: { field: { query+ most }}
                        query_key = 'multi_most'
                        sub_query_key, sub_query_value = tuple(query_value['fields']), query_value['query'].split()
                    elif query_key == 'range':
                        sub_query_key, sub_query_value = list(query_value.items())[0]
                    else:
                        minimized.append(query_element)
                        continue

                # store the current query with others of its kind
                key = (query_key, sub_query_key)
                if key not in query_groups:
                    query_groups[key] = []
                query_groups[key].append(sub_query_value)

            # Start merging queries together. If we do merge something together, it is possible that we then can do
            # more optimizing. For example, "any: buzzcapture amsterdam" can be merged into a single 'or' multi_match
            # query on the default fields, but not in a single step.
            minimize = False
            for (query_key, sub_query_key), sub_query_values in query_groups.items():
                if boolean_query in ['should', 'must_not']:
                    if query_key == 'term':
                        # 1 term: { field: value } -> terms: { field: value+ }
                        sub_query_values = sorted(set(sub_query_values))
                        if len(sub_query_values) == 1:
                            minimized.append(TERM(sub_query_key, sub_query_values[0]))
                        else:
                            minimized.append(TERMS(sub_query_key, sub_query_values))
                            minimize = True
                    elif query_key == 'terms':
                        # 2 terms: { field: value } -> terms: { field: value+ }
                        if len(sub_query_values) > 1:
                            minimize = True
                        minimized.append(TERMS(sub_query_key, sorted(set(sum(sub_query_values, [])))))
                    elif query_key == 'wildcard':
                        # 3 wildcard: { field: value } -> regexp: { field: value+ }
                        if len(sub_query_values) == 1:
                            minimized.append(WILDCARD(sub_query_key, sub_query_values[0]))
                        else:
                            sub_query_values = sorted(set(sub_query_values), key=lambda x: x.replace('*', ''))
                            minimized.append(REGEXP(sub_query_key, '|'.join(list(map(lambda x: x.replace('*', '.*'), sub_query_values)))))
                            minimize = True
                    elif query_key == 'regexp':
                        # 4 regexp: { field: value } -> regexp: {field: value+}
                        if len(sub_query_values) > 1:
                            minimize = True
                        sub_query_values = sorted(set(sum(sub_query_values, [])), key=lambda x: x.replace('.*', ''))
                        minimized.append(REGEXP(sub_query_key, '|'.join(sub_query_values)))
                    elif query_key == 'match_and':
                        # 5 match: { field: { query, and }} -> multi: { field+: { query, most }}
                        if len(sub_query_values) == 1:
                            minimized.append(MATCH_AND(sub_query_values[0], sub_query_key))
                        else:
                            sub_query_values = sorted(set(sub_query_values))
                            minimized.append(MULTI_MATCH(sub_query_values, sub_query_key, 'most_fields', 'and'))
                            minimize = True
                    elif query_key == 'match':
                        # 6 match: { field: query } -> multi: { field+: { query, cross }}
                        minimized.append(MULTI_MATCH(sorted(set(sub_query_values)), sub_query_key, 'cross_fields', 'or'))
                        minimize = True
                    elif query_key == 'match_phrase':
                        # 7 phrase { field: query } -> multi: { field+, phrase }
                        if len(sub_query_values) == 1:
                            minimized.append(MATCH_PHRASE(sub_query_values[0], sub_query_key))
                        else:
                            minimized.append(MULTI_MATCH(sorted(set(sub_query_values)), sub_query_key, 'phrase'))
                            minimize = True
                    elif query_key == 'multi_cross_and':
                        # 8 multi: { field: { query, cross, and }} -> multi: { field: { query+, cross, and }}
                        if len(sub_query_values) > 1:
                            minimize = True
                        sub_query_values = sorted(set(sum(sub_query_values, [])))
                        minimized.append(MULTI_MATCH(sub_query_values, sub_query_key, 'cross_fields', 'and'))
                    elif query_key == 'multi_cross_or':
                        # 9 multi: { field: { query, cross }} -> multi: { field: { query+, cross }}
                        if len(sub_query_values) > 1:
                            minimize = True
                        sub_query_values = sorted(set(sum(sub_query_values, [])))
                        minimized.append(MULTI_MATCH(list(sub_query_key), ' '.join(sub_query_values), 'cross_fields', 'or'))
                    elif query_key == 'multi_most':
                        # 10 multi: { field: { query, most }} -> multi: { field+: { query, most }}
                        if len(sub_query_values) > 1:
                            minimize = True
                        sub_query_values = sorted(set(sum(sub_query_values, [])))
                        minimized.append(MULTI_MATCH(sub_query_values, sub_query_key, 'most_fields', 'and'))
                    elif query_key == 'multi_phrase':
                        # 11 multi: { field: { query, phrase }} -> multi: { field+: { query, phrase }}
                        if len(sub_query_values) == 1:
                            minimized.append(MULTI_MATCH(sub_query_values[0], sub_query_key, 'phrase'))
                        else:
                            sub_query_values = sorted(set(sum(sub_query_values, [])))
                            minimized.append(MULTI_MATCH(sub_query_values, sub_query_key, 'phrase'))
                            minimize = True
                    elif query_key == 'range':
                        if len(sub_query_values) == 1:
                            minimized.append(RANGE(sub_query_key, **sub_query_values[0]))
                        else:
                            # merge ranges
                            ranges = self.merge_ranges(sub_query_values, False)
                            if not ranges[0]:
                                minimized.append(MATCH_ALL())
                            else:
                                minimized.append(SHOULD(RANGE(sub_query_key, **range) for range in ranges))

                elif boolean_query == 'must':
                    if query_key == 'match':
                        # 5 match: { field, query } -> match: { field: { query+, and }}
                        if len(sub_query_values) == 1:
                            minimized.append(MATCH(sub_query_key, sub_query_values[0]))
                        else:
                            minimized.append(MATCH_AND(sub_query_key, ' '.join(sorted(set(sub_query_values)))))
                            minimize = True
                    elif query_key == 'match_and':
                        # 6 match: { field: { query, and }} -> match: { field: { query+, and }}
                        if len(sub_query_values) == 1:
                            minimized.append(MATCH_AND(sub_query_key, ' '.join(sub_query_values[0])))
                        else:
                            sub_query_values = sorted(set(sum(sub_query_values, [])))
                            minimized.append(MATCH_AND(sub_query_key, ' '.join(sub_query_values)))
                            minimize = True
                    elif query_key == 'multi_cross_and':
                        # 8 multi: { field, query, and } -> multi: { field, query+, and }
                        if len(sub_query_values) > 1:
                            minimize = True
                        sub_query_values = sorted(set(sum(sub_query_values, [])))
                        minimized.append(MULTI_MATCH(list(sub_query_key), ' '.join(sub_query_values), 'cross_fields', 'and'))
                    elif query_key == 'multi_cross_or':
                        # 9 multi: { field, query* } -> multi: { field, query+ and}  * iff query contains only one word
                        if len(sub_query_values) == 1:
                            minimized.append(MULTI_MATCH(list(sub_query_key), sub_query_values[0], 'cross_fields', 'or'))
                        else:
                            sub_query_values = sorted(set(sub_query_values))
                            minimized.append(MULTI_MATCH(list(sub_query_key), ' '.join(sub_query_values), 'cross_fields', 'and'))
                            minimize = True
                    elif query_key == 'multi_most':
                        # 10 multi: { field, most } -> multi: { field: { query+ most }}
                        if len(sub_query_values) > 1:
                            minimize = True
                        sub_query_values = sorted(set(sum(sub_query_values, [])))
                        minimized.append(MULTI_MATCH(list(sub_query_key), ' '.join(sub_query_values), 'most_fields', 'and'))
                    elif query_key == 'range':
                        if len(sub_query_values) == 1:
                            minimized.append(RANGE(sub_query_key, **sub_query_values[0]))
                        else:
                            # merge ranges
                            ranges = self.merge_ranges(sub_query_values, True)

                            if not ranges[0]:
                                minimized.append(NOT([MATCH_ALL()]))
                            else:
                                minimized.append(MUST(RANGE(sub_query_key, **range) for range in ranges))

            # iteratively minimize if anything changed
            if minimize:
                minimized = self.minimize(minimized, boolean_query=boolean_query)

        return minimized

    def reduce(self, query_body):
        """
        Reduce some patterns to simpler queries.
        """
        if type(query_body) is dict:
            for key, value in query_body.items():
                if key in ['bool', 'should', 'must', 'must_not']:
                    return {key: self.reduce(value)}
                elif key == 'multi_match':
                    # multi_match queries on a single field, are reduced to match queries
                    if len(value['fields']) == 1:
                        field = value['fields'][0]
                        query = value['query']

                        if value.get('operator') == 'and':
                            return MATCH_AND(field, query)
                        else:
                            return MATCH(field, query)
                elif key == 'terms':
                    # terms queries on a single value are reduced to term queries
                    field, values = list(value.items())[0]
                    if len(values) == 1:
                        return TERM(field, values[0])

            return query_body
        else:
            return list(map(self.reduce, query_body))

    @property
    def parser(self):
        if self._parser:
            return self._parser

        parentheses = pp.Forward()

        word = pp.NotAny(pp.Keyword('or') | pp.Keyword('not')) + pp.Regex('\w+') + ~pp.FollowedBy(pp.Literal(':'))
        word.setParseAction(Word)

        quoted = pp.QuotedString('"')
        quoted.setParseAction(Quoted)

        def commaSeparated(theClass):
            return theClass + pp.ZeroOrMore(pp.Literal(',').suppress() + theClass)

        words = commaSeparated(word)
        ids = commaSeparated(pp.Regex('[\w_]+'))

        # all = pp.Literal('all:').suppress() + pp.OneOrMore(word | quoted)
        # all.setParseAction(All)
        #
        # any = pp.Literal('any:').suppress() + pp.OneOrMore(word | quoted)
        # any.setParseAction(Any)

        # conversations = pp.Literal('conversations:').suppress() + ids
        # conversations.setParseAction(Conversations)

        # topics = pp.Literal('topics:').suppress() + words
        # topics.setParseAction(Topics)

        strings = commaSeparated(word | quoted)
        strings.setParseAction(Strings)

        identifier = pp.Literal('id:').suppress() + ids
        identifier.setParseAction(Identifier)

        authors = pp.Literal('author:').suppress() + words
        authors.setParseAction(Authors)

        exact = pp.Literal('exact:').suppress() + quoted
        exact.setParseAction(Exact)

        related = pp.Regex('(quote|reply|share|related)') + pp.Literal(':').suppress() + parentheses
        related.setParseAction(Related)

        relation = pp.Literal('relation:').suppress() + word
        relation.setParseAction(Relation)

        has = pp.Literal('has:').suppress() + words
        has.setParseAction(Has)

        lists = pp.Literal('list:').suppress() + strings
        lists.setParseAction(Lists)

        listmention = pp.Literal('listmention:').suppress() + strings
        listmention.setParseAction(ListMention)

        language = pp.Literal('lang:').suppress() + words
        language.setParseAction(Language)

        mention = pp.Literal('mention:').suppress() + words
        mention.setParseAction(Mentions)

        fraction = pp.Literal('fraction:').suppress() + pp.Regex('0\.\d+')
        fraction.setParseAction(Fraction)

        # sources = pp.Literal('sources:').suppress() + pp.OneOrMore(pp.Regex('[\w\.]+') + ~pp.FollowedBy(pp.Literal(':')))
        # sources.setParseAction(Sources)

        # source_types = pp.Literal('sourcetypes:').suppress() + pp.OneOrMore(word)
        # source_types.setParseAction(SourceTypes)

        # time_shift = pp.Literal('time_shift:').suppress() + pp.Regex('\d+') + pp.Regex('days|weeks|months|years')
        # time_shift.setParseAction(TimeShift)

        term = strings | identifier | authors | related | relation | has | lists | language | mention | parentheses | fraction | listmention | exact

        negated_term = pp.Literal('not').suppress() + term
        negated_term.setParseAction(Negated)

        terms = pp.OneOrMore(term | negated_term)
        terms.setParseAction(Terms)

        boolean = pp.Forward()
        boolean << terms + pp.ZeroOrMore(pp.Literal('or') + terms)
        boolean.setParseAction(Boolean)

        parentheses << pp.Literal('(').suppress() + boolean + pp.Literal(')').suppress()
        parentheses.setParseAction(Parentheses)

        self._parser = boolean + pp.StringEnd().suppress()

        return self._parser

    def parse(self, query):
        lock.acquire()
        try:
            QueryParser.CONTEXT = {}

            if query:
                ast = self.parser.parseString(query)
                # print('ast')
                # print(ast)

                parsed = ast[0].query(**self._query_kwargs)
            else:
                parsed = MATCH_ALL()
        except:
            print('Error parsing query: {}'.format(query))

            raise ParseException(query)

        finally:
            lock.release()

        # print('query:', query)
        # print('parsed:', parsed)
        # print('context:', QueryParser.CONTEXT)

        return parsed, QueryParser.CONTEXT


class Word:
    def __init__(self, string, location, tokens):
        self.tokens = tokens

    def query(self, **kwargs):
        field = kwargs.get('field_prefix', '') + 'text.word'

        return MATCH(field, self.tokens[0])


class Quoted:
    def __init__(self, string, location, tokens):
        self.tokens = tokens

    def query(self, **kwargs):
        field = kwargs.get('field_prefix', '') + 'text.word'

        return MATCH_PHRASE(field, self.tokens[0])


class Exact:
    def __init__(self, string, location, tokens):
        self.tokens = tokens

    def query(self, **kwargs):
        field = kwargs.get('field_prefix', '') + 'text.character'

        return MATCH_PHRASE(field, self.tokens[0].tokens[0])


class Negated:
    def __init__(self, string, location, tokens):
        # print('Negated init')
        # print(string, location, tokens)

        self.tokens = tokens

    def query(self, **kwargs):
        # print('Negated query')

        return NOT(token.query(**kwargs) for token in self.tokens)


class Strings:
    def __init__(self, string, location, tokens):
        # print('Terms init')
        # print(string, location, tokens)

        self.tokens = tokens

    def query(self, **kwargs):
        # print('Terms query')

        return SHOULD(token.query(**kwargs) for token in self.tokens)

class Terms:
    def __init__(self, string, location, tokens):
        # print('Terms init')
        # print(string, location, tokens)

        self.tokens = tokens

    def query(self, **kwargs):
        # print('Terms query')

        extra_terms = kwargs.pop('extra_terms') if 'extra_terms' in kwargs else None

        queries = [token.query(**kwargs) for token in self.tokens]

        if extra_terms:
            queries.extend(extra_terms)

        return MUST(queries)


# class All:
#     def __init__(self, string, location, tokens):
#         # print('All init')
#         # print(string, location, tokens)
#
#         self.tokens = tokens
#
#     def query(self, **kwargs):
#         # print('All query')
#
#         return MUST(token.query(**kwargs) for token in self.tokens)
#
#
# class Any:
#     def __init__(self, string, location, tokens):
#         # print('All init')
#         # print(string, location, tokens)
#
#         self.tokens = tokens
#
#     def query(self, **kwargs):
#         # print('All query')
#
#         return SHOULD(token.query(**kwargs) for token in self.tokens)
#
#
# class Conversations:
#     def __init__(self, string, location, tokens):
#         self.tokens = tokens
#
#     def query(self, **kwargs):
#         return TERMS('conversation_id', list(self.tokens))
#
#
class Relation:
    def __init__(self, string, location, tokens):
        self.tokens = tokens

    def query(self, **kwargs):
        field = kwargs.get('field_prefix', '') + 'relation'

        return TERM(field, self.tokens[0].tokens[0])


# class Topics:
#     def __init__(self, string, location, tokens):
#         self.tokens = tokens
#
#     def query(self, **kwargs):
#         return SHOULD([{"__TOPIC__": topic.tokens[0]} for topic in self.tokens])


class Identifier:
    def __init__(self, string, location, tokens):
        self.tokens = tokens

    def query(self, **kwargs):
        field = kwargs.get('field_prefix', '') + 'id'

        return TERMS(field, list(self.tokens))

#
# class Sources:
#     def __init__(self, string, location, tokens):
#         self.tokens = tokens
#
#     def query(self, **kwargs):
#         return SHOULD([TERM('source', source) for source in self.tokens])
#
#
# class SourceTypes:
#     def __init__(self, string, location, tokens):
#         self.tokens = tokens
#
#     def query(self, **kwargs):
#         return SHOULD([TERM('source_type', source_type) for source_type in self.tokens])
#

class Authors:
    def __init__(self, string, location, tokens):
        self.tokens = tokens

    def query(self, **kwargs):
        field = kwargs.get('field_prefix', '') + 'author'

        return SHOULD([TERM(field, author.tokens[0]) for author in self.tokens])


class Mentions:
    def __init__(self, string, location, tokens):
        self.tokens = tokens

    def query(self, **kwargs):
        field = kwargs.get('field_prefix', '') + 'mentions'

        return SHOULD([TERM(field, mention.tokens[0]) for mention in self.tokens])


class Language:
    def __init__(self, string, location, tokens):
        self.tokens = tokens

    def query(self, **kwargs):
        field = kwargs.get('field_prefix', '') + 'language'

        return SHOULD([TERM(field, language.tokens[0]) for language in self.tokens])


class Fraction:
    def __init__(self, string, location, tokens):
        self.tokens = tokens

    def query(self, **kwargs):
        field = kwargs.get('field_prefix', '') + 'random'

        value = float(self.tokens[0])

        return RANGE(field, lt=value)


class Related:
    def __init__(self, string, location, tokens):
        self.tokens = tokens

    def query(self, **kwargs):
        relation = self.tokens[0]

        if relation != 'related':
            kwargs['extra_terms'] = [TERM('related.relation', self.tokens[0])]

        return NESTED('related', self.tokens[1].query(field_prefix='related.', **kwargs))

class Parentheses:
    def __init__(self, string, location, tokens):
        self.tokens = tokens

    def query(self, **kwargs):
        return self.tokens[0].query(**kwargs)


class ListMention:
    def __init__(self, string, location, tokens):
        self.tokens = tokens

    def query(self, **kwargs):
        if kwargs.get('execution_hint') == 'expand_index_terms_query':
            list_names = [tag.tokens[0] for tag in self.tokens]

            print('list_names', list_names)

            return {"__LISTS__": list_names}
        else:
            queries = []
            for tag in self.tokens:
                field = kwargs.get('field_prefix', '') + 'mentions'

                path = 'author'

                list_id = '{}_{}'.format(tag.tokens[0].tokens[0], kwargs['account_id'])

                queries.append(TERMS_LOOKUP(list_id, field, path))

            return SHOULD(queries)

class Lists:
    def __init__(self, string, location, tokens):
        self.tokens = tokens

    def query(self, **kwargs):
        if kwargs.get('execution_hint') == 'expand_index_terms_query':
            list_names = [tag.tokens[0] for tag in self.tokens]

            print('list_names', list_names)

            return {"__LISTS__": list_names}
        else:
            queries = []
            for tag in self.tokens:
                field = kwargs.get('field_prefix', '') + 'author'

                path = 'author'

                list_id = '{}_{}'.format(tag.tokens[0].tokens[0], kwargs['account_id'])

                queries.append(TERMS_LOOKUP(list_id, field, path))

            return SHOULD(queries)

class Has:
    def __init__(self, string, location, tokens):
        self.tokens = tokens

    def query(self, **kwargs):
        field_map = {
            'text': 'text.word'
        }

        queries = []
        for token in self.tokens:
            field = token.tokens[0]
            if field in ['images', 'videos', 'related']:
                queries.append(EXISTS(field, True, True))
            else:
                field = kwargs.get('field_prefix', '') + field_map.get(field, field)

                queries.append(EXISTS(field, True, False))

        return SHOULD(queries)
#
# class TimeShift:
#     def __init__(self, string, location, tokens):
#         self.tokens = tokens
#
#     def query(self, **kwargs):
#         QueryParser.CONTEXT['time_shift'] = {'count': self.tokens[0], 'unit': self.tokens[1]}
#
#         return MATCH_ALL()
#

class Boolean:
    def __init__(self, string, location, tokens):
        self.tokens = tokens

    def query(self, **kwargs):
        if len(self.tokens) == 1:
            return self.tokens[0].query(**kwargs)

        else:
            if self.tokens[1] == 'or':
                return SHOULD([self.tokens[0].query(**kwargs), self.tokens[2].query(**kwargs)])
            elif self.tokens[1] == 'and':
                return MUST([self.tokens[0].query(**kwargs), self.tokens[2].query(**kwargs)])

def MATCH(field, value):
    return {'match': {field: value}}

def SHOULD(queries):
    return {'bool': {'should': list(queries)}}

def NOT(queries):
    return {'bool': {'must_not': list(queries)}}

def MUST(queries):
    return {'bool': {'must': list(queries)}}

def TERM(field, value):
    return {'term': {field: value}}

def TERMS(field, values):
    return {'terms': {field: values}}

def MATCH_PHRASE(field, value):
    return {'match_phrase': {field: value}}

def MATCH_AND(field, value):
    return {'match': {field: {'query': value, 'operator': 'and'}}}

def MULTI_MATCH(fields, query, type=None, operator=None):
    multi_match = {
        'multi_match': {
            'fields': fields,
            'query': query,
            'use_dis_max': False,
        }
    }

    if type is not None:
        multi_match['multi_match']['type'] = type

    if operator is not None:
        multi_match['multi_match']['operator'] = operator

    return multi_match

def MATCH_ALL():
    return {'match_all': {}}

def PREFIX(field, value):
    return {'prefix': {field: value}}

def WILDCARD(field, value):
    return {'wildcard': {field: value}}

def REGEXP(field, value):
    return {'regexp': {field: value}}

def RANGE(field, gte=None, gt=None, lt=None, lte=None):
    range_dict = {'range': {field: {}}}

    if gte is not None:
        range_dict['range'][field]['gte'] = gte
    if gt is not None:
        range_dict['range'][field]['gt'] = gt
    if lt is not None:
        range_dict['range'][field]['lt'] = lt
    if lte is not None:
        range_dict['range'][field]['lte'] = lte

    return range_dict

def EXISTS(field, exists=True, nested=False, nestedQuery=None):
    if nested:
        query = {'nested': {'path': field, 'query': nestedQuery or MATCH_ALL()}}
        if not exists:
            query = NOT([query])

        return query
    else:
        return {'exists' if exists else 'missing': {'field': field}}

def NESTED(path, query):
    return {'nested': {'path': path, 'query': query}}

def IDS(ids):
    return {'ids': { 'values': ids}}

def TERMS_LOOKUP(tag, field, path):
    return {'terms': { field: { 'index': 'lists', 'type': 'list', 'id': tag, 'path': path}}}


class ParseException(Exception):
    def __init__(self, query):
        self.query = query


import sys
def printit(element, indent=0):
    if type(element) is dict:
        sys.stdout.write(' ' * indent + '{\n')
        indent += 2
        for key, value in element.items():
            sys.stdout.write(' ' * indent + '"{}":\n'.format(key))
            printit(value, indent+2)
        indent -= 2
        sys.stdout.write(' ' * indent + '}\n')
    elif type(element) is list:
        sys.stdout.write(' ' * indent + '[\n')
        indent += 2
        for value in element:
            printit(value, indent+2)
            sys.stdout.write(' ' * indent + ',\n')
        indent -= 2
        sys.stdout.write(' ' * indent + ']\n')

    else:
        sys.stdout.write(' ' * indent + '"' + str(element) + '"' + '\n')


# # query = 'list: pvv'
# kwargs = {'account_id': 2}
# parser = QueryParser(**kwargs)
# # printit(parser.parse(query)[0])
# printit(parser.optimize(MUST([{'range': {'a': { 'lt': 2}}}, {'range': { 'a': { 'gte': 0}}}])))
