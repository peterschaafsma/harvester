from api import TwitterApi, TWITTER_ES, RSS_ES
from datetime import timedelta
from django.db import connection
from django.db.models import Q
from feeds.models import Feed, TwitterCredentials, TwitterRateLimit
from multiprocessing import Pool
from utils import dt
from utils.event import event
from utils.log import print

import feedparser
import json
import random
import sys
import time
import traceback


class FeedBase:
    def __init__(self, feed):
        self.feed = feed

    def ingest(self, entries):
        print('Ingesting {} entries for {}'.format(len(entries), self.feed))

        exception = None
        for _ in range(3):
            try:
                self.es_client.ingest(entries)

                break
            except Exception as e:
                exception = e

                print('Failed to ingest entries for {} due to {}'.format(self.feed, exception))

        else:
            print('Reraising exception')

            raise exception

    def harvest(self, **kwargs):
        connection.close()

        last_id = None
        new_count = None
        start_time = time.time()
        start_time_utc = dt.iso8601(dt.utc_now())
        try:
            self.feed = Feed.objects.get(pk=self.feed.id)

            print('Start harvest of {}'.format(self.feed))
            event('harvest.start'.format(self.feed.type), 1, None, feed_type=self.feed.type, feed_id=self.feed.id)

            print('Locking feed {}'.format(self.feed))

            self.feed.lock()

            print('Harvesting feed {}'.format(self.feed))

            summary = self._collect_and_ingest_entries(**kwargs)
            if summary['first']:
                last_id = summary['first']['id']

            new_count = summary['count']

        except Exception as exception:
            print('Failed to harvest feed {}'.format(self.feed))

            exc_type, exc_value, exc_traceback = sys.exc_info()
            traceback.print_exception(exc_type, exc_value, exc_traceback, file=sys.stdout)

        finally:
            current_time = time.time()

            print('Updating feed {}: {}, {}'.format(self.feed, start_time_utc, new_count, last_id))
            self._update_feed(start_time, new_count, last_id)

            print('Unlocking feed {}'.format(self.feed))
            self.feed.unlock()

            event('harvest.stop', 1, None, feed_type=self.feed.type, feed_id=self.feed.id)
            event('harvest.time', current_time - start_time, None, feed_type=self.feed.type, feed_id=self.feed.id)

            if new_count is not None:
                event('harvest.count', new_count, None, feed_type=self.feed.type, feed_id=self.feed.id)

            print('Events sent')

    def _update_feed(self, start_time, count, last_id):
        if count is not None:
            self.feed.harvest_count += 1
            self.feed.harvest_next = self._get_harvest_next(start_time, count)
            self.feed.harvest_last = dt.utc_now()
            if self.feed.harvest_first is None:
                self.feed.harvest_first = self.feed.harvest_last
            self.feed.entry_last_count = count
            self.feed.entry_total_count += count

            if last_id is not None:
                self.feed.entry_last_id = str(last_id)

class TwitterFeed(FeedBase):
    def __init__(self, feed):
        super(TwitterFeed, self).__init__(feed)

        self.es_client = TWITTER_ES

    def _collect_entries_page(self, endpoint, api_kwargs, token_type=None):
        print('Waiting for {} credentials for {}'.format(endpoint, self.feed))

        credentials = TwitterCredentials.wait_for_credentials(endpoint, token_type)
        print('Got credentials {} for {}'.format(credentials, self.feed))

        try:
            result = TwitterApi(credentials).get(endpoint, **api_kwargs)

        finally:
            TwitterCredentials.return_credentials(credentials, endpoint, result.response)

        if not result.data:
            return [], api_kwargs

        if 'errors' in result.data:
            print('Errors in fetch for {}: {}'.format(self.feed, result.data['errors']))

            if result.data['errors'][0]['code'] == 136:
                print('Retrying {} with app token: '.format(self.feed))

                return self._collect_entries_page(endpoint, api_kwargs, token_type='app')

            elif result.data['errors'][0]['code'] == 89:
                print('Invalid token, disabling credentials: {}:{}'.format(credentials.token, credentials.secret))

                TwitterCredentials.invalidate_credentials(credentials)

            return [], api_kwargs

        print('Got {} entries for {}'.format(len(result.data), self.feed))

        if type(result.data) is dict and result.data.get('error') == 'Not authorized.':
            print('Protected account {}'.format(self.feed))

            return [], api_kwargs

        try:
            api_kwargs['max_id'] = result.data[-1]['id'] - 1
        except:
            print(self.feed)
            print(result.response, result.data)
            raise

        entries = list(result.data)

        event('credentials.yield', value=len(entries), credentials=str(credentials), endpoint=endpoint)

        return entries, api_kwargs

    def _collect_and_ingest_entries(self, **kwargs):
        endpoint = self._get_api_endpoint()
        api_kwargs = self._get_api_kwargs()

        entries = []
        summary = {
            'first': None,
            'last': None,
            'count': 0,
            'new_count': 0,
        }

        done = False
        while not done:
            new_entries, api_kwargs = self._collect_entries_page(endpoint, api_kwargs)

            if new_entries:
                if summary['first'] is None:
                    summary['first'] = {'id': new_entries[0]['id'], 'created_at': new_entries[0]['created_at']}
                summary['last'] = {'id': new_entries[-1]['id'], 'created_at': new_entries[-1]['created_at']}
                summary['count'] += len(new_entries)
                summary['new_count'] += len(new_entries)

                entries.extend(new_entries)
                # if the last entry is below the last ID of the previous harvest, we're done
                if self.feed.entry_last_id and new_entries[-1]['id'] <= int(self.feed.entry_last_id):
                    for index in range(len(new_entries)):
                        if new_entries[index]['id'] <= int(self.feed.entry_last_id):
                            summary['new_count'] -= len(new_entries) - index

                            break

                    if not kwargs.get('full'):
                        done = True

                if kwargs['one_page']:
                    done = True
            else:
                done = True

            print(json.dumps(summary))

            if (len(entries) > 9900) or (entries and done):
                for entry in entries:
                    entry['source_feed'] = self.feed.id

                self.ingest(entries)

                entries = []

        return summary

class TwitterAccountFeed(TwitterFeed):
    def _get_api_kwargs(self):
        kwargs = {
            'screen_name': self.feed.parameters['screen_name'],
            'include_rts': True,
            'exclude_replies': False,
            'count': 200,
            'tweet_mode': 'extended',
            'include_entities': True
        }

        return kwargs

    def _get_api_endpoint(self):
        return 'statuses/user_timeline.json'

    def _get_harvest_next(self, start_time, count):
        if self.feed.refresh:
            seconds = random.uniform(int(0.5 * self.feed.refresh), int(1.5 * self.feed.refresh))
        else:
            seconds = random.uniform(900, 2700)

        print("refresh {} in {} seconds".format(self.feed, seconds))

        return dt.utc_now() + timedelta(seconds=seconds)

class TwitterFireHoseFeed(TwitterFeed):
    def _get_api_kwargs(self):
        query = 'to:{screen_name} OR @{screen_name}'.format(screen_name=self.feed.parameters['screen_name'])

        kwargs = {
            'q': query,
            'result_type': 'recent',
            'count': 100,
            'tweet_mode': 'extended',
            'include_entities': True,
        }

        return kwargs

    def _get_api_endpoint(self):
        return 'search/tweets.json'

    def _get_harvest_next(self, start_time, count):
        if self.feed.harvest_last:
            total_seconds = start_time - self.feed.harvest_last.timestamp()
        else:
            total_seconds = 7 * 24 * 3600

        if count:
            seconds_for_100 = 100 * total_seconds / count
        else:
            seconds_for_100 = 86400

        if (seconds_for_100 > 6 * 3600):
            seconds_for_100 = random.uniform(5 * 3600, 7 * 3600)

        print('Next harvest for feed {} with {} new entries since {} is in about {} seconds'.format(self.feed, count, self.feed.harvest_last, int(seconds_for_100)))

        return dt.utc_now() + timedelta(seconds = seconds_for_100)

class TwitterUpdateFeed(TwitterFeed):
    def _collect_and_ingest_entries(self, **kwargs):
        time_unit = self.feed.parameters['time_unit']
        count = self.feed.parameters['count']
        last_id = self.feed.entry_last_id or 1

        es_time_frame = '{}{}'.format(count, time_unit[0])

        # update everything since feed.feed_entry_last_id that hasa created_at time more than time_fame ago
        # find all ids matching the description above
        body = {
            'query': {
                'bool': {
                    'must': [
                        {
                            'range': {
                                 'created_at': {
                                     'lte': 'now-{}'.format(es_time_frame)
                                 }
                             },
                        },
                        {
                            'range': {
                                'id': {
                                    'gt': last_id
                                }
                            }
                        }
                    ],
                }
            },
            'sort': {
                'id': 'asc'
            },
            '_source': False
        }

        # batchify
        entries = []
        missing_ids = []

        start_time = dt.utc_now()
        for batch in TWITTER_ES._scan_batch(body=body, batch_size=100):
            id_list = [int(item['_id']) for item in batch]

            new_entries = self._collect_entries_page(id_list)

            entries.extend(new_entries)

            try:
                missing_ids.extend(list(set(id_list) - set(item['id'] for item in new_entries)))
            except:
                print("new_entries", new_entries)

                raise

            if kwargs['one_page']:
                break

            if (dt.utc_now() - start_time).total_seconds() > 30:
                print("We ran for 30 seconds, stopping")
                break

            if not batch:
                print("Empty batch, stopping")
                break


        # print('Retrying {} IDs with app token'.format(len(missing_ids)))
        # for batch in batchify(missing_ids, batch_size=100):
        #     new_entries = self._collect_entries_page(batch, token_type='app')
        #
        #     entries.extend(new_entries)

        entries.sort(key=lambda x: int(x['id']), reverse=True)

        self.ingest(entries)

        if entries:
            summary = {
                'first':{'id': entries[0]['id'], 'created_at': entries[0]['created_at']},
                'last': {'id': entries[-1]['id'], 'created_at': entries[-1]['created_at']},
                'count': len(entries),
                'new_count': 0,
            }

            print(json.dumps(summary))
        else:
            summary = {
                'first': None,
                'last': None,
                'count': 0,
                'new_count': 0,
            }

        return summary

    def _collect_entries_page(self, id_list, token_type=None):
        endpoint = self._get_api_endpoint()

        print('Waiting for {} credentials for {}'.format(endpoint, self.feed))

        credentials = None
        result = None
        try:
            credentials = TwitterCredentials.wait_for_credentials(endpoint, token_type)

            print('Got credentials {} for {}'.format(credentials, self.feed))

            # todo: handle updating posts from authors who blocked the token's owner (check twitter api, it's a parameter)
            api_kwargs = {
                'id': ','.join(map(str, id_list)),
                'tweet_mode': 'extended',
            }

            result = TwitterApi(credentials).get(endpoint, **api_kwargs)

        finally:
            if credentials and result:
                TwitterCredentials.return_credentials(credentials, endpoint, result.response)

        print('Got {} entries for {}'.format(len(result.data), self.feed))

        if not result.data:
            return []

        entries = list(result.data)

        return entries

    def _get_api_endpoint(self):
        return 'statuses/lookup.json'

    def _get_harvest_next(self, start_time, count):
        # always run at every harvest
        return dt.utc_now()

class RSSFeed(FeedBase):
    def __init__(self, feed):
        super(RSSFeed, self).__init__(feed)

        self.es_client = RSS_ES

    def _collect_and_ingest_entries(self):
        contents = feedparser.parse(self.feed.parameters['url'])

        return contents['entries']

    def _get_harvest_next(self, start_time, count):
        hours = random.uniform(0.8, 1.2)

        return dt.utc_now() + timedelta(hours=hours)

class Harvester:
    def __init__(self):
        super(Harvester, self).__init__()

    def run(self, feeds, **kwargs):
        # reset credentials and feeds
        TwitterRateLimit.objects.filter(locked=True).update(locked=False)
        Feed.objects.filter(~Q(state='ready')).update(state='ready')

        now = dt.utc_now()

        print('Harvesting {} feeds'.format(len(feeds)))

        if kwargs.get('debug'):
            for feed in feeds:
                harvest_feed({'feed': feed, **kwargs})

        else:
            harvest_kwargs = [{'feed': feed, **kwargs} for feed in feeds]
            random.shuffle(harvest_kwargs)

            process_count = kwargs.pop('process_count') or 8

            pool = Pool(processes=process_count, maxtasksperchild=1)
            pool.map(harvest_feed, harvest_kwargs)
            pool.close()
            pool.join()

        TWITTER_ES.update_reply_counts(dt.iso8601(now))
        # TWITTER_ES.update_conversations_bottom_up(now)

        print ('Done harvesting {} feeds'.format(len(feeds)))

def harvest_feed(kwargs):
    feed = kwargs.pop('feed')

    print('Harvest feed {}'.format(feed))

    feed_class = {
        'twitter_account': TwitterAccountFeed,
        'twitter_firehose': TwitterFireHoseFeed,
        'twitter_update': TwitterUpdateFeed,
        'rss': RSSFeed,
    }

    FeedClass = feed_class[feed.type]
    feed_impl = FeedClass(feed)
    feed_impl.harvest(**kwargs)

