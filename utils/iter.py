def groupby(iterable, keyfunc):
    data = {}
    for item in iterable:
        key = keyfunc(item)
        if key not in data:
            data[key] = []

        data[key].append(item)

    return data

def filter(iterable, keyfunc):
    trues, falses = [], []

    for item in iterable:
        if keyfunc(item):
            trues.append(item)
        else:
            falses.append(item)

    return trues, falses

def flatten(iterable):
    return (item for sublist in iterable for item in sublist)

def batchify(iterable, batch_size):
    batch = []
    for item in iterable:
        batch.append(item)

        if len(batch) == batch_size:
            yield batch

            batch = []

    if batch:
        yield batch

def as_list(element):
    if type(element) is list:
        return element
    else:
        return [element]