import datetime
from dateutil.parser import parse
import pytz


def utc_now():
    return pytz.utc.localize(datetime.datetime.utcnow())

def iso8601(dt):
    return dt.strftime('%Y-%m-%dT%H:%M:%S.%f%z')

def time_of_day(dt):
    return dt.strftime('%H:%M')

def date_math(op, dt, count, unit):
    params = {}

    params[unit] = int(count)
    if op == 'sub':
        params[unit] = -params[unit]

    dt = parse(dt) + datetime.timedelta(**params)

    return iso8601(dt)

def from_timestamp(ts):
    return pytz.utc.localize(datetime.datetime.fromtimestamp(ts))

