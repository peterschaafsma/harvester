import json
import pika
import time

from utils.dt import utc_now, iso8601

def event(metric, value=1, timestamp=None, **kwargs):
    if timestamp is None:
        timestamp = utc_now()

    if value is None:
        value = 1

    body = {
        'timestamp': iso8601(timestamp),
        'metric': metric,
        'value': value,
    }

    for key, value in kwargs.items():
        body[key] = value

    # client = elasticsearch.Elasticsearch()
    # client.index(index='events', doc_type='event', body=body)

    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()
    channel.queue_declare('events')
    channel.basic_publish(exchange='', routing_key='events', body=json.dumps(body))
    channel.close()

class EventTime:
    def __init__(self, metric, **kwargs):
        self.metric = metric
        self.kwargs = kwargs

    def __enter__(self, *args):
        self.start_time = time.time()
        self.event_time = utc_now()

    def __exit__(self, *args):
        event(self.metric, time.time() - self.start_time, self.event_time, **self.kwargs)

