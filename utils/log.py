from builtins import print as __print__
from inspect import getframeinfo, stack
from utils import dt

import sys


def print(*args):
    caller = getframeinfo(stack()[1][0])

    if len(caller.filename) > 32:
        filename = '...%29s' % caller.filename[-29:]
    else:
        filename = '%32s' % caller.filename

    __print__(dt.utc_now().strftime('%Y-%m-%d %H:%M:%S %Z'), filename, '%4d' % caller.lineno, *args)

    sys.stdout.flush()