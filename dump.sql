-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: localhost    Database: feeds
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `feeds_twitterratelimit`
--

DROP TABLE IF EXISTS `feeds_twitterratelimit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feeds_twitterratelimit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `endpoint` varchar(64) NOT NULL,
  `usage_count` int(11) NOT NULL,
  `remaining` int(11) DEFAULT NULL,
  `limit` int(11) DEFAULT NULL,
  `reset` datetime(6) DEFAULT NULL,
  `credentials_id` int(11) NOT NULL,
  `locked` tinyint(1) NOT NULL,
  `row_created_on` datetime(6) DEFAULT NULL,
  `row_updated_on` datetime(6) DEFAULT NULL,
  `row_locked_at` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `twitter_ratelimit_credentials_id_213054cd_fk_twitter_t` (`credentials_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feeds_twitterratelimit`
--

LOCK TABLES `feeds_twitterratelimit` WRITE;
/*!40000 ALTER TABLE `feeds_twitterratelimit` DISABLE KEYS */;
INSERT INTO `feeds_twitterratelimit` VALUES (16,'statuses/user_timeline.json',11170,775,900,'2019-05-02 18:25:05.000000',1,0,NULL,'2019-05-02 18:20:57.247814','2019-05-02 18:20:56.095883'),(17,'statuses/user_timeline.json',8245,1323,1500,'2019-05-02 18:31:03.000000',10,0,NULL,'2019-05-02 18:21:00.846619','2019-05-02 18:21:00.139594'),(18,'search/tweets.json',17070,89,180,'2019-05-02 18:32:54.000000',1,0,NULL,'2019-05-02 18:20:56.563301','2019-05-02 18:20:56.037793'),(19,'search/tweets.json',22287,271,450,'2019-05-02 18:23:04.000000',10,0,NULL,'2019-05-02 18:20:58.827106','2019-05-02 18:20:58.117602'),(22,'search/tweets.json',10580,89,180,'2019-05-02 18:22:03.000000',14,0,'2019-02-17 12:03:02.629075','2019-05-02 18:20:56.495589','2019-05-02 18:20:56.056696'),(23,'statuses/user_timeline.json',5168,776,900,'2019-05-02 18:31:07.000000',14,0,'2019-02-17 12:35:02.708245','2019-05-02 18:20:54.613969','2019-05-02 18:20:53.932580'),(24,'search/tweets.json',6243,97,180,'2019-04-07 08:03:10.000000',15,0,'2019-02-19 20:21:22.881327','2019-04-07 07:50:27.425259','2019-04-07 07:50:27.146290'),(25,'statuses/user_timeline.json',3187,832,900,'2019-04-07 08:01:05.000000',15,0,'2019-02-20 08:09:03.949120','2019-04-07 07:50:15.226163','2019-04-07 07:50:14.961461'),(26,'search/tweets.json',4662,98,180,'2019-04-07 08:02:03.000000',16,0,'2019-02-27 21:55:04.133173','2019-04-07 07:50:21.236797','2019-04-07 07:50:20.956196'),(27,'statuses/user_timeline.json',1330,832,900,'2019-04-07 08:03:03.000000',16,0,'2019-02-27 21:55:04.544823','2019-04-07 07:50:16.984501','2019-04-07 07:50:16.707890'),(28,'statuses/lookup.json',1870,867,900,'2019-03-28 20:55:54.000000',1,0,'2019-03-21 20:05:26.253909','2019-03-28 20:45:04.055783','2019-03-28 20:45:04.055618'),(29,'statuses/lookup.json',1507,299,300,'2019-05-02 18:32:55.000000',10,0,'2019-03-21 20:06:35.127038','2019-05-02 18:17:54.848196','2019-05-02 18:17:54.541190'),(30,'statuses/lookup.json',613,899,900,'2019-05-02 18:32:54.000000',14,0,'2019-03-21 20:08:17.892410','2019-05-02 18:17:54.480340','2019-05-02 18:17:53.892836'),(31,'statuses/lookup.json',74,897,900,'2019-04-07 19:41:36.000000',15,0,'2019-03-21 20:11:02.900227','2019-04-07 19:36:28.520059','2019-04-07 19:33:05.507765'),(32,'statuses/lookup.json',70,899,900,'2019-04-07 19:51:36.000000',16,0,'2019-03-21 20:12:20.236533','2019-04-07 19:36:37.134392','2019-04-07 19:36:36.752868');
/*!40000 ALTER TABLE `feeds_twitterratelimit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feeds_twittercredentials`
--

DROP TABLE IF EXISTS `feeds_twittercredentials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feeds_twittercredentials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(6) NOT NULL,
  `api_key` varchar(32) NOT NULL,
  `api_key_secret` varchar(64) NOT NULL,
  `token` varchar(64) DEFAULT NULL,
  `secret` varchar(64) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `screen_name` varchar(64) DEFAULT NULL,
  `row_created_on` datetime(6) DEFAULT NULL,
  `row_updated_on` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `feeds_twittercredentials_account_id_edcea245_fk_feeds_account_id` (`account_id`),
  CONSTRAINT `feeds_twittercredentials_account_id_edcea245_fk_feeds_account_id` FOREIGN KEY (`account_id`) REFERENCES `feeds_account` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feeds_twittercredentials`
--

LOCK TABLES `feeds_twittercredentials` WRITE;
/*!40000 ALTER TABLE `feeds_twittercredentials` DISABLE KEYS */;
INSERT INTO `feeds_twittercredentials` VALUES (1,'user','cIBOInjUYT6CzGBj78i9zoRp9','p0jHDw9hEEmgfsOQhB0qqkHNj33zNNlnplhwzoWgPP0Hm0w4fG','1028563435100024832-k6QE5zgrnZYkLyxxbMJDyMa7WScXYW','oAAlCW9QvPgkNQQuZMHMWnhzKqeZfTGY8JYqDCUJ0cDW7',NULL,NULL,NULL,NULL),(10,'app','cIBOInjUYT6CzGBj78i9zoRp9','p0jHDw9hEEmgfsOQhB0qqkHNj33zNNlnplhwzoWgPP0Hm0w4fG',NULL,NULL,NULL,NULL,NULL,NULL),(14,'user','cIBOInjUYT6CzGBj78i9zoRp9','p0jHDw9hEEmgfsOQhB0qqkHNj33zNNlnplhwzoWgPP0Hm0w4fG','980890634423820288-weIH9xxCoOFXwK6ctywMFNjE5AZoy1S','YjuSvHmdiGoloUSqpqgHTiXptsrfo51qe8ENpbuXuksWs',1,'Ysebrand','2019-02-17 07:43:49.464374','2019-04-07 19:32:02.617659'),(15,'user','cIBOInjUYT6CzGBj78i9zoRp9','p0jHDw9hEEmgfsOQhB0qqkHNj33zNNlnplhwzoWgPP0Hm0w4fG',NULL,NULL,1,'johanon_','2019-02-19 20:18:35.156744','2019-04-07 19:34:48.278385'),(16,'user','cIBOInjUYT6CzGBj78i9zoRp9','p0jHDw9hEEmgfsOQhB0qqkHNj33zNNlnplhwzoWgPP0Hm0w4fG',NULL,NULL,1,'PJSchaafsma','2019-02-27 21:52:14.406147','2019-04-07 19:39:46.877176');
/*!40000 ALTER TABLE `feeds_twittercredentials` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-16 14:31:05
